## prerequisites
Python version 3.11
[Python Poetry](https://python-poetry.org/docs/), install via:
```bash
curl -sSL https://install.python-poetry.org | python3 -
```
Hint: in development mode, Poetry needs to be installed as root (```sudo su```).  
Hint: the development.sh checks for the default installation path of poetry. If Poetry was installed somewhere else: 
```bash
which poetry 
export POETRY_PATH=/your/path/to/poetry
```

## setup
``` sh
# download:
git clone --recursive git@gitlab.hrz.tu-chemnitz.de:pika/pika-server.git
cd pika-server
```

<table>
    <td>

``` sh
# development setup, dont copy paste:
./development.sh <option>
vim pika.conf
poetry shell
python src/pika_server.py dev
```
</td>
    <td>

``` sh
# production setup, dont copy paste:
sudo ./production.sh <option>
sudo vim /etc/pika.conf
sudo systemctl restart pika-server
```
</td>
</table>

## api documentation
- visit https://pika.zih.tu-dresden.de/backend/docs
- proper documenataion comming soon, ask the project contributors for help

## description
The Pika server provides access to information from the Pika database, which 
contains comprehensive information about all jobs executed on the Zihs 
supercomputer cluster, Taurus and Bernard.

The Pika Server consists of the following components:
1. REST API:
The REST-API allows the user to retrieve information from the pika database.
Users interested in developing custom tools using this API can request
long-term tokens from the project owner. 
Currently, the REST API powers the Pika Frontend, offering users the ability to
visualize their jobs. It also supports Vampir for visualizing Taurus utilization.

2. Post Processing:
This functionality enhances the information available through the REST API.
It corrects corrupted jobs, labels them appropriately, and calculates average
performance metrics.
The processed data is inserted into the database, enriching the overall dataset.

3. Data Collection:
The server actively collects information on running jobs on the Taurus cluster.
The collected data is inserted into the Pika database, ensuring that the 
database remains up-to-date of the current job execution status.

## development hints
### project structure:
```
$ tree -L 1 src/
src/
├── utils
# utils defines common functionality for every other package,
# allowed to import: python stdlib, third party packages

├── influx (metric.py)
├── sql
# influx and sql are wrappers around third party librarys while also providing
# a lot of pika specific data,
# allowed to import: python stdlib, third party packages, utils

├── data_collection
├── post_processing
├── rest_api
# our 3 service-packages, which should just work by providing some
# configuration to there corresponding main functions
# allowed to import: python stdlib, third party packages, utils, influx, sql

├── clean_post_processing.py
├── manual_post_processing.py
└── pika_server.py
# the executable scripts, which make use of the prior packages

# please follow the hints, to prevent circular imports
```

### view and test available rest-api routes:
- visit https://\<hostname>:\<port>/docs as specified in your `pika.conf`, to see the swagger generated ui

## todo
- write tests
- refactor
