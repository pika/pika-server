#!/usr/bin/env bash

ALL_FLAG=false
REST_API_FLAG=false
POST_PROCESSING_FLAG=false
DATA_COLLECTION_FLAG=false

log_success() {
    local GREEN='\033[0;32m'
    local RESET='\033[0m'
    echo -e "${GREEN}success:${RESET} $1"
}

log_error() {
    local RED='\033[0;31m'
    local RESET='\033[0m'
    echo -e "${RED}error:${RESET} $1"
}

development::check_poetry_path() {
    if command -v poetry &>/dev/null; then
        POETRY_PATH=$(command -v poetry)
    else
        POETRY_PATH=${POETRY_PATH:-"$HOME/.local/bin/poetry"}
    fi
}

development::check_poetry_version() {
    if command -v $POETRY_PATH &>/dev/null; then
        poetry_version=$($POETRY_PATH --version)
        log_success "$poetry_version is installed."
    else
        echo "Poetry was not found."
        echo "Try adding it to your PATH or 'export POETRY_PATH=/your/path/to/poetry'"
        echo "If not already installed visit: https://python-poetry.org/"
        exit 1
    fi
}
development::check_flags() {
    # Initialize variables for each option

    # Process command line arguments
    while [[ $# -gt 0 ]]; do
        case "$1" in
        -a | --all)
            ALL_FLAG=true
            REST_API_FLAG=true
            POST_PROCESSING_FLAG=true
            DATA_COLLECTION_FLAG=true
            ;;
        -r | --rest-api)
            REST_API_FLAG=true
            ;;
        -p | --post-processing)
            POST_PROCESSING_FLAG=true
            ;;
        -d | --data-collection)
            DATA_COLLECTION_FLAG=true
            ;;
        *)
            help
            exit 1
            ;;
        esac
        shift
    done

    # Build the output string based on the specified options
    local GREEN='\033[0;32m'
    local RESET='\033[0m'
    echo -e -n "\033[0;32mselected options: \033[0m"
    if [ "$REST_API_FLAG" = true ]; then
        echo -n "REST-API "
    fi

    if [ "$POST_PROCESSING_FLAG" = true ]; then
        echo -n "Post-Processing "
    fi

    if [ "$DATA_COLLECTION_FLAG" = true ]; then
        echo -n "Data-Collection "
    fi
    echo ""

}

development::create_pika_config() {
    if [ -f pika.conf ]; then
        log_success "found pika configuration"
        return
    fi

    local SECRET_KEY
    SECRET_KEY=$(openssl rand -hex 32)

    if [ $? -eq 0 ]; then
        log_success "created custom secret key"
    else
        log_error "couldnt create secret key, exiting ..."
        exit 1
    fi

    local TMP_CONFIG
    TMP_CONFIG="/tmp/pika.conf"

    sed -e "s/<secret_key_platzhalter>/$SECRET_KEY/" template/pika.conf >$TMP_CONFIG

    if [ "$ALL_FLAG" = true ]; then
        sed -i "s/<rest-api-activation>//" $TMP_CONFIG
        sed -i "s/<post-processing-activation>//" $TMP_CONFIG
        sed -i "s/<data-collection-activation>//" $TMP_CONFIG
        sed -i "s/<post-processing-activation or data-collection-activation>//" $TMP_CONFIG
        sed -i "s/<rest-api-activation or post-processing-activation>//" $TMP_CONFIG
    fi

    if [ "$REST_API_FLAG" = true ]; then
        sed -i "s/<rest-api-activation>//" $TMP_CONFIG
        sed -i "s/<rest-api-activation or post-processing-activation>//" $TMP_CONFIG
    fi
    if [ "$POST_PROCESSING_FLAG" = true ]; then
        sed -i "s/<post-processing-activation>//" $TMP_CONFIG
        sed -i "s/<post-processing-activation or data-collection-activation>//" $TMP_CONFIG
        sed -i "s/<rest-api-activation or post-processing-activation>//" $TMP_CONFIG
    fi
    if [ "$DATA_COLLECTION_FLAG" = true ]; then
        sed -i "s/<data-collection-activation>//" $TMP_CONFIG
        sed -i "s/<post-processing-activation or data-collection-activation>//" $TMP_CONFIG
    fi
    sed -i "/<rest-api-activation>/d" $TMP_CONFIG
    sed -i "/<post-processing-activation>/d" $TMP_CONFIG
    sed -i "/<data-collection-activation>/d" $TMP_CONFIG
    sed -i "/<post-processing-activation or data-collection-activation>/d" $TMP_CONFIG
    sed -i "/<rest-api-activation or post-processing-activation>/d" $TMP_CONFIG

    if cp $TMP_CONFIG .; then
        log_success "generated pika.conf"
    else
        log_error "couldnt generate pika.conf, exiting ..."
        exit 1
    fi

    rm $TMP_CONFIG
}

development::build_poetry_install_command() {
    POETRY_INSTALL_COMMAND="$POETRY_PATH install"
    if [ "$REST_API_FLAG" = true ]; then
        POETRY_INSTALL_COMMAND="${POETRY_INSTALL_COMMAND} -E rest-api"
    fi

    if [ "$DATA_COLLECTION_FLAG" = true ]; then
        POETRY_INSTALL_COMMAND="${POETRY_INSTALL_COMMAND} -E data-collection"
    fi

    if [ "$POST_PROCESSING_FLAG" = true ]; then
        POETRY_INSTALL_COMMAND="${POETRY_INSTALL_COMMAND} -E post-processing"
    fi
}

help() {
    local FORMAT_BOLD=$(tput bold)
    local FORMAT_RESET=$(tput sgr0)

    echo "setup script for the pika server"
    echo
    echo "${FORMAT_BOLD}Usage: $0${FORMAT_RESET} <OPTIONS>"
    echo
    echo "${FORMAT_BOLD}Options:${FORMAT_RESET}"
    echo "  ${FORMAT_BOLD}-a, --all${FORMAT_RESET}              Installs rest-api, post-processing & data-collection"
    echo "  ${FORMAT_BOLD}-r, --rest-api${FORMAT_RESET}         Installs rest-api"
    echo "  ${FORMAT_BOLD}-p, --post-processing${FORMAT_RESET}  Installs post-processing"
    echo "  ${FORMAT_BOLD}-d, --data-collection${FORMAT_RESET}  Installs data-collection"
    echo
    echo "${FORMAT_BOLD}Example:${FORMAT_RESET}"
    echo "  ./development.sh -r -p"
    echo
    echo "${FORMAT_BOLD}Info:${FORMAT_RESET}"
    echo "  disable features in your pika.conf by commenting them out"

}

development::main() {
    if [ "$#" -lt 1 ]; then
        help
        exit 1
    fi
    development::check_flags "$@"
    development::check_poetry_path
    development::check_poetry_version
    development::build_poetry_install_command "$1"
    POETRY_INSTALL_COMMAND="${POETRY_INSTALL_COMMAND} -E dev"
    $POETRY_INSTALL_COMMAND
    development::create_pika_config
}

if [ "${BASH_SOURCE[0]}" == "${0}" ]; then
    development::main "$@"
fi
