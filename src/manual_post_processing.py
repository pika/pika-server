"""
NOT PART OF THE PIKA SERVER
this script is able to perfom post processing based on user specified json input
"""

from argparse import ArgumentParser, RawTextHelpFormatter
from datetime import datetime
from enum import Enum
from sys import argv

from sqlalchemy.engine import Row
from sqlalchemy.orm import Query
from sqlmodel import select

import sql.view.meta
from pika_server import read_config_file
from post_processing.config import PostProcessingConfiguration
from post_processing.post_processing import Postprocessing
from sql.connection import SQLConnection
from sql.model import JobDataFilter, JobDataORM, SQLError
from sql.view.interface import ORMInterface, QueryInterface
from utils.common import (
    debug_sqlalchemy_query,
    read_file_from_project_root,
    three_weeks_ago,
)
from utils.specification import Specification, SpecificationError


class ManualPostProcessingORM(ORMInterface):
    """meta.ManualPostProcessing typesafe sqlalchemy result"""

    uid: int

    @staticmethod
    def from_row(result_set: Row) -> "ManualPostProcessingORM":
        return ManualPostProcessingORM(**result_set)

    @staticmethod
    def from_rows(result_set: list[Row]) -> list["ManualPostProcessingORM"]:
        return [ManualPostProcessingORM(**r) for r in result_set]


class ManualPostProcessingQuery(QueryInterface):
    """meta.manualpostprocessing sqlalchemy query builder"""

    @staticmethod
    def get_selected_columns() -> tuple:
        return tuple([JobDataORM.uid])

    @staticmethod
    def get_query(
        selected_columns: tuple, query_filter: JobDataFilter
    ) -> Query:
        sql_query = (
            select(*selected_columns)
            .select_from(JobDataORM)
            .filter(
                JobDataORM.job_state != "running",
                # (job_data.end_time - job_data.start_time) >= 60,
                # why commented out? read post_processing/post_processing/Postprocessing
            )
        )

        return query_filter.apply_on_query(sql_query)


class PostProcessingMode(Enum):
    """determine what post processing the user wants

    |variable|meaning|base where clause|
    |--------|-------|-----------------|
    |MANUAL|the user supplied json is the heart of the job query|`where job_state!="running" and job_duration>=60`|
    |AUTOMATIC|only check date and make post proc like the automatic one|`where job_state="completed" and job_state="timeout" and footprint_created!=1 and job_duration>=60`|
    |uid|do post processing on this uid|`-`|
    """

    MANUAL = 0
    AUTOMATIC = 1
    uid = 2


class CLIArguments:
    """parsed user supplied arguments"""

    query_filter: JobDataFilter
    mode: PostProcessingMode
    config_file: str


class ArgError(Exception):
    """error happening in argument evaluation"""


def args() -> CLIArguments:
    """evaluate arguments"""

    parser = ArgumentParser(
        prog=argv[0],
        description=("Do post processing on jobs\n"),
        epilog=(
            "Examples\n"
            "python "
            + argv[0]
            + """ --json '{"start_time": 1692686295, "end_time": 1692726001}'\n"""
            "python "
            + argv[0]
            + """ --start "2023-08-22 6:38" --end "2023-08-22 17:40" --json '{"job_state": "failed"}'\n"""
        ),
        formatter_class=RawTextHelpFormatter,
    )
    parser.add_argument(
        "--start",
        help="overwrite start_time in JSON filter, specify as YYYY-MM-DD HH:mm",
        required=False,
    )
    parser.add_argument(
        "--end",
        help="overwrite end_time in JSON filter, specify as YYYY-MM-DD HH:mm",
        required=False,
    )
    parser.add_argument(
        "--json", help="simple where clause replacement", required=False
    )
    parser.add_argument(
        "--config",
        help="use this config file, if not specified use /etc/pika.conf",
        default="/etc/pika.conf",
    )
    parser.add_argument(
        "--uid", help="do post processing on this uid, ignore everything else"
    )

    if len(argv) == 1:
        parser.print_help()
        exit(1)

    parsed_args = parser.parse_args()

    cli_arguments = CLIArguments()

    if parsed_args.config:
        try:
            cli_arguments.config_file = str(parsed_args.config)
        except ValueError as error:
            raise ArgError(f"Option error --config: {error}") from error

    if parsed_args.uid:
        try:
            cli_arguments.query_filter = JobDataFilter(uid=int(parsed_args.uid))
            cli_arguments.mode = PostProcessingMode.uid
            return cli_arguments
        except ValueError as error:
            raise ArgError(f"Option error --uid: {error}") from error

    if parsed_args.json:
        cli_arguments.mode = PostProcessingMode.MANUAL
        query_filter = JobDataFilter().model_validate_json(parsed_args.json)
    else:
        cli_arguments.mode = PostProcessingMode.AUTOMATIC
        query_filter = JobDataFilter()

    query_filter.limit = False

    if parsed_args.start:
        try:
            query_filter.start_time = int(
                datetime.strptime(
                    parsed_args.start, "%Y-%m-%d %H:%M"
                ).timestamp()
            )
        except ValueError as error:
            raise ArgError(f"Option error --start: {error}") from error

    if parsed_args.end:
        try:
            query_filter.end_time = int(
                datetime.strptime(parsed_args.end, "%Y-%m-%d %H:%M").timestamp()
            )
        except ValueError as error:
            raise ArgError(f"Option error --end: {error}") from error

    # safety checks
    if query_filter.start_time is None and query_filter.end_time is None:
        query_filter.start_time = three_weeks_ago()

    cli_arguments.query_filter = query_filter

    return cli_arguments


def main(cli_arguments: CLIArguments) -> None:
    """do the post processing"""

    print(f"READING {cli_arguments.config_file} AS CONFIG FILE")

    configuration = read_config_file(cli_arguments.config_file)
    parsed_config = PostProcessingConfiguration(
        {"execution_schedule": "doenst matter here"},
        configuration["mariadb"],
        configuration["influxdb-st"],
        configuration["influxdb-lt"],
        configuration["rest-api"],
    )

    mariadb = parsed_config.mariadb

    match cli_arguments.mode:
        case PostProcessingMode.MANUAL:
            query = ManualPostProcessingQuery.build(cli_arguments.query_filter)
        case PostProcessingMode.AUTOMATIC:
            query = sql.view.meta.EmptyFootprintQuery.build(
                cli_arguments.query_filter
            )
        case PostProcessingMode.uid:
            query = select(JobDataORM.uid).where(
                JobDataORM.uid == cli_arguments.query_filter.uid
            )

    print("QUERY: ", debug_sqlalchemy_query(query))

    try:
        jobs: list[Row] = SQLConnection(mariadb).execute(query)
    except SQLError as error:
        raise SQLError from error

    job_uids = [job["uid"] for job in jobs]

    print(f"creating footprints for {len(job_uids)} jobs")

    if len(job_uids) == 0:
        return

    try:
        version_file_content = read_file_from_project_root("pyproject.toml")
        tag_file_content = read_file_from_project_root("job_tag.json")
        partition_file_content = read_file_from_project_root("partition.json")
        metric_file_content = read_file_from_project_root("metric.json")
    except FileNotFoundError as error:
        print(f"cant find specification file -> {error}")
        return

    try:
        spec = Specification(
            version_file_content,
            tag_file_content,
            partition_file_content,
            metric_file_content,
        )
    except SpecificationError as error:
        print(f"error in specification -> {error}")
        return

    Postprocessing(
        parsed_config.mariadb,
        parsed_config.influxdb_st,
        parsed_config.influxdb_lt,
        parsed_config.extern_timeline,
        spec,
    ).run(job_uids)

    print("done")


if __name__ == "__main__":
    try:
        user_input = args()
    except ArgError as exp:
        print(exp)
        exit(1)

    try:
        main(user_input)
    except SQLError as exp:
        print(exp)
        exit(1)
