import fastapi
import fastapi.security

import sql.view.issue
from sql.connection import AsyncSQLConnection
from sql.model import SQLError
from utils.logger import logger
from utils.model import FormatRequestError

from ..auth import TokenCrypto
from ..model import (
    JobQuerySettings,
    OptionalHTTPBearer,
    get_name_from_token_or_raise,
)

auth_scheme = OptionalHTTPBearer()  # make swagger login possible


def register_issue_routes(
    router: fastapi.APIRouter,
    token_crypto: TokenCrypto,
    async_sql_connection: AsyncSQLConnection,
) -> None:
    @router.post("/user", summary="group by user")
    async def _(
        request: fastapi.Request,
        settings: JobQuerySettings,
        _: fastapi.security.HTTPAuthorizationCredentials = fastapi.Depends(
            auth_scheme
        ),
    ) -> list[sql.view.issue.UserORM | sql.view.issue.UserResponse]:
        username = get_name_from_token_or_raise(token_crypto, request)

        try:
            (query_filter, format_request) = settings.evaluate(username)
        except FormatRequestError as error:
            raise fastapi.exceptions.HTTPException(400, str(error)) from error

        query = sql.view.issue.UserQuery.build(query_filter)

        try:
            result_set = await async_sql_connection.execute(query)
        except SQLError as error:
            logger.critical(error)
            raise fastapi.exceptions.HTTPException(500) from error

        parsed = sql.view.issue.UserORM.from_rows(result_set)

        if format_request.user_wants_formating is False:
            return parsed

        formated = sql.view.issue.UserResponse.from_orms(parsed, format_request)

        return formated

    @router.post("/job", summary="group by job name")
    async def _(
        request: fastapi.Request,
        settings: JobQuerySettings,
        _: fastapi.security.HTTPAuthorizationCredentials = fastapi.Depends(
            auth_scheme
        ),
    ) -> list[sql.view.issue.JobNameORM | sql.view.issue.JobNameResponse]:
        username = get_name_from_token_or_raise(token_crypto, request)

        try:
            (query_filter, format_request) = settings.evaluate(username)
        except FormatRequestError as error:
            raise fastapi.exceptions.HTTPException(400, str(error)) from error

        query = sql.view.issue.JobNameQuery.build(query_filter)

        try:
            result_set = await async_sql_connection.execute(query)
        except SQLError as error:
            logger.critical(error)
            raise fastapi.exceptions.HTTPException(500) from error

        parsed = sql.view.issue.JobNameORM.from_rows(result_set)

        if format_request.user_wants_formating is False:
            return parsed

        formated = sql.view.issue.JobNameResponse.from_orms(
            parsed, format_request
        )

        return formated

    @router.post("/jobid", summary="group by jobid")
    async def _(
        request: fastapi.Request,
        settings: JobQuerySettings,
        _: fastapi.security.HTTPAuthorizationCredentials = fastapi.Depends(
            auth_scheme
        ),
    ) -> list[sql.view.issue.JobIDORM | sql.view.issue.JobIDResponse]:
        username = get_name_from_token_or_raise(token_crypto, request)

        try:
            (query_filter, format_request) = settings.evaluate(username)
        except FormatRequestError as error:
            raise fastapi.exceptions.HTTPException(400, str(error)) from error

        query = sql.view.issue.JobIDQuery.build(query_filter)

        try:
            result_set = await async_sql_connection.execute(query)
        except SQLError as error:
            logger.critical(error)
            raise fastapi.exceptions.HTTPException(500) from error

        parsed = sql.view.issue.JobIDORM.from_rows(result_set)

        if format_request.user_wants_formating is False:
            return parsed

        formated = sql.view.issue.JobIDResponse.from_orms(
            parsed, format_request
        )

        return formated
