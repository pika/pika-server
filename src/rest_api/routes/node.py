from typing import Union

import fastapi
import fastapi.security

import sql.view.node
import sql.view.reservation
from sql.connection import AsyncSQLConnection
from sql.model import NodeFilter, ReservationFilter, SQLError
from utils.logger import logger
from utils.model import FormatRequestError

from ..auth import TokenCrypto
from ..model import (
    OptionalHTTPBearer,
    QuerySettings,
    get_name_from_token_or_raise,
)


def register_node_routes(
    router: fastapi.APIRouter,
    token_crypto: TokenCrypto,
    async_sql_connection: AsyncSQLConnection,
) -> None:
    auth_scheme = OptionalHTTPBearer()  # make swagger login possible

    @router.post("/node", summary="current node data group by node name")
    async def _(
        request: fastapi.Request,
        settings: QuerySettings[NodeFilter],
        _: fastapi.security.HTTPAuthorizationCredentials = fastapi.Depends(
            auth_scheme
        ),  # fastapi wants us to add this weird argument, which has no effect in our code, but allows us to request this route from the Swagger docs
    ) -> Union[
        list[sql.view.node.NodeOrm],
        list[sql.view.node.NodeResponse],
        list[sql.view.node.NodeReservationOrm],
        list[sql.view.node.NodeReservationResponse],
    ]:

        get_name_from_token_or_raise(token_crypto, request)
        # we dont care about the username, we just want to authorize from token

        try:
            (query_filter, format_request) = settings.evaluate()
        except FormatRequestError as error:
            raise fastapi.HTTPException(400, str(error)) from error

        if query_filter.reservation_data is True:
            query = sql.view.node.NodeReservationQuery.build(query_filter)
        else:
            query = sql.view.node.NodeQuery.build(query_filter)

        try:
            result_set = await async_sql_connection.execute(query)
        except SQLError as error:
            logger.critical(error)
            raise fastapi.HTTPException(500) from error

        if query_filter.reservation_data is True:
            parsed = sql.view.node.NodeReservationOrm.from_rows(result_set)
            if format_request.user_wants_formating is False:
                return parsed
            formatted = sql.view.node.NodeReservationResponse.from_orms(
                parsed, format_request
            )
        else:
            parsed = sql.view.node.NodeOrm.from_rows(result_set)
            if format_request.user_wants_formating is False:
                return parsed
            formatted = sql.view.node.NodeResponse.from_orms(
                parsed, format_request
            )

        return formatted

    @router.post("/reservation", summary="all slurm reservations")
    async def _(
        request: fastapi.Request,
        settings: QuerySettings[ReservationFilter],
        _: fastapi.security.HTTPAuthorizationCredentials = fastapi.Depends(
            auth_scheme
        ),
    ) -> Union[
        list[sql.view.reservation.ReservationOrm],
        list[sql.view.reservation.ReservationResponse],
    ]:
        get_name_from_token_or_raise(token_crypto, request)

        try:
            (query_filter, format_request) = settings.evaluate()
        except FormatRequestError as error:
            raise fastapi.HTTPException(400, str(error)) from error

        query = sql.view.reservation.ReservationQuery.build(query_filter)

        try:
            result_set = await async_sql_connection.execute(query)
        except SQLError as error:
            logger.critical(error)
            raise fastapi.HTTPException(500) from error

        if format_request.user_wants_formating is False:
            return result_set

        parsed = sql.view.reservation.ReservationOrm.from_rows(result_set)
        formatted = sql.view.reservation.ReservationResponse.from_orms(
            parsed, format_request
        )

        return formatted
