import fastapi
import fastapi.security

import utils.specification

from ..auth import TokenCrypto
from ..model import (
    ApiConfiguration,
    OptionalHTTPBearer,
    get_name_from_token_or_raise,
)


def register_specification_routes(
    router: fastapi.APIRouter,
    token_crypto: TokenCrypto,
    spec: utils.specification.Specification,
) -> None:
    auth_scheme = OptionalHTTPBearer()  # make swagger login possible

    @router.get(
        "/partition", summary="hardware specifications group by partition name"
    )
    async def _(
        request: fastapi.Request,
        _: fastapi.security.HTTPAuthorizationCredentials = fastapi.Depends(
            auth_scheme
        ),
    ) -> dict[str, utils.specification.Partition]:

        get_name_from_token_or_raise(token_crypto, request)
        # authenticate user

        return spec.partition

    @router.get("/job_tag", summary="all possible job tags in bit pattern")
    async def _(
        request: fastapi.Request,
        _: fastapi.security.HTTPAuthorizationCredentials = fastapi.Depends(
            auth_scheme
        ),
    ) -> dict[int, str]:

        get_name_from_token_or_raise(token_crypto, request)
        # authenticate user

        return spec.job_tag
