import asyncio
import json
import pathlib
import unittest

import fastapi
import fastapi.testclient

import sql.connection
import sql.model
import sql.test
import sql.view.topdown
import utils.common
import utils.model
import utils.specification

from ... import model, rest_api
from .. import job

# mypy: ignore-errors


class JobRouteTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        # save important variables for reuse
        cls.user_name = "my_name"

        fastapi_config = {
            "hostname": "",
            "port": "",
            "proxy_hostname": "testclient",
            "admins": [cls.user_name],
            "longterm_user": [""],
            "cors_origin": "",
            "proxy_user_key": "shibo",
            "route_root_path": "",
            "secret_key": "x",
            "token_expire_minutes": {
                "normal": 60,
                "longterm": 60,
            },
            "features": {},
        }

        cls.config = rest_api.ApiConfiguration(
            fastapi_config,
            {
                "hostname": "",
                "port": 3,
                "user": "",
                "password": "",
                "database": "",
            },
            {
                "hostname": "",
                "port": 3,
                "user": "",
                "password": "",
                "database": "",
            },
            {
                "hostname": "",
                "port": 3,
                "user": "",
                "password": "",
                "database": "",
            },
        )

        version_file_content = utils.common.read_file_from_project_root(
            "pyproject.toml"
        )
        tag_file_content = utils.common.read_file_from_project_root(
            "job_tag.json"
        )
        partition_file_content = utils.common.read_file_from_project_root(
            "partition.json"
        )
        metric_file_content = utils.common.read_file_from_project_root(
            "metric.json"
        )

        cls.spec = utils.specification.Specification(
            version_file_content,
            tag_file_content,
            partition_file_content,
            metric_file_content,
        )

        # setup async memory db
        async def async_db_setup():
            db = sql.test.AsyncMemoryDB("routes_test_job")
            await db.init()

            test_jobs_path = f"{pathlib.Path(__file__).parent}/test_jobs.json"
            with open(test_jobs_path) as file:
                content = file.read()
            jobs = json.loads(content)["jobs"]

            for job in jobs:
                await db.add(sql.model.JobDataORM(**job))

            return db.uri

        uri = asyncio.run(async_db_setup())

        # create app tester
        app = fastapi.FastAPI()
        cls.testclient = fastapi.testclient.TestClient(app)

        # mount authorization route on app
        rest_api.register_credential_route(
            utils.model.OperationMode.DEVELOPMENT,
            app,
            model.FastApiConfig(fastapi_config),
            "test-version",
        )

        # get token
        response = cls.testclient.get(
            "/credential", headers={"shibo": cls.user_name}
        )
        cls.token = response.json()["admin_token"]

        # mount job routes on app
        job.register_job_routes(
            app,
            cls.config,
            cls.spec,
            sql.connection.SQLConnection(uri),
            sql.connection.AsyncSQLConnection(uri),
        )

    def test_project_raw(self):
        response = self.testclient.post(
            "/jobtable/project",
            json={"filter": {}, "use_date_interval": False},
            headers={"Authorization": self.token},
        )

        result = response.json()

        expected = [
            json.loads(
                sql.view.topdown.ProjectORM(
                    project_name="project_three",
                    job_count=1,
                    max_node_count=1,
                    max_core_count=1,
                    max_gpu_count=0,
                    max_pending_time=20,
                    sum_duration=80,
                    sum_core_duration=80,
                    footprint_count=1,
                ).model_dump_json()
            ),
            json.loads(
                sql.view.topdown.ProjectORM(
                    project_name="project_two",
                    job_count=1,
                    max_node_count=1,
                    max_core_count=1,
                    max_gpu_count=0,
                    max_pending_time=20,
                    sum_duration=80,
                    sum_core_duration=80,
                    footprint_count=1,
                ).model_dump_json()
            ),
            json.loads(
                sql.view.topdown.ProjectORM(
                    project_name="project_one",
                    job_count=1,
                    max_node_count=1,
                    max_core_count=1,
                    max_gpu_count=0,
                    max_pending_time=20,
                    sum_duration=80,
                    sum_core_duration=80,
                    footprint_count=1,
                ).model_dump_json()
            ),
        ]

        # [
        #     {
        #         "project_name": "project_three",
        #         "job_count": 1,
        #         "max_node_count": 1,
        #         "max_core_count": 1,
        #         "max_gpu_count": 0,
        #         "max_pending_time": 20,
        #         "sum_duration": 80,
        #         "sum_core_duration": 80,
        #         "footprint_count": 1,
        #     },
        #     {
        #         "project_name": "project_two",
        #         "job_count": 1,
        #         "max_node_count": 1,
        #         "max_core_count": 1,
        #         "max_gpu_count": 0,
        #         "max_pending_time": 20,
        #         "sum_duration": 80,
        #         "sum_core_duration": 80,
        #         "footprint_count": 1,
        #     },
        #     {
        #         "project_name": "project_one",
        #         "job_count": 1,
        #         "max_node_count": 1,
        #         "max_core_count": 1,
        #         "max_gpu_count": 0,
        #         "max_pending_time": 20,
        #         "sum_duration": 80,
        #         "sum_core_duration": 80,
        #         "footprint_count": 1,
        #     },
        # ]

        self.assertEqual(result, expected)

    def test_project_formated(self):
        response = self.testclient.post(
            "/jobtable/project",
            json={
                "filter": {},
                "format_result": {"utc_timezone": "00:00"},
                "use_date_interval": False,
            },
            headers={"Authorization": self.token},
        )

        result = response.json()

        expected = [
            json.loads(
                sql.view.topdown.ProjectResponse(
                    project_name="project_three",
                    job_count=1,
                    max_node_count=1,
                    max_core_count=1,
                    max_gpu_count=0,
                    footprint_count=1,
                    max_pending_time="00d 00h 00min 20s",
                    sum_duration="0000y 000d 00h 01min",
                    sum_core_duration="0.02",
                ).model_dump_json()
            ),
            json.loads(
                sql.view.topdown.ProjectResponse(
                    project_name="project_two",
                    job_count=1,
                    max_node_count=1,
                    max_core_count=1,
                    max_gpu_count=0,
                    footprint_count=1,
                    max_pending_time="00d 00h 00min 20s",
                    sum_duration="0000y 000d 00h 01min",
                    sum_core_duration="0.02",
                ).model_dump_json()
            ),
            json.loads(
                sql.view.topdown.ProjectResponse(
                    project_name="project_one",
                    job_count=1,
                    max_node_count=1,
                    max_core_count=1,
                    max_gpu_count=0,
                    footprint_count=1,
                    max_pending_time="00d 00h 00min 20s",
                    sum_duration="0000y 000d 00h 01min",
                    sum_core_duration="0.02",
                ).model_dump_json()
            ),
        ]

        # [
        #     {
        #         "project_name": "project_three",
        #         "job_count": 1,
        #         "max_node_count": 1,
        #         "max_core_count": 1,
        #         "max_gpu_count": 0,
        #         "footprint_count": 1,
        #         "max_pending_time": "00d 00h 00min 20s",
        #         "sum_duration": "0000y 000d 00h 01min",
        #         "sum_core_duration": "0000y 000d 00h 01min",
        #     },
        #     {
        #         "project_name": "project_two",
        #         "job_count": 1,
        #         "max_node_count": 1,
        #         "max_core_count": 1,
        #         "max_gpu_count": 0,
        #         "footprint_count": 1,
        #         "max_pending_time": "00d 00h 00min 20s",
        #         "sum_duration": "0000y 000d 00h 01min",
        #         "sum_core_duration": "0000y 000d 00h 01min",
        #     },
        #     {
        #         "project_name": "project_one",
        #         "job_count": 1,
        #         "max_node_count": 1,
        #         "max_core_count": 1,
        #         "max_gpu_count": 0,
        #         "footprint_count": 1,
        #         "max_pending_time": "00d 00h 00min 20s",
        #         "sum_duration": "0000y 000d 00h 01min",
        #         "sum_core_duration": "0000y 000d 00h 01min",
        #     },
        # ]

        self.assertEqual(result, expected)
