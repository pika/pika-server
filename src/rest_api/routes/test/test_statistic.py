import asyncio
import json
import pathlib
import unittest

import fastapi
import fastapi.testclient

import sql.connection
import sql.model
import sql.test
import sql.view.statistic
import utils.model

from ... import auth, model, rest_api
from .. import statistic


class StatisticRouteTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        # save important variables for reuse
        cls.user_name = "my_name"
        cls.token_crypto = auth.TokenCrypto(
            {
                "secret_key": "x",
                "token_expire_minutes": {"normal": 60, "longterm": 60},
            }
        )

        # setup async memory db
        async def async_db_setup():
            db = sql.test.AsyncMemoryDB("routes_test_statistic")
            await db.init()

            test_jobs_path = f"{pathlib.Path(__file__).parent}/test_jobs.json"
            with open(test_jobs_path) as file:
                content = file.read()
            jobs = json.loads(content)["jobs"]

            for job in jobs:
                await db.add(sql.model.JobDataORM(**job))

            return db.uri

        uri = asyncio.run(async_db_setup())

        # create app tester
        app = fastapi.FastAPI()
        cls.testclient = fastapi.testclient.TestClient(app)

        # mount authorization route on app
        rest_api.register_credential_route(
            utils.model.OperationMode.DEVELOPMENT,
            app,
            model.FastApiConfig(
                {
                    "hostname": "",
                    "port": "",
                    "proxy_hostname": "testclient",
                    "admins": [cls.user_name],
                    "longterm_user": [""],
                    "cors_origin": "",
                    "proxy_user_key": "shibo",
                    "route_root_path": "",
                    "secret_key": cls.token_crypto._secret_key,
                    "token_expire_minutes": {
                        "normal": cls.token_crypto._token_expire_minutes_normal,
                        "longterm": cls.token_crypto._token_expire_minutes_long,
                    },
                    "features": {},
                },
            ),
            "test-version",
        )

        # get token
        response = cls.testclient.get(
            "/credential", headers={"shibo": cls.user_name}
        )
        cls.token = response.json()["admin_token"]

        # mount statistic routes on app
        statistic.register_statistic_routes(
            app,
            cls.token_crypto,
            sql.connection.AsyncSQLConnection(uri),
        )

    def test_project(self):
        response = self.testclient.post(
            "/statistic/project",
            json={"filter": {}, "use_date_interval": False},
            headers={"Authorization": self.token},
        )

        result = response.json()

        expected = [
            json.loads(
                sql.view.statistic.ProjectORM(
                    project_name="project_one"
                ).model_dump_json()
            ),
            json.loads(
                sql.view.statistic.ProjectORM(
                    project_name="project_two"
                ).model_dump_json()
            ),
            json.loads(
                sql.view.statistic.ProjectORM(
                    project_name="project_three"
                ).model_dump_json()
            ),
        ]

        # [
        #     {"project_name": "project_one"},
        #     {"project_name": "project_two"},
        #     {"project_name": "project_three"},
        # ]

        self.assertEqual(result, expected)
