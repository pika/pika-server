import fastapi
import fastapi.security

import sql.view.queue
from sql.connection import AsyncSQLConnection
from sql.model import SQLError
from utils.logger import logger
from utils.model import FormatRequestError

from ..auth import TokenCrypto
from ..model import (
    OptionalHTTPBearer,
    QueueQuerySettings,
    get_name_from_token_or_raise,
)

auth_scheme = OptionalHTTPBearer()  # make swagger login possible


def register_jobpending_routes(
    router: fastapi.APIRouter,
    token_crypto: TokenCrypto,
    async_sql_connection: AsyncSQLConnection,
) -> None:
    @router.post("/project", summary="group by project name")
    async def _(
        request: fastapi.Request,
        settings: QueueQuerySettings,
        _: fastapi.security.HTTPAuthorizationCredentials = fastapi.Depends(
            auth_scheme
        ),
    ) -> list[sql.view.queue.ProjectOrm | sql.view.queue.ProjectResponse]:
        username = get_name_from_token_or_raise(token_crypto, request)

        try:
            (query_filter, format_request) = settings.evaluate(username)
        except FormatRequestError as error:
            raise fastapi.exceptions.HTTPException(400, str(error)) from error

        query = sql.view.queue.ProjectQuery.build(query_filter)

        try:
            result_set = await async_sql_connection.execute(query)
        except SQLError as error:
            logger.critical(error)
            raise fastapi.exceptions.HTTPException(500) from error

        parsed = sql.view.queue.ProjectOrm.from_rows(result_set)

        if format_request.user_wants_formating is False:
            return parsed

        formated = sql.view.queue.ProjectResponse.from_orms(
            parsed, format_request
        )
        return formated

    @router.post("/user", summary="group by user")
    async def _(
        request: fastapi.Request,
        settings: QueueQuerySettings,
        _: fastapi.security.HTTPAuthorizationCredentials = fastapi.Depends(
            auth_scheme
        ),
    ) -> list[sql.view.queue.UserOrm | sql.view.queue.UserResponse]:
        username = get_name_from_token_or_raise(token_crypto, request)

        try:
            (query_filter, format_request) = settings.evaluate(username)
        except FormatRequestError as error:
            raise fastapi.exceptions.HTTPException(400, str(error)) from error

        query = sql.view.queue.UserQuery.build(query_filter)

        try:
            result_set = await async_sql_connection.execute(query)
        except SQLError as error:
            logger.critical(error)
            raise fastapi.exceptions.HTTPException(500) from error

        parsed = sql.view.queue.UserOrm.from_rows(result_set)

        if format_request.user_wants_formating is False:
            return parsed

        formated = sql.view.queue.UserResponse.from_orms(parsed, format_request)
        return formated

    @router.post("/job", summary="group by job name")
    async def _(
        request: fastapi.Request,
        settings: QueueQuerySettings,
        _: fastapi.security.HTTPAuthorizationCredentials = fastapi.Depends(
            auth_scheme
        ),
    ) -> list[sql.view.queue.JobNameOrm | sql.view.queue.JobNameResponse]:
        username = get_name_from_token_or_raise(token_crypto, request)

        try:
            (query_filter, format_request) = settings.evaluate(username)
        except FormatRequestError as error:
            raise fastapi.exceptions.HTTPException(400, str(error)) from error

        query = sql.view.queue.JobNameQuery.build(query_filter)

        try:
            result_set = await async_sql_connection.execute(query)
        except SQLError as error:
            logger.critical(error)
            raise fastapi.exceptions.HTTPException(500) from error

        parsed = sql.view.queue.JobNameOrm.from_rows(result_set)

        if format_request.user_wants_formating is False:
            return parsed

        formated = sql.view.queue.JobNameResponse.from_orms(
            parsed, format_request
        )
        return formated

    @router.post("/jobid", summary="group by jobid")
    async def _(
        request: fastapi.Request,
        settings: QueueQuerySettings,
        _: fastapi.security.HTTPAuthorizationCredentials = fastapi.Depends(
            auth_scheme
        ),
    ) -> list[
        sql.view.queue.JobIDOrm
        | sql.view.queue.JobIDResponse
        | sql.view.queue.JobIDReservationOrm
        | sql.view.queue.JobIDReservationResponse
    ]:
        username = get_name_from_token_or_raise(token_crypto, request)

        try:
            (query_filter, format_request) = settings.evaluate(username)
        except FormatRequestError as error:
            raise fastapi.exceptions.HTTPException(400, str(error)) from error

        query = (
            sql.view.queue.JobIDReservationQuery.build(query_filter)
            if query_filter.reservation_data is True
            else sql.view.queue.JobIDQuery.build(query_filter)
        )

        try:
            result_set = await async_sql_connection.execute(query)
        except SQLError as error:
            logger.critical(error)
            raise fastapi.exceptions.HTTPException(500) from error

        parsed = (
            sql.view.queue.JobIDReservationOrm.from_rows(result_set)
            if query_filter.reservation_data is True
            else sql.view.queue.JobIDOrm.from_rows(result_set)
        )

        if format_request.user_wants_formating is False:
            return parsed

        formated = (
            sql.view.queue.JobIDReservationResponse.from_orms(
                parsed, format_request
            )
            if query_filter.reservation_data is True
            else sql.view.queue.JobIDResponse.from_orms(parsed, format_request)
        )
        return formated
