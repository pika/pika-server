from typing import Any

import fastapi
import fastapi.security

import sql.view.metric
import sql.view.statistic
from sql.connection import AsyncSQLConnection
from sql.model import SQLError, VisualizationFilter
from utils.logger import logger
from utils.model import FormatRequestError

from ..auth import TokenCrypto
from ..model import (
    JobQuerySettings,
    OptionalHTTPBearer,
    QuerySettingsFilter,
    get_name_from_token_or_raise,
)

auth_scheme = OptionalHTTPBearer()  # make swagger login possible


def register_statistic_routes(
    router: fastapi.APIRouter,
    token_crypto: TokenCrypto,
    async_sql_connection: AsyncSQLConnection,
) -> None:
    @router.post("/statistic/project", summary="last active projects")
    async def _(
        request: fastapi.Request,
        settings: JobQuerySettings,
        _: fastapi.security.HTTPAuthorizationCredentials = fastapi.Depends(
            auth_scheme
        ),
    ) -> list[sql.view.statistic.ProjectORM]:
        username = get_name_from_token_or_raise(token_crypto, request)

        try:
            (query_filter, _) = settings.evaluate(username)
        except FormatRequestError as error:
            raise fastapi.exceptions.HTTPException(400, str(error)) from error

        query = sql.view.statistic.ProjectQuery.build(query_filter)

        try:
            result_set = await async_sql_connection.execute(query)
        except SQLError as error:
            logger.critical(error)
            raise fastapi.exceptions.HTTPException(500) from error

        parsed_result = sql.view.statistic.ProjectORM.from_rows(result_set)

        return parsed_result

    @router.post(
        "/statistic/user",
        summary="last active users (admin token required)",
    )
    async def _(
        request: fastapi.Request,
        settings: JobQuerySettings,
        _: fastapi.security.HTTPAuthorizationCredentials = fastapi.Depends(
            auth_scheme
        ),
    ) -> list[sql.view.statistic.UserORM]:
        username = get_name_from_token_or_raise(token_crypto, request)

        try:
            (query_filter, _) = settings.evaluate(username)
        except FormatRequestError as error:
            raise fastapi.exceptions.HTTPException(400, str(error)) from error

        query = sql.view.statistic.UserQuery.build(query_filter)

        try:
            result_set = await async_sql_connection.execute(query)
        except SQLError as error:
            logger.critical(error)
            raise fastapi.exceptions.HTTPException(500) from error

        parsed_result = sql.view.statistic.UserORM.from_rows(result_set)

        return parsed_result

    @router.post(
        "/statistic/footprint_job_count",
        summary="tuple of job footprint count and total job count",
    )
    async def _(
        request: fastapi.Request,
        settings: JobQuerySettings,
        _: fastapi.security.HTTPAuthorizationCredentials = fastapi.Depends(
            auth_scheme
        ),
    ) -> list[sql.view.statistic.CountORM]:
        username = get_name_from_token_or_raise(token_crypto, request)

        try:
            (query_filter, _) = settings.evaluate(username)
        except FormatRequestError as error:
            raise fastapi.exceptions.HTTPException(400, str(error)) from error

        query = sql.view.statistic.CountQuery.build(query_filter)

        try:
            result_set = await async_sql_connection.execute(query)
        except SQLError as error:
            logger.critical(error)
            raise fastapi.exceptions.HTTPException(500) from error

        parsed_result = sql.view.statistic.CountORM.from_rows(result_set)

        return parsed_result

    @router.post(
        "/footprint",
        summary="job footprint data for visualizing histograms or scatter plots",
    )
    async def _(
        request: fastapi.Request,
        settings: QuerySettingsFilter[VisualizationFilter],
        _: fastapi.security.HTTPAuthorizationCredentials = fastapi.Depends(
            auth_scheme
        ),
    ) -> Any:
        username = get_name_from_token_or_raise(token_crypto, request)

        query_filter = settings.evaluate(username)

        try:
            match len(query_filter.visualization):
                case 1:
                    result_set = await sql.view.metric.query_histogram(
                        async_sql_connection, query_filter
                    )
                case 2:
                    result_set = await sql.view.metric.query_scatterplot(
                        async_sql_connection, query_filter
                    )
                case _:
                    raise fastapi.HTTPException(
                        405, "A maximum of two values can be visualized"
                    )
        except TypeError:
            raise fastapi.HTTPException(400)
        except SQLError as error:
            logger.critical(error)
            raise fastapi.HTTPException(500) from error

        return result_set
