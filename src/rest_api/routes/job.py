from typing import Any

import fastapi
import fastapi.security

import sql.view.meta
import sql.view.topdown
from influx.connection import AsyncInfluxConnection
from influx.model import TimelineMetric, TimelineParameter, TimelineType
from influx.query import TimelineQueryBuilder
from sql.connection import AsyncSQLConnection, SQLConnection
from sql.model import JobDataFilter, SQLError
from utils.common import debug_sqlalchemy_query, three_weeks_ago
from utils.logger import logger
from utils.model import FormatRequestError
from utils.specification import Specification

from ..download.model import DownloadData
from ..model import (
    ApiConfiguration,
    ExternTimelineMetrics,
    JobQuerySettings,
    OptionalHTTPBearer,
    QuerySettings,
    QuerySettingsURL,
    get_name_from_token_or_raise,
)


def register_job_routes(
    router: fastapi.APIRouter,
    _config: ApiConfiguration,
    spec: Specification,
    sql_connection: SQLConnection,
    async_sql_connection: AsyncSQLConnection,
) -> None:
    auth_scheme = OptionalHTTPBearer()  # make swagger login possible

    @router.post("/jobtable/project", summary="group by project name")
    async def _(
        request: fastapi.Request,
        settings: JobQuerySettings,
        _: fastapi.security.HTTPAuthorizationCredentials = fastapi.Depends(
            auth_scheme
        ),
    ) -> list[sql.view.topdown.ProjectORM | sql.view.topdown.ProjectResponse]:

        username = get_name_from_token_or_raise(
            _config.fastapi.token_crypto, request
        )

        try:
            (query_filter, format_request) = settings.evaluate(username)
        except FormatRequestError as error:
            raise fastapi.exceptions.HTTPException(400, str(error)) from error

        query = sql.view.topdown.ProjectQuery.build(query_filter)

        try:
            result_set = await async_sql_connection.execute(query)
        except SQLError as error:
            logger.critical(error)
            raise fastapi.exceptions.HTTPException(500) from error

        parsed_jobs = sql.view.topdown.ProjectORM.from_rows(result_set)

        if format_request.user_wants_formating is False:
            return parsed_jobs

        formated_jobs = sql.view.topdown.ProjectResponse.from_orms(
            parsed_jobs, format_request
        )

        return formated_jobs

    @router.post("/jobtable/user", summary="group by user")
    async def _(
        request: fastapi.Request,
        settings: JobQuerySettings,
        _: fastapi.security.HTTPAuthorizationCredentials = fastapi.Depends(
            auth_scheme
        ),
    ) -> list[sql.view.topdown.UserORM | sql.view.topdown.UserResponse]:
        username = get_name_from_token_or_raise(
            _config.fastapi.token_crypto, request
        )

        try:
            (query_filter, format_request) = settings.evaluate(username)
        except FormatRequestError as error:
            raise fastapi.security.fastapi.exceptions.HTTPException(
                400, str(error)
            ) from error

        query = sql.view.topdown.UserQuery.build(query_filter)

        try:
            result_set = await async_sql_connection.execute(query)
        except SQLError as error:
            logger.critical(error)
            raise fastapi.exceptions.HTTPException(500) from error

        parsed_jobs = sql.view.topdown.UserORM.from_rows(result_set)

        if format_request.user_wants_formating is False:
            return parsed_jobs

        formated_jobs = sql.view.topdown.UserResponse.from_orms(
            parsed_jobs, format_request
        )

        return formated_jobs

    @router.post("/jobtable/job", summary="group by jobname")
    async def _(
        request: fastapi.Request,
        settings: JobQuerySettings,
        _: fastapi.security.HTTPAuthorizationCredentials = fastapi.Depends(
            auth_scheme
        ),
    ) -> list[sql.view.topdown.JobNameOrm | sql.view.topdown.JobNameResponse]:
        username = get_name_from_token_or_raise(
            _config.fastapi.token_crypto, request
        )

        try:
            (query_filter, format_request) = settings.evaluate(username)
        except FormatRequestError as error:
            raise fastapi.exceptions.HTTPException(400, str(error)) from error

        query = sql.view.topdown.JobNameQuery.build(query_filter)

        try:
            result_set = await async_sql_connection.execute(query)
        except SQLError as error:
            logger.critical(error)
            raise fastapi.exceptions.HTTPException(500) from error

        parsed_jobs = sql.view.topdown.JobNameOrm.from_rows(result_set)

        if format_request.user_wants_formating is False:
            return parsed_jobs

        formated_jobs = sql.view.topdown.JobNameResponse.from_orms(
            parsed_jobs, format_request
        )

        return formated_jobs

    @router.post("/jobtable/jobid", summary="group by jobid")
    async def _(
        request: fastapi.Request,
        settings: JobQuerySettings,
        _: fastapi.security.HTTPAuthorizationCredentials = fastapi.Depends(
            auth_scheme
        ),
    ) -> list[dict]:
        username = get_name_from_token_or_raise(
            _config.fastapi.token_crypto, request
        )

        try:
            (query_filter, format_request) = settings.evaluate(username)
        except FormatRequestError as error:
            raise fastapi.exceptions.HTTPException(400, str(error)) from error

        query = sql.view.topdown.JobIDQuery.build(query_filter)

        try:
            result_set = await async_sql_connection.execute(query)
        except SQLError as error:
            logger.critical(error)
            raise fastapi.exceptions.HTTPException(500) from error

        parsed_jobs = sql.view.topdown.JobIDOrm.from_rows(result_set)

        if format_request.user_wants_formating is False:
            return parsed_jobs

        formated_jobs = sql.view.topdown.JobIDResponse.from_orms(
            parsed_jobs, format_request
        )

        return formated_jobs

    @router.post(
        "/job/{job_id}/{job_start}/{partition}",
        summary="all job metadata",
    )
    async def _(
        request: fastapi.Request,
        job_id: int,
        job_start: int,
        partition: str,
        format_settings: QuerySettingsURL,
        _: fastapi.security.HTTPAuthorizationCredentials = fastapi.Depends(
            auth_scheme
        ),
    ) -> list[
        sql.view.topdown.JobIDEverythingORM
        | sql.view.topdown.JobIDEverythingResponse
    ]:
        username = get_name_from_token_or_raise(
            _config.fastapi.token_crypto, request
        )

        try:
            complete_settings = QuerySettings[JobDataFilter](
                filter=JobDataFilter(
                    job_id=job_id,
                    start_time=job_start,
                    partition_name=partition,
                    limit=False,
                ),
                format_result=format_settings.evaluate(),
            )

            (query_settings, format_request) = complete_settings.evaluate(
                username
            )
        except FormatRequestError:
            raise fastapi.HTTPException(400, detail="invalid timezone")

        query = sql.view.topdown.JobIDEverythingQuery.build(query_settings)

        try:
            result_set = await async_sql_connection.execute(query)
        except SQLError as error:
            logger.critical(error)
            raise fastapi.HTTPException(502) from error

        try:
            parsed_job = sql.view.topdown.JobIDEverythingORM.from_row(
                result_set[0]
            )
        except IndexError as error:
            raise fastapi.HTTPException(404, detail="found no job") from error

        if format_request.user_wants_formating:
            formated_job = sql.view.topdown.JobIDEverythingResponse.from_orm(
                parsed_job, format_request
            )
            return [formated_job]

        return [parsed_job]

    @router.post(
        "/timeline/{metric}/{job_id}/{job_start}/{partition}",
        summary="timeline data for a specific metric",
    )
    async def _(
        request: fastapi.Request,
        metric: TimelineMetric,
        job_id: int,
        job_start: int,
        partition: str,
        params: TimelineParameter,
        _: fastapi.security.HTTPAuthorizationCredentials = fastapi.Depends(
            auth_scheme
        ),
    ) -> Any:

        username = get_name_from_token_or_raise(
            _config.fastapi.token_crypto, request
        )

        partition_data = spec.partition

        (query_settings, _) = JobQuerySettings(
            filter=JobDataFilter(
                job_id=job_id, start_time=job_start, partition_name=partition
            )
        ).evaluate(username)

        try:
            job = sql.view.meta.query_timeline_meta(
                sql_connection, query_settings
            )
        except (SQLError, IndexError) as error:
            logger.critical(error)
            raise fastapi.HTTPException(500) from error

        # check if job has allocated whole node without exclusive flag
        if job.exclusive == 0:
            try:
                if job.core_count == (
                    partition_data[job.partition_name].cpu_num * job.node_count
                ):
                    job.exclusive = 1
            except:
                # happens if job.partition_name is unknown in partition_data
                job.exclusive = 0
                logger.warning(f"Partition {job.partition_name} unknown.")

        query_builder = TimelineQueryBuilder(job, metric, params, spec)

        try:
            queries = query_builder.get_queries()
        except TypeError:
            # this is only a quick fix, MetricQueryBuilder should be refactored
            # this happens when querieng mem_bw on julia partition, because
            # likwid socket cpu are needed
            return {
                "error": "something went wrong, at building the influx queries"
            }

        cred = (
            _config.influxdb_st
            if int(job.start_time) > three_weeks_ago()
            else _config.influxdb_lt
        )

        influx_connection = AsyncInfluxConnection(cred)

        results = await influx_connection.execute_queries(queries)

        # second queries for best and lowest metric
        if params.timeline_type is not None and params.timeline_type in [
            TimelineType.best,
            TimelineType.lowest,
            TimelineType.best_lowest,
        ]:
            queries = query_builder.get_queries_again(results)
            results = await influx_connection.execute_queries(queries)

        return query_builder.format_query_result(results)

    if _config.fastapi.features.extern_timeline:
        from ..extern import timeline

        @router.post(
            "/timeline_extern/{metric}/{job_id}/{job_start}/{partition}",
            summary="timeline data for a specific metric",
        )
        async def _(
            request: fastapi.Request,
            metric: ExternTimelineMetrics,
            job_id: int,
            job_start: int,
            partition: str,
            params: TimelineParameter,
            _: fastapi.security.HTTPAuthorizationCredentials = fastapi.Depends(
                auth_scheme
            ),
        ) -> Any:
            username = get_name_from_token_or_raise(
                _config.fastapi.token_crypto, request
            )

            (query_settings, _) = JobQuerySettings(
                filter=JobDataFilter(
                    job_id=job_id,
                    start_time=job_start,
                    partition_name=partition,
                )
            ).evaluate(username)

            try:
                job = sql.view.meta.query_timeline_meta(
                    sql_connection, query_settings
                )
            except (SQLError, IndexError) as error:
                logger.critical(error)
                raise fastapi.HTTPException(500) from error

            params_dict = {}
            if params:
                params_dict = params.dict()
            return await timeline.query_db(
                job,
                metric,
                params_dict,
                _config.fastapi.features.extern_timeline,
            )

    @router.post(
        "/download/{job_id}/{job_start}/{partition}",
        summary="all job data in json format",
    )
    async def _(
        request: fastapi.Request,
        job_id: int,
        job_start: int,
        partition: str,
        params: DownloadData,
        _: fastapi.security.HTTPAuthorizationCredentials = fastapi.Depends(
            auth_scheme
        ),
    ) -> Any:

        username = get_name_from_token_or_raise(
            _config.fastapi.token_crypto, request
        )

        if _config.fastapi.features.download_jobdata:
            from ..download import job_data

            (query_settings, _) = JobQuerySettings(
                filter=JobDataFilter(
                    job_id=job_id,
                    start_time=job_start,
                    partition_name=partition,
                )
            ).evaluate(username)

            try:
                job = sql.view.meta.query_timeline_meta(
                    sql_connection, query_settings
                )
            except (SQLError, IndexError) as error:
                logger.critical(error)
                raise fastapi.HTTPException(500) from error

            params_dict = {}
            if params:
                params_dict = params.dict()
            return await job_data.download(
                job,
                (_config.influxdb_st, _config.influxdb_lt),
                params_dict,
                spec,
            )
        raise fastapi.HTTPException(510, "job download feature not installed")
