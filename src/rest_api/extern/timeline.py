import asyncio
import json
import logging
import math

import hostlist
import metricq
import numpy as np

import utils.common
from sql.view.meta import TimelineORM

from ..model import ExternTimelineFeature

# mypy: ignore-errors


async def get_values(client, metric, start, end):
    start_time = metricq.Timestamp.from_posix_seconds(start)
    end_time = metricq.Timestamp.from_posix_seconds(end)

    delta = end - start
    max_points = 60
    if delta <= 3600:
        interval_max = metricq.Timedelta.from_s(40)
    if delta > 3600 and delta <= 7200:
        interval_max = metricq.Timedelta.from_s(60)
    if delta > 7200:
        interval_max = metricq.Timedelta.from_s(
            math.ceil((end - start) / max_points)
        )
    return await client.history_aggregate_timeline(
        metric,
        start_time=start_time,
        end_time=end_time,
        interval_max=interval_max,
        timeout=10,
    )


async def collect_data(token, server, metrics, start, end):
    async with metricq.HistoryClient(token, server, add_uuid=True) as client:
        coroutines = []
        for metric in metrics:
            coroutines.append(get_values(client, metric, start, end))
        return await asyncio.gather(*coroutines)


def get_metrics(partition, nodes_compact_str):
    nodelist = hostlist.expand_hostlist(nodes_compact_str)
    metrics = []
    metricq_def = utils.common.read_file_from_project_root("metricq.json")
    data = json.loads(metricq_def)
    metricq_uri = data["metricq"]
    if partition not in metricq_uri.keys():
        return metrics
    node_template = metricq_uri[partition]

    for node in nodelist:
        metrics.append(node_template.format(node))
    return metrics


def shorten_array_with_timestamps(timestamps: np, arr: np, method="mean"):
    target_size = 240
    n = len(arr)
    if len(timestamps) != n:
        raise ValueError(
            "The length of `timestamps` must match the length of `arr`."
        )

    if n < target_size * 2:
        return timestamps, arr

    k = n / target_size  # Reduction factor
    group_size = int(np.ceil(k))  # Group size (rounded up)

    # Prepare the output arrays
    reduced_values = []
    reduced_timestamps = []

    for i in range(0, n, group_size):
        group = arr[i : i + group_size]
        group_timestamps = timestamps[i : i + group_size]

        # Process the values
        if method == "mean":
            reduced_values.append(np.mean(group))
        elif method == "max":
            reduced_values.append(np.max(group))
        elif method == "min":
            reduced_values.append(np.min(group))
        else:
            raise ValueError(
                "Invalid method. Choose from 'mean', 'max', 'min'."
            )

        # For timestamps, take the first timestamp of the group as representative
        reduced_timestamps.append(group_timestamps[0])

    # Convert to NumPy arrays and truncate to exact target size
    return np.array(reduced_timestamps[:target_size]), np.array(
        reduced_values[:target_size]
    )


async def query_db(
    job_detail: TimelineORM,
    metric,
    params,
    config: ExternTimelineFeature,
):
    if metric != "node_power":
        return {"error": metric + " is not supported"}

    # if "MQ_TOKEN" not in os.environ and "MQ_SERVER" not in os.environ:
    #     return {"error": "No data available."}

    token = config.token
    server = config.server

    nodelist = job_detail.node_list
    partition = job_detail.partition_name
    start = params["start"]
    end = params["end"]

    try:
        timeline_type = params["timeline_type"].value
    except AttributeError:
        timeline_type = "mean"

    timestamps = []
    values = []
    metrics = get_metrics(partition, nodelist)
    if not metrics:
        return metrics

    logger = metricq.get_logger()
    logger.setLevel(logging.WARNING)
    try:
        data = await collect_data(token, server, metrics, start, end)
    except:
        return {"error": "Cannot connect to MetricQ"}

    # put all data into numpy array, one row per node metric
    cnt = 0
    first_row_length = 0
    for result in data:
        values_row = []
        for aggregate in result:
            # print(int(aggregate.timestamp.posix), aggregate.minimum, aggregate.maximum, aggregate.mean)
            if cnt == 0:
                timestamps.append(int(aggregate.timestamp.posix))
            values_row.append(aggregate.mean)
        if cnt == 0:
            # remember length
            first_row_length = len(values_row)
            values = np.array([values_row])
        else:
            if len(values_row) == first_row_length:
                values = np.append(values, [values_row], axis=0)
            # else:
            #     logger.warning("MetricQ data not aligned. Skipped node metric.")
        cnt = cnt + 1

    # prepare data for PIKA gui
    timestamps = np.array(timestamps)
    metrics = {}
    metrics["unit"] = "W"

    if timeline_type != "min_mean_max":
        if timeline_type == "aggregated":
            result_timestamps, result_values = shorten_array_with_timestamps(
                timestamps, values.sum(axis=0), "max"
            )
        elif timeline_type == "max":
            result_timestamps, result_values = shorten_array_with_timestamps(
                timestamps, values.max(axis=0), "max"
            )
        elif timeline_type == "mean":
            result_timestamps, result_values = shorten_array_with_timestamps(
                timestamps, values.mean(axis=0), "mean"
            )
        elif timeline_type == "min":
            result_timestamps, result_values = shorten_array_with_timestamps(
                timestamps, values.min(axis=0), "min"
            )
        metrics["timestamps"] = result_timestamps.tolist()
        metrics["node_power"] = []
        metrics["node_power"].append(result_values.tolist())
        metrics["node_power"].append(
            {"mean": "0", "best_node": None, "lowest_node": None}
        )
    else:
        result_timestamps, result_values = shorten_array_with_timestamps(
            timestamps, values.min(axis=0), "min"
        )
        metrics["timestamps"] = result_timestamps.tolist()
        metrics["min"] = []
        metrics["min"].append(result_values.tolist())
        metrics["min"].append(
            {"mean": "0", "best_node": None, "lowest_node": None}
        )

        _, result_values = shorten_array_with_timestamps(
            timestamps, values.mean(axis=0), "mean"
        )
        metrics["mean"] = []
        metrics["mean"].append(result_values.tolist())
        metrics["mean"].append(None)

        _, result_values = shorten_array_with_timestamps(
            timestamps, values.max(axis=0), "max"
        )
        metrics["max"] = []
        metrics["max"].append(result_values.tolist())
        metrics["max"].append(None)

    return metrics
