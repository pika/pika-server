import argparse
from functools import wraps
from typing import Any, Callable, Dict, Optional, Tuple, TypeVar, cast

import requests
from cachetools import TTLCache

from utils.logger import logger

T = TypeVar("T")  # Generic return type

# Define cache (stores up to 100 items, expires after 10min)
cache = TTLCache(maxsize=100, ttl=600)


def conditional_cache(
    func: Callable[..., Optional[T]]
) -> Callable[..., Optional[T]]:
    """Decorator that caches results only if they are not None"""

    @wraps(func)
    def wrapper(
        *args: Tuple[Any, ...], **kwargs: Dict[str, Any]
    ) -> Optional[T]:
        key = (args, frozenset(kwargs.items()))  # Create a unique cache key

        if key in cache:
            return cast(
                Optional[T], cache[key]
            )  # Explicitly cast to Optional[T]

        result = func(*args, **kwargs)  # Call the function

        if result is not None:  # Cache only if the result is NOT None
            cache[key] = result

        return result  # Return the function output

    return wrapper  # Correctly typed function


@conditional_cache
def get_projects(user_name: str) -> list[str] | None:
    """Get current projects for the current user if they have PI status.
    args:
        user_name: username from authentification system
    return:
        list of project ids (e.g. ["p_pika", "p_projectA", ...])
        or empty list if they do not have PI status
    """
    try:
        url = (
            f"https://pdbin.zih.tu-dresden.de/user/{user_name}/status/pipc.json"
        )
        # url = "https://jsonplaceholder.typicode.com/todos/1"

        response = requests.get(url, timeout=3)

        if response.status_code == 200:
            data = response.json()
            if isinstance(data, list):
                return data
            return None
        else:
            logger.critical(
                f"Failed to request PI projects for {user_name}. Status code: {response.status_code}"
            )
        return None
    except requests.exceptions.ConnectionError as e:
        logger.critical(
            f"Connection error occurred while requesting PI projects for {user_name}: {e}"
        )
        return None
    except requests.exceptions.RequestException as e:
        logger.critical(
            f"An error occurred while requesting PI projects for {user_name}: {e}"
        )
        return None


# for testing
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process a user argument.")
    parser.add_argument("user_name", type=str, help="The username to process")
    args = parser.parse_args()
    user_name = args.user_name
    print(get_projects(user_name))
    print("second call from cache:")
    print(get_projects(user_name))
