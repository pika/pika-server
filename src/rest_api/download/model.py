from pydantic import BaseModel


class DownloadData(BaseModel):
    """Specifies which data is to be downloaded (multiple can be selected)"""

    job_metadata: bool | None = None
    performance_data: bool | None = None
    cluster_data: bool | None = None
