import io
import json
import os
import time
import zipfile
from collections import OrderedDict
from statistics import mean

import hostlist
import jsonschema
from aioinflux import iterpoints
from fastapi.responses import StreamingResponse
from jsonschema import RefResolver, validate

from influx.connection import AsyncInfluxConnection
from influx.query import *
from influx.query import DownloadMetricQueryBuilder
from sql.view.meta import TimelineORM
from utils.specification import Specification


class DownloadJobdata:
    def __init__(
        self,
        job_detail: TimelineORM,
        credentials,
        spec: Specification,
    ):
        self.partition_data = spec.partition
        self.metric_data = spec.metric
        self.cc_metrics = spec.cc_metric
        self.cc_subcluster_metrics = spec.cc_subcluster_metrics

        self.jobID = job_detail.job_id
        self.user = job_detail.user_name
        self.project = job_detail.project_name
        self.subCluster = self.__get_SubCluster(job_detail.partition_name)
        self.partition = job_detail.partition_name
        self.cluster = "unknown"
        if self.partition in self.partition_data:
            self.cluster = self.partition_data.get(self.partition).cluster
        self.arrayJobId = job_detail.array_id
        self.nodelist = job_detail.node_list
        self.cpulist = job_detail.core_list
        self.numNodes = job_detail.node_count
        self.numCPUs = job_detail.core_count
        self.numGPUs = job_detail.gpu_count
        self.gpulist = job_detail.gpu_list
        self.exclusive = job_detail.exclusive
        self.startTime = job_detail.start_time
        self.endTime = job_detail.end_time
        job_state = job_detail.job_state
        if job_state == "OOM":
            job_state = "out_of_memory"
        self.jobState = job_state
        self.duration = job_detail.end_time - job_detail.start_time
        self.walltime = job_detail.time_limit
        self.monitoringStatus = 1
        self.smt = 1
        if self.partition in self.partition_data:
            self.smt = self.partition_data.get(self.partition).smt_mode
        self.jobName = job_detail.job_name if job_detail.job_name else ""
        self.jobScript = job_detail.job_script if job_detail.job_script else ""

        influx, influx_lt = credentials
        self.connection = influx_lt
        if int(job_detail.start_time) > (time.time() - 1814400):
            self.connection = influx

        self.metric_query_builder = DownloadMetricQueryBuilder(
            job_detail,
            TimelineMetric["cpu_usage"],
            TimelineParameter(),
            spec,
        )

    def __get_json_schema(self, json_schema):
        with open(
            os.path.dirname(__file__)
            + "/cc-specifications/datastructures/"
            + json_schema,
            "r",
        ) as file:
            schema = json.load(file)
        resolver = RefResolver(
            "file://"
            + os.path.dirname(__file__)
            + "/cc-specifications/datastructures/",
            schema,
        )
        return (schema, resolver)

    def __get_resources(self):
        resources = []
        nodelist = hostlist.expand_hostlist(self.nodelist)
        if self.exclusive != 1:
            cpulist = self.metric_query_builder._convert_cpulist_to_cpulistdict(
                self.cpulist
            )
            cpulist_exp = {}
            for node, value in cpulist.items():
                # convert all strings to int
                cpulist_exp[node] = [
                    int(id) for id in hostlist.expand_hostlist(value)
                ]
            if self.numGPUs > 0:
                gpulist = (
                    self.metric_query_builder._convert_cpulist_to_cpulistdict(
                        self.gpulist
                    )
                )
                gpulist_exp = {}
                for node, value in gpulist.items():
                    # convert all strings to int
                    gpulist_exp[node] = [
                        str(id) for id in hostlist.expand_hostlist(value)
                    ]

        for node in nodelist:
            if self.exclusive != 1:
                if self.numGPUs > 0:
                    resources.append(
                        {
                            "hostname": node,
                            "hwthreads": cpulist_exp[node],
                            "accelerators": gpulist_exp[node],
                        }
                    )
                else:
                    resources.append(
                        {"hostname": node, "hwthreads": cpulist_exp[node]}
                    )
            else:
                cpulist = [
                    int(id)
                    for id in hostlist.expand_hostlist(
                        "[0-" + str(int(self.numCPUs / self.numNodes) - 1) + "]"
                    )
                ]
                if self.numGPUs > 0:
                    gpulist = [
                        str(id)
                        for id in hostlist.expand_hostlist(
                            "[0-"
                            + str(int(self.numGPUs / self.numNodes) - 1)
                            + "]"
                        )
                    ]
                    resources.append(
                        {
                            "hostname": node,
                            "hwthreads": cpulist,
                            "accelerators": gpulist,
                        }
                    )
                else:
                    resources.append({"hostname": node, "hwthreads": cpulist})

        return resources

    def __get_SubCluster(self, partition):
        p = partition
        # remove extensions like -interactive
        if "-" in p:
            p = p.split("-")[0]
        # remove numbers
        p = "".join([i for i in p if not i.isdigit()])

        if p in self.cc_subcluster_metrics:
            return p
        else:
            return "default"

    def __validate(self, meta, schema):
        try:
            validate(
                instance=meta,
                schema=self.__get_json_schema(schema)[0],
                resolver=self.__get_json_schema(schema)[1],
            )
        except jsonschema.exceptions.ValidationError as e:
            return {"error": e.message, "exception": str(e)}

    def __get_pika_metrics(self):
        stat_metrics = []
        for metric_val in self.cc_subcluster_metrics[self.subCluster]:
            if metric_val == "filesystems":
                for filesystem in self.cc_metrics[metric_val]["systems"]:
                    for op in filesystem["ops"]:
                        stat_metrics.append(op["pika_metric"])
            else:
                stat_metrics.append(self.cc_metrics[metric_val]["pika_metric"])
        return stat_metrics

    async def __get_metric_statistics(self):
        # create async queries for all metrics
        stat_metrics = self.__get_pika_metrics()

        stat_queries = []
        for metric in stat_metrics:
            self.metric_query_builder._change_metric(metric)
            stat_queries.append(
                self.metric_query_builder.get_statistic_queries()
            )

        results = await AsyncInfluxConnection(self.connection).execute_queries(
            stat_queries
        )

        formatted_results = {}
        cnt = 0
        for metric in results:
            if "series" in metric["results"][0]:
                for i in iterpoints(metric):
                    formatted_results[stat_metrics[cnt]] = [i[1], i[2], i[3]]
            else:
                formatted_results[stat_metrics[cnt]] = [0, 0, 0]
            cnt = cnt + 1

        return formatted_results

    async def __get_metric_detail_timelines(self, metric):
        self.metric_query_builder._change_metric(metric)
        query = self.metric_query_builder.get_detail_timeline_query()
        results = await AsyncInfluxConnection(self.connection).execute_queries(
            [query]
        )

        formatted_results = []
        if "series" in results[0]["results"][0]:
            data = results[0]["results"][0]["series"]
            for timeline in data:
                timeline_dict = timeline["tags"]
                val_list = []
                for val in timeline["values"]:
                    val_list.append(val[1])
                timeline_dict["values"] = val_list
                formatted_results.append(timeline_dict)
        else:
            formatted_results = None

        return formatted_results

    async def get_metadata(self):
        meta = {}
        meta["jobId"] = self.jobID
        meta["user"] = self.user
        meta["project"] = self.project
        meta["cluster"] = self.cluster
        meta["subCluster"] = self.subCluster
        meta["partition"] = self.partition
        meta["arrayJobId"] = self.arrayJobId
        meta["numNodes"] = self.numNodes
        meta["numHwthreads"] = self.numCPUs
        if self.numGPUs > 0:
            meta["numAcc"] = self.numGPUs
        meta["exclusive"] = self.exclusive
        meta["startTime"] = self.startTime
        job_state = self.jobState
        if job_state == "OOM":
            job_state = "out_of_memory"
        meta["jobState"] = job_state
        meta["duration"] = self.duration
        meta["walltime"] = self.walltime
        meta["monitoringStatus"] = 1
        meta["smt"] = self.smt

        resources = self.__get_resources()
        meta["resources"] = resources
        additional_meta = {}
        additional_meta["jobName"] = self.jobName
        additional_meta["jobScript"] = self.jobScript
        additional_meta["slurmInfo"] = ""
        meta["metaData"] = additional_meta

        statistics = {}
        results = await self.__get_metric_statistics()

        for metric in self.cc_subcluster_metrics[self.subCluster]:
            values = self.cc_metrics[metric]
            if metric != "filesystems":
                pika_metric = values["pika_metric"]
                statistic = {}
                statistic["unit"] = {"base": values["base_unit"]}
                statistic["min"] = results[pika_metric][0]
                statistic["avg"] = results[pika_metric][1]
                statistic["max"] = results[pika_metric][2]
                statistics[metric] = statistic
            else:
                # metrics for each filesystem
                statistics[metric] = []
                for filesystem in values["systems"]:
                    filesystem_stats = {}
                    filesystem_stats["name"] = filesystem["name"]
                    filesystem_stats["type"] = filesystem["type"]
                    for op in filesystem["ops"]:
                        pika_metric = op["pika_metric"]
                        statistic = {}
                        statistic["unit"] = {"base": values["base_unit"]}
                        statistic["min"] = results[pika_metric][0]
                        statistic["avg"] = results[pika_metric][1]
                        statistic["max"] = results[pika_metric][2]
                        filesystem_stats[op["metric"]] = statistic
                    statistics[metric].append(filesystem_stats)

        meta["statistics"] = statistics

        self.__validate(meta, "job-meta.schema.json")
        return meta

    async def get_performance_data(self):
        timeline_data = {}
        for metric in self.cc_subcluster_metrics[self.subCluster]:
            values = self.cc_metrics[metric]
            if metric != "filesystems":
                # get all data of metric
                pika_metric = values["pika_metric"]
                data = await self.__get_metric_detail_timelines(pika_metric)
                if data is None:
                    continue
                # print(metric)

                p = {}
                p["unit"] = {"base": values["base_unit"]}
                p["timestep"] = self.metric_data[pika_metric]["timestep"]
                series_arr = []
                for item in data:
                    # print(item)

                    series = {}
                    series["hostname"] = item["hostname"]
                    if "cpu" in item:
                        series["id"] = str(item["cpu"])
                    if "gpu" in item:
                        series["id"] = str(item["gpu"])

                    series["statistics"] = {
                        "min": min(item["values"]),
                        "avg": mean(item["values"]),
                        "max": max(item["values"]),
                    }
                    series["data"] = item["values"]
                    series_arr.append(series)

                p["series"] = series_arr

                # only for cc
                # p['statisticsSeries'] = {}
                scope = self.metric_data[pika_metric]["scope"]
                if scope == "gpu":
                    scope = "accelerator"
                timeline_data[metric] = {scope: p}
                # timeline_data[metric] = {'node':p}
            else:
                # metrics for each filesystem
                timeline_data[metric] = []
                for filesystem in values["systems"]:
                    filesystem_stats = {}
                    filesystem_stats["name"] = filesystem["name"]
                    filesystem_stats["type"] = filesystem["type"]
                    for op in filesystem["ops"]:
                        pika_metric = op["pika_metric"]
                        data = await self.__get_metric_detail_timelines(
                            pika_metric
                        )
                        if data is None:
                            continue
                        p = {}
                        p["unit"] = {"base": values["base_unit"]}
                        p["timestep"] = values["timestep"]
                        series_arr = []
                        for item in data:
                            # print(item)

                            series = {}
                            series["hostname"] = item["hostname"]
                            if "cpu" in item:
                                series["id"] = str(item["cpu"])
                            if "gpu" in item:
                                series["id"] = str(item["gpu"])

                            series["statistics"] = {
                                "min": min(item["values"]),
                                "avg": mean(item["values"]),
                                "max": max(item["values"]),
                            }
                            series["data"] = item["values"]
                            series_arr.append(series)

                        p["series"] = series_arr
                        scope = values["scope"]
                        filesystem_stats[op["metric"]] = {scope: p}

                    timeline_data[metric].append(filesystem_stats)

        self.__validate(timeline_data, "job-data.schema.json")
        return timeline_data

    async def get_cluster_data(self):
        with open(
            os.path.dirname(__file__) + "/cluster/" + self.cluster + ".json"
        ) as f:
            cluster_def_from_file = json.load(f, object_pairs_hook=OrderedDict)

        # self.__validate(cluster_def_from_file, 'cluster.schema.json')
        return cluster_def_from_file


async def download(
    job_detail: TimelineORM,
    credentials: tuple,
    params,
    spec: Specification,
):
    # logging.info(f"download data: {params}")
    files = []

    data = DownloadJobdata(job_detail, credentials, spec)
    if params["job_metadata"] == True:
        files.append(
            (
                "meta.json",
                io.BytesIO(
                    json.dumps(await data.get_metadata()).encode("utf-8")
                ),
            )
        )

    if params["performance_data"] == True:
        files.append(
            (
                "data.json",
                io.BytesIO(
                    json.dumps(await data.get_performance_data()).encode(
                        "utf-8"
                    )
                ),
            )
        )

    if params["cluster_data"] == True:
        files.append(
            (
                "cluster.json",
                io.BytesIO(
                    json.dumps(await data.get_cluster_data()).encode("utf-8")
                ),
            )
        )

    zip_bytes_io = io.BytesIO()
    with zipfile.ZipFile(
        zip_bytes_io, "a", zipfile.ZIP_DEFLATED, False
    ) as zip_file:
        # Add each file to the ZIP archive, using the data type as the file name
        for file_name, data in files:
            zip_file.writestr(file_name, data.getvalue())

    return StreamingResponse(
        iter([zip_bytes_io.getvalue()]),
        media_type="application/x-zip-compressed",
        headers={
            "Content-Disposition": f"attachment;filename=data.zip",
            "Content-Length": str(zip_bytes_io.getbuffer().nbytes),
        },
    )
