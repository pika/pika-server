"""unit tests for rest_api/auth.py"""

from unittest import TestCase

from rest_api.auth import TokenCrypto, TokenError
from rest_api.model import ConfigurationError


class TokenCryptoTest(TestCase):
    """unit tests for rest_api.auth.TokenCrypto"""

    def test_init(self):
        """unit test for rest_api.auth.TokenCrypto.__init__"""

        with self.subTest("incomplete config"):
            with self.assertRaises(ConfigurationError):
                TokenCrypto({"secret_key": "geheim"})

        with self.subTest("wrong types"):
            with self.assertRaises(ConfigurationError):
                TokenCrypto(
                    {
                        "secret_key": "geheim",
                        "token_expire_minutes": {
                            "normal": 23,
                            "longterm": "lang",
                        },
                    }
                )

    def setUp(self) -> None:
        self._tokencrypto = TokenCrypto(
            {
                "secret_key": "geheim",
                "token_expire_minutes": {
                    "normal": -1,
                    "longterm": 999,
                },
            }
        )
        return super().setUp()

    def test_create_token(self):
        """unit test for rest_api.auth.TokenCrypto.create_token"""

        token = self._tokencrypto.create_token("paul", True)

        with self.subTest("token instance"):
            self.assertIsInstance(token, str)

        with self.subTest("token length"):
            self.assertTrue(len(token) > 5)

        token = self._tokencrypto.create_token(None, None)

        with self.subTest("token instance wrong input"):
            self.assertIsInstance(token, str)

        with self.subTest("token length wrong input"):
            self.assertTrue(len(token) > 5)

    def test_check_token(self):
        """unit test for rest_api.auth.TokenCrypto.check_token"""

        with self.subTest("correct input"):
            token = self._tokencrypto.create_token("paul", True)
            name = self._tokencrypto.check_token(token)
            self.assertEqual(name, "paul")

        with self.subTest("correct input, expired"):
            token = self._tokencrypto.create_token("paul", False)
            with self.assertRaises(TokenError):
                self._tokencrypto.check_token(token)

        with self.subTest("invalid token"):
            with self.assertRaises(TokenError):
                self._tokencrypto.check_token("test")

        with self.subTest("no token"):
            with self.assertRaises(TokenError):
                self._tokencrypto.check_token(None)

    def test_token_expire_minutes(self):
        """unit test for rest_api.auth.TokenCrypto.token_expire_minutes"""

        with self.subTest("longterm"):
            minutes = self._tokencrypto.token_expire_minutes(True)
            self.assertEqual(minutes, 999)

        with self.subTest("normal"):
            minutes = self._tokencrypto.token_expire_minutes(False)
            self.assertEqual(minutes, -1)

        with self.subTest("None"):
            minutes = self._tokencrypto.token_expire_minutes(None)
            self.assertEqual(minutes, -1)
