"""Frequently Utilized Classes in REST API Development"""

import json
import typing
from enum import Enum
from typing import List

import fastapi
import fastapi.exceptions
import fastapi.security
from pydantic import BaseModel

from sql.model import JobDataFilter, JobQueueFilter
from utils.common import one_week_ago
from utils.logger import logger
from utils.model import ConfigurationError, DBCredential, FormatRequest

from .auth import TokenCrypto, TokenError


class OptionalHTTPBearer(fastapi.security.HTTPBearer):
    async def __call__(self, request: fastapi.Request) -> typing.Optional[str]:
        from fastapi import status

        try:
            r = await super().__call__(request)
            token = r.credentials
        except fastapi.exceptions.HTTPException as ex:
            assert ex.status_code == status.HTTP_403_FORBIDDEN, ex
            token = None
        return token


class QuerySettings[T](BaseModel):
    """
    - representation of the json body from the users http post request
    - we are not instanciating this class in our code, fastapi is translating
      the users json body into a QuerySettings object

    inherit:
        - BaseModel: make this class recieveable from a rest-api endpoint (
                        fastapi requires this)

    variables:
        - filter:
            - explanation: a sql filter like object
        - format_result:
            - explanation:
                - False -> the user wants "raw utc00 not human formated" data
                - True -> the user wants the result time formated in utc00
                - FormatRequest -> the user want the result time formated in his timezone
            - default value: False
    """

    filter: T
    format_result: FormatRequest | bool = False

    @staticmethod
    def _check_format_result(
        format_request: typing.Union[FormatRequest, bool],
    ) -> FormatRequest:
        """look at class doc-string variables.format-result

        raises:
            - FormatRequestError
        """

        if isinstance(format_request, FormatRequest):
            format_request.evaluate()

        if isinstance(format_request, bool):
            if format_request is True:
                format_request = FormatRequest(utc_timezone="00:00")
            else:
                format_request = FormatRequest(user_wants_formating=False)

        return format_request

    @staticmethod
    def _restrict_view(filter: T, username: str) -> T:
        """restrict the common user to only see his own jobs"""

        # username is either a string or a list of project ids
        if username.startswith("["):
            username = json.loads(username)
            if isinstance(username, list):
                filter.project_name_list = username
        elif username != "admin":
            filter.user_name = username

        return filter

    @staticmethod
    def _restrict_time(filter: T, restrict_time: bool) -> T:
        """
        - anti ddos protection
        - its very easy for a user to send a filter without time restriction and
          accidently query the whole database
        - time_filter is True by default so we can set a time restriction in the
          filter
        """

        if restrict_time and (filter.start_time is None):
            filter.start_time = one_week_ago()

        return filter

    def evaluate(
        self, username: str = None, restrict_time: bool = None
    ) -> tuple[T, FormatRequest]:
        """
        - check what fastapi has generated for us
        - return the inner variables

        return:
            - [0]: the users sql query filter
            - [1]: the users format request
        """

        if username is not None:
            self.filter = self._restrict_view(self.filter, username)

        if restrict_time is not None:
            self.filter = self._restrict_time(self.filter, restrict_time)

        self.format_result = self._check_format_result(self.format_result)

        return (self.filter, self.format_result)


class QuerySettingsURL(BaseModel):
    """
    - read QuerySettings first
    - some routes do not need a filter object
    """

    format_result: FormatRequest | bool = False

    def evaluate(self) -> FormatRequest:
        """
        raises:
            - FormatRequestError
        """
        self.format_result = QuerySettings._check_format_result(
            self.format_result
        )

        return self.format_result


class QuerySettingsFilter[T](BaseModel):
    """
    - read QuerySettings doc first
    - some routes do not need a formatrequest object
    """

    filter: T

    def evaluate(self, username: str) -> T:
        filter = QuerySettings._restrict_view(self.filter, username)
        return filter


class QueueQuerySettings(QuerySettings[JobQueueFilter]):
    """remove"""


class JobQuerySettings(QuerySettings[JobDataFilter]):
    """add date_interval field to query settings"""

    use_date_interval: bool | None = True

    def evaluate(self, username: str) -> tuple[JobDataFilter, FormatRequest]:
        return QuerySettings.evaluate(self, username, self.use_date_interval)


class User(BaseModel):
    """
    BaseModel is needed for fastapi, to generate docu from this class,
    BaseModel also means, its not a class anymore but a c like struct (source:
    pydantic website)
    """

    version: str
    user_name: str
    user_token: str
    is_pi: bool
    is_admin: bool
    token_expire_minutes: float
    admin_token: str | None
    pi_token: str | None


class UserLogic:
    """
    FastAPI automatically generates documentation for our REST API when the
    objects returned by a route inherit from the `pydantic.BaseModel` class.

    Issue: Pydantic treats classes primarily as containers for variables.
    Since our classes typically have both data and logic, like regular classes,
    we are forced to separate the data representation into a distinct class.

    The representation class is named User, as it reflects what the end user
    of our REST API interacts with.
    """

    def __init__(
        self,
        pika_server_version: str,
        name: str,
        token_crypto: TokenCrypto,
        admins: list,
        longterm_user: list,
        pi_role_activated: bool,
    ) -> None:
        self._version = pika_server_version
        self._user_name = str(name)

        self._is_longterm: bool = name in longterm_user
        self._user_token = token_crypto.create_token(name, self._is_longterm)

        self._is_admin: bool = name in admins
        self._token_expire_minutes = float(
            token_crypto.token_expire_minutes(self._is_longterm)
        )
        self._admin_token = (
            token_crypto.create_token("admin", self._is_longterm)
            if self._is_admin
            else None
        )
        self._is_pi = False
        self._pi_token = None
        if pi_role_activated:
            # get project ids from internal pdbin
            from .extern import pi

            pi_projects = pi.get_projects(self._user_name)
            if pi_projects:
                self._is_pi = True
                self._pi_token = (
                    token_crypto.create_token(
                        "pi", self._is_longterm, pi_projects
                    )
                    if self._is_pi
                    else None
                )

    def as_dict(self) -> User:
        """
        Provide UserLogic's private variables as representations for the
        REST API.
        """

        return User(
            version=self._version,
            user_name=self._user_name,
            user_token=self._user_token,
            is_pi=self._is_pi,
            is_admin=self._is_admin,
            token_expire_minutes=self._token_expire_minutes,
            pi_token=self._pi_token,
            admin_token=self._admin_token,
        )


class ExternTimelineFeature:
    """internal repr of external timeline feature"""

    def __init__(self, config: dict) -> None:
        """
        Raises:
            utils.model.ConfigurationError
        """

        try:
            self._token = str(config["token"])
            self._server = str(config["server"])
        except (KeyError, TypeError) as error:
            raise ConfigurationError(f"extern timeline -> {error}") from error

    @property
    def token(self) -> str:
        """return the secret token of the external timeline feature"""
        return self._token

    @property
    def server(self) -> str:
        """return the hostname of the extern timeline server"""
        return self._server


class ApiFeatures:
    """internal repr of api features"""

    def __init__(self, config: dict) -> None:
        """
        Raises:
            utils.model.ConfigurationError
        """
        # features are optional

        if config.get("download_jobdata") is None:
            self._download_jobdata = None
            # None evaluates to False
        else:
            self._download_jobdata = True

        if config.get("extern_timeline") is None:
            self._extern_timeline = None
        else:
            try:
                self._extern_timeline = ExternTimelineFeature(
                    config["extern_timeline"]
                )
            except ConfigurationError as error:
                raise ConfigurationError(f"API Features -> {error}") from error

        if config.get("pi_role") is None:
            self._pi_role = None
            # None evaluates to False
        else:
            self._pi_role = True

    @property
    def extern_timeline(self) -> ExternTimelineFeature:
        """return the extern timeline configuration"""
        return self._extern_timeline

    @property
    def download_jobdata(self) -> bool:
        """return the download feature configuration"""
        return self._download_jobdata

    @property
    def pi_role(self) -> bool:
        """return the pi role configuration"""
        return self._pi_role


class FastApiConfig:
    def __init__(self, config: dict) -> None:
        try:
            self._hostname = str(config["hostname"])
            self._port = str(config["port"])
            self._proxy_hostname = str(config["proxy_hostname"])

            self._admins: List[str] = config["admins"]
            if isinstance(self._admins, list) is False:
                raise ConfigurationError(
                    "admins -> expected array with strings"
                )

            self._longterm_user: List[str] = config["longterm_user"]
            if isinstance(self._longterm_user, list) is False:
                raise ConfigurationError(
                    "longterm user -> expected array with strings"
                )

            self._cors_origin = str(config["cors_origin"])
            self._proxy_user_key = str(config["proxy_user_key"])
            self._route_root_path = str(config["route_root_path"])

            self._token_crypto = TokenCrypto(
                {
                    "secret_key": config["secret_key"],
                    "token_expire_minutes": config["token_expire_minutes"],
                }
            )

            self._features = ApiFeatures(config["features"])

        except (ConfigurationError, KeyError, TypeError) as error:
            raise ConfigurationError(f"fastapi config -> {error}") from error

    @property
    def features(self) -> ApiFeatures:
        """return rest api feature configurations
        (pi role, download and extern timeline feature)"""
        return self._features

    @property
    def token_crypto(self) -> TokenCrypto:
        """return the authentication class which was build form the configuration"""
        return self._token_crypto

    @property
    def hostname(self) -> str:
        """return the hostname, the rest api will run on"""
        return self._hostname

    @property
    def port(self) -> str:
        """return the port, the rest api will run on"""
        return self._port

    @property
    def proxy_hostname(self) -> str:
        """return the proxy hostname, the rest api will hide behind"""
        return self._proxy_hostname

    @property
    def admins(self) -> list:
        """return the list of admins, using this restapi"""
        return self._admins

    @property
    def longterm_user(self) -> list:
        """return the list of longterm users, using this rest api"""
        return self._longterm_user

    @property
    def cors_origin(self) -> str:
        """return the hostname from the frontends which will interact with this api"""
        return self._cors_origin

    @property
    def proxy_user_key(self) -> str:
        """return this key with which we will find the username value in the
        http header key value pairs
        """
        return self._proxy_user_key

    @property
    def route_root_path(self) -> str:
        """return the root path, we tihs restapi will be routed to, behind the proxy"""
        return self._route_root_path


class ApiConfiguration:
    """Internel representation of the rest api configuration"""

    def __init__(
        self,
        fast_api_config: dict,
        mariadb_config: dict,
        influx_st_config: dict,
        influx_lt_config: dict,
    ) -> None:
        """
        Raises:
            utils.model.ConfigurationError
        """
        # error propagation is used to build error message

        try:
            self._fast_api_config = FastApiConfig(fast_api_config)
        except ConfigurationError as error:
            raise ConfigurationError(f"api config -> {error}") from error

        try:
            self._mariadb_readonly = DBCredential(mariadb_config)
        except ConfigurationError as error:
            raise ConfigurationError(f"mariadb readonly -> {error}") from error

        try:
            self._influxdb_st = DBCredential(influx_st_config)
        except ConfigurationError as error:
            raise ConfigurationError(f"influx-st -> {error}") from error

        try:
            self._influxdb_lt = DBCredential(influx_lt_config)
        except ConfigurationError as error:
            raise ConfigurationError(f"influxdb-lt -> {error}") from error

    @property
    def fastapi(self) -> FastApiConfig:
        """return fastapi config"""
        return self._fast_api_config

    @property
    def mariadb(self) -> DBCredential:
        """return the mariadb credentials"""
        return self._mariadb_readonly

    @property
    def influxdb_st(self) -> DBCredential:
        """return the influx st credentials"""
        return self._influxdb_st

    @property
    def influxdb_lt(self) -> DBCredential:
        """return the influx lt credentials"""
        return self._influxdb_lt


class ExternTimelineMetrics(str, Enum):
    node_power = "node_power"


def get_name_from_token_or_raise(
    token_crypto: TokenCrypto, request: fastapi.Request
) -> str:
    try:
        username = token_crypto.check_token(
            request.headers.get("Authorization")
        )
    except TokenError as error:
        raise fastapi.exceptions.HTTPException(401, str(error)) from error
    return username
