"""authentification classes for the restapi"""

import json
from datetime import UTC, datetime, timedelta

from jose import JWTError, exceptions, jwt

from utils.model import ConfigurationError


class TokenError(Exception):
    """representing a error in the rest api auth module"""


class TokenCrypto:
    """takes care of all authorization work"""

    def __init__(self, config: dict) -> None:
        """
        initialize TokenCrypto

        Raising:
            utils.model.ConfigurationError
        """

        try:
            self._secret_key = str(config["secret_key"])
            # the secret key is used to generate unique tokens
        except (KeyError, TypeError) as error:
            raise ConfigurationError(f"secret key -> {error}") from error

        try:
            self._token_expire_minutes_normal = float(
                config["token_expire_minutes"]["normal"]
            )

            self._token_expire_minutes_long = float(
                config["token_expire_minutes"]["longterm"]
            )
        except (KeyError, TypeError, ValueError) as error:
            raise ConfigurationError(
                f"token expire minutes -> {error}"
            ) from error

    def create_token(
        self, name: str, is_longterm_user: bool, pi_projects: list[str] = []
    ) -> str:
        """creates a token based on name and expiration date"""

        token_expire_minutes = (
            self._token_expire_minutes_long
            if is_longterm_user
            else self._token_expire_minutes_normal
        )

        expire = datetime.now(UTC) + timedelta(minutes=token_expire_minutes)
        payload = {"sub": name, "exp": expire}
        if len(pi_projects) > 0:
            payload["pi_projects"] = pi_projects

        return jwt.encode(
            payload,
            self._secret_key,
            algorithm="HS256",
        )

    def check_token(self, token: str) -> str:
        """Read and return the username from the token

        Raising:
            TokenError: invalid token
        """

        try:
            # allow with Bearer and without Baerer prefix
            token_list = token.split(" ")
            if len(token_list) > 1:
                token = token_list[1]
            else:
                token = token_list[0]

            payload = jwt.decode(token, self._secret_key, algorithms="HS256")
            username: str = payload.get("sub")
            if username == "pi":
                return json.dumps(payload.get("pi_projects"))
            return username
        except exceptions.ExpiredSignatureError as error:
            raise TokenError("Token expired") from error
        except JWTError as error:
            raise TokenError("Invalid token") from error
        except AttributeError as error:
            raise TokenError("No token provided") from error

    def token_expire_minutes(self, is_longterm_user: bool) -> float:
        """return the expire time of the token for a user"""
        if is_longterm_user:
            return self._token_expire_minutes_long

        return self._token_expire_minutes_normal
