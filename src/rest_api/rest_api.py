"""rest api routes"""

from logging import CRITICAL, DEBUG

import uvicorn
from fastapi import APIRouter, FastAPI, HTTPException, Request
from fastapi.middleware.cors import CORSMiddleware

from sql.connection import AsyncSQLConnection, SQLConnection
from utils.logger import logger
from utils.model import OperationMode
from utils.specification import Specification

from .model import ApiConfiguration, FastApiConfig, User, UserLogic
from .routes.issue import register_issue_routes
from .routes.job import register_job_routes
from .routes.node import register_node_routes
from .routes.pending import register_jobpending_routes
from .routes.specification import register_specification_routes
from .routes.statistic import register_statistic_routes


def main(
    mode: OperationMode, config: ApiConfiguration, spec: Specification
) -> None:
    """run the rest api"""
    logger.info("starting rest-api")

    app = FastAPI(title="pika rest-api")
    app.add_middleware(
        CORSMiddleware,
        allow_origins=[config.fastapi.cors_origin],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    sql_connection = SQLConnection(config.mariadb)
    async_sql_connection = AsyncSQLConnection(config.mariadb)

    credential_router = APIRouter(tags=["authentication"])
    register_credential_route(
        mode, credential_router, config.fastapi, spec.pika_server_version
    )
    app.include_router(credential_router)

    specification_router = APIRouter(tags=["specifications"])
    register_specification_routes(
        specification_router, config.fastapi.token_crypto, spec
    )
    app.include_router(specification_router)

    node_router = APIRouter(tags=["node statistics"])
    register_node_routes(
        node_router, config.fastapi.token_crypto, async_sql_connection
    )
    app.include_router(node_router)

    jobtable_router = APIRouter(tags=["live and completed jobs"])
    register_job_routes(
        jobtable_router, config, spec, sql_connection, async_sql_connection
    )
    app.include_router(jobtable_router)

    pendig_router = APIRouter(
        prefix="/jobtable_pend", tags=["pending jobs in the slurm queue"]
    )
    register_jobpending_routes(
        pendig_router, config.fastapi.token_crypto, async_sql_connection
    )
    app.include_router(pendig_router)

    issue_router = APIRouter(
        prefix="/issuetable", tags=["jobs with performance issues"]
    )
    register_issue_routes(
        issue_router, config.fastapi.token_crypto, async_sql_connection
    )
    app.include_router(issue_router)

    statistic_router = APIRouter(tags=["job statistics"])
    register_statistic_routes(
        statistic_router, config.fastapi.token_crypto, async_sql_connection
    )
    app.include_router(statistic_router)

    return uvicorn.run(
        app,
        host=config.fastapi.hostname,
        port=int(config.fastapi.port),
        root_path=config.fastapi.route_root_path,
        log_level=(CRITICAL if mode == OperationMode.PRODUCTION else DEBUG),
    )


def register_credential_route(
    mode: OperationMode,
    router: APIRouter,
    config: FastApiConfig,
    pika_server_version: str,
) -> None:
    """register all routes of this restapi"""

    @router.get(
        "/credential",
        summary="token generation (authentication system required)",
    )
    async def _(request: Request) -> User:
        # hidden behind Shibboleht authentication and proxy

        username = request.headers.get(config.proxy_user_key)
        # the username is sent by the shibboleth service in the http header
        # e.g. GET /credential HTTP/1.1 remote_user: beju577d

        if username is None:
            if mode == OperationMode.DEVELOPMENT:
                first_admin_user = "unknown"
                if len(config.admins) > 0:
                    first_admin_user = config.admins[0]
                logger.warning(
                    "no username provided, logging in as %s due to development "
                    "mode",
                    first_admin_user,
                )
                username = first_admin_user
            else:
                raise HTTPException(
                    401, "Username not detected in Shibboleth header"
                )

        if len(config.proxy_hostname) > 0:
            if request.client.host != config.proxy_hostname:
                # only requests from the proxy will be accepted
                logger.warning(
                    "%s tries to connect but does not "
                    "match the proxy hostname, request is not accepted",
                    request.client.host,
                )
                raise HTTPException(
                    407, "You are not authorized to access the Pika backend."
                )
        else:
            logger.warning(
                "%s proxy_hostname not set! "
                "Please use only for testing! "
                "It is strongly recommended to setup a proxy server that "
                "handles user authentication.",
                request.client.host,
            )
            if mode == OperationMode.PRODUCTION:
                raise HTTPException(
                    407,
                    "In production mode, you are not authorized to access"
                    "the Pika backend without a valid proxy server.",
                )

        return UserLogic(
            pika_server_version,
            username,
            config.token_crypto,
            config.admins,
            config.longterm_user,
            config.features.pi_role,
        ).as_dict()
