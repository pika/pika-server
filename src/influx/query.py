"""building influx queries, influx is not using sqlalchmey but string sql queries"""

import json
import math
from json.decoder import JSONDecodeError
from typing import Any

import hostlist
from aioinflux import iterpoints
from influxdb.resultset import ResultSet

from sql.view.meta.timeline import TimelineORM
from utils.logger import logger
from utils.specification import Specification

from .model import TimelineMetric, TimelineParameter, TimelineType


class MetricQueryBuilder:
    """abstract class which build a influx class"""

    def __init__(
        self,
        job_detail: TimelineORM,
        metric: TimelineMetric,
        params: TimelineParameter,
        spec: Specification,
    ):
        partition_data = spec.partition
        self.metric: str = metric.value
        self.metric_data = spec.metric
        self.metric_def = spec.metric[self.metric]
        self.nodelist = job_detail.node_list
        self.cpulist = job_detail.core_list
        self.gpu_count = job_detail.gpu_count
        self.gpulist = job_detail.gpu_list
        self.num_cores = job_detail.core_count

        # if partition does not exist in partition_table use default
        try:
            partition = partition_data[str(job_detail.partition_name)]
        except:
            partition = partition_data["default"]

        self.partition_name = str(job_detail.partition_name)
        self.cpu_num = partition.cpu_num
        self.socket_num = partition.socket_num

        try:
            self.socket_cpu: list[int] = json.loads(
                str(partition.likwid_socket_cpu)
            )
        except JSONDecodeError:  # probably julia partition
            self.socket_cpu = []

        self.exclusive = int(job_detail.exclusive)

        job_start = int(job_detail.start_time)
        job_end = 0
        if job_detail.end_time:
            job_end = int(job_detail.end_time)

        # to ensure alignment
        job_start = ((job_start + 29) // 30) * 30  # round up to next 30s
        job_end = (job_end // 30) * 30  # round down to previous 30s

        if (params.start is not None) and (
            params.start >= job_start and params.start < job_end
        ):
            self.zoom_start = params.start
        else:
            self.zoom_start = job_start

        if (params.end is not None) and (
            params.end > job_start and params.end <= job_end
        ):
            self.zoom_end = params.end
        else:
            self.zoom_end = job_end

        self.timeline_type = (
            TimelineType.mean
            if params.timeline_type is None
            else params.timeline_type
        )
        self.auto_aggregation = (
            True if params.auto_aggregation is None else params.auto_aggregation
        )
        self.mean_line = False if params.mean_line is None else params.mean_line

        # the main interface of this class is: self.hostnames_clause,
        # self.time_clause & self._change_metric()
        self.hostnames_clause = self.__get_metric_hostname_clause()
        self.time_clause = self.__get_time_clause()

    def _change_metric(self, metric: str, idle_threshold: float = -1.0) -> None:
        self.metric = metric
        self.metric_def = self.metric_data[metric]
        self.hostnames_clause = self.__get_metric_hostname_clause(
            idle_threshold
        )
        self.time_clause = self.__get_time_clause()

    def _convert_cpulist_to_cpulistdict(self, cpulist: str) -> dict[str, Any]:
        """
        convert cpulist string to python dictionary
        ```
        assert self._convert_cpulist_to_cpulistdict(
            "taurusi6580[12],taurusi6583[0-5]"
        ) == {"taurusi6580": "[12]", "taurusi6583": "[0-5]"}
        ```
        """
        cpulist = cpulist.replace("[", '":"[').replace("],", ']","')
        cpulistdict: dict[str, Any] = json.loads('{"' + cpulist + '"}')
        return cpulistdict

    def __get_pu_clause(self, pulist: list[str], unit_type: str) -> str:
        """
        get processing unit clause
        ```
        assert (
                self.__get_pu_clause(pulist=["14", "15", "20"], unit_type="cpu")
                == "(cpu='14' or cpu='15' or cpu='20')"
            )
        ```
        """
        pu_clause = "("

        for i, pu in enumerate(pulist):
            pu_clause += f"{unit_type}='{pu}'"

            if i < len(pulist) - 1:
                pu_clause += " or "
            else:
                pu_clause += ")"

        return pu_clause

    def _validate_value(self, value: float | None) -> float | None:
        if value is None:
            return None
        # we removed Mega prefix from flops on March 15, 2019
        if self.zoom_start < 1552654800 and self.metric == "flops":
            value *= 1e6
        return round(value, 4)

    def _outliers(self, field: str) -> str:
        # from March 15, 2019 we handle outliers in collectd
        if self.zoom_start < 1552654800:
            if self.metric == "ipc":
                return " and " + str(field) + " < 10"
            elif self.metric == "flops":
                return " and " + str(field) + " < 100000000000"
            elif self.metric == "mem_bw":
                return " and " + str(field) + " < 1000000000000"
            else:
                return ""
        else:
            return ""

    def __get_pu_hostnames_clause(
        self,
        pulist_dict: dict[str, Any],
        unit_type: str = "cpu",
        idle_threshold: float = -1.0,
    ) -> str:
        """
        docu
        ```
        assert (
            self.__get_pu_hostnames_clause(
                pulist_dict={
                    "taurusi6580": "[0-2,12]",
                    "taurusi6583": "[21-22]",
                },
                unit_type="cpu",
                idle_threshold=-1,
            )
            == "((hostname='taurusi6580') and
            (cpu='0' or cpu='1' or cpu='2' or cpu='12') or
            (hostname='taurusi6583') and (cpu='21' or cpu='22'))"
        )
        ```
        """
        metric = "used" if unit_type == "cpu" else "gpu_used"
        hostnames_clause = "("

        for i, node in enumerate(pulist_dict):
            pus = hostlist.expand_hostlist(pulist_dict[node])

            pu_clause = self.__get_pu_clause(pus, unit_type)

            if idle_threshold >= 0:
                hostnames_clause += (
                    f"(hostname='{node}') and {pu_clause}"
                    f"and {metric}<={idle_threshold}"
                )
            else:
                hostnames_clause += f"(hostname='{node}') and {pu_clause}"

            if i < len(pulist_dict) - 1:
                hostnames_clause += " or "
            else:
                hostnames_clause += ")"

        return hostnames_clause

    def __get_pure_hostnames_clause(
        self,
        nodelist: list[str],
        unit_type: str = "cpu",
        idle_threshold: float = -1.0,
    ) -> str:
        """
        docu
        ```
        assert (
            self.__get_pure_hostnames_clause(
                nodelist=["taurusi6580", "taurusi6582"],
                unit_type="cpu",
                idle_threshold=-1,
            )
            == "(hostname='taurusi6580' or hostname='taurusi6582')"
        )
        ```
        """

        metric = "used" if unit_type == "cpu" else "gpu_used"
        hostnames_clause = "("

        for i, node in enumerate(nodelist):
            if idle_threshold >= 0:
                hostnames_clause += (
                    f"(hostname='{node}' and {metric}<={idle_threshold})"
                )
            else:
                hostnames_clause += f"hostname='{node}'"

            if i < len(nodelist) - 1:
                hostnames_clause += " or "
            else:
                hostnames_clause += ")"

        return hostnames_clause

    def __get_socket_cpulist_dict(
        self, cpulist_dict: dict[str, Any], likwid_sockets: list[int]
    ) -> dict[str, Any]:
        """
        retuns a dictionary of used likwid cpu socket
        ```
        assert self.__get_socket_cpulist_dict(
                cpulist_dict={
                    "taurusi6580": "[0-10,12,24]",
                    "taurusi6582": "[14-15,20]",
                },
                likwid_sockets=[0, 24],
            ) == {
                "taurusi6580": "[0,24]",
                "taurusi6582": "[0]",
            }
        ```
        """
        socket_cpulist_dict = {}

        for node, cpus in cpulist_dict.items():
            socket_cpulist = []
            cpulist = hostlist.expand_hostlist(cpus)

            for cpu in cpulist:
                for socket_cpu in reversed(likwid_sockets):
                    if (
                        int(cpu) >= int(socket_cpu)
                        and int(socket_cpu) not in socket_cpulist
                    ):
                        socket_cpulist.append(int(socket_cpu))
                        break

            socket_cpulist.sort()
            socket_cpulist_dict[node] = json.dumps(
                socket_cpulist, separators=(",", ":")
            )

        return socket_cpulist_dict

    def __get_hostnames_clauses(
        self, idle_threshold: float
    ) -> tuple[str, str | None, str, str] | None:
        """docu"""
        cpulist_dict = (
            {}
            if self.exclusive == 1
            else self._convert_cpulist_to_cpulistdict(self.cpulist)
        )
        if self.gpulist:
            gpulist_dict = (
                {}
                if self.exclusive == 1 or self.gpu_count == 0
                else self._convert_cpulist_to_cpulistdict(self.gpulist)
            )
        else:
            gpulist_dict = {}
        nodelist_ex = hostlist.expand_hostlist(self.nodelist)
        pure_hostnames_clause = self.__get_pure_hostnames_clause(
            nodelist_ex, "cpu", idle_threshold
        )

        if self.exclusive != 1:
            if len(nodelist_ex) != len(cpulist_dict):
                logger.info("len(nodelist) != len(cpulist)")
                return None

            cpu_hostnames_clause = self.__get_pu_hostnames_clause(
                cpulist_dict, "cpu", idle_threshold
            )

            if gpulist_dict:
                gpu_hostnames_clause = self.__get_pu_hostnames_clause(
                    gpulist_dict, "gpu", idle_threshold
                )
            else:
                gpu_hostnames_clause = self.__get_pure_hostnames_clause(
                    nodelist_ex, "gpu", idle_threshold
                )

            if not cpu_hostnames_clause:
                logger.info("Could not generate shared hostname clause.")
                return None

            socket_cpulist_dict = self.__get_socket_cpulist_dict(
                cpulist_dict, self.socket_cpu
            )
            try:
                socket_hostnames_clause = self.__get_pu_hostnames_clause(
                    socket_cpulist_dict
                )
            except hostlist.BadHostlist:  # probably julia partition
                socket_hostnames_clause = None

        else:
            cpu_hostnames_clause = pure_hostnames_clause
            socket_hostnames_clause = cpu_hostnames_clause
            gpu_hostnames_clause = self.__get_pure_hostnames_clause(
                nodelist_ex, "gpu", idle_threshold
            )

        return (
            pure_hostnames_clause,
            socket_hostnames_clause,
            cpu_hostnames_clause,
            gpu_hostnames_clause,
        )

    def __get_metric_hostname_clause(self, idle_threshold: float = -1.0) -> str:
        hostname_clause_tuple = self.__get_hostnames_clauses(idle_threshold)

        if hostname_clause_tuple:
            (
                pure_hostnames_clause,
                socket_hostnames_clause,
                cpu_hostnames_clause,
                gpu_hostnames_clause,
            ) = hostname_clause_tuple
        else:
            logger.info(
                "Could not generate host name clauses",
                (
                    self.nodelist,
                    self.cpulist,
                    self.gpulist,
                    self.socket_cpu,
                    self.exclusive,
                ),
            )

        scope = self.metric_def["scope"]

        # Determine hostnames_clause based on scope and metric
        match scope:
            case "core":
                hostnames_clause = cpu_hostnames_clause
            case "socket":
                if socket_hostnames_clause is None:
                    return ""
                hostnames_clause = socket_hostnames_clause
            case _:
                hostnames_clause = pure_hostnames_clause

        if "gpu" in self.metric:
            hostnames_clause = gpu_hostnames_clause

        return hostnames_clause

    def __get_time_clause(self) -> str:
        return f" and time >= {self.zoom_start}s and time <= {self.zoom_end}s"


class TimelineQueryBuilder(MetricQueryBuilder):
    """child class building the influx queries for a timeline graphic"""

    def __init__(
        self,
        job: TimelineORM,
        metric: TimelineMetric,
        params: TimelineParameter,
        spec: Specification,
    ):
        super().__init__(job, metric, params, spec)
        self.lowest = ""
        self.best = ""

    def __aggregation_width(self) -> int:
        aggregation_width = (
            30  # int(self.metric_def["timestep"]) use always smallest timestep
        )
        width = aggregation_width

        if self.auto_aggregation is False:
            return width

        delta = self.zoom_end - self.zoom_start

        if aggregation_width == 30:
            aggregation_start = (
                3600  # start aggreagtion if delta greater than 1 hour
            )
        if aggregation_width == 60:
            aggregation_start = (
                7200  # start aggreagtion if delta greater than 2 hour2
            )

        if delta > aggregation_start:
            max_points = 120
            width = (
                math.floor(math.floor(delta / max_points) / aggregation_width)
                * aggregation_width
            )
        return width

    def __get_best_lowest_queries(self, results: ResultSet) -> list[str]:
        if "series" in results[0]["results"][0]:
            result = results[0]["results"][0]["series"]
            mean_values = {}
            cnt = 0
            for val in result:
                cnt += 1
                mean_values[cnt] = [val["tags"], val["values"][0][1]]
            # sort dict by values --> result is a tuple
            mean_values = sorted(mean_values.items(), key=lambda x: x[1][1])  # type: ignore
            lowest = mean_values[0][1]  # content of first tuple
            best = mean_values[-1][1]  # content of last tuple

            # create lables for lowest and best unit
            self.lowest = lowest[0]["hostname"]
            self.best = best[0]["hostname"]
            scope = self.metric_def["scope"]
            if scope != "node":
                tag_unit = "cpu"
                if "gpu" in self.metric:
                    tag_unit = "gpu"
                self.lowest += ":" + tag_unit + lowest[0][tag_unit]
                self.best += ":" + tag_unit + best[0][tag_unit]

            # setup queries
            query_strs = []

            dbconf = self.metric_def["dbconf"][0]
            measurement = dbconf["measurement"]
            if "measurement_suffix" in dbconf:
                partition_name = self.partition_name.split("-")[0]
                measurement = f"{measurement}{dbconf["measurement_suffix"][partition_name]}"
            field = dbconf["field"]

            if self.mean_line is True:
                query_str = (
                    'SELECT mean("' + str(field) + '") as mean_line '
                    'FROM "'
                    + str(measurement)
                    + '" WHERE '
                    + self.hostnames_clause
                    + self._outliers(field)
                    + self.time_clause
                )
                query_strs.append(query_str)

            mapping = {"lowest": lowest, "best": best}
            for unit in ["lowest", "best"]:
                hostname_clause = (
                    "(hostname='" + mapping[unit][0]["hostname"] + "'"
                )
                if scope != "node":
                    hostname_clause += (
                        " and "
                        + tag_unit
                        + "='"
                        + mapping[unit][0][tag_unit]
                        + "'"
                    )
                hostname_clause += ")"
                query_strs.append(
                    'SELECT mean("' + str(field) + '") as ' + unit + " "
                    'FROM "'
                    + str(measurement)
                    + '" WHERE '
                    + hostname_clause
                    + self._outliers(field)
                    + self.time_clause
                    + " GROUP BY time("
                    + str(self.__aggregation_width())
                    + "s) fill(null)"
                )

            # print(query_strs)
            return query_strs
        return []

    def get_queries_again(self, previous_results: ResultSet) -> list[str]:
        """generate best and lowest metrics"""
        if len(previous_results) > 0 and self.timeline_type in [
            TimelineType.best,
            TimelineType.lowest,
            TimelineType.best_lowest,
        ]:
            return self.__get_best_lowest_queries(previous_results)
        return []

    def get_queries(self) -> list[str]:
        """return the influx queries"""
        query_strs = []
        dbconf = self.metric_def["dbconf"]

        for timeline in dbconf:
            measurement = timeline["measurement"]
            if "measurement_suffix" in timeline:
                partition_name = self.partition_name.split("-")[0]
                measurement = f"{measurement}{timeline["measurement_suffix"][partition_name]}"
            field = timeline["field"]

            timeline_type = self.timeline_type

            # setup mean line query if required
            if self.mean_line is True and timeline_type not in (
                TimelineType.best,
                TimelineType.lowest,
                TimelineType.best_lowest,
                TimelineType.histogram,
            ):
                query_str = (
                    'SELECT mean("' + str(field) + '") as mean_line '
                    'FROM "'
                    + str(measurement)
                    + '" WHERE '
                    + self.hostnames_clause
                    + self._outliers(field)
                    + self.time_clause
                )
                query_strs.append(query_str)

            query_end = (
                ' FROM "'
                + str(measurement)
                + '" WHERE '
                + self.hostnames_clause
                + self._outliers(field)
                + self.time_clause
                + " GROUP BY time("
                + str(self.__aggregation_width())
                + "s) fill(null)"
            )

            match timeline_type:
                case TimelineType.aggregated:
                    # query_str = (
                    #     "SELECT "
                    #     + "sum"
                    #     + '("'
                    #     + str(field)
                    #     + '") as '
                    #     + timeline["name"]
                    #     + query_end
                    # )

                    # special treatment for aggregated timelines as interpolation for the query
                    # above does not work for long timelines greater than 1 hour, because all values
                    # within an interval are summed up
                    # we use a subquery for explicit filtering
                    query_str = f"""
                        SELECT max(sum_value) AS {timeline["name"]} FROM 
                        (SELECT sum("{field}") AS sum_value FROM "{measurement}" WHERE
                        {self.hostnames_clause} {self._outliers(field)} {self.time_clause}
                        GROUP BY time({self.metric_def["timestep"]}s) fill(null))
                        GROUP BY time({self.__aggregation_width()}s) fill(none)
                    """
                case TimelineType.min | TimelineType.mean | TimelineType.max:
                    query_str = (
                        "SELECT "
                        + str(timeline_type.value)
                        + '("'
                        + str(field)
                        + '") as '
                        + timeline["name"]
                        + query_end
                    )
                case TimelineType.min_mean_max:
                    query_str = (
                        "SELECT "
                        'min("' + str(field) + '") as min, '
                        'mean("' + str(field) + '") as mean, '
                        'max("' + str(field) + '") as max ' + query_end
                    )
                case TimelineType.meanstddev:
                    query_str = (
                        "SELECT "
                        'mean("'
                        + str(field)
                        + '") as '
                        + timeline["name"]
                        + ", "
                        'stddev("' + str(field) + '") as stddev ' + query_end
                    )
                case (
                    TimelineType.best
                    | TimelineType.lowest
                    | TimelineType.best_lowest
                    | TimelineType.histogram
                ):
                    # requires second query for the best or lowest timeline vector
                    scope = self.metric_def["scope"]
                    group_by_tags = "GROUP BY hostname"
                    timestep = self.metric_def["timestep"]
                    inner_group_by = (
                        f"GROUP BY time({timestep}s), hostname fill(0.0)"
                    )
                    if scope != "node":
                        tag_unit = "cpu"
                        if "gpu" in self.metric:
                            tag_unit = "gpu"
                        group_by_tags = "GROUP BY hostname," + str(tag_unit)
                        inner_group_by = f"GROUP BY time({timestep}s), hostname, {tag_unit} fill(0.0)"

                    time_clause = self.time_clause
                    if timeline_type == TimelineType.histogram:
                        time_clause = (
                            " and time >= "
                            + str(
                                self.zoom_start - self.__aggregation_width() - 1
                            )
                            + "s and time <= "
                            + str(
                                self.zoom_end + self.__aggregation_width() - 1
                            )
                            + "s"
                        )

                    query_str = f"""
                        SELECT mean("{field}") as mean_{field}
                        FROM (SELECT mean("{field}") as {field} FROM
                        {measurement} WHERE
                        {self.hostnames_clause}
                        {self._outliers(field)}
                        {time_clause} {inner_group_by})
                        {group_by_tags}
                    """
                    query_str = " ".join(query_str.split())
                    # print(query_str)

            query_strs.append(query_str)

        return query_strs

    def __format_histogram_result(self, results: ResultSet) -> dict[str, Any]:
        if "series" in results[0]["results"][0]:
            result = results[0]["results"][0]["series"]
            x = []
            for val in result:
                x.append(val["values"][0][1])
            x.sort()

            # fill with zeros if scope is core and x < num_cores
            if self.metric_def["scope"] == "core" and len(x) < self.num_cores:
                x += [0] * (self.num_cores - len(x))

            min = math.floor(x[0])
            max = None
            bin_size = None

            if self.metric == "cpu_usage" or self.metric == "gpu_usage":
                min = 0
                max = 1
                bin_size = 0.1

            return {
                "x": x,
                "min": min,
                "max": max,
                "bin_size": bin_size,
                "unit": self.metric_def["unit"],
            }
        return {}

    def format_query_result(
        self,
        results: ResultSet,
    ) -> dict[str, Any]:
        if self.timeline_type == TimelineType.histogram:
            return self.__format_histogram_result(results)

        formatted_result = {}
        init_timestamps = False
        init_metric_head = False
        mean_line_value = 0

        for result in results:
            timestamps = []
            values: list[Any] = []
            value_types = []

            if "series" in result["results"][0]:
                data = result["results"][0]["series"][0]
                column_num = (
                    len(data["columns"]) - 1
                )  # we skip the timestamps column
                for i in range(column_num):
                    values.append([])
                    value_types.append(data["columns"][i + 1])

                item: list[Any]
                if self.mean_line is True and "mean_line" in value_types:
                    for item in iterpoints(result):
                        mean_line_value = item[1]
                    continue

                for item in iterpoints(result):
                    if init_timestamps is False:
                        timestamps.append(item[0] / 1e9)
                    for j in range(column_num):
                        # print(i, j)
                        values[j].append(self._validate_value(item[j + 1]))
                init_timestamps = True

                if init_metric_head is False:
                    formatted_result = {
                        "unit": self.metric_def["unit"],
                        "timestamps": timestamps,
                    }
                    init_metric_head = True

                if (
                    self.timeline_type == TimelineType.lowest
                    or self.timeline_type == TimelineType.best
                ) and self.timeline_type.value != value_types[0]:
                    # we provide either lowest or best, not both
                    continue

                for j in range(column_num):
                    if value_types[j] == "stddev":
                        continue
                    # do not provide IO metrics where all values are zero
                    if (
                        "io_" in self.metric
                        and sum([0 if v is None else v for v in values[j]]) == 0
                    ):
                        continue
                    formatted_result[value_types[j]] = []
                    formatted_result[value_types[j]].append(values[j])
                    formatted_result[value_types[j]].append(
                        {
                            "mean": mean_line_value,
                            "best_node": self.best,
                            "lowest_node": self.lowest,
                        }
                    )

                    if self.timeline_type == TimelineType.meanstddev:
                        formatted_result[value_types[j]].append(values[j + 1])

        return formatted_result


class DownloadMetricQueryBuilder(MetricQueryBuilder):
    """influx query builder for restapi download feature"""

    def __init__(
        self,
        job_detail: TimelineORM,
        metric: TimelineMetric,
        params: TimelineParameter,
        spec: Specification,
    ):
        super().__init__(job_detail, metric, params, spec)

        self.metric_data = spec.metric

    def get_statistic_queries(self) -> str:
        dbconf = self.metric_data[self.metric]["dbconf"][0]
        # print(dbconf['field'], dbconf['measurement'], dbconf['tags'])
        query_str = (
            "SELECT "
            'min("' + str(dbconf["field"]) + '") as min, '
            'mean("' + str(dbconf["field"]) + '") as mean, '
            'max("' + str(dbconf["field"]) + '") as max '
            'FROM "'
            + str(dbconf["measurement"])
            + '" WHERE '
            + self.hostnames_clause
            + self._outliers(dbconf["field"])
            + self.time_clause
        )
        return query_str

    def get_detail_timeline_query(self) -> str:
        dbconf = self.metric_data[self.metric]["dbconf"][0]
        # print(dbconf['field'], dbconf['measurement'], dbconf['tags'])
        query_str = (
            "SELECT " + str(dbconf["field"]) + " as " + str(self.metric) + " "
            'FROM "'
            + str(dbconf["measurement"])
            + '" WHERE '
            + self.hostnames_clause
            + self._outliers(dbconf["field"])
            + self.time_clause
            + " "
            "GROUP BY " + str(dbconf["tags"])
        )
        return query_str


class FootprintQueryBuilder(MetricQueryBuilder):
    """influx queries for post processing"""

    def __init__(
        self,
        job_detail: TimelineORM,
        metric: TimelineMetric,
        params: TimelineParameter,
        spec: Specification,
    ):
        super().__init__(job_detail, metric, params, spec)

        self.metric_data = spec.metric

    def get_footprint_query(self, label: str, aggregation: str) -> str:
        """retung generated influx queries"""
        dbconf = self.metric_data[self.metric]["dbconf"][0]

        query_str = f"""
            SELECT {aggregation}("{dbconf['field']}") as {label}
            FROM "{dbconf['measurement']}"
            WHERE {self.hostnames_clause}
            {self._outliers(dbconf['field'])}
            {self.time_clause}
        """

        return query_str


class IssueQueryBuilder(MetricQueryBuilder):
    def __init__(
        self,
        job_detail: TimelineORM,
        metric: TimelineMetric,
        params: TimelineParameter,
        spec: Specification,
    ):
        super().__init__(job_detail, metric, params, spec)

        self.metric_data = spec.metric

    def get_detail_aggregated_timeline_query(
        self, aggregation: str = "mean"
    ) -> str:
        dbconf = self.metric_data[self.metric]["dbconf"][0]
        # print(dbconf['field'], dbconf['measurement'], dbconf['tags'])
        measurement_interval = self.metric_data[self.metric]["timestep"]
        query_str = f"""
            SELECT {aggregation}("{dbconf["field"]}") as {self.metric}
            FROM {dbconf["measurement"]}
            WHERE {self.hostnames_clause}
            {self._outliers(dbconf["field"])}
            {self.time_clause}
            GROUP BY time({measurement_interval}s) fill(null)
        """
        return query_str

    def get_pu_stat_query(self, unit_type: str) -> str:
        dbconf = self.metric_data[self.metric]["dbconf"][0]

        query_str = f"""
            SELECT count("{dbconf['field']}") as idle_num
            FROM "{dbconf['measurement']}"
            WHERE {self.hostnames_clause}
            {self._outliers(dbconf['field'])}
            {self.time_clause}
            GROUP BY hostname, {unit_type}
        """

        return query_str
