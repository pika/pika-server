"""containers needed for influx reasons"""

from enum import Enum

from pydantic import BaseModel


class InfluxError(Exception):
    """represent a error, which happend in this influx package"""


class TimelineMetric(str, Enum):
    lustre_io = "lustre_io"
    lustre_io_meta = "lustre_io_meta"
    horse_read_bw = "horse_read_bw"
    horse_write_bw = "horse_write_bw"
    octopus_read_bw = "octopus_read_bw"
    octopus_write_bw = "octopus_write_bw"
    scratch_read_bw = "scratch_read_bw"
    scratch_write_bw = "scratch_write_bw"
    highiops_read_bw = "highiops_read_bw"
    highiops_write_bw = "highiops_write_bw"
    horse_read_requests = "horse_read_requests"
    horse_write_requests = "horse_write_requests"
    horse_open = "horse_open"
    horse_close = "horse_close"
    horse_fsync = "horse_fsync"
    horse_create = "horse_create"
    horse_seek = "horse_seek"
    octopus_read_requests = "octopus_read_requests"
    octopus_write_requests = "octopus_write_requests"
    octopus_open = "octopus_open"
    octopus_close = "octopus_close"
    octopus_fsync = "octopus_fsync"
    octopus_create = "octopus_create"
    octopus_seek = "octopus_seek"
    scratch_read_requests = "scratch_read_requests"
    scratch_write_requests = "scratch_write_requests"
    scratch_open = "scratch_open"
    scratch_close = "scratch_close"
    scratch_fsync = "scratch_fsync"
    scratch_create = "scratch_create"
    scratch_seek = "scratch_seek"
    highiops_read_requests = "highiops_read_requests"
    highiops_write_requests = "highiops_write_requests"
    highiops_open = "highiops_open"
    highiops_close = "highiops_close"
    highiops_fsync = "highiops_fsync"
    highiops_create = "highiops_create"
    highiops_seek = "highiops_seek"
    local_io = "local_io"
    read_bw = "read_bw"
    write_bw = "write_bw"
    local_io_meta = "local_io_meta"
    read_ops = "read_ops"
    write_ops = "write_ops"
    cpu_usage = "cpu_usage"
    mem_used = "mem_used"
    ipc = "ipc"
    flops = "flops"
    mem_bw = "mem_bw"
    infiniband_bw = "infiniband_bw"
    gpu_usage = "gpu_usage"
    gpu_power = "gpu_power"
    gpu_mem = "gpu_mem"
    gpu_temperature = "gpu_temperature"
    cpu_power = "cpu_power"
    cpi = "cpi"
    job_mem_used = "job_mem_used"
    ethernet_bw = "ethernet_bw"


class TimelineType(str, Enum):
    """specifys what graph should be drawn on the timeline"""

    aggregated = "aggregated"
    mean = "mean"
    min = "min"
    max = "max"
    min_mean_max = "min_mean_max"
    meanstddev = "meanstddev"
    best = "best"
    lowest = "lowest"
    best_lowest = "best_lowest"
    histogram = "histogram"


class TimelineParameter(BaseModel):
    """where clause for the influx query ?"""

    start: int | None = None
    end: int | None = None
    timeline_type: TimelineType | None = None
    auto_aggregation: bool | None = None
    mean_line: bool | None = None
