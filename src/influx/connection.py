"""query the influx database"""

import asyncio
from typing import Dict, List

from aioinflux import InfluxDBClient as AsyncInfluxDBClient
from influxdb import InfluxDBClient
from influxdb.exceptions import InfluxDBClientError
from influxdb.resultset import ResultSet

from utils.model import DBCredential

from .model import InfluxError


class AsyncInfluxConnection:
    """https://aioinflux.readthedocs.io/en/stable/usage.html"""

    def __init__(self, cred: DBCredential) -> None:
        # aioinflux doesnt seem to use a connection pool, thats why every
        # query is building a connection by itself

        self._cred = cred

    async def execute_queries(self, influx_queries: List[str]) -> list:
        """https://aioinflux.readthedocs.io/en/stable/usage.html

        Raising:
            aiohttp.client_exceptions.ClientConnectionError: connection problems
            aioinflux.client.InfluxDBError: invalid queries
        """

        async with AsyncInfluxDBClient(
            host=self._cred.hostname,
            port=self._cred.port,
            username=self._cred.user,
            password=self._cred.password,
            database=self._cred.database,
        ) as connection:
            coroutines = [connection.query(q) for q in influx_queries]
            result = await asyncio.gather(*coroutines)
            # execute many queries concurrently

        return result


class InfluxConnection:
    """Synchronous Influx querying"""

    def __init__(self, cred: DBCredential) -> None:
        self._connection = InfluxDBClient(
            host=cred.hostname,
            port=cred.port,
            username=cred.user,
            password=cred.password,
            database=cred.database,
        )

    def execute_query(self, influx_query: str) -> ResultSet:
        try:
            result_set: ResultSet = self._connection.query(
                influx_query, epoch="s"
            )
        except InfluxDBClientError as error:
            raise InfluxError(error)

        return result_set

    def expect_one_result(self, influx_query: str) -> Dict[str, float]:
        """executes an influx query, expects one result e.g. {"cpu": 0.23}

        Raising:
            influx.connection.InfluxError: invalid syntax of influx query
            (e.g. select None)
            AttributeError: invalid format of influx query
            (e.g. got list instead of str)
        """

        try:
            result_set: ResultSet = self._connection.query(influx_query)
        except InfluxDBClientError as error:
            raise InfluxError(error) from error

        return self._format_one_result(result_set)

    def _format_one_result(self, result: ResultSet) -> dict:
        """
        assert self._format_one_result(
            ResultSet(
                {
                    '("cpu", None)': [
                        {
                            "time": "2023-07-25T11:28:19Z",
                            "cpu_used_mean_per_core": 0.1251877184097681,
                        }
                    ]
                }
            )
        ) == {"cpu_used_mean_per_core": 0.0023184236814114546}
        """

        for r in result.get_points():
            # will be only one iteration, get_points() is returning a generator
            del r["time"]
            return r
        return {}

    def __del__(self) -> None:
        self._connection.close()
