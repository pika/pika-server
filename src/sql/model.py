"""
defining:
- sqlalchemy python orms, related to their corresponding pika sql database table
- sql filtering
- python exception, being raised from actions in this sql package
"""

from pydantic.main import BaseModel
from sqlalchemy import Label  # https://github.com/tiangolo/sqlmodel/issues/290
from sqlalchemy.orm import Query
from sqlmodel import Field, SQLModel, and_, case, func, literal_column, or_


class JobDataORM(SQLModel, table=True):
    """python orm of the `job_data` table in the sql database, most queries
    use this table, as it contains the most information about jobs that are
    executed on Taurus.

    Args:
        SQLModel (SQLModel): make this class a sqlalchemy and pydantic class
        table (bool): `True`: 1:1 python orm sql db representation

    |column|meaning|
    |------|-------|
    |uid|pika internal id, which exists to join the jobdata with its metrics located in the footprint tables|
    |job_id|id of the job (every job is supposed to have its own id, but it is not)|
    |user_name|name of the user, which executed this job|
    |project_name|name of the project, the job was executed in|
    |job_state|status of the job: `running`, `completed`, `out of memory` ...|
    |node_count|number of nodes the job was run on|
    |core_list|a string name list, containing all cpus the job has run on|
    |core_count|number of cpu cores the job was run on|
    |submit_time|unix timestamp when to job was queried for start|
    |start_time|unix timestamp when the job actualy started to execute|
    |end_time|unix timestamp when the job was terminated|
    |job_name|name of the job (many jobs can have the same name)|
    |time_limit|the time span for the job to run specified in the batch slurm script|
    |partition_name|slurm partition, the job was run on|
    |exclusive|`1` job was exclusive, `0` job wasnt exclusive|
    |property_id|pika internal id, for joining jobdata with property table (random notes)|
    |array_id|?|
    |tags|?|
    |footprint_created|`1` the job has performance metrics in the pikas footprint tables, `0` no metrics|
    |gpu_count|number of gpus, used by the job|
    |gpu_list|a string name list, on which gpus the job was run on|
    |smt_mode|?|
    |job_script|the complete slurm batch script, which started the job|
    |mem_per_cpu|how much memory is allocated per cpu|
    |mem_per_node|how much memory is allocated per node|
    """

    __tablename__ = "job_data"

    uid: int = Field(primary_key=True)
    job_id: int | None
    user_name: str | None
    project_name: str | None
    job_state: str | None
    node_count: int | None
    node_list: str | None
    core_list: str | None
    core_count: int | None
    submit_time: int | None
    start_time: int | None
    end_time: int | None
    job_name: str | None
    time_limit: int | None
    partition_name: str | None
    exclusive: int | None
    property_id: int | None
    array_id: int | None
    tags: int | None
    footprint_created: int | None
    gpu_count: int | None
    gpu_list: str | None
    smt_mode: int | None
    job_script: str | None
    mem_per_cpu: int | None
    mem_per_node: int | None
    reservation_id: int | None

    @classmethod
    def job_number(cls) -> Label[int]:
        """`count(job_id) as job_count`

        Returns:
            `sqlalchemy impl detail`: part of the sql select clause
        """
        return func.count(cls.job_id).label("job_count")

    @classmethod
    def max_nodes(cls) -> Label[int]:
        """`max(node_count) as max_node_count`

        Returns:
            `sqlalchemy impl detail`: part of the sql select clause
        """
        return func.max(cls.node_count).label("max_node_count")

    @classmethod
    def max_cores(cls) -> Label[int]:
        """`max(core_count) as max_core_count`

        Returns:
            `sqlalchemy impl detail`: part of the sql select clause
        """
        return func.max(cls.core_count).label("max_core_count")

    @classmethod
    def max_pending_time(cls) -> Label[int]:
        """`max(start_time-submit_time) as max_pending_time`

        Returns:
            `sqlalchemy impl detail`: part of the sql select clause
        """
        return func.max(cls.start_time - cls.submit_time).label(
            "max_pending_time"
        )

    @classmethod
    def sum_duration(cls) -> Label[int]:
        """`sum(end_time-start_time) as sum_duration`

        Returns:
            `sqlalchemy impl detail`: part of the sql select clause
        """
        return func.sum(cls.end_time - cls.start_time).label("sum_duration")

    @classmethod
    def sum_core_duration(cls) -> Label[int]:
        """`sum(core_count*(end_time-start_time)) as sum_core_duration`

        only makes sense when grouping by `column`

        Returns:
            `sqlalchemy impl detail`: part of the sql select clause
        """
        return func.sum(cls.core_count * (cls.end_time - cls.start_time)).label(
            "sum_core_duration"
        )

    @classmethod
    def sum_footprints(cls) -> Label[int]:
        """`sum(footprint_created) as footprint_count`

        only makes sense when grouping by `<column>`

        Returns:
            `sqlalchemy impl detail`: part of the sql select clause
        """

        return case(
            (func.sum(cls.footprint_created).is_(None), 0),
            (func.sum(cls.footprint_created) <= -1, 0),
            else_=func.sum(cls.footprint_created),
        ).label("footprint_count")

    @classmethod
    def num_footprints(cls) -> Label[int]:
        """`count(footprint_created) as footprint_count`

        Returns:
            `sqlalchemy impl detail`: part of the sql select clause
        """
        return func.count(cls.footprint_created).label("footprint_count")

    @classmethod
    def pending_time(cls) -> Label[int]:
        """`(start_time-submit_time) as pending_time`

        Returns:
            `sqlalchemy impl detail`: part of the sql select clause
        """
        return case(
            (cls.start_time - cls.submit_time is None, 0),
            else_=cls.start_time - cls.submit_time,
        ).label("pending_time")

    @classmethod
    def duration(cls) -> Label[int]:
        """`(end_time-start_time) as job_duration`

        Returns:
            `sqlalchemy impl detail`: part of the sql select clause
        """
        return (cls.end_time - cls.start_time).label("job_duration")

    @classmethod
    def core_duration(cls) -> Label[int]:
        """`(core_count*(end_time-start_time)) as core_duration`

        Returns:
           `sqlalchemy impl detail`: part of the sql select clause
        """
        return (cls.core_count * (cls.end_time - cls.start_time)).label(
            "core_duration"
        )

    @classmethod
    def num_status(cls) -> Label[int]:
        """`count(job_state) as footprint_1`

        the naming is weird because the frontend wants the values as it wants it

        Returns:
           `sqlalchemy impl detail`: part of the sql select clause
        """
        return func.count(cls.job_state).label("footprint_1")

    @classmethod
    def num_tags(cls) -> Label[int]:
        """`count(tags) as footprint_1`

        Returns:
           `sqlalchemy impl detail`: part of the sql select clause
        """
        return func.count(cls.tags).label("footprint_1")

    @classmethod
    def max_time_limit(cls) -> Label[int]:
        """`max(time_limit) as max_time_limit`

        Returns:
           `sqlalchemy impl detail`: part of the sql select clause
        """
        return func.max(cls.time_limit).label("max_time_limit")

    @classmethod
    def issue_runs(cls) -> Label[int]:
        """`count(job_id) as issue_runs`

        Returns:
           `sqlalchemy impl detail`: part of the sql select clause
        """
        return func.count(cls.job_id).label("issue_runs")

    @classmethod
    def get_gpu_count(cls) -> Label[int]:
        """a wrapper around gpu_count which returns 0 when no gpu was used
        instead of the default sql `NULL` value

        Returns:
           `sqlalchemy impl detail`: part of the sql select clause
        """
        return case((cls.gpu_count is None, 0), else_=cls.gpu_count).label(
            "gpu_count"
        )

    @classmethod
    def max_gpu_count(cls) -> Label[int]:
        """`max(get_gpu_count()) as max_gpu_count`

        Returns:
            `sqlalchemy impl detail`: part of the sql select clause
        """
        return func.max(cls.get_gpu_count()).label("max_gpu_count")

    @classmethod
    def get_smt_mode(cls):
        """a wrapper around smt_mode witch return 1 instead of default sql `NULL`

        Returns:
           `sqlalchemy impl detail`: part of the sql select clause
        """
        return case((cls.smt_mode is None, 1), else_=cls.smt_mode).label(
            "smt_mode"
        )

    @classmethod
    def sum_gpu_duration(cls) -> Label[int]:
        """`sum(gpu_count*(end_time-start_time)) as sum_gpu_duration`

        Returns:
           `sqlalchemy impl detail`: part of the sql select clause
        """
        return func.sum(
            cls.get_gpu_count() * (cls.end_time - cls.start_time)
        ).label("sum_gpu_duration")

    @classmethod
    def gpu_duration(cls) -> Label[int]:
        """`(gpu_count*(end_time-start_time)) as gpu_duration`

        Returns:
           `sqlalchemy impl detail`: part of the sql select clause
        """

        return (cls.get_gpu_count() * (cls.end_time - cls.start_time)).label(
            "gpu_duration"
        )


class FootprintAnalysisORM(SQLModel, table=True):
    """python orm of the `footprint_analysis` sql database table, this table
    holds information about how bad a jobs performance usage was
    (bad performance usage means: not using reserved resources)

    Args:
        SQLModel (SQLModel): make this class a sqlalchemy and pydantic class
        table (bool): `True`: 1:1 python orm sql db representation
    """

    __tablename__ = "footprint_analysis"

    uid: int = Field(primary_key=True)
    core_idle_ratio: float | None
    core_load_imbalance: float | None
    core_duration_idle: int | None
    io_blocking: int | None
    mem_leak: float | None
    gpu_idle_ratio: float | None
    gpu_load_imbalance: float | None
    gpu_duration_idle: int | None
    synchronous_offloading: int | None
    io_blocking_detail: str | None
    io_congestion: int | None
    severity: int | None
    unused_mem: float | None

    @classmethod
    def sum_core_duration_idle(cls) -> Label[int]:
        """`sum(core_duration_idle) as sum_core_duration_idle`

        Returns:
            `sqlalchemy impl detail`: part of the sql select statement
        """
        return func.sum(cls.core_duration_idle).label("sum_core_duration_idle")

    @classmethod
    def core_unused_ratio(cls) -> Label[float]:
        """`core_idle_ratio as core_unused_ratio`

        Returns:
             `sqlalchemy impl detail`: part of the sql select statement
        """
        return cls.core_idle_ratio.label("core_unused_ratio")

    @classmethod
    def gpu_unused_ratio(cls) -> Label[float]:
        """`gpu_idle_ratio as gpu_unused_ratio`

        Returns:
            `sqlalchemy impl detail`: part of the sql select statement
        """
        return cls.gpu_idle_ratio.label("gpu_unused_ratio")

    @classmethod
    def max_core_unused_ratio(cls) -> Label[float]:
        """`max(core_idle_ratio) as max_core_unused_ratio`

        Returns:
            `sqlalchemy impl detail`: part of the sql select statement
        """
        return func.max(cls.core_idle_ratio).label("max_core_unused_ratio")

    @classmethod
    def max_core_load_imbalance(cls) -> Label[float]:
        """`max(core_load_imbalance) as max_core_load_imbalance`

        Returns:
           `sqlalchemy impl detail`: part of the sql select statement
        """
        return func.max(cls.core_load_imbalance).label(
            "max_core_load_imbalance"
        )

    @classmethod
    def max_io_blocking(cls) -> Label[int]:
        """`max(io_blocking) as max_io_blocking`

        Returns:
           `sqlalchemy impl detail`: part of the sql select statement
        """
        return func.max(cls.io_blocking).label("max_io_blocking")

    @classmethod
    def max_io_congestion(cls) -> Label[int]:
        """`max(io_congestion) as max_io_congestion`

        Returns:
            `sqlalchemy impl detail`: part of the sql select statement
        """
        return func.max(cls.io_congestion).label("max_io_congestion")

    @classmethod
    def max_mem_leak(cls) -> Label[float]:
        """`max(mem_leak) as max_mem_leak`

        Returns:
            `sqlalchemy impl detail`: part of the sql select statement
        """
        return func.max(cls.mem_leak).label("max_mem_leak")

    @classmethod
    def sum_gpu_duration_idle(cls) -> Label[int]:
        """`sum(gpu_duration_idle) as sum_gpu_duration_idle`

        Returns:
            `sqlalchemy impl detail`: part of the sql select statement
        """
        return func.sum(cls.gpu_duration_idle).label("sum_gpu_duration_idle")

    @classmethod
    def max_gpu_unused_ratio(cls) -> Label[float]:
        """`max(gpu_idle_ratio) as max_gpu_unused_ratio`

        Returns:
            `sqlalchemy impl detail`: part of the sql select statement
        """
        return func.max(cls.gpu_idle_ratio).label("max_gpu_unused_ratio")

    @classmethod
    def max_gpu_load_imbalance(cls) -> Label[float]:
        """`max(gpu_load_imbalance) as max_gpu_load_imbalance`

        Returns:
            `sqlalchemy impl detail`: part of the sql select statement
        """
        return func.max(cls.gpu_load_imbalance).label("max_gpu_load_imbalance")

    @classmethod
    def max_synchronous_offloading(cls) -> Label[int]:
        """`max(synchronous_offloading) as max_synchronous_offloading`

        Returns:
            `sqlalchemy impl detail`: part of the sql select statement
        """
        return func.max(cls.synchronous_offloading).label(
            "max_synchronous_offloading"
        )

    @classmethod
    def max_severity(cls) -> Label[int]:
        """`max(severity) as max_severity`

        Returns:
            `sqlalchemy impl detail`: part of the sql select statement
        """
        return func.max(cls.severity).label("max_severity")

    @classmethod
    def max_unused_mem(cls) -> Label[float]:
        """`max(unused_mem) as max_unused_mem`

        Returns:
            `sqlalchemy impl detail`: part of the sql select statement
        """
        return func.max(cls.unused_mem).label("max_unused_mem")


class FootprintBaseORM(SQLModel, table=True):
    """python orm of the `footprint_base` sql db table, this table holds
    information about "common" performance metrics

    Args:
        SQLModel (SQLModel): make this class a sqlalchemy and pydantic class
        table (bool): `True`: 1:1 python orm sql db representation
    """

    __tablename__ = "footprint_base"

    uid: int = Field(primary_key=True)
    ipc_mean_per_core: float | None
    cpu_used_mean_per_core: float | None
    flops_any_mean_per_core: float | None
    mem_bw_mean_per_socket: float | None
    cpu_power_mean_per_socket: float | None
    ib_bw_mean_per_node: float | None
    host_mem_used_max_per_node: float | None
    energy_in_kwh: float | None
    energy_efficiency_in_gflops_per_watt: float | None


class FootprintFileioORM(SQLModel, table=True):
    """python orm of the `footprint_fileio` sql db table, this table holds
    perfomance metrics about input/output

    Args:
        SQLModel (SQLModel): make this class a sqlalchemy and pydantic class
        table (bool): `True`: 1:1 python orm sql db representation
    """

    __tablename__ = "footprint_fileio"

    uid: int = Field(primary_key=True)
    read_bytes: int | None
    write_bytes: int | None
    fsname: str | None


class FootprintGpuORM(SQLModel, table=True):
    """python orm of the `footprint_gpu` sql db table, which holds performance
    metrics about gpu

    Args:
       SQLModel (SQLModel): make this class a sqlalchemy and pydantic class
       table (bool): `True`: 1:1 python orm sql db representation
    """

    __tablename__ = "footprint_gpu"

    uid: int = Field(primary_key=True)
    used_mean_per_gpu: float | None
    mem_used_max_per_gpu: float | None
    power_mean_per_gpu: float | None


class NodeORM(SQLModel, table=True):
    """python orm of the `node_info` sql db table.

    Args:
       SQLModel (SQLModel): make this class a sqlalchemy and pydantic class
       table (bool): `True`: 1:1 python orm sql db representation
    """

    __tablename__ = "node_info"
    sample_time: int = Field(primary_key=True)
    partition_name: str = Field(primary_key=True)
    node_name: str = Field(primary_key=True)
    node_state: str | None
    alloc_cpus: int | None
    idle_cpus: int | None
    cpu_load: int | None
    alloc_gpus: int | None
    gres_used: str | None
    free_mem: int | None
    alloc_mem: int | None
    tmp_disk: int | None
    reason: str | None
    reservation_id: int | None

    @classmethod
    def max_sample_time(cls) -> Label[int]:
        """`max(sample_time) as max-sample_time`

        Returns:
            sqlalchemy impl detail: part of the sql select clause
        """
        return func.max(cls.sample_time).label("max_sample_time")


class ReservationORM(SQLModel, table=True):
    """python orm of the `reservation` sql db table.

    Args:
       SQLModel (SQLModel): make this class a sqlalchemy and pydantic class
       table (bool): `True`: 1:1 python orm sql db representation
    """

    __tablename__ = "reservation"
    id: int = Field(primary_key=True)
    reservation_name: str | None
    start_time: int | None
    end_time: int | None
    node_count: int | None
    node_list: str | None
    partition_name: str | None
    accounts: str | None


class JobQueueORM(SQLModel, table=True):
    """python orm of the `job_queue` sql db table

    this table keeps information about jobs which are in the slurm queue

    Args:
       SQLModel (SQLModel): make this class a sqlalchemy and pydantic class
       table (bool): `True`: 1:1 python orm sql db representation

    |column|meaning|
    |-|-|
    |sample_time|the current unixtimestamp, where a job is in the queue|
    |partition_name|name of the supercomputer cluster, the job will run on|
    |job_id|partition wide unique job id|
    |array_job_id|?|
    |job_name|name of the job, many jobs can have the same name|
    |user_name|name of the user, which started this job|
    |project_name|name of the project, in which the job was started|
    |job_state|state of the job|
    |node_count|on how many nodes a job will run|
    |scheduled_nodes|?|
    |excluded_nodes|?|
    |required_nodes|?|
    |exclusive|whether a job reserves a whole node|
    |cpus|how many cpus a job will use|
    |requested_cpus|?|
    |gpus|how many gpus a job will use|
    |requested_gpus|?|
    |memory|kb? mb?|
    |mem_per_cpu_flag|?|
    |time_limit|how long a job can run in seconds|
    |submit_time|unixtimestamp, when the job was put into the queue|
    |estimate_start_time|unixtimestamp, when the job might start running|
    |priority|?|
    |state_reason|the reason why the job is still in the queue|
    |reservation_id|id of ther corresponding reservation|
    |queue_position|derived from slurm's priority item|
    |dependency|job ids before job can start if state_reason='Dependency'|
    """

    __tablename__ = "job_queue"

    sample_time: int
    partition_name: str = Field(primary_key=True)
    job_id: int = Field(primary_key=True)
    array_job_id: int | None
    job_name: str | None
    user_name: str | None
    project_name: str | None
    job_state: str | None
    node_count: int | None
    scheduled_nodes: str | None
    excluded_nodes: str | None
    required_nodes: str | None
    exclusive: int | None
    cpus: int | None
    requested_cpus: int | None
    gpus: int | None
    requested_gpus: int | None
    memory: int | None
    mem_per_cpu_flag: int | None
    time_limit: int | None
    submit_time: int | None
    estimate_start_time: int | None
    priority: int | None
    state_reason: str | None
    reservation_id: int | None
    queue_position: int | None
    dependency: str | None

    @classmethod
    def max_sample_time(cls) -> Label[int]:
        """`max(sample_time) as max-sample_time`

        Returns:
            sqlalchemy impl detail: part of the sql select clause
        """
        return func.max(cls.sample_time).label("max_sample_time")

    @classmethod
    def job_count(cls) -> Label[int]:
        """`count(job_id) as job_count`

        Returns:
           sqlalchemy impl detail: part of the sql select clause
        """
        return func.count(cls.job_id).label("job_count")

    @classmethod
    def max_node_count(cls) -> Label[int]:
        """`max(node_count)`

        Returns:
           sqlalchemy impl detail: part of the sql select clause
        """
        return func.max(cls.node_count).label("max_node_count")

    @classmethod
    def max_cpu_count(cls) -> Label[int]:
        """`max(cpus)`

        Returns:
           sqlalchemy impl detail: part of the sql select clause
        """
        return func.max(cls.cpus).label("max_cpu_count")

    @classmethod
    def max_gpu_count(cls) -> Label[int]:
        """`max(gpus)`

        Returns:
           sqlalchemy impl detail: part of the sql select clause
        """
        return func.max(cls.gpus).label("max_gpu_count")

    @classmethod
    def max_pending_time(cls) -> Label[int]:
        """`max(sample_time - submit_time) as max_pending_time`

        Returns:
           sqlalchemy impl detail: part of the sql select clause
        """
        return func.max(
            case(
                (
                    cls.sample_time - cls.submit_time < 0,
                    0,
                ),
                else_=cls.sample_time - cls.submit_time,
            )
        ).label("max_pending_time")

    @classmethod
    def pending_time(cls) -> Label[int]:
        """`(sample_time - submit_time) as pending_time`

        Returns:
           sqlalchemy impl detail: part of the sql select clause
        """
        return (
            case(
                (
                    cls.sample_time - cls.submit_time < 0,
                    0,
                ),
                else_=cls.sample_time - cls.submit_time,
            )
        ).label("pending_time")

    @classmethod
    def max_time_limit(cls) -> Label[int]:
        """`max(time_limit) max_time_limit`

        Returns:
           sqlalchemy impl detail: part of the sql select clause
        """
        return func.max(cls.time_limit).label("max_time_limit")


class IntervalFilter(BaseModel):
    """building block of a sql filter, representing a interval"""

    min: float | None = None
    max: float | None = None


class FootprintFilter(BaseModel):
    """building block for a sql filter, representing a performance metric"""

    name: str
    min: float | None = None
    max: float | None = None


class JobQueueFilter(BaseModel):
    """The sql filter for the `JobQueueOrm`

    Args:
        BaseModel: make the class receivable from fastapi as endpoint parameter
            and produce swagger docu
    """

    # =
    sample_time: int | None = None
    partition_name: str | None = None
    array_job_id: int | None = None
    job_name: str | None = None
    user_name: str | None = None
    project_name: str | None = None
    project_name_list: list[str] | None = None
    exclusive: int | None = None

    # join
    reservation_data: bool | None = False

    # like
    partition_name_like: str | None = None
    job_name_like: str | None = None
    user_name_like: str | None = None
    project_name_like: str | None = None

    # intervals
    node_count_interval: IntervalFilter | None = None
    cpus_interval: IntervalFilter | None = None
    gpus_interval: IntervalFilter | None = None
    time_limit_interval: IntervalFilter | None = None
    sample_time_interval: IntervalFilter | None = None

    def apply_on_query(self, query: Query[JobQueueORM]) -> Query[JobQueueORM]:
        """apply the sql filter on the sqlalchemy query

        Args:
            query (Query): the sqlalchemy query

        Returns:
            Query: the sqlalchemy query which the filter applied on
        """

        filter_equal = [
            "sample_time",
            "partition_name",
            "array_job_id",
            "job_name",
            "user_name",
            "project_name",
            "exclusive",
        ]

        for f in filter_equal:
            if self.__dict__[f] is not None:
                query = query.filter(
                    JobQueueORM.__dict__[f] == self.__dict__[f]
                )

        filter_like = {
            "partition_name": "partition_name_like",
            "job_name": "job_name_like",
            "user_name": "user_name_like",
            "project_name": "project_name_like",
        }

        for key, value in filter_like.items():
            if self.__dict__[value] is not None:
                query = query.filter(
                    JobQueueORM.__dict__[key].like(f"%{self.__dict__[value]}%")
                )

        filter_interval = {
            "node_count": "node_count_interval",
            "cpus": "cpus_interval",
            "gpus": "gpus_interval",
            "time_limit": "time_limit_interval",
            "sample_time": "sample_time_interval",
        }

        for key, value in filter_interval.items():
            interval: IntervalFilter = self.__dict__[value]

            if interval is not None:
                if interval.max is not None:
                    query = query.filter(
                        JobQueueORM.__dict__[key] <= interval.max
                    )
                if interval.min is not None:
                    query = query.filter(
                        JobQueueORM.__dict__[key] >= interval.min
                    )

        # project_name_list
        if self.project_name_list is not None:
            conditions = [
                JobQueueORM.project_name == project_name
                for project_name in self.project_name_list
            ]
            query = query.filter(or_(*conditions))

        return query


class NodeFilter(BaseModel):
    """The sql filter for the `NodeOrm`

    Args:
        BaseModel: make the class receivable from fastapi as endpoint parameter
            and produce swagger docu
    """

    # =
    sample_time: int | None = None
    partition_name: str | None = None
    reservation_data: bool | None = False

    # intervals
    sample_time_interval: IntervalFilter | None = None

    def apply_on_query(self, query: Query[NodeORM]) -> Query[NodeORM]:
        """apply the sql filter on the sqlalchemy query

        Args:
            query (Query): the sqlalchemy query

        Returns:
            Query: the sqlalchemy query which the filter applied on
        """

        filter_equal = ["sample_time", "partition_name"]

        for f in filter_equal:
            if self.__dict__[f] is not None:
                query = query.filter(NodeORM.__dict__[f] == self.__dict__[f])

        filter_interval = {
            "sample_time": "sample_time_interval",
        }

        for key, value in filter_interval.items():
            interval: IntervalFilter = self.__dict__[value]

            if interval is not None:
                if interval.max is not None:
                    query = query.filter(NodeORM.__dict__[key] <= interval.max)
                if interval.min is not None:
                    query = query.filter(NodeORM.__dict__[key] >= interval.min)

        return query


class ReservationFilter(BaseModel):
    """The sql filter for the `ReservationOrm`

    Args:
        BaseModel: make the class receivable from fastapi as endpoint parameter
            and produce swagger docu
    """

    # =
    partition_name: str | None = None

    def apply_on_query(
        self, query: Query[ReservationORM]
    ) -> Query[ReservationORM]:
        """apply the sql filter on the sqlalchemy query

        Args:
            query (Query): the sqlalchemy query

        Returns:
            Query: the sqlalchemy query which the filter applied on
        """

        filter_equal = ["partition_name"]

        for f in filter_equal:
            if self.__dict__[f] is not None:
                query = query.filter(
                    ReservationORM.__dict__[f] == self.__dict__[f]
                )

        return query


class JobDataFilter(BaseModel):
    """The sql filter for the `JobDataOrm`

    Args:
        BaseModel: make the class receivable from fastapi as endpoint parameter
            and produce swagger docu

    |variable|meaning|example|example eval|
    |--------|-------|-------|------------|
    |job_id|id of the job(not unique as synchronizing thousands of nodes is to much overhead)|job_id=123|where job_id = 123|
    |uid|pika internal job id|uid=123|where uid=123|
    |user_name|Name of the user which executed the job|user_name="beju577d"|where user_name = "beju577d|
    |job_state|status of the job|job_state="running"|where job_state = "running"|
    |project_name|the project in which the job was started in|project_name="zih-tools"|where project_name = "zih-tools"|
    |exclusive|whether this job ran exclusive (had a whole node for itself)|ECLUSIVE_SLURM=1|where exclusive = 1|
    |partition_name|the Slurm partition on which the job was executed|partition_name="alpha"|where partition_name = "alpha"|
    |job_name|the name of the job (many jobs can have the same name)|job_name="gromacs"|where job_name="gromacs"|
    |node_name|?|node_name="taurusi8000"|where node_list="taurusi8000" or core_list="taurusi8000"|
    |smt_mode|deprecated???|||
    |array_id|?|||
    |partition_name_like|refer to partition_name|partition_name_like="alpha"|where partition_name like %alpha%|
    |job_name_like|refer to job_name|job_name_like="gromacs"|where job_name like %gromacs%|
    |node_name_like|refer to node_name|node_name_like="taurusi8000"|where node_list like %taurusi8000% or core_list like %taurusi8000%|
    |tags|(somewhat a bit pattern)`single value`: and, `multiple values`: or|tags=[16,2]|where TAG = 16 `or` TAG = 2|
    |start_time|unix timestamp how soon a job was started (not the queue arrival)|start_time=1680698752|where start_time >= 1680698752|
    ...
    """

    # job_id = 23 -> job_data.job_id = 23
    job_id: int | None = None
    uid: int | None = None
    user_name: str | None = None
    job_state: str | None = None
    project_name: str | None = None
    project_name_list: list[str] | None = None
    exclusive: int | None = None
    partition_name: str | None = None
    job_name: str | None = None
    node_name: str | None = None
    smt_mode: int | None = None
    array_id: int | None = None

    # partition_name_like = "alpha" -> job_data.P_partition_name like %alpha%
    partition_name_like: str | None = None
    job_name_like: str | None = None
    node_name_like: str | None = None

    # bit pattern, read implementation
    tags: list[int] | None = None

    # start_time = 1680698752 -> job_data.start_time >= start_time
    start_time: int | None = None
    # end_time = 1680698752 -> job_data.start_time < end_time
    end_time: int | None = None
    overlap: bool = False

    # live = False -> job_data.job_state != running
    # live = True -> job_data.job_state = running
    live: bool | None = None

    # "node_count_interval": { "min": "3", "max": "4" } -> ...
    node_count_interval: IntervalFilter | None = None
    core_count_interval: IntervalFilter | None = None
    gpu_count_interval: IntervalFilter | None = None
    time_limit_interval: IntervalFilter | None = None
    pending_time_interval: IntervalFilter | None = None
    duration_interval: IntervalFilter | None = None
    core_duration_interval: IntervalFilter | None = None

    # "footprint": [
    #   {"name":"ipc_mean_per_core", "min":"2", "max":"3.5"},
    #   {"name":"cpu_used_per_core", "min":"0.6", "max":"0.9"}
    # ]
    footprint: list[FootprintFilter] | None = None

    limit: bool | None = True
    # Typically, we set a limit of 20,000 results for our SQL queries to ensure
    # a smooth user experience in web browsers (True by default). However,
    # services like Vampir require access to all results without this limitation.

    # Only available for completed eligible jobs after post processing
    fsname: str | None = None

    def __hash__(self):
        """make hashing possible, needed for caching"""
        return hash(self.model_dump_json)

    def apply_on_query(self, query: Query[JobDataORM]) -> Query[JobDataORM]:
        """apply the sql filter on the sqlalchemy query

        Args:
            query (Query): the sqlalchemy query

        Returns:
            Query: the sqlalchemy query which the filter applied on
        """

        two_weeks_in_seconds = 1209600
        query = query.filter(
            and_(
                (
                    JobDataORM.job_id == self.job_id
                    if self.job_id is not None
                    else True
                ),
                JobDataORM.uid == self.uid if self.uid is not None else True,
                (
                    JobDataORM.user_name == self.user_name
                    if self.user_name is not None
                    else True
                ),
                (
                    JobDataORM.job_state == self.job_state
                    if self.job_state is not None
                    else True
                ),
                (
                    JobDataORM.project_name == self.project_name
                    if self.project_name is not None
                    else True
                ),
                (JobDataORM.exclusive == 1 if self.exclusive == 1 else True),
                (
                    JobDataORM.exclusive != 1
                    if self.exclusive != 1 and self.exclusive is not None
                    else True
                ),
                (
                    JobDataORM.partition_name == self.partition_name
                    if self.partition_name is not None
                    else True
                ),
                (
                    JobDataORM.job_name == self.job_name
                    if self.job_name is not None
                    else True
                ),
                (
                    JobDataORM.smt_mode == self.smt_mode
                    if self.smt_mode is not None
                    else True
                ),
                (
                    JobDataORM.array_id == self.array_id
                    if self.array_id is not None
                    else True
                ),
                (
                    JobDataORM.partition_name.like(
                        f"%{self.partition_name_like}%"
                    )
                    if self.partition_name_like is not None
                    else True
                ),
                (
                    JobDataORM.job_name.like(f"%{self.job_name_like}%")
                    if self.job_name_like is not None
                    else True
                ),
                (
                    JobDataORM.start_time >= self.start_time
                    if self.start_time is not None and self.overlap is False
                    else True
                ),
                (
                    JobDataORM.start_time < self.end_time
                    if self.end_time is not None and self.overlap is False
                    else True
                ),
                (
                    JobDataORM.start_time
                    >= (self.start_time - two_weeks_in_seconds)
                    if self.start_time is not None and self.overlap is True
                    else True
                ),
                (
                    JobDataORM.start_time <= self.end_time
                    if self.end_time is not None and self.overlap is True
                    else True
                ),
                (
                    or_(
                        JobDataORM.end_time >= self.start_time,
                        JobDataORM.end_time == None,
                    )
                    if self.overlap is True
                    else True
                ),
                (
                    or_(
                        JobDataORM.end_time
                        <= (self.end_time + two_weeks_in_seconds),
                        JobDataORM.end_time == None,
                    )
                    if self.overlap is True
                    else True
                ),
                (
                    JobDataORM.job_state != "running"
                    if self.live is False
                    else True
                ),
                (
                    JobDataORM.job_state == "running"
                    if self.live is True
                    else True
                ),
                (
                    FootprintFileioORM.fsname == self.fsname
                    if self.fsname is not None
                    else True
                ),
            ),
            or_(
                (
                    JobDataORM.node_list == self.node_name
                    if self.node_name is not None
                    else True
                ),
                (
                    JobDataORM.core_list == self.node_name
                    if self.node_name is not None
                    else True
                ),
            ),
            or_(
                (
                    JobDataORM.node_list.like(f"%{self.node_name_like}%")
                    if self.node_name_like is not None
                    else True
                ),
                (
                    JobDataORM.core_list.like(f"%{self.node_name_like}%")
                    if self.node_name_like is not None
                    else True
                ),
            ),
        )

        # intervals

        query = query.filter(
            and_(
                (
                    JobDataORM.node_count <= self.node_count_interval.max
                    if self.node_count_interval is not None
                    and self.node_count_interval.max is not None
                    else True
                ),
                (
                    JobDataORM.node_count >= self.node_count_interval.min
                    if self.node_count_interval is not None
                    and self.node_count_interval.min is not None
                    else True
                ),
                (
                    JobDataORM.core_count <= self.core_count_interval.max
                    if self.core_count_interval is not None
                    and self.core_count_interval.max is not None
                    else True
                ),
                (
                    JobDataORM.core_count >= self.core_count_interval.min
                    if self.core_count_interval is not None
                    and self.core_count_interval.min is not None
                    else True
                ),
                (
                    JobDataORM.gpu_count <= self.gpu_count_interval.max
                    if self.gpu_count_interval is not None
                    and self.gpu_count_interval.max is not None
                    else True
                ),
                (
                    JobDataORM.gpu_count >= self.gpu_count_interval.min
                    if self.gpu_count_interval is not None
                    and self.gpu_count_interval.min is not None
                    else True
                ),
                (
                    JobDataORM.time_limit <= self.time_limit_interval.max
                    if self.time_limit_interval is not None
                    and self.time_limit_interval.max is not None
                    else True
                ),
                (
                    JobDataORM.time_limit >= self.time_limit_interval.min
                    if self.time_limit_interval is not None
                    and self.time_limit_interval.min is not None
                    else True
                ),
                (
                    JobDataORM.pending_time() <= self.pending_time_interval.max
                    if self.pending_time_interval is not None
                    and self.pending_time_interval.max is not None
                    else True
                ),
                (
                    JobDataORM.pending_time() >= self.pending_time_interval.min
                    if self.pending_time_interval is not None
                    and self.pending_time_interval.min is not None
                    else True
                ),
                (
                    JobDataORM.duration() <= self.duration_interval.max
                    if self.duration_interval is not None
                    and self.duration_interval.max is not None
                    else True
                ),
                (
                    JobDataORM.duration() >= self.duration_interval.min
                    if self.duration_interval is not None
                    and self.duration_interval.min is not None
                    else True
                ),
                (
                    JobDataORM.core_duration()
                    <= self.core_duration_interval.max
                    if self.core_duration_interval is not None
                    and self.core_duration_interval.max is not None
                    else True
                ),
                (
                    JobDataORM.core_duration()
                    >= self.core_duration_interval.min
                    if self.core_duration_interval is not None
                    and self.core_duration_interval.min is not None
                    else True
                ),
            )
        )

        # footprint
        if self.footprint is not None:
            for _, footprint in enumerate(self.footprint):
                query = query.filter(
                    and_(
                        (
                            literal_column(footprint.name).is_not(None)
                            if footprint is not None
                            else True
                        ),
                        (
                            literal_column(footprint.name) <= footprint.max
                            if footprint is not None
                            and footprint.max is not None
                            else True
                        ),
                        (
                            literal_column(footprint.name) >= footprint.min
                            if footprint is not None
                            and footprint.min is not None
                            else True
                        ),
                    )
                )

        # tags

        # normal bit pattern (1,2,4,8,16)
        # e.g. value = 18 -> Job with tag 16 and 2
        if self.tags is not None:
            if len(self.tags) == 1:
                query = query.filter(JobDataORM.tags == self.tags[0])

            # special meaning
            # e.g. value = [16, 2] -> job with tag 16 or 2
            if len(self.tags) > 1:
                conditions = [value == JobDataORM.tags for value in self.tags]
                query = query.filter(or_(*conditions))

        # project_name_list
        if self.project_name_list is not None:
            conditions = [
                JobDataORM.project_name == project_name
                for project_name in self.project_name_list
            ]
            query = query.filter(or_(*conditions))

        if self.limit:
            query = query.limit(20_000)

        return query


class VisualizationFilter(JobDataFilter):
    """sql filter for the `footprint` route

    Args:
        JobDataFilter: inherit variables

    |variable|meaning|
    |--------|-------|
    |binning|how much to seperate the result set|
    |visualization|what performance metrics should be displayed|
    """

    binning: int | None = None
    visualization: list[FootprintFilter] | None = None


class SQLError(Exception):
    """custom error class representing a error which happend in this sql package"""
