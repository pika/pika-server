"""unittests for

- [ ] histogram # need footprint tables
- [ ] plot
- [ ] state
- [ ] tag
"""

from unittest import TestCase

from ...model import JobDataFilter, JobDataORM, VisualizationFilter
from ...test import MemoryDB
from .histogram import HistogramQuery, query_histogram
from .plot import ScatterplotQuery, query_scatterplot
from .state import StateORM, StateQuery
from .tag import TagORM, TagQuery

jobs = [
    {
        "uid": "42227220",
        "job_id": "29667954",
        "user_name": "notbeju577d",
        "project_name": "project_uno",
        "job_state": "completed",
        "node_count": 1,
        "node_list": "taurusi6603",
        "core_list": "taurusi6603[12]",
        "core_count": 1,
        "submit_time": "1664870658",
        "start_time": "1664870665",
        "end_time": "1664871885",
        "job_name": "interactive",
        "time_limit": 28800,
        "P_partition_name": "haswell64",
        "exclusive": 0,
        "property_id": 0,
        "array_id": "0",
        "tags": "0",
        "footprint_created": 1,
        "gpu_count": 0,
        "gpu_list": "",
        "smt_mode": 1,
        "job_script": "(null)",
    },
    {
        "uid": 42227941,
        "job_id": 29668639,
        "user_name": "beju577d",
        "project_name": "project_uno",
        "job_state": "completed",
        "node_count": 1,
        "node_list": "taurusi6308",
        "core_list": "taurusi6308[0-11,16-19]",
        "core_count": 16,
        "submit_time": 1664872171,
        "start_time": 1664872180,
        "end_time": 1664872508,
        "job_name": "interactive",
        "time_limit": 1860,
        "P_partition_name": "haswell64",
        "exclusive": 0,
        "property_id": 0,
        "array_id": "0",
        "tags": 0,
        "footprint_created": 1,
        "gpu_count": 0,
        "gpu_list": "",
        "smt_mode": 1,
        "job_script": None,
    },
    {
        "uid": 42228026,
        "job_id": 29668722,
        "user_name": "beju577d",
        "project_name": "project_dos",
        "job_state": "failed",
        "node_count": 1,
        "node_list": "taurusi6604",
        "core_list": "taurusi6604[13]",
        "core_count": 1,
        "submit_time": 1664872926,
        "start_time": 1664872944,
        "end_time": 1664872945,
        "job_name": "gromacs.sh",
        "time_limit": 28800,
        "P_partition_name": "haswell64",
        "exclusive": 0,
        "property_id": 0,
        "array_id": "0",
        "tags": None,
        "footprint_created": None,
        "gpu_count": 0,
        "gpu_list": "",
        "smt_mode": 1,
        "job_script": None,
    },
]


# class HistogramTest(TestCase):
#     """tesing histogram view"""

#     def setUp(self) -> None:
#         self.memory_db = MemoryDB()

#         self.memory_db.add(JobDataORM(**jobs[0]))
#         self.memory_db.add(JobDataORM(**jobs[1]))
#         self.memory_db.add(JobDataORM(**jobs[2]))

#         return super().setUp()

#     def test_without_filter(self):
#         """simple test with empty filter"""

#         query = HistogramQuery.build(VisualizationFilter())
#         print(query)


#     self.assertEqual(
#         typesafe_result,
#         [
#             ProjectORM(project_name="project_uno"),
#             ProjectORM(project_name="project_dos"),
#         ],
#     )

# def test_with_filter(self):
#     """simple test with filter"""

#     query = ProjectQuery.build(JobDataFilter(user_name="notbeju577d"))
#     result_set = self.memory_db.execute(query)
#     typesafe_result = ProjectORM.from_rows(result_set)

#     self.assertEqual(typesafe_result, [ProjectORM(project_name="project_uno")])
