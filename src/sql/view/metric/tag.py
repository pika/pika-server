"""metric.tag view"""

from typing import Any

from sqlalchemy.orm import Query
from sqlmodel import select

from ...model import (
    FootprintBaseORM,
    FootprintFileioORM,
    FootprintGpuORM,
    JobDataFilter,
    JobDataORM,
    JobQueueFilter,
    JobQueueORM,
)
from ..interface import ORMInterface, QueryInterface


class TagORM(ORMInterface):
    """metric.tag typesafe sqlalchemy result"""

    TAG: str
    footprint_1: int

    @staticmethod
    def from_row(result_set: dict[str, Any]) -> "TagORM":
        return TagORM(**result_set)

    @staticmethod
    def from_rows(result_set: list[dict[str, Any]]) -> list[ORMInterface]:
        return [TagORM(**r) for r in result_set]


class TagQuery(QueryInterface):
    """metric.tag sqlalchemy query builder"""

    @staticmethod
    def get_selected_columns() -> list[Any]:
        return [JobDataORM.tags, JobDataORM.num_tags()]

    @staticmethod
    def get_query(
        selected_columns: list[Any],
        query_filter: JobDataFilter | JobQueueFilter,
    ) -> Query[JobDataORM | JobQueueORM]:
        sql_query = (
            select(*selected_columns)
            .select_from(JobDataORM)
            .outerjoin(FootprintBaseORM, JobDataORM.uid == FootprintBaseORM.uid)
            .outerjoin(
                FootprintFileioORM, JobDataORM.uid == FootprintFileioORM.uid
            )
            .outerjoin(FootprintGpuORM, JobDataORM.uid == FootprintGpuORM.uid)
        )

        sql_query = query_filter.apply_on_query(sql_query)

        sql_query = (
            sql_query.filter(JobDataORM.tags.is_not(None))
            .group_by(JobDataORM.tags)
            .order_by(JobDataORM.num_tags().desc())
        )

        return sql_query
