"""metric.histogram view"""

from typing import Any, cast

from sqlalchemy.engine import Row
from sqlalchemy.orm import Query
from sqlmodel import func, select

from utils.common import calculate_bin_width

from ...connection import AsyncSQLConnection
from ...model import (
    FootprintBaseORM,
    FootprintFileioORM,
    FootprintFilter,
    FootprintGpuORM,
    IntervalFilter,
    JobDataFilter,
    JobDataORM,
    JobQueueFilter,
    JobQueueORM,
    VisualizationFilter,
)
from ..interface import QueryInterface
from ..meta import query_min_max_footprint, translate_footprint_select
from .state import StateQuery
from .tag import TagQuery

# mypy: ignore-errors


class HistogramQuery(QueryInterface):
    """metric.histogram sqlalchemy query builder"""

    @staticmethod
    def get_selected_columns() -> list[Any]:
        return []

    @staticmethod
    def get_query(
        _: list[Any], query_filter: JobDataFilter | JobQueueFilter
    ) -> Query[JobDataORM | JobQueueORM]:
        query_filter = cast(VisualizationFilter, query_filter)

        if query_filter.binning == 0:
            query_filter.binning = 15

        if query_filter.visualization[0].name == "gpu_count":
            # we query jobs with at least one gpu
            query_filter.gpu_count_interval = IntervalFilter(min=1, max=None)

        sql_select = select(
            translate_footprint_select(
                query_filter.visualization[0].name
            ).label("footprint")
        )

        subquery = (
            sql_select.select_from(JobDataORM)
            .outerjoin(FootprintBaseORM, JobDataORM.uid == FootprintBaseORM.uid)
            .outerjoin(
                FootprintFileioORM, JobDataORM.uid == FootprintFileioORM.uid
            )
            .outerjoin(FootprintGpuORM, JobDataORM.uid == FootprintGpuORM.uid)
            .filter(JobDataORM.job_state != "running")
            .filter(
                translate_footprint_select(query_filter.visualization[0].name)
                is not None,
                translate_footprint_select(query_filter.visualization[0].name)
                <= query_filter.visualization[0].max,
                translate_footprint_select(query_filter.visualization[0].name)
                >= query_filter.visualization[0].min,
            )
            .order_by("footprint")
        )

        subquery = query_filter.apply_on_query(subquery)
        subquery = subquery.subquery()

        bin_width = calculate_bin_width(
            float(query_filter.visualization[0].min),
            float(query_filter.visualization[0].max),
            query_filter.binning,
        )
        overlap_bin_width = bin_width

        # for integer binning without overlapping ranges
        if query_filter.visualization[0].name in [
            "node_count",
            "core_count",
            "gpu_count",
            "time_limit",
            "duration",
            "core_duration",
            "pending_time",
        ]:
            bin_width = round(bin_width)
            overlap_bin_width = bin_width - 1

        sql_query = (
            select(
                func.concat(
                    bin_width * func.floor(subquery.c.footprint / bin_width),
                    "-",
                    bin_width * func.floor(subquery.c.footprint / bin_width)
                    + overlap_bin_width,
                ).label("RANGE"),
                func.count().label("footprint_1"),
            )
            .select_from(subquery)
            .group_by("RANGE")
            .order_by(subquery.c.footprint)
        )

        return sql_query


async def query_histogram(
    connection: AsyncSQLConnection,
    query_filter: VisualizationFilter,
) -> list[Row]:
    """respond with formatted result to build a histogram from

    Raising:
        sql.model.SQLError
    """

    visual: FootprintFilter = query_filter.visualization[0]

    query_filter.limit = False
    # TODO this should be fixed in the front end

    match visual.name:
        # states and tags are no footprints, so we need to handle them seperatly
        case "states":
            query = StateQuery.build(query_filter)
        case "tags":
            query = TagQuery.build(query_filter)
        case _:
            if visual.min is None or visual.max is None:
                (
                    visual.min,
                    visual.max,
                ) = await query_min_max_footprint(
                    connection,
                    visual.name,
                    query_filter.user_name,
                    query_filter.start_time,
                    query_filter.end_time,
                )
            query = HistogramQuery.build(query_filter)

    result = await connection.execute(query)
    return result
