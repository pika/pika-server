"""metric.state view"""

from typing import Any

from sqlalchemy.engine import Row
from sqlalchemy.orm import Query
from sqlmodel import select

from ...model import (
    FootprintBaseORM,
    FootprintFileioORM,
    FootprintGpuORM,
    JobDataFilter,
    JobDataORM,
    JobQueueFilter,
    JobQueueORM,
)
from ..interface import ORMInterface, QueryInterface


class StateORM(ORMInterface):
    """metric.state type safe sqlalchemy result"""

    job_state: str
    footprint_1: int

    @staticmethod
    def from_row(result_set: dict[str, Any]) -> "StateORM":
        return StateORM(**result_set)

    @staticmethod
    def from_rows(result_set: list[dict[str, Any]]) -> list[ORMInterface]:
        return [StateORM(**r) for r in result_set]


class StateQuery(QueryInterface):
    """metric.state sqlalchemy query builder"""

    @staticmethod
    def get_selected_columns() -> list[Any]:
        return [JobDataORM.job_state, JobDataORM.num_status()]

    @staticmethod
    def get_query(
        selected_columns: list[Any],
        query_filter: JobDataFilter | JobQueueFilter,
    ) -> Query[JobQueueORM | JobDataORM]:
        sql_query = (
            select(*selected_columns)
            .select_from(JobDataORM)
            .outerjoin(FootprintBaseORM, JobDataORM.uid == FootprintBaseORM.uid)
            .outerjoin(
                FootprintFileioORM, JobDataORM.uid == FootprintFileioORM.uid
            )
            .outerjoin(FootprintGpuORM, JobDataORM.uid == FootprintGpuORM.uid)
        )

        sql_query = query_filter.apply_on_query(sql_query)

        sql_query = sql_query.group_by(JobDataORM.job_state).order_by(
            JobDataORM.num_status().desc()
        )

        return sql_query
