"""metric.scatterplot view"""

from asyncio import gather

from sqlalchemy.orm import Query
from sqlmodel import func, select

from sql.model import (
    FootprintBaseORM,
    FootprintFileioORM,
    FootprintGpuORM,
    IntervalFilter,
    JobDataORM,
    VisualizationFilter,
)
from utils.common import calculate_bin_width

from ...connection import AsyncSQLConnection
from ..interface import QueryInterface
from ..meta import query_min_max_footprint, translate_footprint_select

# mypy: ignore-errors


class ScatterplotQuery(QueryInterface):
    """metric.scatterplot sqlalchemy query builder"""

    @staticmethod
    def get_selected_columns() -> tuple:
        pass

    @staticmethod
    def get_query(_: tuple, query_filter: VisualizationFilter) -> Query:
        if query_filter.binning == 0:
            query_filter.binning = 15

        if (
            query_filter.visualization[0].name == "gpu_count"
            or query_filter.visualization[1].name == "gpu_count"
        ):
            # we query jobs with at least one gpu
            query_filter.gpu_count_interval = IntervalFilter(min=1, max=None)

        bin_width_0 = calculate_bin_width(
            query_filter.visualization[0].min,
            query_filter.visualization[0].max,
            query_filter.binning,
        )

        bin_width_1 = calculate_bin_width(
            query_filter.visualization[1].min,
            query_filter.visualization[1].max,
            query_filter.binning,
        )

        select_clause = select(
            (
                (
                    func.floor(
                        translate_footprint_select(
                            query_filter.visualization[0].name
                        )
                        / bin_width_0
                    )
                    * bin_width_0
                )
                + (bin_width_0 / 2)
            ).label("footprint_1"),
            (
                (
                    func.floor(
                        translate_footprint_select(
                            query_filter.visualization[1].name
                        )
                        / bin_width_1
                    )
                    * bin_width_1
                )
                + (bin_width_1 / 2)
            ).label("footprint_2"),
            func.count().label("footprint_count"),
        )

        sql_query = (
            select_clause.select_from(JobDataORM)
            .outerjoin(FootprintBaseORM, JobDataORM.uid == FootprintBaseORM.uid)
            .outerjoin(
                FootprintFileioORM, JobDataORM.uid == FootprintFileioORM.uid
            )
            .outerjoin(FootprintGpuORM, JobDataORM.uid == FootprintGpuORM.uid)
            .filter(
                translate_footprint_select(query_filter.visualization[0].name)
                is not None,
                translate_footprint_select(query_filter.visualization[1].name)
                is not None,
                translate_footprint_select(query_filter.visualization[0].name)
                <= query_filter.visualization[0].max,
                translate_footprint_select(query_filter.visualization[0].name)
                >= query_filter.visualization[0].min,
                translate_footprint_select(query_filter.visualization[1].name)
                <= query_filter.visualization[1].max,
                translate_footprint_select(query_filter.visualization[1].name)
                >= query_filter.visualization[1].min,
            )
        )

        sql_query = query_filter.apply_on_query(sql_query)

        sql_query = sql_query.group_by("footprint_1", "footprint_2").order_by(
            "footprint_1"
        )

        return sql_query


async def query_scatterplot(
    connection: AsyncSQLConnection, query_filter: VisualizationFilter
):
    """respond with formatted output to build a scatterplot from

    Raising:
        sql.model.SQLError
    """

    x_visual = query_filter.visualization[0]
    y_visual = query_filter.visualization[1]

    if (x_visual.min is None or x_visual.max is None) and (
        y_visual.min is None or y_visual.max is None
    ):
        # run get_min_max concurrently
        coroutines = []

        coroutines.append(
            query_min_max_footprint(
                connection,
                x_visual.name,
                query_filter.user_name,
                query_filter.start_time,
                query_filter.end_time,
            )
        )
        coroutines.append(
            query_min_max_footprint(
                connection,
                y_visual.name,
                query_filter.user_name,
                query_filter.start_time,
                query_filter.end_time,
            )
        )

        results = await gather(*coroutines)
        x_visual.min, x_visual.max = results[0]
        y_visual.min, y_visual.max = results[1]

    else:

        # if only one range has to be calculated
        if x_visual.min is None or x_visual.max is None:
            x_visual.min, x_visual.max = await query_min_max_footprint(
                connection,
                x_visual.name,
                query_filter.user_name,
                query_filter.start_time,
                query_filter.end_time,
            )

        if y_visual.min is None or y_visual.max is None:
            y_visual.min, y_visual.max = await query_min_max_footprint(
                connection,
                y_visual.name,
                query_filter.user_name,
                query_filter.start_time,
                query_filter.end_time,
            )

    query = ScatterplotQuery.build(query_filter)

    results = await connection.execute(query)

    results.append(
        {
            "class_width_1": calculate_bin_width(
                x_visual.min,
                x_visual.max,
                query_filter.binning,
            )
        }
    )

    results.append(
        {
            "class_width_2": calculate_bin_width(
                y_visual.min,
                x_visual.max,
                query_filter.binning,
            )
        }
    )

    return results
