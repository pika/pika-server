"""
viewing the performance data from jobs

histogram: returns a bar chart of jobs based on a specific performance metric
plot: returns a scatterplot of jobs based on a specific performance metric
"""

from .histogram import HistogramQuery, query_histogram
from .plot import ScatterplotQuery, query_scatterplot
