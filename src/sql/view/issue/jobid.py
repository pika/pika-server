"""
issue.jobid view
"""

from typing import Any

from sqlalchemy import select
from sqlalchemy.orm import Query

import utils
import utils.common
from utils.common import format_hour
from utils.model import FormatRequest

from ...model import (
    FootprintAnalysisORM,
    JobDataFilter,
    JobDataORM,
    JobQueueFilter,
    JobQueueORM,
)
from ..interface import (
    ORMInterface,
    ORMType,
    QueryFilterType,
    QueryInterface,
    ResponseInterface,
)

# mypy: ignore-errors


class JobIDORM(ORMInterface):
    """typesafe sqlalchemy result for the issue.jobid query"""

    uid: int | None
    job_id: int | None
    start_time: int | None
    array_id: int | None
    project_name: str | None
    partition_name: str | None
    core_duration_idle: int | None
    core_duration: int | None
    core_unused_ratio: float | None
    core_load_imbalance: float | None
    io_blocking: int | None
    io_congestion: int | None
    mem_leak: float | None
    gpu_duration_idle: int | None
    gpu_duration: int | None
    gpu_unused_ratio: float | None
    gpu_load_imbalance: float | None
    synchronous_offloading: int | None
    severity: int | None
    unused_mem: float | None

    @staticmethod
    def from_row(result_set: dict[str, Any]) -> "JobIDORM":
        return JobIDORM(**result_set)


class JobIDQuery(QueryInterface):
    """issue.jobid sqlalchemy query builder"""

    @staticmethod
    def get_selected_columns() -> list[Any]:
        return [
            FootprintAnalysisORM.uid,
            JobDataORM.job_id,
            JobDataORM.start_time,
            JobDataORM.array_id,
            JobDataORM.project_name,
            JobDataORM.partition_name,
            FootprintAnalysisORM.core_duration_idle,
            JobDataORM.core_duration(),
            FootprintAnalysisORM.core_unused_ratio(),
            FootprintAnalysisORM.core_load_imbalance,
            FootprintAnalysisORM.io_blocking,
            FootprintAnalysisORM.io_congestion,
            FootprintAnalysisORM.mem_leak,
            FootprintAnalysisORM.gpu_duration_idle,
            JobDataORM.gpu_duration(),
            FootprintAnalysisORM.gpu_unused_ratio(),
            FootprintAnalysisORM.gpu_load_imbalance,
            FootprintAnalysisORM.synchronous_offloading,
            FootprintAnalysisORM.severity,
            FootprintAnalysisORM.unused_mem,
        ]

    @staticmethod
    def get_query(
        selected_columns: list[Any],
        query_filter: QueryFilterType,
    ) -> Query[ORMType]:
        sql_query = (
            select(*selected_columns)
            .select_from(JobDataORM)
            .join(
                FootprintAnalysisORM,
                JobDataORM.uid == FootprintAnalysisORM.uid,
            )
        )

        sql_query = query_filter.apply_on_query(sql_query)

        sql_query = sql_query.order_by(
            FootprintAnalysisORM.core_duration_idle.desc()
        )

        return sql_query


class JobIDResponse(ResponseInterface):
    """issue.jobid response"""

    uid: int | None
    job_id: int | None
    start_time: str | None
    array_id: int | None
    project_name: str | None
    partition_name: str | None
    core_duration_idle: str | None
    core_duration: int | None
    core_unused_ratio: float | None
    core_load_imbalance: float | None
    io_blocking: int | None
    io_congestion: int | None
    mem_leak: float | None
    gpu_duration_idle: str | None
    gpu_duration: int | None
    gpu_unused_ratio: float | None
    gpu_load_imbalance: float | None
    synchronous_offloading: int | None
    severity: int | None
    unused_mem: float | None

    gpu_idle_ratio: float | None
    core_idle_ratio: float | None

    start_unixtime: int | None

    @staticmethod
    def from_orm(orm: ORMInterface, request: FormatRequest) -> "JobIDResponse":
        response = orm.__dict__

        response["start_unixtime"] = response["start_time"]
        response["start_time"] = request.translate_timezone(
            response["start_time"]
        )

        response["start_time"] = utils.common.format_date(
            response["start_time"], request.time_format
        )

        response["core_idle_ratio"] = round(
            response["core_duration_idle"] / response["core_duration"], 2
        )

        response["core_duration_idle"] = format_hour(
            response["core_duration_idle"]
        )

        if response["gpu_duration_idle"] > 0 and response["gpu_duration"] > 0:
            response["gpu_idle_ratio"] = round(
                response["gpu_duration_idle"] / response["gpu_duration"], 2
            )
            response["gpu_duration_idle"] = format_hour(
                response["gpu_duration_idle"]
            )
        else:
            response["gpu_idle_ratio"] = 0
            response["gpu_duration_idle"] = format_hour(0)

        return JobIDResponse(**response)
