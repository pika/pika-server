"""
issue.user view
"""

from typing import Any

from sqlalchemy.orm import Query
from sqlmodel import select

from utils.common import format_hour
from utils.model import FormatRequest

from ...model import FootprintAnalysisORM, JobDataORM, JobQueueORM
from ..interface import (
    ORMInterface,
    ORMType,
    QueryFilterType,
    QueryInterface,
    ResponseInterface,
)

# mypy: ignore-errors


class UserORM(ORMInterface):
    """typesafe sqlalchemy result for the issue.user query"""

    user_name: str | None
    issue_runs: int | None
    project_name: str | None
    sum_core_duration_idle: int | None
    sum_core_duration: int | None
    max_core_unused_ratio: float | None
    max_core_load_imbalance: float | None
    max_io_blocking: int | None
    max_io_congestion: int | None
    max_mem_leak: float | None
    sum_gpu_duration_idle: int | None
    max_gpu_unused_ratio: float | None
    sum_gpu_duration: int | None
    max_gpu_load_imbalance: float | None
    max_synchronous_offloading: int | None
    max_severity: int | None
    max_unused_mem: float | None

    @staticmethod
    def from_row(result_set: dict[str, Any]) -> "UserORM":
        return UserORM(**result_set)


class UserQuery(QueryInterface):
    """issue.user sqlalchemy query builder"""

    @staticmethod
    def get_selected_columns() -> list[Any]:
        return [
            JobDataORM.user_name,
            JobDataORM.issue_runs(),
            JobDataORM.project_name,
            FootprintAnalysisORM.sum_core_duration_idle(),
            JobDataORM.sum_core_duration(),
            FootprintAnalysisORM.max_core_unused_ratio(),
            FootprintAnalysisORM.max_core_load_imbalance(),
            FootprintAnalysisORM.max_io_blocking(),
            FootprintAnalysisORM.max_io_congestion(),
            FootprintAnalysisORM.max_mem_leak(),
            FootprintAnalysisORM.sum_gpu_duration_idle(),
            FootprintAnalysisORM.max_gpu_unused_ratio(),
            JobDataORM.sum_gpu_duration(),
            FootprintAnalysisORM.max_gpu_load_imbalance(),
            FootprintAnalysisORM.max_synchronous_offloading(),
            FootprintAnalysisORM.max_severity(),
            FootprintAnalysisORM.max_unused_mem(),
        ]

    @staticmethod
    def get_query(
        selected_columns: list[Any],
        query_filter: QueryFilterType,
    ) -> Query[ORMType]:
        sql_query = (
            select(*selected_columns)
            .select_from(JobDataORM)
            .with_hint(JobDataORM, "USE INDEX (USER_INDEX)")
            .join(
                FootprintAnalysisORM,
                JobDataORM.uid == FootprintAnalysisORM.uid,
            )
        )

        sql_query = query_filter.apply_on_query(sql_query)

        sql_query = sql_query.group_by(JobDataORM.user_name).order_by(
            FootprintAnalysisORM.sum_core_duration_idle().desc()
        )

        return sql_query


class UserResponse(ResponseInterface):
    """issue.user response"""

    user_name: str | None
    issue_runs: int | None
    project_name: str | None
    sum_core_duration_idle: int | None
    sum_core_duration: int | None
    max_core_unused_ratio: float | None
    max_core_load_imbalance: float | None
    max_io_blocking: int | None
    max_io_congestion: int | None
    max_mem_leak: float | None
    sum_gpu_duration_idle: int | None
    max_gpu_unused_ratio: float | None
    sum_gpu_duration: int | None
    max_gpu_load_imbalance: float | None
    max_synchronous_offloading: int | None
    max_severity: int | None
    max_unused_mem: float | None

    core_duration_idle: str | None
    core_idle_ratio: float | None
    gpu_duration_idle: str | None
    gpu_idle_ratio: float | None

    @staticmethod
    # type: ignore[override]
    def from_orm(orm: ORMInterface, _: FormatRequest) -> "UserResponse":
        response = orm.__dict__
        response["core_duration_idle"] = format_hour(
            response["sum_core_duration_idle"]
        )

        if response["sum_core_duration"] > 0:
            response["core_idle_ratio"] = round(
                response["sum_core_duration_idle"]
                / response["sum_core_duration"],
                2,
            )
        else:
            response["core_idle_ratio"] = 1

        response["gpu_duration_idle"] = format_hour(
            response["sum_gpu_duration_idle"]
        )

        if response["sum_gpu_duration"] > 0:
            response["gpu_idle_ratio"] = round(
                response["sum_gpu_duration_idle"]
                / response["sum_gpu_duration"],
                2,
            )
        else:
            response["gpu_idle_ratio"] = 0

        return UserResponse(**response)
