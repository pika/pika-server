"""unittests for

- [x] user
- [x] jobid
- [x] jobname
"""

from unittest import TestCase

from utils.model import FormatRequest

from ...model import FootprintAnalysisORM, JobDataFilter, JobDataORM
from ...test import MemoryDB
from .jobid import JobIDORM, JobIDQuery, JobIDResponse
from .jobname import JobNameORM, JobNameQuery, JobNameResponse
from .user import UserORM, UserQuery, UserResponse

# mypy: ignore-errors

jobs = [
    {
        "uid": 42227220,
        "job_id": 29667954,
        "user_name": "notbeju577d",
        "project_name": "project_uno",
        "job_state": "completed",
        "node_count": 1,
        "node_list": "taurusi6603",
        "core_list": "taurusi6603[12]",
        "core_count": 1,
        "submit_time": 1664870658,
        "start_time": 1664870665,
        "end_time": 1664871885,
        "job_name": "interactive",
        "time_limit": 28800,
        "partition_name": "haswell64",
        "exclusive": 0,
        "property_id": 0,
        "array_id": "0",
        "tags": "0",
        "footprint_created": 0,
        "gpu_count": 0,
        "gpu_list": "",
        "smt_mode": 1,
        "job_script": "(null)",
    },
    {
        "uid": 42227941,
        "job_id": 29668639,
        "user_name": "beju577d",
        "project_name": "project_uno",
        "job_state": "completed",
        "node_count": 1,
        "node_list": "taurusi6308",
        "core_list": "taurusi6308[0-11,16-19]",
        "core_count": 16,
        "submit_time": 1664872171,
        "start_time": 1664872180,
        "end_time": 1664872508,
        "job_name": "interactive",
        "time_limit": 1860,
        "partition_name": "haswell64",
        "exclusive": 0,
        "property_id": 0,
        "array_id": "0",
        "tags": 0,
        "footprint_created": 1,
        "gpu_count": 0,
        "gpu_list": "",
        "smt_mode": 1,
        "job_script": None,
    },
    {
        "uid": 42228026,
        "job_id": 29668722,
        "user_name": "beju577d",
        "project_name": "project_dos",
        "job_state": "timeout",
        "node_count": 1,
        "node_list": "taurusi6604",
        "core_list": "taurusi6604[13]",
        "core_count": 1,
        "submit_time": 1664872926,
        "start_time": 1664872944,
        "end_time": 1664872945,
        "job_name": "gromacs.sh",
        "time_limit": 28800,
        "partition_name": "haswell64",
        "exclusive": 0,
        "property_id": 0,
        "array_id": "0",
        "tags": None,
        "footprint_created": None,
        "gpu_count": 0,
        "gpu_list": "",
        "smt_mode": 1,
        "job_script": None,
    },
]

analysis_footprint = [
    {
        "uid": 42227220,
        "core_idle_ratio": 2.2,
        "core_load_imbalance": 2.2,
        "core_duration_idle": 2,
        "io_blocking": 2,
        "mem_leak": 2.2,
        "gpu_idle_ratio": 2.2,
        "gpu_load_imbalance": 2.2,
        "gpu_duration_idle": 2,
        "synchronous_offloading": 2,
        "io_blocking_detail": None,
        "io_congestion": 2,
        "severity": 2,
        "unused_mem": 0,
    },
    {
        "uid": 42227941,
        "core_idle_ratio": 3.3,
        "core_load_imbalance": 3.3,
        "core_duration_idle": 3,
        "io_blocking": 3,
        "mem_leak": 3.3,
        "gpu_idle_ratio": 3.3,
        "gpu_load_imbalance": None,
        "gpu_duration_idle": 3,
        "synchronous_offloading": 3,
        "io_blocking_detail": 3,
        "io_congestion": 3,
        "severity": 3,
        "unused_mem": 0,
    },
    {
        "uid": 42228026,
        "core_idle_ratio": 5.2,
        "core_load_imbalance": 5.2,
        "core_duration_idle": 25,
        "io_blocking": 25,
        "mem_leak": 25.2,
        "gpu_idle_ratio": 5.2,
        "gpu_load_imbalance": 2.2,
        "gpu_duration_idle": 5,
        "synchronous_offloading": 1,
        "io_blocking_detail": None,
        "io_congestion": 1,
        "severity": 1,
        "unused_mem": 0,
    },
]


class UserTest(TestCase):
    """tesing user view"""

    def setUp(self) -> None:
        self.memory_db = MemoryDB("sql.view.issue.test.UserTest")

        for job in jobs:
            self.memory_db.add(JobDataORM(**job))

        for f in analysis_footprint:
            self.memory_db.add(FootprintAnalysisORM(**f))

        return super().setUp()

    def test_without_filter(self):
        """simple test with empty filter"""

        query = UserQuery.build(JobDataFilter())
        result_set = self.memory_db.execute(query)
        typesafe_result = UserORM.from_rows(result_set)
        result = UserResponse.from_orms(
            typesafe_result, FormatRequest(utc_timezone="00:00")
        )

        self.assertEqual(
            result,
            [
                UserResponse(
                    user_name="beju577d",
                    issue_runs=2,
                    project_name="project_uno",
                    sum_core_duration_idle=28,
                    sum_core_duration=5249,
                    max_core_unused_ratio=5.2,
                    max_core_load_imbalance=5.2,
                    max_io_blocking=25,
                    max_io_congestion=3,
                    max_mem_leak=25.2,
                    sum_gpu_duration_idle=8,
                    max_gpu_unused_ratio=5.2,
                    sum_gpu_duration=0,
                    max_gpu_load_imbalance=2.2,
                    max_synchronous_offloading=3,
                    max_severity=3,
                    core_duration_idle="0.01",
                    core_idle_ratio=0.01,
                    gpu_duration_idle="0.00",
                    gpu_idle_ratio=0.0,
                    max_unused_mem=0,
                ),
                UserResponse(
                    user_name="notbeju577d",
                    issue_runs=1,
                    project_name="project_uno",
                    sum_core_duration_idle=2,
                    sum_core_duration=1220,
                    max_core_unused_ratio=2.2,
                    max_core_load_imbalance=2.2,
                    max_io_blocking=2,
                    max_io_congestion=2,
                    max_mem_leak=2.2,
                    sum_gpu_duration_idle=2,
                    max_gpu_unused_ratio=2.2,
                    sum_gpu_duration=0,
                    max_gpu_load_imbalance=2.2,
                    max_synchronous_offloading=2,
                    max_severity=2,
                    core_duration_idle="0.00",
                    core_idle_ratio=0.0,
                    gpu_duration_idle="0.00",
                    gpu_idle_ratio=0.0,
                    max_unused_mem=0,
                ),
            ],
        )

    def test_with_filter(self):
        """simple test with filter"""

        query = UserQuery.build(JobDataFilter(project_name="project_dos"))
        result_set = self.memory_db.execute(query)
        typesafe_result = UserORM.from_rows(result_set)
        result = UserResponse.from_orms(
            typesafe_result, FormatRequest(utc_timezone="00:00")
        )

        self.assertEqual(
            result,
            [
                UserResponse(
                    user_name="beju577d",
                    issue_runs=1,
                    project_name="project_dos",
                    sum_core_duration_idle=25,
                    sum_core_duration=1,
                    max_core_unused_ratio=5.2,
                    max_core_load_imbalance=5.2,
                    max_io_blocking=25,
                    max_io_congestion=1,
                    max_mem_leak=25.2,
                    sum_gpu_duration_idle=5,
                    max_gpu_unused_ratio=5.2,
                    sum_gpu_duration=0,
                    max_gpu_load_imbalance=2.2,
                    max_synchronous_offloading=1,
                    max_severity=1,
                    core_duration_idle="0.01",
                    core_idle_ratio=25.0,
                    gpu_duration_idle="0.00",
                    gpu_idle_ratio=0.0,
                    max_unused_mem=0,
                )
            ],
        )


class JobNameTest(TestCase):
    """testing jobname view"""

    def setUp(self) -> None:
        self.memory_db = MemoryDB("sql.view.issue.test.JobNameTest")

        for job in jobs:
            self.memory_db.add(JobDataORM(**job))

        for f in analysis_footprint:
            self.memory_db.add(FootprintAnalysisORM(**f))

        return super().setUp()

    def test_with_filter(self):
        """simple test with filter"""

        query = JobNameQuery.build(JobDataFilter(project_name="project_uno"))
        result_set = self.memory_db.execute(query)
        typesafe_result = JobNameORM.from_rows(result_set)
        result = JobNameResponse.from_orms(
            typesafe_result, FormatRequest(utc_timezone="00:00")
        )

        self.assertEqual(
            result,
            [
                JobNameResponse(
                    user_name="notbeju577d",
                    issue_runs=2,
                    project_name="project_uno",
                    sum_core_duration_idle=5,
                    sum_core_duration=6468,
                    max_core_unused_ratio=3.3,
                    max_core_load_imbalance=3.3,
                    max_io_blocking=3,
                    max_io_congestion=3,
                    max_mem_leak=3.3,
                    sum_gpu_duration_idle=5,
                    max_gpu_unused_ratio=3.3,
                    sum_gpu_duration=0,
                    max_gpu_load_imbalance=2.2,
                    max_synchronous_offloading=3,
                    max_severity=3,
                    core_duration_idle="0.00",
                    core_idle_ratio=0.0,
                    gpu_duration_idle="0.00",
                    gpu_idle_ratio=0.0,
                    job_name="interactive",
                    max_unused_mem=0,
                )
            ],
        )


class JobIDTest(TestCase):
    """tesing jobid view"""

    def setUp(self) -> None:
        self.memory_db = MemoryDB("sql.view.issue.test.JobIDTest")

        for job in jobs:
            self.memory_db.add(JobDataORM(**job))

        for f in analysis_footprint:
            self.memory_db.add(FootprintAnalysisORM(**f))

        return super().setUp()

    def test_with_filter(self):
        """simple test with filter"""

        query = JobIDQuery.build(JobDataFilter(project_name="project_uno"))
        result_set = self.memory_db.execute(query)
        typesafe_result = JobIDORM.from_rows(result_set)
        result = JobIDResponse.from_orms(
            typesafe_result, FormatRequest(utc_timezone="+02:00")
        )

        self.assertEqual(
            result,
            [
                JobIDResponse(
                    uid=42227941,
                    job_id=29668639,
                    start_time="04/10/2022 10:29:40",
                    start_unixtime=1664872180,
                    array_id=0,
                    project_name="project_uno",
                    partition_name="haswell64",
                    core_duration_idle="0.00",
                    core_duration=5248,
                    core_unused_ratio=3.3,
                    core_load_imbalance=3.3,
                    io_blocking=3,
                    io_congestion=3,
                    mem_leak=3.3,
                    gpu_duration_idle="0.00",
                    gpu_duration=0,
                    gpu_unused_ratio=3.3,
                    gpu_load_imbalance=None,
                    synchronous_offloading=3,
                    severity=3,
                    gpu_idle_ratio=0.0,
                    core_idle_ratio=0.0,
                    unused_mem=0,
                ),
                JobIDResponse(
                    uid=42227220,
                    job_id=29667954,
                    start_time="04/10/2022 10:04:25",
                    start_unixtime=1664870665,
                    array_id=0,
                    project_name="project_uno",
                    partition_name="haswell64",
                    core_duration_idle="0.00",
                    core_duration=1220,
                    core_unused_ratio=2.2,
                    core_load_imbalance=2.2,
                    io_blocking=2,
                    io_congestion=2,
                    mem_leak=2.2,
                    gpu_duration_idle="0.00",
                    gpu_duration=0,
                    gpu_unused_ratio=2.2,
                    gpu_load_imbalance=2.2,
                    synchronous_offloading=2,
                    severity=2,
                    gpu_idle_ratio=0.0,
                    core_idle_ratio=0.0,
                    unused_mem=0,
                ),
            ],
        )
