"""
issue.jobname view
"""

from typing import Any

from sqlalchemy.orm import Query
from sqlmodel import select

from utils.model import FormatRequest

from ...model import FootprintAnalysisORM, JobDataORM
from ..interface import ORMInterface, ORMType, QueryFilterType, QueryInterface
from .user import UserORM, UserQuery, UserResponse

# mypy: ignore-errors


class JobNameORM(UserORM):
    """typesafe sqlalchemy result for the issue.jobname query

    Args:
        UserORM: inherit variables
    """

    job_name: str | None

    @staticmethod
    def from_row(result_set: dict[str, Any]) -> "JobNameORM":
        return JobNameORM(**result_set)


class JobNameQuery(QueryInterface):
    """issue.jobname sqlalchemy query builder"""

    @staticmethod
    def get_selected_columns() -> list[Any]:
        columns = UserQuery.get_selected_columns()
        return [*columns, FootprintAnalysisORM.uid, JobDataORM.job_name]

    @staticmethod
    def get_query(
        selected_columns: list[Any],
        query_filter: QueryFilterType,
    ) -> Query[ORMType]:
        sql_query = (
            select(*selected_columns).select_from(JobDataORM)
            # .with_hint(JobDataORM, "USE INDEX (JOB_INDEX)")
            .join(
                FootprintAnalysisORM,
                JobDataORM.uid == FootprintAnalysisORM.uid,
            )
        )

        sql_query = query_filter.apply_on_query(sql_query)

        sql_query = sql_query.group_by(JobDataORM.job_name).order_by(
            FootprintAnalysisORM.core_duration_idle.desc()
        )

        return sql_query


class JobNameResponse(UserResponse):
    """issue.jobname response"""

    job_name: str | None

    @staticmethod
    def from_orm(
        orm: ORMInterface, request: FormatRequest
    ) -> "JobNameResponse":
        response = orm.__dict__
        user_resp = UserResponse.from_orm(orm, request).__dict__
        response.update(user_resp)

        return JobNameResponse(**response)
