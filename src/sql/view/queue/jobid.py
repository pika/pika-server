from typing import Any

from sqlalchemy import select
from sqlalchemy.orm import Query

from utils.common import format_date, format_duration
from utils.model import FormatRequest

from ...model import JobQueueORM, ReservationORM
from ..interface import (
    ORMInterface,
    ORMType,
    QueryFilterType,
    QueryInterface,
    ResponseInterface,
)


class JobIDBaseQuery(QueryInterface):
    columns: list[Any] = [
        JobQueueORM.job_id,
        JobQueueORM.partition_name,
        JobQueueORM.sample_time,
        JobQueueORM.array_job_id,
        JobQueueORM.job_name,
        JobQueueORM.user_name,
        JobQueueORM.project_name,
        JobQueueORM.job_state,
        JobQueueORM.node_count,
        JobQueueORM.scheduled_nodes,
        JobQueueORM.excluded_nodes,
        JobQueueORM.required_nodes,
        JobQueueORM.exclusive,
        JobQueueORM.cpus,
        JobQueueORM.requested_cpus,
        JobQueueORM.gpus,
        JobQueueORM.requested_gpus,
        JobQueueORM.memory,
        JobQueueORM.mem_per_cpu_flag,
        JobQueueORM.time_limit,
        JobQueueORM.submit_time,
        JobQueueORM.estimate_start_time,
        JobQueueORM.priority,
        JobQueueORM.state_reason,
        JobQueueORM.reservation_id,
        JobQueueORM.pending_time(),
        JobQueueORM.queue_position,
        JobQueueORM.dependency,
    ]


class JobIDQuery(JobIDBaseQuery):
    @staticmethod
    def get_selected_columns() -> list[Any]:
        return JobIDBaseQuery.columns

    @staticmethod
    def get_query(
        selected_columns: list[Any],
        query_filter: QueryFilterType,
    ) -> Query[ORMType]:
        select_statement = select(*selected_columns)

        if (
            query_filter.sample_time is None
            and query_filter.sample_time_interval is None
        ):
            select_statement = select_statement.filter(
                JobQueueORM.sample_time.in_(
                    select(JobQueueORM.max_sample_time())
                )
            )

        query = select_statement.select_from(JobQueueORM)
        query = query_filter.apply_on_query(query)
        query = query.order_by(JobQueueORM.pending_time().desc())

        return query


class JobIDReservationQuery(JobIDBaseQuery):
    @staticmethod
    def get_selected_columns() -> list[Any]:
        columns = JobIDBaseQuery.columns
        columns = columns + [
            ReservationORM.reservation_name,
            ReservationORM.start_time,
            ReservationORM.end_time,
            ReservationORM.node_count,
            ReservationORM.node_list,
            ReservationORM.accounts,
        ]
        return columns

    @staticmethod
    def get_query(
        selected_columns: list[Any],
        query_filter: QueryFilterType,
    ) -> Query[ORMType]:
        select_statement = select(*selected_columns).outerjoin(
            ReservationORM,
            JobQueueORM.reservation_id == ReservationORM.id,
        )

        if (
            query_filter.sample_time is None
            and query_filter.sample_time_interval is None
        ):
            select_statement = select_statement.filter(
                JobQueueORM.sample_time.in_(
                    select(JobQueueORM.max_sample_time())
                )
            )

        query = select_statement.select_from(JobQueueORM)
        query = query_filter.apply_on_query(query)
        query = query.order_by(JobQueueORM.pending_time().desc())

        return query


class JobIDBaseOrm(ORMInterface):
    job_id: int | None
    partition_name: str | None
    sample_time: int | None
    array_job_id: int | None
    job_name: str | None
    user_name: str | None
    project_name: str | None
    job_state: str | None
    node_count: int | None
    scheduled_nodes: str | None
    excluded_nodes: str | None
    required_nodes: str | None
    exclusive: int | None
    cpus: int | None
    requested_cpus: int | None
    gpus: int | None
    requested_gpus: int | None
    memory: int | None
    mem_per_cpu_flag: int | None
    time_limit: int | None
    submit_time: int | None
    estimate_start_time: int | None
    priority: int | None
    state_reason: str | None
    reservation_id: int | None
    pending_time: int | None
    queue_position: int | None
    dependency: str | None


class JobIDOrm(JobIDBaseOrm):
    @staticmethod
    def from_row(result_set: dict[str, Any]) -> "JobIDOrm":
        return JobIDOrm(**result_set)


class JobIDReservationOrm(JobIDBaseOrm):
    reservation_name: str | None
    start_time: int | None
    end_time: int | None
    node_count_1: int | None  # due join
    node_list: str | None
    accounts: str | None

    @staticmethod
    def from_row(result_set: dict[str, Any]) -> "JobIDReservationOrm":
        return JobIDReservationOrm(**result_set)


class JobIDBaseResponse(ResponseInterface):
    job_id: int | None
    partition_name: str | None
    sample_time: str | None
    array_job_id: int | None
    job_name: str | None
    user_name: str | None
    project_name: str | None
    job_state: str | None
    node_count: int | None
    scheduled_nodes: str | None
    excluded_nodes: str | None
    required_nodes: str | None
    exclusive: int | None
    cpus: int | None
    requested_cpus: int | None
    gpus: int | None
    requested_gpus: int | None
    memory: str | None
    mem_per_cpu_flag: int | None
    time_limit: str | None
    submit_time: str | None
    estimate_start_time: str | None
    priority: int | None
    state_reason: str | None
    reservation_id: int | None
    pending_time: str | None
    queue_position: int | None
    dependency: str | None

    @staticmethod
    def get_formatted_results(orm: ORMInterface, request: FormatRequest) -> Any:
        response = orm.__dict__

        # set Nones to 0 on int
        int_keys = [
            "job_id",
            "array_job_id",
            "node_count",
            "exclusive",
            "cpus",
            "requested_cpus",
            "gpus",
            "requested_gpus",
            "mem_per_cpu_flag",
            "priority",
            "node_count_1",
        ]
        for key in int_keys:
            if response.get(key) is None:
                response[key] = 0

        # set Nones to "n/a" on str
        str_keys = [
            "partition_name",
            "job_name",
            "user_name",
            "project_name",
            "job_state",
            "scheduled_nodes",
            "excluded_nodes",
            "required_nodes",
            "state_reason",
            "node_list",
            "accounts",
            "dependency",
        ]
        for key in str_keys:
            if response.get(key) is None:
                response[key] = "n/a"

        try:
            response["queue_position"] = str(response["queue_position"])
        except TypeError:
            response["sample_time"] = "n/a"

        try:
            response["sample_time"] = format_date(
                response["sample_time"], request.time_format
            )
        except TypeError:
            response["sample_time"] = "n/a"

        # convert from bytes to gigabyte
        try:
            response["memory"] = str(round(response["memory"] / 10**9, 2))
        except TypeError:
            response["memory"] = "n/a"

        try:
            response["time_limit"] = format_duration(response["time_limit"])
        except TypeError:
            response["time_limit"] = "n/a"

        try:
            response["submit_time"] = request.translate_timezone(
                response["submit_time"]
            )
            response["submit_time"] = format_date(
                response["submit_time"], request.time_format
            )
        except TypeError:
            response["submit_time"] = "n/a"

        if response["estimate_start_time"] == 0:
            response["estimate_start_time"] = "n/a"
        else:
            response["estimate_start_time"] = request.translate_timezone(
                response["estimate_start_time"]
            )
            try:
                response["estimate_start_time"] = format_date(
                    response["estimate_start_time"], request.time_format
                )
            except TypeError:
                response["estimate_start_time"] = "n/a"

        try:
            response["pending_time"] = format_duration(response["pending_time"])
        except TypeError:
            response["pending_time"] = "n/a"

        if response["scheduled_nodes"] == "n/a":
            response["scheduled_nodes"] = "none"

        if response["excluded_nodes"] == "n/a":
            response["excluded_nodes"] = "none"

        if response["required_nodes"] == "n/a":
            response["required_nodes"] = "none"

        if "start_time" in response:
            try:
                response["start_unixtime"] = response["start_time"]
                response["start_time"] = request.translate_timezone(
                    response["start_time"]
                )

                response["start_time"] = format_date(
                    response["start_time"], request.time_format
                )
            except TypeError:
                response["start_time"] = "n/a"

        if "end_time" in response:
            try:
                response["end_time"] = request.translate_timezone(
                    response["end_time"]
                )
                response["end_time"] = format_date(
                    response["end_time"], request.time_format
                )
            except TypeError:
                response["end_time"] = "n/a"

        if "reservation_name" in response:
            if response.get("reservation_name") is None:
                response["reservation_name"] = "none"

        if response["dependency"] == "n/a":
            response["dependency"] = "none"

        return response


class JobIDResponse(JobIDBaseResponse):
    @staticmethod
    # type: ignore[override]
    def from_orm(orm: ORMInterface, request: FormatRequest) -> "JobIDResponse":
        response = JobIDBaseResponse.get_formatted_results(orm, request)
        return JobIDResponse(**response)


class JobIDReservationResponse(JobIDBaseResponse):
    reservation_name: str | None
    start_time: str | None
    end_time: str | None
    node_count_1: int | None  # due to join
    node_list: str | None
    accounts: str | None
    start_unixtime: int | None

    @staticmethod
    # type: ignore[override]
    def from_orm(
        orm: ORMInterface, request: FormatRequest
    ) -> "JobIDReservationResponse":
        response = JobIDBaseResponse.get_formatted_results(orm, request)

        return JobIDReservationResponse(**response)
