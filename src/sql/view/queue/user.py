from typing import Any

from sqlalchemy import select
from sqlalchemy.orm import Query

from utils.common import format_duration
from utils.model import FormatRequest

from ...model import JobQueueORM
from ..interface import (
    ORMInterface,
    ORMType,
    QueryFilterType,
    QueryInterface,
    ResponseInterface,
)


class UserQuery(QueryInterface):
    @staticmethod
    def get_selected_columns() -> list[Any]:
        return [
            JobQueueORM.user_name,
            JobQueueORM.project_name,
            JobQueueORM.job_count(),
            JobQueueORM.max_node_count(),
            JobQueueORM.max_cpu_count(),
            JobQueueORM.max_gpu_count(),
            JobQueueORM.max_pending_time(),
            JobQueueORM.max_time_limit(),
            JobQueueORM.partition_name,
        ]

    @staticmethod
    def get_query(
        selected_columns: list[Any],
        query_filter: QueryFilterType,
    ) -> Query[ORMType]:
        if (
            query_filter.sample_time is None
            and query_filter.sample_time_interval is None
        ):
            query = (
                select(*selected_columns)
                .select_from(JobQueueORM)
                .filter(
                    JobQueueORM.sample_time.in_(
                        select(JobQueueORM.max_sample_time()).select_from(
                            JobQueueORM
                        )
                    )
                )
            )
        else:
            query = select(*selected_columns).select_from(JobQueueORM)

        query = query_filter.apply_on_query(query)

        query = query.group_by(JobQueueORM.user_name).order_by(
            JobQueueORM.max_pending_time().desc()
        )

        return query


class UserOrm(ORMInterface):
    """queue.user"""

    user_name: str | None
    project_name: str | None
    job_count: int | None
    max_node_count: int | None
    max_cpu_count: int | None
    max_gpu_count: int | None
    max_pending_time: int | None
    max_time_limit: int | None
    partition_name: str | None

    @staticmethod
    def from_row(result_set: dict[str, Any]) -> "UserOrm":
        return UserOrm(**result_set)


class UserResponse(ResponseInterface):
    user_name: str | None
    project_name: str | None
    job_count: int | None
    max_node_count: int | None
    max_cpu_count: int | None
    max_gpu_count: int | None
    max_pending_time: str | None
    max_time_limit: str | None
    partition_name: str | None

    @staticmethod
    # type: ignore[override]
    def from_orm(orm: ORMInterface, _: FormatRequest) -> "UserResponse":
        response = orm.__dict__

        response["max_pending_time"] = format_duration(
            response["max_pending_time"]
        )
        response["max_time_limit"] = format_duration(response["max_time_limit"])

        return UserResponse(**response)
