"""
topdown.project view
"""

from typing import Any, cast

from sqlalchemy import func, select
from sqlalchemy.orm import Query

from utils.common import format_duration, format_duration_year, format_hour
from utils.model import FormatRequest

from ...model import (
    FootprintBaseORM,
    FootprintFileioORM,
    FootprintGpuORM,
    JobDataFilter,
    JobDataORM,
)
from ..interface import (
    ORMInterface,
    ORMType,
    QueryFilterType,
    QueryInterface,
    ResponseInterface,
)

# mypy: ignore-errors


class ProjectQuery(QueryInterface):
    """topdown.project sqlalchemy query builder"""

    @staticmethod
    def get_selected_columns() -> list[Any]:
        return [
            JobDataORM.project_name,
            JobDataORM.job_number(),
            JobDataORM.max_nodes(),
            JobDataORM.max_cores(),
            JobDataORM.max_gpu_count(),
            JobDataORM.max_pending_time(),
            JobDataORM.sum_duration(),
            JobDataORM.sum_core_duration(),
            JobDataORM.sum_footprints(),
        ]

    @staticmethod
    def get_query(
        selected_columns: list[Any],
        query_filter: QueryFilterType,
    ) -> Query[ORMType]:
        query_filter = cast(JobDataFilter, query_filter)

        if query_filter.footprint is not None:
            sql_query = (
                select(*selected_columns)
                .outerjoin(
                    FootprintBaseORM, JobDataORM.uid == FootprintBaseORM.uid
                )
                .outerjoin(
                    FootprintFileioORM,
                    JobDataORM.uid == FootprintFileioORM.uid,
                )
                .outerjoin(
                    FootprintGpuORM, JobDataORM.uid == FootprintGpuORM.uid
                )
            )
        elif query_filter.fsname is not None:
            sql_query = (
                select(*selected_columns)
                .select_from(JobDataORM)
                .outerjoin(
                    FootprintFileioORM,
                    JobDataORM.uid == FootprintFileioORM.uid,
                )
            )
        else:
            sql_query = select(*selected_columns)

        sql_query = query_filter.apply_on_query(sql_query)

        sql_query = sql_query.group_by(JobDataORM.project_name).order_by(
            func.max(JobDataORM.start_time).desc()
        )

        return sql_query


class ProjectORM(ORMInterface):
    """typesafe topdown.project sqlalchemy result"""

    project_name: str | None
    job_count: int | None
    max_node_count: int | None
    max_core_count: int | None
    max_gpu_count: int | None
    max_pending_time: int | None
    sum_duration: int | None
    sum_core_duration: int | None
    footprint_count: int | None

    @staticmethod
    def from_row(result_set: dict[str, Any]) -> "ProjectORM":
        return ProjectORM(**result_set)


class ProjectResponse(ResponseInterface):
    """topdown.project formatted response"""

    project_name: str | None
    job_count: int | None
    max_node_count: int | None
    max_core_count: int | None
    max_gpu_count: int | None
    footprint_count: int | None

    max_pending_time: str | None
    sum_duration: str | None
    sum_core_duration: str | None

    @staticmethod
    # type: ignore[override]
    def from_orm(orm: ORMInterface, _: FormatRequest) -> "ProjectResponse":
        response = orm.__dict__
        if response["max_gpu_count"] is None:
            response["max_gpu_count"] = 0

        response["max_pending_time"] = format_duration(
            response["max_pending_time"]
        )
        response["sum_duration"] = format_duration_year(
            response["sum_duration"]
        )
        response["sum_core_duration"] = format_hour(
            response["sum_core_duration"]
        )

        return ProjectResponse(**response)
