"""
topdown.user view
"""

from typing import Any, cast

from sqlalchemy import func, select
from sqlalchemy.orm import Query

from utils.common import format_duration
from utils.model import FormatRequest

from ...model import (
    FootprintBaseORM,
    FootprintFileioORM,
    FootprintGpuORM,
    JobDataFilter,
    JobDataORM,
)
from ..interface import ORMInterface, ORMType, QueryFilterType, QueryInterface
from .project import ProjectORM, ProjectQuery, ProjectResponse

# mypy: ignore-errors


class UserQuery(QueryInterface):
    """topdown.user sqlalchemy query builder"""

    @staticmethod
    def get_selected_columns() -> list[Any]:
        project_columns = ProjectQuery.get_selected_columns()
        return [
            *project_columns,
            JobDataORM.user_name,
            JobDataORM.max_time_limit(),
        ]

    @staticmethod
    def get_query(
        selected_columns: list[Any],
        query_filter: QueryFilterType,
    ) -> Query[ORMType]:
        query_filter = cast(JobDataFilter, query_filter)

        if query_filter.footprint is not None:
            sql_query = (
                select(*selected_columns)
                .select_from(JobDataORM)
                .outerjoin(
                    FootprintBaseORM, JobDataORM.uid == FootprintBaseORM.uid
                )
                .outerjoin(
                    FootprintFileioORM,
                    JobDataORM.uid == FootprintFileioORM.uid,
                )
                .outerjoin(
                    FootprintGpuORM, JobDataORM.uid == FootprintGpuORM.uid
                )
            )
        elif query_filter.fsname is not None:
            sql_query = (
                select(*selected_columns)
                .select_from(JobDataORM)
                .outerjoin(
                    FootprintFileioORM,
                    JobDataORM.uid == FootprintFileioORM.uid,
                )
            )
        else:
            sql_query = select(*selected_columns)

        sql_query = query_filter.apply_on_query(sql_query)

        sql_query = sql_query.group_by(JobDataORM.user_name).order_by(
            func.max(JobDataORM.start_time).desc()
        )

        return sql_query


class UserORM(ProjectORM):
    """typesafe topdown.user sqlalchemy result"""

    user_name: str | None
    max_time_limit: int | None

    @staticmethod
    def from_row(result_set: dict[str, Any]) -> "UserORM":
        return UserORM(**result_set)


class UserResponse(ProjectResponse):
    """topdown.user formatted response"""

    user_name: str | None
    max_time_limit: str | None

    @staticmethod
    # type: ignore[override]
    def from_orm(orm: ORMInterface, request: FormatRequest) -> "UserResponse":
        project_dict = ProjectResponse.from_orm(orm, request).__dict__
        user_dict = orm.__dict__

        user_dict.update(project_dict)
        user_dict["max_time_limit"] = format_duration(
            user_dict["max_time_limit"]
        )

        return UserResponse(**user_dict)
