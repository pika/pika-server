"""unittests for

- [x] project
- [x] user
- [x] jobname
- [x] jobid
- [x] jobideverything
"""

from unittest import TestCase

from utils.model import FormatRequest

from ...model import (
    FootprintAnalysisORM,
    FootprintBaseORM,
    FootprintFileioORM,
    FootprintFilter,
    FootprintGpuORM,
    JobDataFilter,
    JobDataORM,
)
from ...test import MemoryDB
from .jobid import JobIDOrm, JobIDQuery, JobIDResponse
from .jobideverything import (
    JobIDEverythingORM,
    JobIDEverythingQuery,
    JobIDEverythingResponse,
)
from .jobname import JobNameOrm, JobNameQuery, JobNameResponse
from .project import ProjectORM, ProjectQuery, ProjectResponse
from .user import UserORM, UserQuery, UserResponse

jobs = [
    {
        "uid": 42227220,
        "job_id": 29667954,
        "user_name": "notbeju577d",
        "project_name": "p_zih-psw-tools",
        "job_state": "completed",
        "node_count": 1,
        "node_list": "taurusi6603",
        "core_list": "taurusi6603[12]",
        "core_count": 1,
        "submit_time": "1664870658",
        "start_time": "1664870665",
        "end_time": "1664871885",
        "job_name": "interactive",
        "time_limit": 28800,
        "partition_name": "haswell64",
        "exclusive": 0,
        "property_id": 0,
        "array_id": "0",
        "tags": "0",
        "footprint_created": 1,
        "gpu_count": 0,
        "gpu_list": "",
        "smt_mode": 1,
        "job_script": "(null)",
    },
    {
        "uid": 42227941,
        "job_id": 29668639,
        "user_name": "beju577d",
        "project_name": "p_zih-psw-tools",
        "job_state": "completed",
        "node_count": 1,
        "node_list": "taurusi6308",
        "core_list": "taurusi6308[0-11,16-19]",
        "core_count": 16,
        "submit_time": 1664872171,
        "start_time": 1664872180,
        "end_time": 1664872508,
        "job_name": "interactive",
        "time_limit": 1860,
        "partition_name": "haswell64",
        "exclusive": 0,
        "property_id": 0,
        "array_id": "0",
        "tags": 0,
        "footprint_created": 1,
        "gpu_count": 8,
        "gpu_list": "",
        "smt_mode": 1,
        "job_script": None,
    },
    {
        "uid": 42228026,
        "job_id": 29668722,
        "user_name": "beju577d",
        "project_name": "p_zih-psw-tools",
        "job_state": "failed",
        "node_count": 1,
        "node_list": "taurusi6604",
        "core_list": "taurusi6604[13]",
        "core_count": 1,
        "submit_time": 1664872926,
        "start_time": 1664872944,
        "end_time": 1664872945,
        "job_name": "gromacs.sh",
        "time_limit": 28800,
        "partition_name": "haswell64",
        "exclusive": 0,
        "property_id": 0,
        "array_id": "0",
        "tags": None,
        "footprint_created": 0,
        "gpu_count": 0,
        "gpu_list": "",
        "smt_mode": 1,
        "job_script": None,
    },
]

base_footprints = [
    {
        "uid": 42227220,
        "ipc_mean_per_core": 5.11,
        "cpu_used_mean_per_core": 1.11,
        "flops_any_mean_per_core": 1.11,
        "mem_bw_mean_per_socket": 1.11,
        "cpu_power_mean_per_socket": 1.11,
        "ib_bw_mean_per_node": 1.11,
        "host_mem_used_max_per_node": None,
        "job_energy": None,
        "job_energy_efficiency": None,
    },
    {
        "uid": 42227941,
        "ipc_mean_per_core": 4.33,
        "cpu_used_mean_per_core": 2.22,
        "flops_any_mean_per_core": 2.22,
        "mem_bw_mean_per_socket": 2.22,
        "cpu_power_mean_per_socket": 2.22,
        "ib_bw_mean_per_node": 2.22,
        "host_mem_used_max_per_node": 2.22,
        "job_energy": None,
        "job_energy_efficiency": None,
    },
]

filio_footprints = [
    {"uid": 42227220, "read_bytes": 23, "write_bytes": None, "fsname": "ext4"},
    {
        "uid": 42227941,
        "read_bytes": 23331,
        "write_bytes": 1289,
        "fsname": "ext4",
    },
]

gpu_fooltprints = [
    {
        "uid": 42227941,
        "used_mean_per_gpu": 1.1,
        "mem_used_max_per_gpu": 1.1,
        "power_mean_per_gpu": 1.1,
    }
]

analysis_footprint = [
    {
        "uid": 42227220,
        "core_idle_ratio": 2.2,
        "core_load_imbalance": 2.2,
        "core_duration_idle": 2,
        "io_blocking": 2,
        "mem_leak": 2.2,
        "gpu_idle_ratio": 2.2,
        "gpu_load_imbalance": 2.2,
        "gpu_duration_idle": 2,
        "synchronous_offloading": 2,
        "io_blocking_detail": None,
        "io_congestion": 2,
        "severity": 2,
        "unused_mem": 0,
    }
]


class ProjectTest(TestCase):
    """unittesting project view"""

    def setUp(self) -> None:
        self.memory_db = MemoryDB("sql.view.topdown.test.ProjectTest")

        for job in jobs:
            self.memory_db.add(JobDataORM(**job))

        for f in base_footprints:
            self.memory_db.add(FootprintBaseORM(**f))

        for f in filio_footprints:
            self.memory_db.add(FootprintFileioORM(**f))

        for f in gpu_fooltprints:
            self.memory_db.add(FootprintGpuORM(**f))

        return super().setUp()

    def test_without_filter(self) -> None:
        """test ProjectQuery builder with empty JobDataFilter"""

        query = ProjectQuery.build(JobDataFilter())
        result_set = self.memory_db.execute(query)
        parsed_jobs = ProjectORM.from_rows(result_set)
        formatted_jobs = ProjectResponse.from_orms(
            parsed_jobs, FormatRequest(utc_timezone="00:00")
        )

        self.assertEqual(
            formatted_jobs,
            [
                ProjectResponse(
                    project_name="p_zih-psw-tools",
                    job_count=3,
                    max_node_count=1,
                    max_core_count=16,
                    max_gpu_count=8,
                    footprint_count=2,
                    max_pending_time="00d 00h 00min 18s",
                    sum_duration="0000y 000d 00h 25min",
                    sum_core_duration="1.80",
                )
            ],
        )

    def test_with_filter(self) -> None:
        """test projectquery builder with filter"""

        query = ProjectQuery.build(JobDataFilter(uid=42228026))
        result_set = self.memory_db.execute(query)
        parsed_jobs = ProjectORM.from_rows(result_set)
        formatted_jobs = ProjectResponse.from_orms(
            parsed_jobs, FormatRequest(utc_timezone="00:00")
        )

        self.assertEqual(
            formatted_jobs,
            [
                ProjectResponse(
                    project_name="p_zih-psw-tools",
                    job_count=1,
                    max_node_count=1,
                    max_core_count=1,
                    max_gpu_count=0,
                    footprint_count=0,
                    max_pending_time="00d 00h 00min 18s",
                    sum_duration="0000y 000d 00h 00min",
                    sum_core_duration="0.00",
                )
            ],
        )

    def test_fsname_case(self) -> None:
        """testing projectquery fsname case"""

        query = ProjectQuery.build(JobDataFilter(fsname="ext4"))
        result_set = self.memory_db.execute(query)
        typesafe_result = ProjectORM.from_rows(result_set)
        restapi_result = ProjectResponse.from_orms(
            typesafe_result, FormatRequest(utc_timezone="00:00")
        )

        self.assertEqual(
            restapi_result,
            [
                ProjectResponse(
                    project_name="p_zih-psw-tools",
                    job_count=2,
                    max_node_count=1,
                    max_core_count=16,
                    max_gpu_count=8,
                    footprint_count=2,
                    max_pending_time="00d 00h 00min 09s",
                    sum_duration="0000y 000d 00h 25min",
                    sum_core_duration="1.80",
                )
            ],
        )

    def test_footprint_case(self) -> None:
        """testing projectquery footprint case"""

        query = ProjectQuery.build(
            JobDataFilter(footprint=[FootprintFilter(name="used_mean_per_gpu")])
        )
        result_set = self.memory_db.execute(query)
        typesafe_result = ProjectORM.from_rows(result_set)
        restapi_result = ProjectResponse.from_orms(
            typesafe_result, FormatRequest(utc_timezone="00:00")
        )

        self.assertEqual(
            restapi_result,
            [
                ProjectResponse(
                    project_name="p_zih-psw-tools",
                    job_count=1,
                    max_node_count=1,
                    max_core_count=16,
                    max_gpu_count=8,
                    footprint_count=1,
                    max_pending_time="00d 00h 00min 09s",
                    sum_duration="0000y 000d 00h 05min",
                    sum_core_duration="1.46",
                )
            ],
        )


class UserTest(TestCase):
    """unittesting user view"""

    def setUp(self) -> None:
        self.memory_db = MemoryDB("sql.view.topdown.test.UserTest")

        for job in jobs:
            self.memory_db.add(JobDataORM(**job))

        for f in base_footprints:
            self.memory_db.add(FootprintBaseORM(**f))

        for f in filio_footprints:
            self.memory_db.add(FootprintFileioORM(**f))

        for f in gpu_fooltprints:
            self.memory_db.add(FootprintGpuORM(**f))

        return super().setUp()

    def test_without_filter(self) -> None:
        """empty filter"""

        query = UserQuery.build(JobDataFilter())
        result_set = self.memory_db.execute(query)
        parsed_jobs = UserORM.from_rows(result_set)
        formatted_jobs = UserResponse.from_orms(
            parsed_jobs, FormatRequest(utc_timezone="00:00")
        )

        self.assertEqual(
            formatted_jobs,
            [
                UserResponse(
                    project_name="p_zih-psw-tools",
                    job_count=2,
                    max_node_count=1,
                    max_core_count=16,
                    max_gpu_count=8,
                    footprint_count=1,
                    max_pending_time="00d 00h 00min 18s",
                    sum_duration="0000y 000d 00h 05min",
                    sum_core_duration="1.46",
                    user_name="beju577d",
                    max_time_limit="00d 08h 00min 00s",
                ),
                UserResponse(
                    project_name="p_zih-psw-tools",
                    job_count=1,
                    max_node_count=1,
                    max_core_count=1,
                    max_gpu_count=0,
                    footprint_count=1,
                    max_pending_time="00d 00h 00min 07s",
                    sum_duration="0000y 000d 00h 20min",
                    sum_core_duration="0.34",
                    user_name="notbeju577d",
                    max_time_limit="00d 08h 00min 00s",
                ),
            ],
        )

    def test_with_filter(self) -> None:
        "test with filter"

        query = UserQuery.build(JobDataFilter(user_name="beju577d"))
        result_set = self.memory_db.execute(query)
        parsed_jobs = UserORM.from_rows(result_set)
        formatted_jobs = UserResponse.from_orms(
            parsed_jobs, FormatRequest(utc_timezone="00:00")
        )

        self.assertEqual(
            formatted_jobs,
            [
                UserResponse(
                    project_name="p_zih-psw-tools",
                    job_count=2,
                    max_node_count=1,
                    max_core_count=16,
                    max_gpu_count=8,
                    footprint_count=1,
                    max_pending_time="00d 00h 00min 18s",
                    sum_duration="0000y 000d 00h 05min",
                    sum_core_duration="1.46",
                    user_name="beju577d",
                    max_time_limit="00d 08h 00min 00s",
                )
            ],
        )

    def test_fsname_case(self) -> None:
        """test user view fsname case"""

        query = UserQuery.build(JobDataFilter(fsname="ext4"))
        result_set = self.memory_db.execute(query)
        parsed_jobs = UserORM.from_rows(result_set)
        formatted_jobs = UserResponse.from_orms(
            parsed_jobs, FormatRequest(utc_timezone="00:00")
        )

        self.assertEqual(
            formatted_jobs,
            [
                UserResponse(
                    project_name="p_zih-psw-tools",
                    job_count=1,
                    max_node_count=1,
                    max_core_count=16,
                    max_gpu_count=8,
                    footprint_count=1,
                    max_pending_time="00d 00h 00min 09s",
                    sum_duration="0000y 000d 00h 05min",
                    sum_core_duration="1.46",
                    user_name="beju577d",
                    max_time_limit="00d 00h 31min 00s",
                ),
                UserResponse(
                    project_name="p_zih-psw-tools",
                    job_count=1,
                    max_node_count=1,
                    max_core_count=1,
                    max_gpu_count=0,
                    footprint_count=1,
                    max_pending_time="00d 00h 00min 07s",
                    sum_duration="0000y 000d 00h 20min",
                    sum_core_duration="0.34",
                    user_name="notbeju577d",
                    max_time_limit="00d 08h 00min 00s",
                ),
            ],
        )

    def test_footprint_case(self) -> None:
        """testing projectquery footprint case"""

        query = UserQuery.build(
            JobDataFilter(footprint=[FootprintFilter(name="used_mean_per_gpu")])
        )
        result_set = self.memory_db.execute(query)
        typesafe_result = UserORM.from_rows(result_set)
        restapi_result = UserResponse.from_orms(
            typesafe_result, FormatRequest(utc_timezone="00:00")
        )

        self.assertEqual(
            restapi_result,
            [
                UserResponse(
                    project_name="p_zih-psw-tools",
                    job_count=1,
                    max_node_count=1,
                    max_core_count=16,
                    max_gpu_count=8,
                    footprint_count=1,
                    max_pending_time="00d 00h 00min 09s",
                    sum_duration="0000y 000d 00h 05min",
                    sum_core_duration="1.46",
                    user_name="beju577d",
                    max_time_limit="00d 00h 31min 00s",
                )
            ],
        )


class JobNameTest(TestCase):
    """unittesting jobname view"""

    def setUp(self) -> None:
        self.memory_db = MemoryDB("sql.view.topdown.test.JobNameTest")

        for job in jobs:
            self.memory_db.add(JobDataORM(**job))

        for f in base_footprints:
            self.memory_db.add(FootprintBaseORM(**f))

        for f in filio_footprints:
            self.memory_db.add(FootprintFileioORM(**f))

        for f in gpu_fooltprints:
            self.memory_db.add(FootprintGpuORM(**f))

        return super().setUp()

    def test_without_filter(self) -> None:
        """test with empty filter"""

        query = JobNameQuery.build(JobDataFilter())
        result_set = self.memory_db.execute(query)
        parsed_jobs = JobNameOrm.from_rows(result_set)
        formatted_jobs = JobNameResponse.from_orms(
            parsed_jobs, FormatRequest(utc_timezone="00:00")
        )

        self.assertEqual(
            formatted_jobs,
            [
                JobNameResponse(
                    project_name="p_zih-psw-tools",
                    job_count=1,
                    max_node_count=1,
                    max_core_count=1,
                    max_gpu_count=0,
                    footprint_count=0,
                    max_pending_time="00d 00h 00min 18s",
                    sum_duration="0000y 000d 00h 00min",
                    sum_core_duration="0.00",
                    user_name="beju577d",
                    max_time_limit="00d 08h 00min 00s",
                    job_name="gromacs.sh",
                ),
                JobNameResponse(
                    project_name="p_zih-psw-tools",
                    job_count=2,
                    max_node_count=1,
                    max_core_count=16,
                    max_gpu_count=8,
                    footprint_count=2,
                    max_pending_time="00d 00h 00min 09s",
                    sum_duration="0000y 000d 00h 25min",
                    sum_core_duration="1.80",
                    user_name="beju577d",
                    max_time_limit="00d 08h 00min 00s",
                    job_name="interactive",
                ),
            ],
        )

    def test_with_filter(self) -> None:
        """test with filter"""

        query = JobNameQuery.build(JobDataFilter(job_name="gromacs.sh"))
        result_set = self.memory_db.execute(query)
        parsed_jobs = JobNameOrm.from_rows(result_set)
        formatted_jobs = JobNameResponse.from_orms(
            parsed_jobs, FormatRequest(utc_timezone="00:00")
        )

        self.assertEqual(
            formatted_jobs,
            [
                JobNameResponse(
                    project_name="p_zih-psw-tools",
                    job_count=1,
                    max_node_count=1,
                    max_core_count=1,
                    max_gpu_count=0,
                    footprint_count=0,
                    max_pending_time="00d 00h 00min 18s",
                    sum_duration="0000y 000d 00h 00min",
                    sum_core_duration="0.00",
                    user_name="beju577d",
                    max_time_limit="00d 08h 00min 00s",
                    job_name="gromacs.sh",
                )
            ],
        )

    def test_fsname_case(self) -> None:
        """testing jobnamequery fsname case"""

        query = JobNameQuery.build(JobDataFilter(fsname="ext4"))
        result_set = self.memory_db.execute(query)
        parsed_jobs = JobNameOrm.from_rows(result_set)
        formatted_jobs = JobNameResponse.from_orms(
            parsed_jobs, FormatRequest(utc_timezone="00:00")
        )

        self.assertEqual(
            formatted_jobs,
            [
                JobNameResponse(
                    project_name="p_zih-psw-tools",
                    job_count=2,
                    max_node_count=1,
                    max_core_count=16,
                    max_gpu_count=8,
                    footprint_count=2,
                    max_pending_time="00d 00h 00min 09s",
                    sum_duration="0000y 000d 00h 25min",
                    sum_core_duration="1.80",
                    user_name="beju577d",
                    max_time_limit="00d 08h 00min 00s",
                    job_name="interactive",
                )
            ],
        )

    def test_footprint_case(self) -> None:
        """testing projectquery footprint case"""

        query = JobNameQuery.build(
            JobDataFilter(footprint=[FootprintFilter(name="used_mean_per_gpu")])
        )
        result_set = self.memory_db.execute(query)
        typesafe_result = JobNameOrm.from_rows(result_set)
        restapi_result = JobNameResponse.from_orms(
            typesafe_result, FormatRequest(utc_timezone="00:00")
        )

        self.assertEqual(
            restapi_result,
            [
                JobNameResponse(
                    project_name="p_zih-psw-tools",
                    job_count=1,
                    max_node_count=1,
                    max_core_count=16,
                    max_gpu_count=8,
                    footprint_count=1,
                    max_pending_time="00d 00h 00min 09s",
                    sum_duration="0000y 000d 00h 05min",
                    sum_core_duration="1.46",
                    user_name="beju577d",
                    max_time_limit="00d 00h 31min 00s",
                    job_name="interactive",
                )
            ],
        )


class JobIDTest(TestCase):
    """unittesting jobid view"""

    def setUp(self) -> None:
        self.memory_db = MemoryDB("sql.view.topdown.test.JobIDTest")

        for job in jobs:
            self.memory_db.add(JobDataORM(**job))

        for f in base_footprints:
            self.memory_db.add(FootprintBaseORM(**f))

        for f in filio_footprints:
            self.memory_db.add(FootprintFileioORM(**f))

        for f in gpu_fooltprints:
            self.memory_db.add(FootprintGpuORM(**f))

        return super().setUp()

    def test_without_filter(self) -> None:
        """test with empty filter"""

        query = JobIDQuery.build(JobDataFilter())
        result_set = self.memory_db.execute(query)
        parsed_jobs = JobIDOrm.from_rows(result_set)
        formatted_jobs = JobIDResponse.from_orms(
            parsed_jobs, FormatRequest(utc_timezone="+02:00")
        )

        self.assertEqual(
            formatted_jobs,
            [
                {
                    "job_id": 29668722,
                    "project_name": "p_zih-psw-tools",
                    "user_name": "beju577d",
                    "start_time": "04/10/2022 10:42:24",
                    "job_name": "gromacs.sh",
                    "end_time": "04/10/2022 10:42:25",
                    "job_state": "failed",
                    "node_count": 1,
                    "node_list": "taurusi6604",
                    "core_count": 1,
                    "core_list": "taurusi6604[13]",
                    "gpu_list": "",
                    "gpu_count": 0,
                    "pending_time": "00d 00h 00min 18s",
                    "time_limit": "00d 08h 00min 00s",
                    "exclusive": 0,
                    "partition_name": "haswell64",
                    "job_duration": "00d 00h 00min 01s",
                    "core_duration": "0.00",
                    "property_id": 0,
                    "array_id": 0,
                    "mem_per_cpu": None,
                    "mem_per_node": None,
                    "reservation_id": None,
                    "job_duration_time_limit_ratio": 0.0,
                    "start_unixtime": 1664872944,
                },
                {
                    "job_id": 29668639,
                    "project_name": "p_zih-psw-tools",
                    "user_name": "beju577d",
                    "start_time": "04/10/2022 10:29:40",
                    "job_name": "interactive",
                    "end_time": "04/10/2022 10:35:08",
                    "job_state": "completed",
                    "node_count": 1,
                    "node_list": "taurusi6308",
                    "core_count": 16,
                    "core_list": "taurusi6308[0-11,16-19]",
                    "gpu_list": "",
                    "gpu_count": 8,
                    "pending_time": "00d 00h 00min 09s",
                    "time_limit": "00d 00h 31min 00s",
                    "exclusive": 0,
                    "partition_name": "haswell64",
                    "job_duration": "00d 00h 05min 28s",
                    "core_duration": "1.46",
                    "property_id": 0,
                    "array_id": 0,
                    "mem_per_cpu": None,
                    "mem_per_node": None,
                    "reservation_id": None,
                    "job_duration_time_limit_ratio": 17.63,
                    "start_unixtime": 1664872180,
                },
                {
                    "job_id": 29667954,
                    "project_name": "p_zih-psw-tools",
                    "user_name": "notbeju577d",
                    "start_time": "04/10/2022 10:04:25",
                    "job_name": "interactive",
                    "end_time": "04/10/2022 10:24:45",
                    "job_state": "completed",
                    "node_count": 1,
                    "node_list": "taurusi6603",
                    "core_count": 1,
                    "core_list": "taurusi6603[12]",
                    "gpu_list": "",
                    "gpu_count": 0,
                    "pending_time": "00d 00h 00min 07s",
                    "time_limit": "00d 08h 00min 00s",
                    "exclusive": 0,
                    "partition_name": "haswell64",
                    "job_duration": "00d 00h 20min 20s",
                    "core_duration": "0.34",
                    "property_id": 0,
                    "array_id": 0,
                    "mem_per_cpu": None,
                    "mem_per_node": None,
                    "reservation_id": None,
                    "job_duration_time_limit_ratio": 4.24,
                    "start_unixtime": 1664870665,
                },
            ],
        )

    def test_with_filter(self) -> None:
        """test with filter"""

        query = JobIDQuery.build(JobDataFilter(job_name="gromacs.sh"))
        result_set = self.memory_db.execute(query)
        parsed_jobs = JobIDOrm.from_rows(result_set)
        formatted_jobs = JobIDResponse.from_orms(
            parsed_jobs, FormatRequest(utc_timezone="+02:00")
        )
        self.assertEqual(
            formatted_jobs,
            [
                {
                    "job_id": 29668722,
                    "project_name": "p_zih-psw-tools",
                    "user_name": "beju577d",
                    "start_time": "04/10/2022 10:42:24",
                    "job_name": "gromacs.sh",
                    "end_time": "04/10/2022 10:42:25",
                    "job_state": "failed",
                    "node_count": 1,
                    "node_list": "taurusi6604",
                    "core_count": 1,
                    "core_list": "taurusi6604[13]",
                    "gpu_list": "",
                    "gpu_count": 0,
                    "pending_time": "00d 00h 00min 18s",
                    "time_limit": "00d 08h 00min 00s",
                    "exclusive": 0,
                    "partition_name": "haswell64",
                    "job_duration": "00d 00h 00min 01s",
                    "core_duration": "0.00",
                    "property_id": 0,
                    "array_id": 0,
                    "mem_per_cpu": None,
                    "mem_per_node": None,
                    "reservation_id": None,
                    "job_duration_time_limit_ratio": 0.0,
                    "start_unixtime": 1664872944,
                }
            ],
        )

    def test_fsname_case(self) -> None:
        """testing jobid fsname case"""

        query = JobIDQuery.build(JobDataFilter(fsname="ext4"))
        result_set = self.memory_db.execute(query)
        typesafe_result = JobIDOrm.from_rows(result_set)
        restapi_result = JobIDResponse.from_orms(
            typesafe_result, FormatRequest(utc_timezone="+02:00")
        )

        self.assertEqual(
            restapi_result,
            [
                {
                    "job_id": 29668639,
                    "project_name": "p_zih-psw-tools",
                    "user_name": "beju577d",
                    "start_time": "04/10/2022 10:29:40",
                    "job_name": "interactive",
                    "end_time": "04/10/2022 10:35:08",
                    "job_state": "completed",
                    "node_count": 1,
                    "node_list": "taurusi6308",
                    "core_count": 16,
                    "core_list": "taurusi6308[0-11,16-19]",
                    "gpu_list": "",
                    "gpu_count": 8,
                    "pending_time": "00d 00h 00min 09s",
                    "time_limit": "00d 00h 31min 00s",
                    "exclusive": 0,
                    "partition_name": "haswell64",
                    "job_duration": "00d 00h 05min 28s",
                    "core_duration": "1.46",
                    "property_id": 0,
                    "array_id": 0,
                    "mem_per_cpu": None,
                    "mem_per_node": None,
                    "reservation_id": None,
                    "job_duration_time_limit_ratio": 17.63,
                    "start_unixtime": 1664872180,
                },
                {
                    "job_id": 29667954,
                    "project_name": "p_zih-psw-tools",
                    "user_name": "notbeju577d",
                    "start_time": "04/10/2022 10:04:25",
                    "job_name": "interactive",
                    "end_time": "04/10/2022 10:24:45",
                    "job_state": "completed",
                    "node_count": 1,
                    "node_list": "taurusi6603",
                    "core_count": 1,
                    "core_list": "taurusi6603[12]",
                    "gpu_list": "",
                    "gpu_count": 0,
                    "pending_time": "00d 00h 00min 07s",
                    "time_limit": "00d 08h 00min 00s",
                    "exclusive": 0,
                    "partition_name": "haswell64",
                    "job_duration": "00d 00h 20min 20s",
                    "core_duration": "0.34",
                    "property_id": 0,
                    "array_id": 0,
                    "mem_per_cpu": None,
                    "mem_per_node": None,
                    "reservation_id": None,
                    "job_duration_time_limit_ratio": 4.24,
                    "start_unixtime": 1664870665,
                },
            ],
        )

    def test_footprint_case(self) -> None:
        """testing projectquery footprint case"""

        query = JobIDQuery.build(
            JobDataFilter(footprint=[FootprintFilter(name="used_mean_per_gpu")])
        )
        result_set = self.memory_db.execute(query)
        typesafe_result = JobIDOrm.from_rows(result_set)
        restapi_result = JobIDResponse.from_orms(
            typesafe_result, FormatRequest(utc_timezone="+02:00")
        )

        self.assertEqual(
            restapi_result,
            [
                {
                    "job_id": 29668639,
                    "project_name": "p_zih-psw-tools",
                    "user_name": "beju577d",
                    "start_time": "04/10/2022 10:29:40",
                    "job_name": "interactive",
                    "end_time": "04/10/2022 10:35:08",
                    "job_state": "completed",
                    "node_count": 1,
                    "node_list": "taurusi6308",
                    "core_count": 16,
                    "core_list": "taurusi6308[0-11,16-19]",
                    "gpu_list": "",
                    "gpu_count": 8,
                    "pending_time": "00d 00h 00min 09s",
                    "time_limit": "00d 00h 31min 00s",
                    "exclusive": 0,
                    "partition_name": "haswell64",
                    "job_duration": "00d 00h 05min 28s",
                    "core_duration": "1.46",
                    "property_id": 0,
                    "array_id": 0,
                    "mem_per_cpu": None,
                    "mem_per_node": None,
                    "reservation_id": None,
                    "used_mean_per_gpu": 1.1,
                    "job_duration_time_limit_ratio": 17.63,
                    "start_unixtime": 1664872180,
                }
            ],
        )


class JobIDEverythingTest(TestCase):
    """unittest for jobideveryting view"""

    def setUp(self) -> None:
        self.memory_db = MemoryDB("sql.view.topdown.test.JobIDEverythingTest")

        for job in jobs:
            self.memory_db.add(JobDataORM(**job))

        for f in base_footprints:
            self.memory_db.add(FootprintBaseORM(**f))

        for f in filio_footprints:
            self.memory_db.add(FootprintFileioORM(**f))

        for f in gpu_fooltprints:
            self.memory_db.add(FootprintGpuORM(**f))

        for f in analysis_footprint:
            self.memory_db.add(FootprintAnalysisORM(**f))

        return super().setUp()

    def test_with_filter(self) -> None:
        "test with filter"

        query = JobIDEverythingQuery.build(
            JobDataFilter(user_name="notbeju577d")
        )
        result_set = self.memory_db.execute(query)
        typesafe_result = JobIDEverythingORM.from_rows(result_set)
        restapi_result = JobIDEverythingResponse.from_orms(
            typesafe_result, FormatRequest(utc_timezone="+02:00")
        )

        self.assertEqual(
            restapi_result,
            [
                JobIDEverythingResponse(
                    uid=42227220,
                    job_id=29667954,
                    user_name="notbeju577d",
                    project_name="p_zih-psw-tools",
                    start_unixtime=1664870665,
                    end_unixtime=1664871885,
                    job_state="completed",
                    node_count=1,
                    node_list="taurusi6603",
                    core_list="taurusi6603[12]",
                    core_count=1,
                    submit_time="04/10/2022 10:04:18",
                    start_time="04/10/2022 10:04:25",
                    end_time="04/10/2022 10:24:45",
                    job_name="interactive",
                    partition_name="haswell64",
                    exclusive=0,
                    property_id=0,
                    array_id=0,
                    tags=0,
                    footprint_created=1,
                    gpu_count=0,
                    gpu_list="",
                    smt_mode=1,
                    job_script="(null)",
                    mem_per_cpu=None,
                    mem_per_node=None,
                    reservation_id=None,
                    core_idle_ratio=0.0,
                    core_unused_ratio=2.2,
                    core_load_imbalance=2.2,
                    core_duration_idle="0.00",
                    io_blocking=2,
                    mem_leak=2.2,
                    gpu_idle_ratio=0.0,
                    gpu_unused_ratio=2.2,
                    gpu_load_imbalance=2.2,
                    gpu_duration_idle="0.00",
                    gpu_duration=0,
                    synchronous_offloading=2,
                    io_blocking_detail=None,
                    io_congestion=2,
                    severity=2,
                    ipc_mean_per_core=5.11,
                    cpu_used_mean_per_core=1.11,
                    flops_any_mean_per_core=1.11,
                    mem_bw_mean_per_socket=1.11,
                    cpu_power_mean_per_socket=1.11,
                    ib_bw_mean_per_node=1.11,
                    host_mem_used_max_per_node=None,
                    energy_in_kwh="n/a",
                    energy_efficiency_in_gflops_per_watt="n/a",
                    read_bytes=23,
                    write_bytes=None,
                    fsname="ext4",
                    used_mean_per_gpu=None,
                    mem_used_max_per_gpu=None,
                    power_mean_per_gpu=None,
                    time_limit="8h 0s",
                    job_duration_time_limit_ratio=4.24,
                    pending_time="7s",
                    job_duration="20min 20s",
                    core_duration="0.34",
                    elapsed_time=None,
                    unused_mem=0,
                )
            ],
        )
