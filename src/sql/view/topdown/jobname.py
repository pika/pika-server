"""
topdown.jobname view
"""

from typing import Any, cast

from sqlalchemy import func, select
from sqlalchemy.orm import Query

from utils.model import FormatRequest

from ...model import (
    FootprintBaseORM,
    FootprintFileioORM,
    FootprintGpuORM,
    JobDataFilter,
    JobDataORM,
)
from ..interface import ORMInterface, ORMType, QueryFilterType, QueryInterface
from .user import UserORM, UserQuery, UserResponse

# mypy: ignore-errors


class JobNameQuery(QueryInterface):
    """topdown.jobname sqlalchemy query builder"""

    @staticmethod
    def get_selected_columns() -> list[Any]:
        columns = UserQuery.get_selected_columns()
        return [*columns, JobDataORM.job_name]

    @staticmethod
    def get_query(
        selected_columns: list[Any],
        query_filter: QueryFilterType,
    ) -> Query[ORMType]:
        query_filter = cast(JobDataFilter, query_filter)

        if query_filter.footprint is not None:
            sql_query = (
                select(*selected_columns)
                .select_from(JobDataORM)
                .outerjoin(
                    FootprintBaseORM, JobDataORM.uid == FootprintBaseORM.uid
                )
                .outerjoin(
                    FootprintFileioORM,
                    JobDataORM.uid == FootprintFileioORM.uid,
                )
                .outerjoin(
                    FootprintGpuORM, JobDataORM.uid == FootprintGpuORM.uid
                )
            )
        elif query_filter.fsname is not None:
            sql_query = (
                select(*selected_columns)
                .select_from(JobDataORM)
                .outerjoin(
                    FootprintFileioORM,
                    JobDataORM.uid == FootprintFileioORM.uid,
                )
            )
        else:
            sql_query = select(*selected_columns)

        sql_query = query_filter.apply_on_query(sql_query)

        sql_query = sql_query.group_by(JobDataORM.job_name).order_by(
            func.max(JobDataORM.start_time).desc()
        )

        return sql_query


class JobNameOrm(UserORM):
    """typesafe topdown.jobname sqlalchemy result"""

    job_name: str | None

    @staticmethod
    def from_row(result_set: dict[str, Any]) -> "JobNameOrm":
        return JobNameOrm(**result_set)


class JobNameResponse(UserResponse):
    """topdown.jobname formatted response"""

    job_name: str | None

    @staticmethod
    # type: ignore[override]
    def from_orm(
        orm: ORMInterface, request: FormatRequest
    ) -> "JobNameResponse":
        user_dict = UserResponse.from_orm(orm, request).__dict__
        jobname_dict = orm.__dict__

        jobname_dict.update(user_dict)

        return JobNameResponse(**jobname_dict)
