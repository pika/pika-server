"""
pikas main approach to visualize job data most efficiently (also called the
topdown approach) is to show jobs:

1. ordered by a project (one project has many users)
2. ordered by a user (one user can send many jobs)
3. ordered by job name (one job name can represent many jobs)
4. job id (one jobid is supposed to represent one job, due to
decision made by the taurus/slurm operators, jobids arent unique because
synchronising thousands of computers would be to much overhead)
5. job id everything (this last point, gives us every information possible
we can find related to this single job, querying everytime this much information
would be unnesecary)
"""

from .jobid import JobIDOrm, JobIDQuery, JobIDResponse
from .jobideverything import (
    JobIDEverythingORM,
    JobIDEverythingQuery,
    JobIDEverythingResponse,
)
from .jobname import JobNameOrm, JobNameQuery, JobNameResponse
from .project import ProjectORM, ProjectQuery, ProjectResponse
from .user import UserORM, UserQuery, UserResponse
