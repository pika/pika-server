"""topdown.jobideverything view"""

from time import time
from typing import Any

from sqlalchemy.orm import Query
from sqlmodel import select

from utils.common import (
    format_date,
    format_duration,
    format_duration_year,
    format_hour,
)
from utils.model import FormatRequest

from ...model import (
    FootprintAnalysisORM,
    FootprintBaseORM,
    FootprintFileioORM,
    FootprintGpuORM,
    JobDataORM,
)
from ..interface import (
    ORMInterface,
    ORMType,
    QueryFilterType,
    QueryInterface,
    ResponseInterface,
)

# mypy: ignore-errors


class JobIDEverythingORM(ORMInterface):
    """topdown.jobideverything sqlalchemy result"""

    uid: int
    job_id: int
    user_name: str | None
    project_name: str | None
    job_state: str | None
    node_count: int | None
    node_list: str | None
    core_list: str | None
    core_count: int | None
    submit_time: int | None
    start_time: int | None
    end_time: int | None
    job_name: str | None
    time_limit: int | None
    partition_name: str | None
    exclusive: int | None
    property_id: int | None
    array_id: int | None
    tags: int | None
    footprint_created: int | None
    gpu_count: int | None
    gpu_list: str | None
    smt_mode: int | None
    job_script: str | None
    mem_per_cpu: int | None
    mem_per_node: int | None
    reservation_id: int | None

    core_idle_ratio: float | None
    core_unused_ratio: float | None
    core_load_imbalance: float | None
    core_duration_idle: int | None
    core_duration: int | None
    io_blocking: int | None
    mem_leak: float | None
    gpu_idle_ratio: float | None
    gpu_unused_ratio: float | None
    gpu_load_imbalance: float | None
    gpu_duration_idle: int | None
    gpu_duration: int | None
    synchronous_offloading: int | None
    io_blocking_detail: str | None
    io_congestion: int | None
    severity: int | None
    unused_mem: float | None

    ipc_mean_per_core: float | None
    cpu_used_mean_per_core: float | None
    flops_any_mean_per_core: float | None
    mem_bw_mean_per_socket: float | None
    cpu_power_mean_per_socket: float | None
    ib_bw_mean_per_node: float | None
    host_mem_used_max_per_node: float | None
    energy_in_kwh: float | None
    energy_efficiency_in_gflops_per_watt: float | None

    read_bytes: int | None
    write_bytes: int | None
    fsname: str | None

    used_mean_per_gpu: float | None
    mem_used_max_per_gpu: float | None
    power_mean_per_gpu: float | None

    @staticmethod
    def from_row(result_set: dict[str, Any]) -> "JobIDEverythingORM":
        return JobIDEverythingORM(**result_set)


class JobIDEverythingQuery(QueryInterface):
    """topdown.jobideverything sqlalchemy query builder"""

    @staticmethod
    def get_selected_columns() -> list[Any]:
        return [
            JobDataORM.uid,
            JobDataORM.job_id,
            JobDataORM.user_name,
            JobDataORM.project_name,
            JobDataORM.job_state,
            JobDataORM.node_count,
            JobDataORM.node_list,
            JobDataORM.core_list,
            JobDataORM.core_count,
            JobDataORM.submit_time,
            JobDataORM.start_time,
            JobDataORM.end_time,
            JobDataORM.job_name,
            JobDataORM.time_limit,
            JobDataORM.partition_name,
            JobDataORM.exclusive,
            JobDataORM.property_id,
            JobDataORM.array_id,
            JobDataORM.tags,
            JobDataORM.footprint_created,
            JobDataORM.gpu_count,
            JobDataORM.gpu_list,
            JobDataORM.smt_mode,
            JobDataORM.job_script,
            JobDataORM.mem_per_cpu,
            JobDataORM.mem_per_node,
            JobDataORM.reservation_id,
            FootprintAnalysisORM.core_idle_ratio,
            JobDataORM.core_duration(),
            FootprintAnalysisORM.core_unused_ratio(),
            FootprintAnalysisORM.core_load_imbalance,
            FootprintAnalysisORM.core_duration_idle,
            FootprintAnalysisORM.io_blocking,
            FootprintAnalysisORM.mem_leak,
            FootprintAnalysisORM.gpu_idle_ratio,
            JobDataORM.gpu_duration(),
            FootprintAnalysisORM.gpu_unused_ratio(),
            FootprintAnalysisORM.gpu_load_imbalance,
            FootprintAnalysisORM.gpu_duration_idle,
            FootprintAnalysisORM.synchronous_offloading,
            FootprintAnalysisORM.io_blocking_detail,
            FootprintAnalysisORM.io_congestion,
            FootprintAnalysisORM.severity,
            FootprintAnalysisORM.unused_mem,
            FootprintBaseORM.ipc_mean_per_core,
            FootprintBaseORM.cpu_used_mean_per_core,
            FootprintBaseORM.flops_any_mean_per_core,
            FootprintBaseORM.mem_bw_mean_per_socket,
            FootprintBaseORM.cpu_power_mean_per_socket,
            FootprintBaseORM.ib_bw_mean_per_node,
            FootprintBaseORM.host_mem_used_max_per_node,
            FootprintBaseORM.energy_in_kwh,
            FootprintBaseORM.energy_efficiency_in_gflops_per_watt,
            FootprintFileioORM.read_bytes,
            FootprintFileioORM.write_bytes,
            FootprintFileioORM.fsname,
            FootprintGpuORM.used_mean_per_gpu,
            FootprintGpuORM.mem_used_max_per_gpu,
            FootprintGpuORM.power_mean_per_gpu,
        ]

    @staticmethod
    def get_query(
        selected_columns: list[Any],
        query_filter: QueryFilterType,
    ) -> Query[ORMType]:
        sql_query = (
            select(*selected_columns)
            .outerjoin(
                FootprintAnalysisORM,
                JobDataORM.uid == FootprintAnalysisORM.uid,
            )
            .outerjoin(
                FootprintBaseORM,
                JobDataORM.uid == FootprintBaseORM.uid,
            )
            .outerjoin(
                FootprintFileioORM,
                JobDataORM.uid == FootprintFileioORM.uid,
            )
            .outerjoin(
                FootprintGpuORM,
                JobDataORM.uid == FootprintGpuORM.uid,
            )
        )

        sql_query = query_filter.apply_on_query(sql_query)

        return sql_query


class JobIDEverythingResponse(ResponseInterface):
    """topdown.jobideverything formatted result"""

    uid: int
    job_id: int
    user_name: str | None
    project_name: str | None
    job_state: str | None
    node_count: int | None
    node_list: str | None
    core_list: str | None
    core_count: int | None
    submit_time: str | None
    start_time: str | None
    end_time: str | None
    job_name: str | None
    partition_name: str | None
    exclusive: int | None
    property_id: int | None
    array_id: int | None
    tags: int | None
    footprint_created: int | None
    gpu_count: int | None
    gpu_list: str | None
    smt_mode: int | None
    job_script: str | None
    mem_per_cpu: int | None
    mem_per_node: int | None
    reservation_id: int | None

    core_idle_ratio: float | None
    core_unused_ratio: float | None
    core_load_imbalance: float | None
    core_duration_idle: str | None
    io_blocking: int | None
    mem_leak: float | None
    gpu_idle_ratio: float | None
    gpu_unused_ratio: float | None
    gpu_load_imbalance: float | None
    gpu_duration_idle: str | None
    gpu_duration: int | None
    synchronous_offloading: int | None
    io_blocking_detail: str | None
    io_congestion: int | None
    severity: int | None
    unused_mem: float | None

    ipc_mean_per_core: float | None
    cpu_used_mean_per_core: float | None
    flops_any_mean_per_core: float | None
    mem_bw_mean_per_socket: float | None
    cpu_power_mean_per_socket: float | None
    ib_bw_mean_per_node: float | None
    host_mem_used_max_per_node: float | None
    energy_in_kwh: str | None
    energy_efficiency_in_gflops_per_watt: str | None

    read_bytes: int | None
    write_bytes: int | None
    fsname: str | None

    used_mean_per_gpu: float | None
    mem_used_max_per_gpu: float | None
    power_mean_per_gpu: float | None

    time_limit: str

    job_duration_time_limit_ratio: float | None
    pending_time: str | None
    job_duration: str | None
    core_duration: str | None
    elapsed_time: str | None
    start_unixtime: int | None
    end_unixtime: int | None

    @staticmethod
    def from_orm(
        orm: ORMInterface, request: FormatRequest
    ) -> "JobIDEverythingResponse":
        response = orm.__dict__

        # live job aware
        if response["end_time"] is None:
            response["end_time"] = int(time())
            response["elapsed_time"] = format_duration(
                response["end_time"] - response["start_time"], True
            )
        else:
            response["elapsed_time"] = None

        # set Nones to 0 on ints
        int_keys = [
            "gpu_count",
            "smt_mode",
            "gpu_duration_idle",
            "gpu_duration",
        ]
        for key in int_keys:
            if response.get(key) is None:
                response[key] = 0

        # aditional info
        try:
            response["job_duration_time_limit_ratio"] = round(
                (response["end_time"] - response["start_time"])
                * 100
                / response["time_limit"],
                2,
            )
        except TypeError:
            response["job_duration_time_limit_ratio"] = 0

        try:
            response["pending_time"] = format_duration(
                response["start_time"] - response["submit_time"], True
            )
        except TypeError:
            response["pending_time"] = "n/a"

        response["job_duration"] = format_duration(
            response["end_time"] - response["start_time"], True
        )

        try:
            response["time_limit"] = format_duration(
                response["time_limit"], True
            )
        except TypeError:
            response["time_limit"] = "n/a"

        try:
            response["core_idle_ratio"] = str(
                round(
                    response["core_duration_idle"] / response["core_duration"],
                    2,
                )
            )
        except TypeError:
            response["core_idle_ratio"] = 0

        try:
            response["core_duration_idle"] = format_hour(
                response["core_duration_idle"]
            )
        except TypeError:
            response["core_duration_idle"] = format_hour(0)

        try:
            response["core_duration"] = format_hour(
                response["core_count"]
                * (response["end_time"] - response["start_time"])
            )
        except TypeError:
            response["core_duration"] = format_hour(0)

        if response["gpu_duration_idle"] > 0 and response["gpu_duration"] > 0:
            response["gpu_idle_ratio"] = str(
                round(
                    response["gpu_duration_idle"] / response["gpu_duration"],
                    2,
                )
            )
            response["gpu_duration_idle"] = format_hour(
                response["gpu_duration_idle"]
            )
        else:
            response["gpu_idle_ratio"] = 0
            response["gpu_duration_idle"] = format_hour(0)

        response["start_unixtime"] = response["start_time"]
        response["end_unixtime"] = response["end_time"]

        # timezone aware
        time_keys = ["start_time", "end_time", "submit_time"]
        for key in time_keys:
            if response[key] is not None:
                response[key] = request.translate_timezone(response[key])
                response[key] = format_date(response[key], request.time_format)

        try:
            if response["energy_in_kwh"] is not None:
                response["energy_in_kwh"] = f"{response["energy_in_kwh"]} kWh"
            else:
                response["energy_in_kwh"] = "n/a"
            if response["energy_efficiency_in_gflops_per_watt"] is not None:
                response["energy_efficiency_in_gflops_per_watt"] = (
                    f"{response["energy_efficiency_in_gflops_per_watt"]} GFLOPS/W"
                )
            else:
                response["energy_efficiency_in_gflops_per_watt"] = "n/a"
        except TypeError:
            response["energy_in_kwh"] = "n/a"
            response["energy_efficiency_in_gflops_per_watt"] = "n/a"

        return JobIDEverythingResponse(**response)
