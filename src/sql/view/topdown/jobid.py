"""
topdown.jobid view
"""

from time import time
from typing import Any, cast

from sqlalchemy import func, literal_column, select
from sqlalchemy.orm import Query

from utils.common import (
    format_date,
    format_duration,
    format_duration_year,
    format_hour,
)
from utils.model import FormatRequest

from ...model import (
    FootprintBaseORM,
    FootprintFileioORM,
    FootprintGpuORM,
    JobDataFilter,
    JobDataORM,
)
from ..interface import (
    ORMInterface,
    ORMType,
    QueryFilterType,
    QueryInterface,
    ResponseInterface,
)

# mypy: ignore-errors


class JobIDOrm(ORMInterface):
    """topdown.jobid sqlalchemy result

    impl detail:
    caution, this view cant provide any typesafe wrappers as the user
    can select what additional performance metric is about to be in the result
    set, we are still using those classes for uniform handling
    """

    @staticmethod
    def from_row(result_set: dict[str, Any]) -> ORMInterface:
        return result_set


class JobIDQuery(QueryInterface):
    """topdown.jobid sqlalchemy query builder"""

    @staticmethod
    def get_selected_columns() -> list[Any]:
        return [
            JobDataORM.job_id,
            JobDataORM.project_name,
            JobDataORM.user_name,
            JobDataORM.start_time,
            JobDataORM.job_name,
            JobDataORM.end_time,
            JobDataORM.job_state,
            JobDataORM.node_count,
            JobDataORM.node_list,
            JobDataORM.core_count,
            JobDataORM.core_list,
            JobDataORM.gpu_list,
            JobDataORM.get_gpu_count(),
            JobDataORM.pending_time(),
            JobDataORM.time_limit,
            JobDataORM.exclusive,
            JobDataORM.partition_name,
            JobDataORM.duration(),
            JobDataORM.core_duration(),
            JobDataORM.property_id,
            JobDataORM.array_id,
            JobDataORM.mem_per_cpu,
            JobDataORM.mem_per_node,
            JobDataORM.reservation_id,
        ]

    @staticmethod
    def get_query(
        selected_columns: list[Any],
        query_filter: QueryFilterType,
    ) -> Query[ORMType]:
        query_filter = cast(JobDataFilter, query_filter)

        if query_filter.footprint is not None:
            sql_query = (
                select(
                    *selected_columns,
                    func.round(
                        literal_column(query_filter.footprint[0].name), 3
                    ).label(query_filter.footprint[0].name),
                )
                .select_from(JobDataORM)
                .outerjoin(
                    FootprintBaseORM, JobDataORM.uid == FootprintBaseORM.uid
                )
                .outerjoin(
                    FootprintFileioORM,
                    JobDataORM.uid == FootprintFileioORM.uid,
                )
                .outerjoin(
                    FootprintGpuORM, JobDataORM.uid == FootprintGpuORM.uid
                )
            )
        elif query_filter.fsname is not None:
            sql_query = (
                select(*selected_columns)
                .select_from(JobDataORM)
                .outerjoin(
                    FootprintFileioORM,
                    JobDataORM.uid == FootprintFileioORM.uid,
                )
            )
        else:
            sql_query = select(*selected_columns)

        sql_query = query_filter.apply_on_query(sql_query)

        sql_query = sql_query.order_by(JobDataORM.start_time.desc())

        return sql_query


class JobIDResponse(ResponseInterface):
    """topdown.jobid formated response"""

    @staticmethod
    # type: ignore[override]
    def from_orm(orm: ORMInterface, request: FormatRequest) -> "JobIDResponse":
        jobid_resp = cast(dict[str, Any], orm)

        if jobid_resp["gpu_count"] is None:
            jobid_resp["gpu_count"] = 0

        if jobid_resp["job_state"] == "running":
            jobid_resp["elapsed_time"] = format_duration(
                time() - jobid_resp["start_time"]
            )
        else:
            jobid_resp["job_duration_time_limit_ratio"] = round(
                (jobid_resp["end_time"] - jobid_resp["start_time"])
                * 100
                / jobid_resp["time_limit"],
                2,
            )
            jobid_resp["job_duration"] = format_duration(
                jobid_resp["job_duration"]
            )
            jobid_resp["core_duration"] = format_hour(
                jobid_resp["core_duration"]
            )

        jobid_resp["pending_time"] = format_duration(jobid_resp["pending_time"])
        jobid_resp["time_limit"] = format_duration(jobid_resp["time_limit"])

        jobid_resp["start_unixtime"] = jobid_resp["start_time"]
        jobid_resp["start_time"] = request.translate_timezone(
            jobid_resp["start_time"]
        )

        jobid_resp["start_time"] = format_date(
            jobid_resp["start_time"], request.time_format
        )

        try:
            jobid_resp["end_time"] = request.translate_timezone(
                jobid_resp["end_time"]
            )
            jobid_resp["end_time"] = format_date(
                jobid_resp["end_time"], request.time_format
            )
        except TypeError:
            jobid_resp["end_time"] = "n/a"
            # live job awareness

        return jobid_resp
