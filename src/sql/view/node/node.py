from typing import Any

from sqlalchemy import select
from sqlalchemy.orm import Query

from utils.common import format_date
from utils.model import FormatRequest

from ...model import NodeORM, ReservationORM
from ..interface import (
    ORMInterface,
    ORMType,
    QueryFilterType,
    QueryInterface,
    ResponseInterface,
)


class NodeBaseQuery(QueryInterface):
    columns: list[Any] = [
        NodeORM.sample_time,
        NodeORM.partition_name,
        NodeORM.node_name,
        NodeORM.node_state,
        NodeORM.alloc_cpus,
        NodeORM.idle_cpus,
        NodeORM.cpu_load,
        NodeORM.alloc_gpus,
        NodeORM.gres_used,
        NodeORM.alloc_mem,
        NodeORM.free_mem,
        NodeORM.tmp_disk,
        NodeORM.reason,
        NodeORM.reservation_id,
    ]


class NodeQuery(NodeBaseQuery):
    @staticmethod
    def get_selected_columns() -> list[Any]:
        return NodeBaseQuery.columns

    @staticmethod
    def get_query(
        selected_columns: list[Any],
        query_filter: QueryFilterType,
    ) -> Query[ORMType]:
        select_statement = select(*selected_columns)

        if (
            query_filter.sample_time is None
            and query_filter.sample_time_interval is None
        ):
            select_statement = select_statement.filter(
                NodeORM.sample_time.in_(select(NodeORM.max_sample_time()))
            )

        query = select_statement.select_from(NodeORM)
        query = query_filter.apply_on_query(query)
        query = query.order_by(NodeORM.node_name.asc())

        return query


class NodeReservationQuery(NodeBaseQuery):
    @staticmethod
    def get_selected_columns() -> list[Any]:
        columns = NodeBaseQuery.columns
        columns = columns + [
            ReservationORM.reservation_name,
            ReservationORM.start_time,
            ReservationORM.end_time,
            ReservationORM.node_count,
            ReservationORM.node_list,
            ReservationORM.accounts,
        ]
        return columns

    @staticmethod
    def get_query(
        selected_columns: list[Any],
        query_filter: QueryFilterType,
    ) -> Query[ORMType]:
        select_statement = select(*selected_columns).outerjoin(
            ReservationORM,
            NodeORM.reservation_id == ReservationORM.id,
        )

        if (
            query_filter.sample_time is None
            and query_filter.sample_time_interval is None
        ):
            select_statement = select_statement.filter(
                NodeORM.sample_time.in_(select(NodeORM.max_sample_time()))
            )

        query = select_statement.select_from(NodeORM)
        query = query_filter.apply_on_query(query)
        query = query.order_by(NodeORM.node_name.asc())

        return query


class NodeBaseOrm(ORMInterface):
    sample_time: int | None
    partition_name: str | None
    node_name: str | None
    node_state: str | None
    alloc_cpus: int | None
    idle_cpus: int | None
    cpu_load: float | None
    alloc_gpus: int | None
    gres_used: str | None
    free_mem: int | None
    alloc_mem: int | None
    tmp_disk: int | None
    reason: str | None
    reservation_id: int | None


class NodeOrm(NodeBaseOrm):
    @staticmethod
    def from_row(result_set: dict[str, Any]) -> "NodeOrm":
        return NodeOrm(**result_set)


class NodeReservationOrm(NodeBaseOrm):
    reservation_name: str | None
    start_time: int | None
    end_time: int | None
    node_count: int | None
    node_list: str | None
    accounts: str | None

    @staticmethod
    def from_row(result_set: dict[str, Any]) -> "NodeReservationOrm":
        return NodeReservationOrm(**result_set)


class NodeBaseResponse(ResponseInterface):
    sample_time: str | None
    partition_name: str | None
    node_name: str | None
    node_state: str | None
    alloc_cpus: int | None
    idle_cpus: int | None
    cpu_load: float | None
    alloc_gpus: int | None
    gres_used: str | None
    free_mem: float | None
    alloc_mem: float | None
    tmp_disk: int | None
    reason: str | None
    reservation_id: int | None

    @staticmethod
    def get_formatted_results(orm: ORMInterface, request: FormatRequest) -> Any:
        response = orm.__dict__

        # set Nones to 0 on int
        int_keys = [
            "alloc_cpus",
            "idle_cpus",
            "cpu_load",
            "alloc_gpus",
            "tmp_disk",
            "node_count",
        ]
        for key in int_keys:
            if response.get(key) is None:
                response[key] = 0

        # set Nones to "n/a" on str
        str_keys = [
            "partition_name",
            "node_name",
            "node_state",
            "gres_used",
            "free_mem",
            "alloc_mem",
            "node_list",
            "accounts",
        ]
        for key in str_keys:
            if response.get(key) is None:
                response[key] = "n/a"

        try:
            response["sample_time"] = format_date(
                response["sample_time"], request.time_format
            )
        except TypeError:
            response["sample_time"] = "n/a"

        # convert from bytes to gigabyte
        try:
            response["free_mem"] = round(response["free_mem"] / 10**9, 2)
        except TypeError:
            response["free_mem"] = "n/a"

        try:
            response["alloc_mem"] = round(response["alloc_mem"] / 10**9, 2)
        except TypeError:
            response["alloc_mem"] = "n/a"

        # hard coded hack (we need smt_mode from current partition)
        smt_mode: int = 2
        if response["partition_name"] == "julia":
            smt_mode = 1

        # we provide alloc_cpus, idle_cpus and cpu_load as cores
        response["alloc_cpus"] = int(response["alloc_cpus"] / smt_mode)
        response["idle_cpus"] = int(response["idle_cpus"] / smt_mode)
        response["cpu_load"] = float(
            round((response["cpu_load"] / smt_mode / 100.0), 2)
        )

        if "start_time" in response:
            try:
                response["start_unixtime"] = response["start_time"]
                response["start_time"] = request.translate_timezone(
                    response["start_time"]
                )

                response["start_time"] = format_date(
                    response["start_time"], request.time_format
                )
            except TypeError:
                response["start_time"] = "n/a"

        if "end_time" in response:
            try:
                response["end_time"] = request.translate_timezone(
                    response["end_time"]
                )
                response["end_time"] = format_date(
                    response["end_time"], request.time_format
                )
            except TypeError:
                response["end_time"] = "n/a"

        if "reservation_name" in response:
            if response.get("reservation_name") is None:
                response["reservation_name"] = "none"

        return response


class NodeResponse(NodeBaseResponse):
    @staticmethod
    # type: ignore[override]
    def from_orm(orm: ORMInterface, request: FormatRequest) -> "NodeResponse":
        response = NodeBaseResponse.get_formatted_results(orm, request)
        return NodeResponse(**response)


class NodeReservationResponse(NodeBaseResponse):
    reservation_name: str | None
    start_time: str | None
    end_time: str | None
    node_count: int | None
    node_list: str | None
    accounts: str | None

    start_unixtime: int | None

    @staticmethod
    # type: ignore[override]
    def from_orm(
        orm: ORMInterface, request: FormatRequest
    ) -> "NodeReservationResponse":
        response = NodeBaseResponse.get_formatted_results(orm, request)

        return NodeReservationResponse(**response)
