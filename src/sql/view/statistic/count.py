"""
statistic.count view
"""

from typing import Any

from sqlalchemy.orm import Query
from sqlmodel import select

from ...model import (
    FootprintBaseORM,
    FootprintFileioORM,
    FootprintGpuORM,
    JobDataORM,
)
from ..interface import ORMInterface, ORMType, QueryFilterType, QueryInterface


class CountORM(ORMInterface):
    """statistic.count typesafe sqlalchemy result"""

    job_count: int
    footprint_count: int

    @staticmethod
    def from_row(result_set: dict[str, Any]) -> "CountORM":
        return CountORM(**result_set)


class CountQuery(QueryInterface):
    """statistic.count sqlalchemy query builder"""

    @staticmethod
    def get_selected_columns() -> list[Any]:
        return [JobDataORM.job_number(), JobDataORM.num_footprints()]

    @staticmethod
    def get_query(
        selected_columns: list[Any],
        query_filter: QueryFilterType,
    ) -> Query[ORMType]:
        sql_query = (
            select(*selected_columns)
            .outerjoin(FootprintBaseORM, JobDataORM.uid == FootprintBaseORM.uid)
            .outerjoin(
                FootprintFileioORM, JobDataORM.uid == FootprintFileioORM.uid
            )
            .outerjoin(FootprintGpuORM, JobDataORM.uid == FootprintGpuORM.uid)
        )

        sql_query = query_filter.apply_on_query(sql_query)

        return sql_query
