"""statistic.project view"""

from typing import Any

from sqlalchemy.orm import Query
from sqlmodel import select

from ...model import JobDataORM
from ..interface import ORMInterface, ORMType, QueryFilterType, QueryInterface


class ProjectORM(ORMInterface):
    """statistic.project sqlalchemy result"""

    project_name: str | None

    @staticmethod
    def from_row(result_set: dict[str, Any]) -> "ProjectORM":
        return ProjectORM(**result_set)


class ProjectQuery(QueryInterface):
    """statistic.project sqlalchemy query builder"""

    @staticmethod
    def get_selected_columns() -> list[Any]:
        return [JobDataORM.project_name]

    @staticmethod
    def get_query(
        selected_columns: list[Any],
        query_filter: QueryFilterType,
    ) -> Query[ORMType]:
        sql_query = select(*selected_columns).distinct()

        sql_query = query_filter.apply_on_query(sql_query)

        return sql_query
