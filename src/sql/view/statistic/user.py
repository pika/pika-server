"""statistic.user view"""

from typing import Any

from sqlalchemy.orm import Query
from sqlmodel import select

from ...model import JobDataORM
from ..interface import ORMInterface, ORMType, QueryFilterType, QueryInterface


class UserORM(ORMInterface):
    """statistic.user typesafe sql result"""

    user_name: str

    @staticmethod
    def from_row(result_set: dict[str, Any]) -> "UserORM":
        return UserORM(**result_set)


class UserQuery(QueryInterface):
    """statistic.user sqlalchemy query builder"""

    @staticmethod
    def get_selected_columns() -> list[Any]:
        return [JobDataORM.user_name]

    @staticmethod
    def get_query(
        selected_columns: list[Any],
        query_filter: QueryFilterType,
    ) -> Query[ORMType]:
        sql_query = select(*selected_columns).select_from(JobDataORM).distinct()

        sql_query = query_filter.apply_on_query(sql_query)

        return sql_query
