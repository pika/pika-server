"""meta.minmax_footprint view"""

import sqlalchemy
from sqlmodel import and_, func, literal_column, select

from ...connection import AsyncSQLConnection
from ...model import (
    FootprintBaseORM,
    FootprintFileioORM,
    FootprintGpuORM,
    JobDataORM,
)


async def query_min_max_footprint(
    connection: AsyncSQLConnection,
    footprint_name: str,
    filter_user: str,
    filter_start: int,
    filter_end: int,
):
    """
    needed by histogram and scatterplot to know what intervals the bins will be

    Raising:
        SQLError: DBAPI Error
    """

    select_clause = select(
        func.min(translate_footprint_select(footprint_name)).label("min"),
        func.max(translate_footprint_select(footprint_name)).label("max"),
    )

    query = (
        select_clause.select_from(JobDataORM)
        .outerjoin(FootprintBaseORM, JobDataORM.uid == FootprintBaseORM.uid)
        .outerjoin(FootprintGpuORM, JobDataORM.uid == FootprintGpuORM.uid)
        .outerjoin(FootprintFileioORM, JobDataORM.uid == FootprintFileioORM.uid)
        .filter(
            and_(
                (
                    JobDataORM.user_name == filter_user
                    if filter_user is not None
                    else True
                ),
                (
                    JobDataORM.start_time >= filter_start
                    if filter_start is not None
                    else True
                ),
                (
                    JobDataORM.start_time < filter_end
                    if filter_end is not None
                    else True
                ),
                JobDataORM.job_state != "running",
                literal_column(footprint_name) is not None,
            )
        )
    )

    result = await connection.execute(query)
    result = result[0]

    try:
        min_val = float(result["min"])
    except (TypeError, ValueError):
        min_val = 0.0
    try:
        max_val = float(result["max"])
    except (TypeError, ValueError):
        max_val = 0.0

    return (min_val, max_val)


def translate_footprint_select(custom_select: str) -> sqlalchemy.ClauseElement:
    """
    create a sqlalchemy selectable from a string select

    why?:
        if the user wants to see footprints, we allow him to take shortcuts fot better user experience.
        instead of querying "orm.endtime-orm.starttime" the user can just ask for "duration"
    """
    match custom_select:
        case "duration":
            return JobDataORM.end_time - JobDataORM.start_time
        case "core_duration":
            return JobDataORM.core_count * (
                JobDataORM.end_time - JobDataORM.start_time
            )
        case "pending_time":
            return JobDataORM.start_time - JobDataORM.submit_time
        case _:
            return literal_column(custom_select)
