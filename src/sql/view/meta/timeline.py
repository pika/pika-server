"""
meta.timeline view
"""

from time import time

from cachetools import TTLCache, cached
from sqlalchemy.orm import Query
from sqlmodel import select

from ...connection import SQLConnection
from ...model import JobDataFilter, JobDataORM, SQLError
from ..interface import ORMInterface, QueryInterface


class TimelineORM(ORMInterface):
    """typesafe meta.timeline sqlalchemy result"""

    start_time: int
    end_time: int | None
    partition_name: str | None
    uid: int | None
    node_list: str | None
    gpu_list: str | None
    core_list: str | None
    gpu_count: int | None
    exclusive: int | None
    job_state: str | None
    node_count: int | None
    core_count: int | None
    job_id: int | None
    user_name: str | None
    array_id: int | None
    time_limit: int | None
    job_name: str | None
    job_script: str | None
    project_name: str | None
    mem_per_cpu: int | None
    mem_per_node: int | None
    smt_mode: int | None

    @staticmethod
    def from_row(result_set: dict) -> "TimelineORM":
        return TimelineORM(**result_set)

    @staticmethod
    def from_rows(result_set: list[dict]) -> list["TimelineORM"]:
        return [TimelineORM(**r) for r in result_set]


class TimelineQuery(QueryInterface):
    """meta.timeline sqlalchemy query builder"""

    @staticmethod
    def get_selected_columns() -> tuple:
        return (
            JobDataORM.start_time,
            JobDataORM.end_time,
            JobDataORM.partition_name,
            JobDataORM.uid,
            JobDataORM.node_list,
            JobDataORM.gpu_list,
            JobDataORM.core_list,
            JobDataORM.get_gpu_count(),
            JobDataORM.exclusive,
            JobDataORM.job_state,
            JobDataORM.node_count,
            JobDataORM.core_count,
            JobDataORM.job_id,
            JobDataORM.user_name,
            JobDataORM.array_id,
            JobDataORM.time_limit,
            JobDataORM.job_name,
            JobDataORM.job_script,
            JobDataORM.project_name,
            JobDataORM.mem_per_cpu,
            JobDataORM.mem_per_node,
            JobDataORM.smt_mode,
        )

    @staticmethod
    def get_query(
        selected_columns: tuple, query_filter: JobDataFilter
    ) -> Query:
        query = select(*selected_columns)
        return query_filter.apply_on_query(query)


@cached(cache=TTLCache(maxsize=50, ttl=20))
# low cache time so that the job is as up to date as possible and
# influx builds all timeline queries with the same job
def query_timeline_meta(
    connection: SQLConnection, query_filter: JobDataFilter
) -> TimelineORM:
    """influx timelines need this remaining inforamtion about a job, this
    function is aware of live jobs

    this function will be queried a lot of times per influx timeline route,
    so we cache this function result

    this function will return only a single job

    Args:
        connection (SQLConnection): _description_
        query_filter (JobDataFilter): _description_

    Raises:
        SQLError
        IndexError: when user request a job which doesnt exists

    Returns:
        TimelineORM: _description_
    """

    query = TimelineQuery.build(query_filter)

    try:
        result_set = connection.execute(query)
    except SQLError as error:
        raise SQLError(error) from error

    job = TimelineORM.from_row(result_set[0])

    if job.end_time is None:
        job.end_time = time()
        # it is very likely that this job has not ended yet, as we support live
        # job viewing so we simulate a end time

    return job
