"""unittests for

- [x] empty_footprint
- [ ] minmax_footprint # needs refactor
- [ ] partition # needs partitionorm entrys
- [x] timeline
"""

from unittest import TestCase

from ...model import JobDataFilter, JobDataORM
from ...test import MemoryDB
from .empty_footprint import EmptyFootprintORM, EmptyFootprintQuery
from .timeline import TimelineORM, TimelineQuery

jobs = [
    {
        "uid": 42227220,
        "job_id": 29667954,
        "user_name": "notbeju577d",
        "project_name": "project_uno",
        "job_state": "completed",
        "node_count": 1,
        "node_list": "taurusi6603",
        "core_list": "taurusi6603[12]",
        "core_count": 1,
        "submit_time": 1664870658,
        "start_time": 1664870665,
        "end_time": 1664871885,
        "job_name": "interactive",
        "time_limit": 28800,
        "partition_name": "haswell64",
        "exclusive": 0,
        "property_id": 0,
        "array_id": "0",
        "tags": "0",
        "footprint_created": 0,
        "gpu_count": 0,
        "gpu_list": "",
        "smt_mode": 1,
        "job_script": "(null)",
        "mem_per_cpu": 1,
        "mem_per_node": None,
    },
    {
        "uid": 42227941,
        "job_id": 29668639,
        "user_name": "beju577d",
        "project_name": "project_uno",
        "job_state": "completed",
        "node_count": 1,
        "node_list": "taurusi6308",
        "core_list": "taurusi6308[0-11,16-19]",
        "core_count": 16,
        "submit_time": 1664872171,
        "start_time": 1664872180,
        "end_time": 1664872508,
        "job_name": "interactive",
        "time_limit": 1860,
        "partition_name": "haswell64",
        "exclusive": 0,
        "property_id": 0,
        "array_id": "0",
        "tags": 0,
        "footprint_created": 1,
        "gpu_count": 0,
        "gpu_list": "",
        "smt_mode": 1,
        "job_script": None,
        "mem_per_cpu": 1,
        "mem_per_node": None,
    },
    {
        "uid": 42228026,
        "job_id": 29668722,
        "user_name": "beju577d",
        "project_name": "project_dos",
        "job_state": "timeout",
        "node_count": 1,
        "node_list": "taurusi6604",
        "core_list": "taurusi6604[13]",
        "core_count": 1,
        "submit_time": 1664872926,
        "start_time": 1664872944,
        "end_time": 1664872945,
        "job_name": "gromacs.sh",
        "time_limit": 28800,
        "partition_name": "haswell64",
        "exclusive": 0,
        "property_id": 0,
        "array_id": "0",
        "tags": None,
        "footprint_created": None,
        "gpu_count": 0,
        "gpu_list": "",
        "smt_mode": 1,
        "job_script": None,
        "mem_per_cpu": 1,
        "mem_per_node": None,
    },
]


class EmptyFootprintTest(TestCase):
    """tesing empty footprint view"""

    def setUp(self) -> None:
        self.memory_db = MemoryDB("sql.view.meta.test.EmptyFootprintTest")

        for job in jobs:
            self.memory_db.add(JobDataORM(**job))

        return super().setUp()

    def test_without_filter(self):
        """simple test with empty filter"""

        query = EmptyFootprintQuery.build(JobDataFilter())
        result_set = self.memory_db.execute(query)
        typesafe_result = EmptyFootprintORM.from_rows(result_set)

        self.assertEqual(
            typesafe_result,
            [EmptyFootprintORM(uid=42227220), EmptyFootprintORM(uid=42228026)],
        )

    def test_with_filter(self):
        """simple test with filter"""

        query = EmptyFootprintQuery.build(
            JobDataFilter(project_name="project_dos")
        )
        result_set = self.memory_db.execute(query)
        typesafe_result = EmptyFootprintORM.from_rows(result_set)

        self.assertEqual([EmptyFootprintORM(uid=42228026)], typesafe_result)


class TimelineTest(TestCase):
    """tesing timeline view"""

    def setUp(self) -> None:
        self.memory_db = MemoryDB("sql.view.meta.test.TimelineTest")

        for job in jobs:
            self.memory_db.add(JobDataORM(**job))

        return super().setUp()

    def test_without_filter(self):
        """simple test with empty filter"""

        query = TimelineQuery.build(JobDataFilter())
        result_set = self.memory_db.execute(query)
        typesafe_result = TimelineORM.from_rows(result_set)

        self.assertEqual(
            typesafe_result,
            [
                TimelineORM(
                    start_time=1664870665,
                    end_time=1664871885,
                    partition_name="haswell64",
                    uid=42227220,
                    node_list="taurusi6603",
                    gpu_list="",
                    core_list="taurusi6603[12]",
                    gpu_count=0,
                    exclusive=0,
                    job_state="completed",
                    node_count=1,
                    core_count=1,
                    job_id=29667954,
                    user_name="notbeju577d",
                    array_id=0,
                    time_limit=28800,
                    job_name="interactive",
                    job_script="(null)",
                    project_name="project_uno",
                    mem_per_cpu=1,
                    mem_per_node=None,
                    smt_mode=1,
                ),
                TimelineORM(
                    start_time=1664872180,
                    end_time=1664872508,
                    partition_name="haswell64",
                    uid=42227941,
                    node_list="taurusi6308",
                    gpu_list="",
                    core_list="taurusi6308[0-11,16-19]",
                    gpu_count=0,
                    exclusive=0,
                    job_state="completed",
                    node_count=1,
                    core_count=16,
                    job_id=29668639,
                    user_name="beju577d",
                    array_id=0,
                    time_limit=1860,
                    job_name="interactive",
                    job_script=None,
                    project_name="project_uno",
                    mem_per_cpu=1,
                    mem_per_node=None,
                    smt_mode=1,
                ),
                TimelineORM(
                    start_time=1664872944,
                    end_time=1664872945,
                    partition_name="haswell64",
                    uid=42228026,
                    node_list="taurusi6604",
                    gpu_list="",
                    core_list="taurusi6604[13]",
                    gpu_count=0,
                    exclusive=0,
                    job_state="timeout",
                    node_count=1,
                    core_count=1,
                    job_id=29668722,
                    user_name="beju577d",
                    array_id=0,
                    time_limit=28800,
                    job_name="gromacs.sh",
                    job_script=None,
                    project_name="project_dos",
                    mem_per_cpu=1,
                    mem_per_node=None,
                    smt_mode=1,
                ),
            ],
        )

    def test_with_filter(self):
        """simple test with filter"""

        query = TimelineQuery.build(JobDataFilter(project_name="project_dos"))
        result_set = self.memory_db.execute(query)
        typesafe_result = TimelineORM.from_rows(result_set)

        self.assertEqual(
            typesafe_result,
            [
                TimelineORM(
                    start_time=1664872944,
                    end_time=1664872945,
                    partition_name="haswell64",
                    uid=42228026,
                    node_list="taurusi6604",
                    gpu_list="",
                    core_list="taurusi6604[13]",
                    gpu_count=0,
                    exclusive=0,
                    job_state="timeout",
                    node_count=1,
                    core_count=1,
                    job_id=29668722,
                    user_name="beju577d",
                    array_id=0,
                    time_limit=28800,
                    job_name="gromacs.sh",
                    job_script=None,
                    project_name="project_dos",
                    mem_per_cpu=1,
                    mem_per_node=None,
                    smt_mode=1,
                )
            ],
        )
