"""meta.empty_footprint view"""

from typing import Any

from sqlalchemy.orm import Query
from sqlmodel import or_, select

from ...connection import SQLConnection
from ...model import (
    JobDataFilter,
    JobDataORM,
    JobQueueFilter,
    JobQueueORM,
    SQLError,
)
from ..interface import ORMInterface, QueryInterface


class EmptyFootprintORM(ORMInterface):
    """type safe meta.emptyfootprint sqlalchemy result"""

    uid: int

    @staticmethod
    def from_row(result_set: dict[str, Any]) -> "EmptyFootprintORM":
        return EmptyFootprintORM(**result_set)

    @staticmethod
    def from_rows(result_set: list[dict[str, Any]]) -> list[ORMInterface]:
        return [EmptyFootprintORM(**r) for r in result_set]


class EmptyFootprintQuery(QueryInterface):
    """meta.emptyfootprint sqlalchemy query builder"""

    @staticmethod
    def get_selected_columns() -> list[Any]:
        return [JobDataORM.uid]

    @staticmethod
    def get_query(
        selected_columns: list[Any],
        query_filter: JobDataFilter | JobQueueFilter,
    ) -> Query[JobDataORM | JobQueueORM]:
        sql_query = (
            select(*selected_columns)
            .select_from(JobDataORM)
            .filter(
                or_(
                    JobDataORM.job_state == "completed",
                    JobDataORM.job_state == "timeout",
                ),
                or_(
                    JobDataORM.footprint_created != 1,
                    JobDataORM.footprint_created.is_(None),
                ),
                # job_data.footprint_created = 1 bedeutet, dass für den job schon sein
                # auslastungs durschnitt berechnet wurde
            )
        )

        return query_filter.apply_on_query(sql_query)


def query_empty_footprint(
    sql_connection: SQLConnection, query_filter: JobDataFilter
) -> list[EmptyFootprintORM]:
    """query jobs which have no footprint metrics inserted yet by
    post processing

    Args:
        sql_connection (SQLConnection)
        query_filter (JobDataFilter)

    Raises:
        SQLError

    Returns:
        list[EmptyFootprintORM]:
    """

    query = EmptyFootprintQuery.build(query_filter)

    try:
        result_set = sql_connection.execute(query)
    except SQLError as error:
        raise SQLError(error) from error

    parsed_jobs = EmptyFootprintORM.from_rows(result_set)

    return parsed_jobs
