"""
some operations in the restapi (visualizing timelines with influx data or
post processing) need to know information about a job to function,
(e.g. to draw a timeline graph we need to now the start and end time for a job,
those information are provided by `meta`)

those views already provide ready to use query function as the callers always
want a ORM instead of a human readable response, and some meta functions are
being cached
"""

from .empty_footprint import (
    EmptyFootprintORM,
    EmptyFootprintQuery,
    query_empty_footprint,
)
from .minmax_footprint import (
    query_min_max_footprint,
    translate_footprint_select,
)
from .timeline import TimelineORM, TimelineQuery, query_timeline_meta
