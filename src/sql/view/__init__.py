"""
the pika sql database is just a table of jobs, to make sense out of this data
various views are supplied

each view contains a sqlalchemy query builder, a type safe sql result container
and a response class wich formats the result_set in a human readable way

each class is build from static methods, so you dont need to instatiate a class

usage:
```python
sql_query_filter = JobDataFilter(user_name="beju577d")
sqlalchemy_query = XQuery.build(sql_query_filter)

result_set = sql_connection.execute(sqlalchemy_query)

parsed_jobs = XORM.from_rows(result_set)
formated_jobs = XResponse.from_orms(parsed_jobs)
```

implementation detail:
we are using sqlalchemy and pydantic tightly together, sadly there are no
complete librarys which make this easy, because of this we have to write somewhat
redundand classes (query, orm, response)
"""
