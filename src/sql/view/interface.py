"""
abstract classes defining the behaviour of the query builder, orm and response
"""

from abc import ABC, abstractmethod
from typing import Any, Union

from pydantic import BaseModel
from sqlalchemy.orm import Query

from utils.model import FormatRequest

from ..model import (
    JobDataFilter,
    JobDataORM,
    JobQueueFilter,
    JobQueueORM,
    NodeFilter,
    NodeORM,
    ReservationFilter,
    ReservationORM,
)

QueryFilterType = Union[
    JobDataFilter, JobQueueFilter, NodeFilter, ReservationFilter
]
ORMType = Union[JobQueueORM, JobDataORM, NodeORM, ReservationORM]


class QueryInterface(ABC):
    """interface for the sqlalchemy query builder

    Args:
        ABC: make the class abstract
    """

    @staticmethod
    @abstractmethod
    def get_selected_columns() -> list[Any]:
        """get a tuple containing all selected columns for the sqlalchemy
        select clause

        Returns:
            tuple: selected columns
        """

    @staticmethod
    @abstractmethod
    def get_query(
        selected_columns: list[Any],
        query_filter: QueryFilterType,
    ) -> Query[ORMType]:
        """build the sqlalchemy query containing the selected columns and the
        filter

        Args:
            selected_columns (tuple)
            query_filter (JobDataFilter)

        Returns:
            Query: the sqlalchemy query
        """

    @classmethod
    def build(cls, query_filter: QueryFilterType) -> Query[ORMType]:
        """build the sqlalchemy query, shorter version of calling
        cls.get_query(cls.get_selected_columns(), query_filter)

        Args:
            query_filter (JobDataFilter)

        Returns:
            Query: the sqlalchemy query
        """
        selected_columns = cls.get_selected_columns()
        return cls.get_query(selected_columns, query_filter)


class ORMInterface(ABC, BaseModel):
    """a typesafe container which holds the results from the sqlalchemy query

    Args:
        ABC: make the class abstract
        BaseModel: use easy class construction
    """

    @staticmethod
    @abstractmethod
    def from_row(result_set: dict[str, Any]) -> "ORMInterface":
        """create self from a "sqlalchemy result row mapping" which is a dict

        Args:
            result_set (dict): the sqlalchemy result (row mapping) dict

        Returns:
            self: the constructed type safe container
        """

    @classmethod
    def from_rows(
        cls, result_set: list[dict[str, Any]]
    ) -> list["ORMInterface"]:
        """create a list of self from many "sqlalchemy result row mappings"

        Args:
            result_set (list[dict]): the sqlalchemy results

        Returns:
            self: a list of the constructed type safe container
        """
        return [cls.from_row(result) for result in result_set]


class ResponseInterface(ABC, BaseModel):
    """a typesafe container for the formated ORMInterface we will respond to the
    user

    Args:
        ABC: make this class abstract
        BaseModel: make use of easy class construction
    """

    @staticmethod
    @abstractmethod
    # pylint: disable=arguments-renamed, arguments-differ
    # type: ignore
    # pydantic.BaseModel also has a from_orm method, which we dont use, because
    # BaseModel.from_orm is the equivalent to our ORMInterface.from_row
    def from_orm(
        orm: ORMInterface, request: FormatRequest
    ) -> "ResponseInterface":
        """create self from a orm

        Args:
            orm (ORMInterface)

        Returns:
            ResponseInterface
        """

    @classmethod
    def from_orms(
        cls, orms: list[ORMInterface], request: FormatRequest
    ) -> list["ResponseInterface"]:
        """create a list of selfs from a list of orms

        Args:
            orms (list[ORMInterface])

        Returns:
            list[ResponseInterface]
        """
        return [cls.from_orm(orm, request) for orm in orms]
