from typing import Any

from sqlalchemy import select
from sqlalchemy.orm import Query

from utils.common import format_date
from utils.model import FormatRequest

from ...model import ReservationORM
from ..interface import (
    ORMInterface,
    ORMType,
    QueryFilterType,
    QueryInterface,
    ResponseInterface,
)


class ReservationQuery(QueryInterface):
    @staticmethod
    def get_selected_columns() -> list[Any]:
        return [
            ReservationORM.id,
            ReservationORM.reservation_name,
            ReservationORM.start_time,
            ReservationORM.end_time,
            ReservationORM.node_count,
            ReservationORM.node_list,
            ReservationORM.partition_name,
            ReservationORM.accounts,
        ]

    @staticmethod
    def get_query(
        selected_columns: list[Any],
        query_filter: QueryFilterType,
    ) -> Query[ORMType]:
        query = select(*selected_columns).select_from(ReservationORM)

        query = query_filter.apply_on_query(query)

        query = query.order_by(ReservationORM.id.asc())

        return query


class ReservationOrm(ORMInterface):
    id: int | None
    reservation_name: str | None
    start_time: int | None
    end_time: int | None
    node_count: int | None
    node_list: str | None
    partition_name: str | None
    accounts: str | None

    @staticmethod
    def from_row(result_set: dict[str, Any]) -> "ReservationOrm":
        return ReservationOrm(**result_set)

    @staticmethod
    def from_rows(result_set: list[dict[str, Any]]) -> list[ORMInterface]:
        return [ReservationOrm(**r) for r in result_set]


class ReservationResponse(ResponseInterface):
    id: int | None
    reservation_name: str | None
    start_time: str | None
    end_time: str | None
    node_count: int | None
    node_list: str | None
    partition_name: str | None
    accounts: str | None

    start_unixtime: int | None

    @staticmethod
    # type: ignore[override]
    def from_orm(
        orm: ORMInterface, request: FormatRequest
    ) -> "ReservationResponse":
        response = orm.__dict__

        # set Nones to 0 on int
        int_keys = ["node_count"]
        for key in int_keys:
            if response.get(key) is None:
                response[key] = 0

        # set Nones to "n/a" on str
        str_keys = [
            "node_list",
            "partition_name",
            "accounts",
        ]
        for key in str_keys:
            if response.get(key) is None:
                response[key] = "n/a"

        try:
            response["start_unixtime"] = response["start_time"]
            response["start_time"] = request.translate_timezone(
                response["start_time"]
            )

            response["start_time"] = format_date(
                response["start_time"], request.time_format
            )
        except TypeError:
            response["start_time"] = "n/a"

        try:
            response["end_time"] = request.translate_timezone(
                response["end_time"]
            )
            response["end_time"] = format_date(
                response["end_time"], request.time_format
            )
        except TypeError:
            response["end_time"] = "n/a"

        return ReservationResponse(**response)
