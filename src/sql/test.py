"""unit tests for

- [x] MemoryDB
- [x] sql.connection:
    - [x] SQLConnection
    - [x] AsyncSQLConnection
- [x] sql.model.JobDataFilter
"""

# mypy: ignore-errors

import asyncio
from typing import Any
from unittest import TestCase

import sqlalchemy.ext.asyncio
from sqlalchemy import create_engine, select
from sqlalchemy.orm import Query, Session
from sqlmodel import Field, SQLModel

from utils.common import debug_sqlalchemy_query

from .connection import AsyncSQLConnection, SQLConnection
from .model import FootprintFilter, IntervalFilter, JobDataFilter, JobDataORM


class AsyncMemoryDB:
    def __init__(self, unique_path: str) -> None:
        self.uri = f"sqlite+aiosqlite:///file:{unique_path}?mode=memory&cache=shared&uri=true"

        self.engine = sqlalchemy.ext.asyncio.create_async_engine(self.uri)

    async def init(self):
        async with self.engine.begin() as connection:
            await connection.run_sync(SQLModel.metadata.create_all)

    async def execute(self, query):
        async with sqlalchemy.ext.asyncio.AsyncSession(self.engine) as session:
            result_set = await session.execute(query)
            await session.commit()
            result_set = result_set.fetchall()
            result_set = [dict(result._mapping) for result in result_set]
            return result_set

    async def add(self, obj):
        async with sqlalchemy.ext.asyncio.AsyncSession(self.engine) as session:
            session.add(obj)
            await session.commit()

    async def delete(self):
        await self.engine.dispose()


class AsyncMemoryDBtest(TestCase):
    def test(self):

        async def test():
            db = AsyncMemoryDB("AsyncMemeoryDBTest")
            await db.init()
            await db.add(JobDataORM(uid=2))
            result = await db.execute(select(JobDataORM.uid))
            await db.delete()
            return result

        result = asyncio.run(test())

        self.assertEqual(result, [{"uid": 2}])


class MemoryDB:
    """
    In-memory SQLite database designed so that each unit test class can create
    its own memorydb without interference
    """

    def __init__(self, unique_id: str) -> None:
        """create a in-memory SQLite database

        Args:
            unique_id (str): when creating a memory database, it is important
                             to specify a unique identifier so that each
                             database can be created in isolation from each
                             other, the unique_id could be the python path to
                             the currently unit test class
        """

        self.uri = (
            f"sqlite:///file:{unique_id}?mode=memory&cache=shared&uri=true"
        )

        self._engine = create_engine(
            self.uri,
            connect_args={"check_same_thread": False},
        )

        SQLModel.metadata.create_all(self._engine)

    def execute(self, query: Query) -> list[dict[str, Any]]:
        """execute sqlalchemy query"""

        with Session(self._engine, expire_on_commit=False) as session:
            result_set = session.execute(query)
            session.commit()
            result_set = result_set.fetchall()
            result_set = [dict(result._mapping) for result in result_set]
            return result_set

    def add(self, obj):
        """insert a object into the memorydb"""

        with Session(self._engine) as session:
            session.add(obj)
            session.commit()

    def __del__(self):
        self._engine.dispose()


class MemoryDBTest(TestCase):
    def test(self):
        memorydb = MemoryDB("sql.test.MemoryDBTest")
        memorydb.add(JobDataORM(job_id=23))
        self.assertEqual(
            memorydb.execute(select(JobDataORM.job_id)), [{"job_id": 23}]
        )

    def test_storage(self):
        """test if every old memorydb will be deleted"""

        memorydb = MemoryDB("sql.test.MemoryDBTest")
        memorydb.add(JobDataORM(job_id=21))
        self.assertEqual(
            memorydb.execute(select(JobDataORM.job_id)), [{"job_id": 21}]
        )


class Person(SQLModel, table=True):

    id: int = Field(primary_key=True)
    age: int | None
    name: str | None


class AsyncSQLConnectionTest(TestCase):

    def setUp(self) -> None:

        async def setup():
            p1 = Person(**{"id": 1, "age": 12, "name": "p1"})
            p2 = Person(**{"id": 2, "age": 13, "name": "p2"})
            p3 = Person(**{"id": 3, "age": 14, "name": "p3"})

            db = AsyncMemoryDB("sql_test_async")
            await db.init()
            await db.add(p1)
            await db.add(p2)
            await db.add(p3)

            return db

        self.db = asyncio.run(setup())

    def test(self):
        async def test():
            db = AsyncSQLConnection(self.db.uri)
            result = await db.execute(select(Person.age))
            return result

        result = asyncio.run(test())

        self.assertEqual([{"age": 12}, {"age": 13}, {"age": 14}], result)


class SQLConnectionTest(TestCase):
    """testing sql.connection.SQLConnection"""

    def setUp(self) -> None:
        p1 = Person(**{"id": 1, "age": 12, "name": "p1"})
        p2 = Person(**{"id": 2, "age": 13, "name": "p2"})
        p3 = Person(**{"id": 3, "age": 14, "name": "p3"})

        self._memory_db = MemoryDB("sql.test.SQLConnectionTest")
        self._memory_db.add(p1)
        self._memory_db.add(p2)
        self._memory_db.add(p3)
        return super().setUp()

    def test(self):
        """simple test"""

        sql_connection = SQLConnection(self._memory_db.uri)
        sql_query = select(Person.age, Person.name)
        result_set = sql_connection.execute(sql_query)
        self.assertEqual(
            result_set,
            [
                {"age": 12, "name": "p1"},
                {"age": 13, "name": "p2"},
                {"age": 14, "name": "p3"},
            ],
        )


class JobDataFilterTest(TestCase):
    """testing sql.model.JobDataFilter"""

    def test_and(self):
        """test query building"""

        query = JobDataFilter(
            user_name="beju577d", job_state="running"
        ).apply_on_query(select(JobDataORM.job_id))

        query = debug_sqlalchemy_query(query)

        expected_query = """SELECT job_data.job_id 
FROM job_data 
WHERE job_data.user_name = 'beju577d' AND job_data.job_state = 'running' AND true = 1 AND true = 1 AND true = 1 
 LIMIT 20000"""

        self.assertEqual(query, expected_query)

    def test_or(self):
        """test query building"""

        query = JobDataFilter(
            user_name="beju577d", job_state="running", node_name="taurusi8000"
        ).apply_on_query(select(JobDataORM.job_id))

        query = debug_sqlalchemy_query(query)

        expected_query = """SELECT job_data.job_id 
FROM job_data 
WHERE job_data.user_name = 'beju577d' AND job_data.job_state = 'running' AND (job_data.node_list = 'taurusi8000' OR job_data.core_list = 'taurusi8000') AND true = 1 AND true = 1 
 LIMIT 20000"""

        self.assertEqual(query, expected_query)

    def test_second_or(self):
        """test query building"""
        query = JobDataFilter(
            user_name="beju577d",
            job_state="running",
            node_name_like="taurusi8000",
        ).apply_on_query(select(JobDataORM.job_id))

        query = debug_sqlalchemy_query(query)

        expected_query = """SELECT job_data.job_id 
FROM job_data 
WHERE job_data.user_name = 'beju577d' AND job_data.job_state = 'running' AND true = 1 AND (job_data.node_list LIKE '%%taurusi8000%%' OR job_data.core_list LIKE '%%taurusi8000%%') AND true = 1 
 LIMIT 20000"""

        self.assertEqual(query, expected_query)

    def test_intervals(self):
        """test query building"""

        with self.subTest("on build in columns"):
            query = JobDataFilter(
                user_name="beju577d",
                job_state="running",
                node_name_like="taurusi8000",
                core_count_interval=IntervalFilter(min=2.3, max=3.2),
            ).apply_on_query(select(JobDataORM.job_id))

            query = debug_sqlalchemy_query(query)

            expected_query = """SELECT job_data.job_id 
FROM job_data 
WHERE job_data.user_name = 'beju577d' AND job_data.job_state = 'running' AND true = 1 AND (job_data.node_list LIKE '%%taurusi8000%%' OR job_data.core_list LIKE '%%taurusi8000%%') AND job_data.core_count <= 3.2 AND job_data.core_count >= 2.3 
 LIMIT 20000"""

            self.assertEqual(query, expected_query)

        with self.subTest("on custom function"):
            query = JobDataFilter(
                user_name="beju577d",
                job_state="running",
                node_name_like="taurusi8000",
                core_duration_interval=IntervalFilter(min=2.5, max=3.2),
            ).apply_on_query(select(JobDataORM.job_id))

            query = debug_sqlalchemy_query(query)

            expected_query = """SELECT job_data.job_id 
FROM job_data 
WHERE job_data.user_name = 'beju577d' AND job_data.job_state = 'running' AND true = 1 AND (job_data.node_list LIKE '%%taurusi8000%%' OR job_data.core_list LIKE '%%taurusi8000%%') AND job_data.core_count * (job_data.end_time - job_data.start_time) <= 3.2 AND job_data.core_count * (job_data.end_time - job_data.start_time) >= 2.5 
 LIMIT 20000"""

            self.assertEqual(query, expected_query)

    def test_footprint(self):
        """test query building"""

        query = JobDataFilter(
            user_name="beju577d",
            job_state="running",
            node_name_like="taurusi8000",
            core_duration_interval=IntervalFilter(min=2.5, max=3.2),
            footprint=[
                FootprintFilter(name="ipc_mean_per_core", max=3.2, min=1.2)
            ],
        ).apply_on_query(select(JobDataORM.job_id))

        query = debug_sqlalchemy_query(query)

        expected_query = """SELECT job_data.job_id 
FROM job_data 
WHERE job_data.user_name = 'beju577d' AND job_data.job_state = 'running' AND true = 1 AND (job_data.node_list LIKE '%%taurusi8000%%' OR job_data.core_list LIKE '%%taurusi8000%%') AND job_data.core_count * (job_data.end_time - job_data.start_time) <= 3.2 AND job_data.core_count * (job_data.end_time - job_data.start_time) >= 2.5 AND ipc_mean_per_core IS NOT NULL AND ipc_mean_per_core <= 3.2 AND ipc_mean_per_core >= 1.2 
 LIMIT 20000"""

        self.assertEqual(query, expected_query)

    def test_tags_normal_bit_pattern(self):
        """test query building"""

        with self.subTest("normal bit pattern"):
            query = JobDataFilter(
                node_name_like="taurusi8000", tags=[18]
            ).apply_on_query(select(JobDataORM.job_id))

            query = debug_sqlalchemy_query(query)

            expected_query = """SELECT job_data.job_id 
FROM job_data 
WHERE true = 1 AND true = 1 AND (job_data.node_list LIKE '%%taurusi8000%%' OR job_data.core_list LIKE '%%taurusi8000%%') AND true = 1 AND job_data.tags = 18 
 LIMIT 20000"""

            self.assertEqual(query, expected_query)

        with self.subTest("tags or"):
            query = JobDataFilter(
                node_name_like="taurusi8000", tags=[16, 2], limit=False
            ).apply_on_query(select(JobDataORM.job_id))

            query = debug_sqlalchemy_query(query)

            expected_query = """SELECT job_data.job_id 
FROM job_data 
WHERE true = 1 AND true = 1 AND (job_data.node_list LIKE '%%taurusi8000%%' OR job_data.core_list LIKE '%%taurusi8000%%') AND true = 1 AND (job_data.tags = 16 OR job_data.tags = 2)"""

            self.assertEqual(query, expected_query)
