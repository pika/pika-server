"""
- create sql connection pools
- query sqlalchemy queries
- test sql credentials on reading and writing
"""

from typing import Any, Optional, overload

import sqlalchemy
from sqlalchemy import create_engine, update
from sqlalchemy.exc import DatabaseError, OperationalError, ResourceClosedError
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import Query, Session
from sqlalchemy.sql import Insert, Update

from utils.model import DBCredential

from .model import FootprintAnalysisORM, SQLError


class SQLConnection:
    """
    - create a connection pool to the sql database (5 connections)
    - execute sqlalchemy queries
    """

    @overload
    def __init__(self, cred: str) -> None: ...
    @overload
    def __init__(self, cred: DBCredential) -> None: ...
    def __init__(self, cred: DBCredential | str) -> None:
        """
        make sqlconnection initializable with a uri, so we can write
        unittests for this class, uris for databases sadly dont follow the same pattern

        create a connection pool to the sql database"""

        if isinstance(cred, DBCredential):
            uri = build_authentication_uri(
                "mysql+mysqlconnector", cred, "connect_timeout=5"
            )
        elif isinstance(cred, str):
            uri = cred

        self._engine = create_engine(uri, pool_pre_ping=True, pool_recycle=3600)
        # create 5 connection pools (default pool size), we keep this pool size
        # low because in post processing each process is working with its own
        # pool, mariadb will break at circa 1000 connections, we are currently
        # using 700

    def execute(self, query: Insert | Update) -> list[dict[str, Any]]:
        """execute a sqlalchemy query

        Args:
            query (Query): sqlalchemy query

        Raises:
            SQLError: sql driver problem

        Returns:
            list[Row[any, ...]]: the result set we got from the database
        """
        with Session(self._engine, expire_on_commit=False) as session:
            try:
                result_set = session.execute(query)  # type: ignore
                session.commit()
                # "insert" or "update" queries need commiting to take effect

                result_set = result_set.fetchall()
                result_set = [dict(result._mapping) for result in result_set]
                return result_set  # type: ignore
            except (OperationalError, DatabaseError) as error:
                raise SQLError(error) from error
            except ResourceClosedError:
                # "delete" or "update" queries will fail on fetching the result
                return [{}]


class AsyncSQLConnection:
    """
    - create an asynchronous connection pool to the sql database
      (50 connections)
    - execute sqlalchemy queries

    p.s. try to reuse one connection pool
    """

    @overload
    def __init__(self, cred: str) -> None: ...
    @overload
    def __init__(self, cred: DBCredential) -> None: ...
    def __init__(self, cred: DBCredential | str) -> None:
        """create a asynchronous connection pool to the sql database

        Args:
            cred (DBCredential): the sql credentials
        """

        if isinstance(cred, DBCredential):
            url = build_authentication_uri(
                "mysql+asyncmy", cred, "connect_timeout=5"
            )
            self._engine = create_async_engine(
                url,
                pool_pre_ping=True,
                pool_recycle=3600,
                pool_size=50,
                connect_args={
                    "read_timeout": ((cred.async_select_timeout_minutes * 60))
                },
                # https://github.com/long2ice/asyncmy/blob/main/asyncmy/connection.pyx read_timeout, this setting is driver dependent
            )
        else:
            url = cred
            self._engine = create_async_engine(url)

    async def execute(self, query: Query) -> list[dict[str, Any]]:
        """execute a sqlalchemy query asynchronously

        Args:
            query (Query): the sqlalchemy query

        Raises:
            SQLError: sql driver problem

        Returns:
            list[Row[any, ...]]: result set
        """
        async with AsyncSession(
            self._engine, expire_on_commit=False
        ) as session:
            try:
                result_set = await session.execute(query)  # type: ignore
                await session.commit()
                # `insert` or `update` queries need commiting to take effect

                result_set = result_set.fetchall()
                result_set = [dict(result._mapping) for result in result_set]
                return result_set  # type: ignore
            except (OperationalError, DatabaseError) as error:
                raise SQLError(error) from error
            except ResourceClosedError:
                # "delete" or "update" queries will fail on fetching the results

                return [{}]


def build_authentication_uri(
    protocol: str, cred: DBCredential, arguments: Optional[str] = None
) -> str:
    """
    - build a sql uri, for communication

    >>> build_authentication_uri("mysql+asyncmy", cred, "arg=5")
    >>> "mysql+asyncmy://user:password@host:port/database?arg=5"

    Args:
        protocol (str): sql driver
        cred (DBCredential): sql credentials

    Returns:
        str: uri
    """

    db_resource = (
        f"{cred.user}:"
        f"{cred.password}@"
        f"{cred.hostname}:"
        f"{cred.port}/"
        f"{cred.database}"
    )

    if arguments is None:
        return f"{protocol}://{db_resource}"

    return f"{protocol}://{db_resource}?{arguments}"


def test_read(cred: DBCredential) -> None:
    """Verify that credentials are able to read from the SQL database

    Raises:
        SQLError: invalid credentials or sql driver problem
    """

    sql_query = sqlalchemy.text("select 'yes' as reading_permission")

    try:
        result = SQLConnection(cred).execute(sql_query)
    except (ValueError, SQLError) as error:
        raise SQLError(error) from error

    if [{"reading_permission": "yes"}] != result:
        raise SQLError("got an unexpected result")


def test_write(cred: DBCredential) -> None:
    """Verify that credentials are able to read and write from the SQL database

    Raising:
        SQLError: invalid credentials or sql driver problem
    """

    sql_query = update(FootprintAnalysisORM).values(uid=1).where(False)
    # UPDATE FootprintAnalysisORM SET uid=1 WHERE False

    try:
        SQLConnection(cred).execute(sql_query)
    except SQLError as error:
        raise SQLError from error
