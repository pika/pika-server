import asyncio
import json
import re
from typing import Any, Dict, List, Optional, Tuple

import hostlist
import metricq
from metricq.exceptions import AgentStopped, ConnectFailed, ReconnectTimeout
from metricq.history_client import HistoryClient

import sql.view.meta
import sql.view.topdown
from sql.model import JobDataFilter, SQLError
from sql.view.meta.timeline import JobDataORM
from utils.common import read_file_from_project_root
from utils.logger import logger

# mypy: ignore-errors


def initialize_energy_processing(
    sql_connection: Any, token: str, server: str
) -> Any:
    """create eventloop and objects for
    classes:JobBlockInfo, AsyncClientManager, MetricqData, JobEnergy

    args:
            sql_connection: mariadb sql connection,
            token:metricq token,
            server:metricQ server

    return: job_energy,client_manager,eventlopp objects"""

    eventloop = asyncio.new_event_loop()
    asyncio.set_event_loop(eventloop)
    data = json.loads(read_file_from_project_root("metricq.json"))
    metricq_uri = data["metricq"]
    check_client = data["checkclient"]
    job_block = JobBlockInfo(sql_connection)
    client_manager = AsyncClientManager(token, server, check_client)
    metricq_data = MetricqData(metricq_uri)
    job_energy = JobEnergy(job_block, client_manager, metricq_data, metricq_uri)
    return job_energy, client_manager, eventloop


class MetricqData:
    """class for handling data's from metricq"""

    def __init__(self, metricq_uri: Dict[str, str]) -> None:
        """constructor for MetricqData class"""
        self.metricq_uri = metricq_uri

    def joules_to_kwh(self, joules: float) -> float:
        """converting joules into KWh"""
        return joules / (3.6 * 10**6)

    async def meanpower(
        self, metric: str, start_time: Any, end_time: Any, client: Any
    ) -> int:
        """get mean power for specified duration (starttime,endtime) from metricq
        args:
             metric:metric url for different clusters,
             start_time:unix_timestamp,
             end_time:unix_timestamp,
             client:metricq client connection

        return : meanpower
        """

        try:
            aggregate = await client.history_aggregate(
                metric, start_time, end_time, timeout=20
            )
            if aggregate.sum == 0 and aggregate.count == 0:
                mean = 0
                return mean
            mean = (aggregate.sum) / (aggregate.count)
            return mean
        except metricq.exceptions.InvalidHistoryResponse as e:
            logger.warning(
                "[WARNING] Invalid history response for metric %s:%s", metric, e
            )
            return -1
        except ZeroDivisionError as e:
            logger.warning("[WARNING] Zero division error for %s:%s", metric, e)
            return -1
        except TimeoutError as e:
            logger.warning("[WARNING] Timeout for %s:%s", metric, e)
            return -2

        except Exception as e:
            logger.warning("[WARNING] Unable to process for %s: %s", metric, e)
            return -1

    async def get_energy_for_non_exclusive(
        self, datas: Dict[str, Any], client: Any, partition_name: str
    ) -> List[Any]:
        """calculate energy for non exclusive jobs
        args:
            datas:job blocks within the node,
            client:metricq client connection,
            partition_name: name of the cluster

        return: energy results"""
        try:
            energy_results = []
            for node, values in datas.items():

                energy_per_job_in_node = {}
                used_core_naive: dict[str, float] = {}
                try:

                    for block, block_details in values.items():

                        duration = (
                            block_details["end_time"]
                            - block_details["start_time"]
                        )
                        start_time = metricq.Timestamp.from_posix_seconds(
                            block_details["start_time"]
                        )
                        end_time = metricq.Timestamp.from_posix_seconds(
                            block_details["end_time"]
                        )

                        metric = self.metricq_uri.get(partition_name)
                        if metric is not None:
                            metric = metric.format(node)
                        else:
                            raise ValueError(
                                f"Metric for partition {partition_name} is None"
                            )

                        mean_power = await self.meanpower(
                            metric, start_time, end_time, client
                        )
                        if mean_power == -1 or mean_power == -2:
                            logger.warning(
                                "[WARNING] Unable to get mean value (for non exclusive jobs) from metricq"
                            )
                            return [mean_power]
                        total_used_cores = 0
                        for used_cores in block_details.get("job").values():
                            total_used_cores += used_cores

                        for job_id, job_core in block_details.get(
                            "job"
                        ).items():

                            energy_per_job = (
                                0
                                if mean_power == 0
                                else self.joules_to_kwh(
                                    (duration * mean_power * job_core)
                                    / total_used_cores
                                )
                            )

                            used_core_naive[job_id] = (
                                used_core_naive.get(job_id, 0) + energy_per_job
                            )

                            energy_per_job_in_node["used_core_naives"] = (
                                used_core_naive
                            )
                except Exception as e:
                    logger.warning(
                        "[WARNING] Failed to process blocks in node %s for non exclusive jobs: %s",
                        node,
                        e,
                    )
                    return [-1]
                energy_results.append({node: energy_per_job_in_node})
            return energy_results
        except Exception as e:
            logger.warning(
                "[WARNING] Unable to process get_energy_for_non_exclusive %s:",
                e,
            )
            return [-1]

    async def get_energy_for_exclusive(
        self,
        start_time: Any,
        end_time: Any,
        client: Any,
        partition_name: str,
        job_id: int,
        node: str,
    ) -> List[Any]:
        """calculate energy for exclusive jobs
        args:
            start_time:unix_timestamp (job start time),
            end_time:unix_timestamp (job end time),
            client:metricq client connection,
            partition_name: name of the cluster,
            job_id: id of the specific job,
            node: node name on which job is running

        return: energy results
        """
        try:
            energy_results = []
            energy_per_job_in_node = {}
            duration = end_time - start_time

            start_time = metricq.Timestamp.from_posix_seconds(start_time)
            end_time = metricq.Timestamp.from_posix_seconds(end_time)
            metric = self.metricq_uri.get(partition_name)
            if metric is not None:
                metric = metric.format(node)
            else:
                raise ValueError(
                    f"Metric for partition {partition_name} is None"
                )

            mean_power = await self.meanpower(
                metric, start_time, end_time, client
            )
            if mean_power == -1 or mean_power == -2:
                logger.warning(
                    "[WARNING] Unable to  get mean value (for exclusive jobs) from metricq"
                )
                return [mean_power]
            energy = self.joules_to_kwh(mean_power * duration)
            energy_per_job_in_node["used_core_naives"] = {job_id: energy}
            energy_results.append({node: energy_per_job_in_node})
        except Exception as e:
            logger.warning(
                "[WARNING] Failed to process for node %s for exclusive jobs: %s",
                node,
                e,
            )
            return [-1]
        return energy_results


class JobBlockInfo:
    """get the overlapping jobs from mariadb for given job id and split into job blocks"""

    def __init__(self, sql_connection: Any) -> None:
        """constructor for JobBlockInfo class"""
        self.__sql_connection = sql_connection
        self.__job_block: Optional[List[Any]] = None

    def set_job_block(
        self,
        detailed_job_node_list: Optional[List[str]] = None,
        job_start_time: Optional[Any] = None,
        job_end_time: Optional[Any] = None,
    ) -> None:
        """
        Sets the job block based on the given job nodes and time range.

        If all arguments are provided, it retrieves overlapping job data and splits the job block.
        If arguments is missing, it sets the job block to None.

        Args:
            detailed_job_node_list: List of nodes involved in the job.
            job_start_time: Start time of the job.
            job_end_time: End time of the job.

        Returns:
            None
        """
        if detailed_job_node_list and job_start_time and job_end_time:
            overlapping_jobs = self.get_shared_jobs_data(
                detailed_job_node_list, job_start_time, job_end_time
            )
            self.__job_block = self.job_split(
                overlapping_jobs, job_start_time, job_end_time
            )
        else:
            self.__job_block = None

    def get_job_block(self) -> Optional[List[Any]]:
        """return the job block list"""
        return self.__job_block

    def expand_corelist(self, core_list: str, node_name: str) -> List[int]:
        """
        Expands a list of core identifiers in the format 'node_name[core_ranges]'
        into an expanded list of core numbers.

        Args:
            core_list (str): A string representing the core list with ranges,
                              e.g., 'node[1-4,7,10-12]'.
            node_name (str): The name of the node to match in the core list.

        Returns:
            list: A list of integers representing individual core numbers.
                  For example, 'node[1-4,7,10-12]' will be expanded to
                  [1, 2, 3, 4, 7, 10, 11, 12]"""

        pattern = rf"{node_name}\[([^\]]+)\]"
        corelist = []

        matches = re.findall(pattern, str(core_list))

        if matches:
            ranges = matches[0]
            for part in ranges.split(","):
                if "-" in part:
                    start, end = map(int, part.split("-"))
                    corelist.extend(list(range(start, end + 1)))
                else:
                    corelist.append(int(part))

        return corelist

    def start_list(
        self, values: List[Dict[str, Any]], start_time: Any, end_time: Any
    ) -> List[Any]:
        """create a start time list for the given time range(start_time, end_time)
        args:
             values:overlapping jobs for specific node in the given time range
             start_time: start time of specific job
             end_time:end time of specific job
        Returns:
             list: A list of start times within the given range."""

        start_time_list = []
        for value in values:
            time = value["start_time"]
            if time >= start_time and time <= end_time:
                start_time_list.append(time)
        return start_time_list

    def end_list(
        self,
        values: List[Dict[str, Any]],
        start_time: Any,
        end_time: Any,
        time_list: List[Any],
    ) -> List[Any]:
        """create a end time list for the given time range(start_time, end_time)
        args:
             values:overlapping jobs for specific node in the given time range
             start_time: start time of specific job
             end_time:end time of specific job
             time_list:start timelist
        Returns:
             list: A list of end times within the given range."""
        for value in values:
            time = value["end_time"]
            if time >= start_time and time <= end_time:
                if time not in time_list:
                    time_list.append(time)
        return time_list

    def get_job_core(
        self, values: List[Dict[str, Any]], start_time: Any, end_time: Any
    ) -> Dict[str, Any]:
        """
        Returns job core for jobs that overlap with the given time range.

        Args:
            values: A list of job data.
            start_time: start time from sorted timelist.
            end_time: end time from sorted timelist.

        Returns:
            dict: A dictionary with the start and end time, and job IDs with their core usage.
        """
        job = {}

        for value in values:
            st = value["start_time"]
            et = value["end_time"]
            if st <= start_time and et >= end_time:
                job[value.get("job_id")] = value.get("total_core")
        return {"start_time": start_time, "end_time": end_time, "job": job}

    def job_split(
        self, node_data: Dict[str, Any], start_time: Any, end_time: Any
    ) -> List[Any]:
        """Generate job blocks for nodes based on the provided start and end times.

        Args:
        node_data (dict): A dictionary where keys represent node identifiers
                          and values contain data relevant to the job for each node.
        start_time (Any): The initial start time of the job.
        end_time (Any): The final end time of the jobs.

        Returns:
        list: A list of dictionaries, where each dictionary represents a node
              and its corresponding job blocks. Each job block is identified
              by a key (e.g., "block1", "block2", ...) and contains the job
              data for the respective time interval."""
        try:
            node_block = []
            end_fixed = end_time
            start_fixed = start_time

            for node, values in node_data.items():
                try:
                    job_split_data = {}
                    start_time = start_fixed
                    end_time = end_fixed
                    time_list = self.start_list(values, start_time, end_time)
                    time_list = self.end_list(
                        values, start_time, end_time, time_list
                    )
                    time_list = list(dict.fromkeys(time_list))
                    time_list.sort()
                except Exception as e:
                    logger.warning(
                        "[WARNING] Failed to create timelist for node %s: %s",
                        node,
                        e,
                    )
                    continue

                for i in range(1, (len(time_list))):
                    try:
                        start_time = time_list[i - 1]
                        end_time = time_list[i]
                        job = self.get_job_core(values, start_time, end_time)
                        job_split_data[f"block{i}"] = job
                    except Exception as e:
                        logger.warning(
                            "[WARNING] Failed to split job for node %s in block %s: %s",
                            node,
                            i,
                            e,
                        )
                        continue

                node_block.append({node: job_split_data})
            return node_block
        except Exception as e:
            logger.warning(
                "[WARNING] Unable to split the job: %s",
                e,
            )
            return []

    def get_shared_jobs_data(
        self, detailed_node_list: List[str], start_time: Any, end_time: Any
    ) -> Dict[str, Any]:
        """
        Retrieves overlapping jobs for a specified time range across nodes.

        Args:
            detailed_node_list (list): List of nodes to query for overlapping jobs.
            start_time (Any): The initial start time of the job.
            end_time (Any): The final end time of the job.

        Returns:
            dict: A dictionary where keys are node names and values are lists of
                  overlapping job details, including job ID, start/end times, core count,
                  and core list.
        """

        overlapping_jobs: dict[str, List[Dict[str, Any]]] = {}

        parsed_job = []
        for node in detailed_node_list:

            query_filter = JobDataFilter(
                start_time=start_time,
                end_time=end_time,
                node_name_like=node,
                overlap=True,
            )
            query = sql.view.topdown.JobIDQuery.build(query_filter)

            try:
                result_set = self.__sql_connection.execute(query=query)
                parsed_jobs = sql.view.topdown.JobIDOrm.from_rows(result_set)

                parsed_job.append(parsed_jobs)

            except SQLError as error:
                logger.error(error)

            # Collect job information and expand core list only once
            try:
                overlapping_jobs.setdefault(node, []).extend(
                    {
                        "job_id": value["job_id"],
                        "start_time": value["start_time"],
                        "end_time": (
                            value["end_time"]
                            if value["end_time"] is not None
                            else end_time
                        ),
                        "total_core": len(core_list_expanded),
                        "core_list": core_list_expanded,
                    }
                    for value in parsed_jobs
                    if value["job_state"] != "Corrupt"
                    and (
                        core_list_expanded := self.expand_corelist(
                            value["core_list"], node
                        )
                    )
                )

            except KeyError as error:
                logger.warning(
                    "[WARNING] Missing key in parsed job data for node %s: %s",
                    node,
                    error,
                )

        return overlapping_jobs


class AsyncClientManager:
    """Handles client operations"""

    def __init__(
        self, token: str, server: str, check_client: Dict[str, Any]
    ) -> None:
        """constructor for AsyncClientManager class"""
        self.__token = token
        self.__server = server
        self.__check_client = check_client
        self.__client: Optional[HistoryClient] = None
        self._initialize = False

    async def get_client(self) -> Any:
        """return the client"""
        if self.__client is None and not self._initialize:
            logger.info("[INFO] Creating new client")
            await self._create_client()
        return self.__client

    async def _create_client(self) -> Any:
        """Create the client when self.__client is None and self._initialize is False"""
        try:
            client = metricq.HistoryClient(
                self.__token, self.__server, add_uuid=True
            )
            await client.connect()
            logger.info("[INFO] Client Created: %s", id(client))
            self.__client = client
            self._initialize = True
            return self.__client
        except Exception as e:
            logger.error("[ERROR] Failed to create metricq client:%s", e)

    async def stop_client(self) -> None:
        """stop the client"""
        if self.__client is None:
            logger.info("[INFO] Client not initialized")
            return
        try:
            await self.__client.stop()
            logger.info("[INFO] client stopped")
        except Exception as e:
            logger.warning("[WARNING] Unable to stop the client: %s", e)

    async def check_client(self) -> bool:
        """check the client connection"""
        try:
            metric = self.__check_client["metric"]

            start_time = metricq.Timestamp.from_posix_seconds(
                self.__check_client["start_time"]
            )
            end_time = metricq.Timestamp.from_posix_seconds(
                self.__check_client["end_time"]
            )
            if self.__client is not None:
                aggregate = await self.__client.history_aggregate(
                    metric, start_time, end_time
                )
                if aggregate.count == 2:
                    logger.info("[INFO] Client checked successfully")
                    return False
            return True
        except (
            AssertionError,
            AgentStopped,
            ConnectFailed,
            ReconnectTimeout,
            Exception,
        ) as e:
            logger.warning(
                "[WARNING] Unexpected happened: %s - %s while checking the client",
                type(e).__name__,
                e,
            )
            logger.info("[INFO] Client checked failed")
            self._initialize = False
            self.__client = None
            return True


class JobEnergy:
    """calculate energy usage and efficiency for jobs"""

    def __init__(
        self,
        job_block: Any,
        client_manager: Any,
        metricq_data: Any,
        metricq_uri: Dict[str, str],
    ):
        """constructor for JobEnergy class"""

        self.__job_block = job_block
        self.__client_manager = client_manager
        self.__metricq_data = metricq_data
        self.__metricq_uri = metricq_uri

    def energy_per_job(
        self, lists: List[List[Dict[str, Any]]], job_id: int
    ) -> Dict[int, Dict[str, float]]:
        """
        Calculate total energy usage for a specific job based on provided data.

        Args:
            lists: A list of job-related energy data.
            job_id: The ID of the job to calculate energy for.

        Returns:
            dict: A dictionary containing the total energy usage for the job.
                  Example: {"job_id": {"used_core_naives": <value>}}

            Returns None if an error occurs.
        """
        try:

            used_core_naive = sum(
                values["used_core_naives"].get(job_id, 0)
                for list in lists
                for i in list
                for node, values in i.items()
            )

            return {
                job_id: {
                    "used_core_naives": used_core_naive,
                }
            }
        except Exception as e:
            logger.warning(
                "[WARNING] Failed to calculate job energy for %s: %s", job_id, e
            )
            return {}

    def job_energy_efficiency(
        self,
        job_energy_in_kwh: float,
        job_data: JobDataORM,
        flops_any_mean_per_core: float,
    ) -> float | None:
        """
        Calculate the energy efficiency of a job.

        Args:
            job_energy_in_kwh (float): Total energy used by the job in kwh.
            job_data: Object containing job details like core count, start time,
                      and end time.
            flops_any_mean_per_core (float): Mean FLOPS (Floating Point Operations Per Second)
                                             per core.
        Returns:
            float: The energy efficiency (GFLOPS/W) of the job in GFLOPS per Watt.
                   Returns 0 if job_energy is zero and None if an error occurs.
        """
        try:
            job_duration = job_data.end_time - job_data.start_time
            if job_energy_in_kwh == 0 or job_duration == 0:
                return 0.0
            gflops_any = (flops_any_mean_per_core * job_data.core_count) / 10**9
            power_in_watts = (job_energy_in_kwh * 1000 * 3600) / job_duration
            energy_efficiency_gflops_per_watt = round(
                gflops_any / power_in_watts, 2
            )
            return energy_efficiency_gflops_per_watt

        except Exception as e:
            logger.warning(
                "[WARNING] Failed to calculate job efficiency for %s: %s",
                job_data.job_id,
                e,
            )
            return None

    async def exec_energy_calculations(
        self, job_block: List[Optional[dict[str, Any]]], job_data: JobDataORM
    ) -> List[Any]:
        """
        Perform energy calculations for jobs asynchronously.

        Args:
            job_block: Job blocks containing the time and partition information.
            job_data: Object containing job details like start time, end time,
                      partition, and node list.

        Returns:
            list: A list of energy calculation results for each node.

        """
        try:
            client = await self.__client_manager.get_client()

            if job_block is None and job_data.exclusive == 1:

                result = [
                    asyncio.create_task(
                        self.__metricq_data.get_energy_for_exclusive(
                            job_data.start_time,
                            job_data.end_time,
                            client,
                            job_data.partition_name,
                            job_data.job_id,
                            node,
                        )
                    )
                    for node in hostlist.expand_hostlist(job_data.node_list)
                ]
                results = await asyncio.gather(*result)
                return (
                    [-2]
                    if any(result == [-2] for result in results)
                    else (
                        [-1]
                        if any(result == [-1] for result in results)
                        else results
                    )
                )
            result = [
                asyncio.create_task(
                    self.__metricq_data.get_energy_for_non_exclusive(
                        block,
                        client,
                        job_data.partition_name,
                    )
                )
                for block in job_block
            ]

            results = await asyncio.gather(*result)
            return (
                [-2]
                if any(result == [-2] for result in results)
                else (
                    [-1]
                    if any(result == [-1] for result in results)
                    else results
                )
            )
        except Exception as e:
            logger.warning(
                "[WARNING] Unable to process exec energy calculations: %s",
                e,
            )
            return [-1]

    async def energy_results_for_job(
        self, job_data: JobDataORM, flops_any_mean_per_core: float
    ) -> Tuple[Optional[float], Optional[float]]:
        """
        Generate energy usage and efficiency results for a specific job.

        Args:
            job_data: Object containing job details like start time, end time,
                      node list, and whether the job is exclusive.
            flops_per_core (float): The number of FLOPs (floating-point
                                    operations) performed per core.

        Returns:
            tuple: A tuple containing:
                - job_energy (float): Total energy used by the job in kwh.
                - job_energy_efficiency (float): Energy efficiency in GFLOPs per Watt.

            Returns (None, None) if an error occurs or maximum retries are reached.
        """
        try:
            if job_data.partition_name not in self.__metricq_uri.keys():
                return None, None

            if job_data.exclusive != 1:
                self.__job_block.set_job_block(
                    hostlist.expand_hostlist(job_data.node_list),
                    job_data.start_time,
                    job_data.end_time,
                )
            else:
                self.__job_block.set_job_block()

            job_block = self.__job_block.get_job_block()

            max_tries = 2

            for i in range(max_tries):
                try:
                    results = await asyncio.wait_for(
                        self.exec_energy_calculations(job_block, job_data),
                        timeout=120,
                    )
                    if results[0] == -2:
                        logger.warning(
                            "[WARNING] Skipped processing for %d as the node %s in the metricq doesn't exist",
                            job_data.job_id,
                            hostlist.expand_hostlist(job_data.node_list),
                        )
                        return None, None

                    if results[0] != -1:
                        job_energy = self.energy_per_job(
                            results, job_data.job_id
                        )
                        job_energy_dict = job_energy.get(job_data.job_id)
                        if job_energy_dict is not None:

                            job_energy_value = round(
                                job_energy_dict["used_core_naives"],
                                2,
                            )
                        else:
                            job_energy_value = 0.0
                        job_energy_efficiency = self.job_energy_efficiency(
                            job_energy_value, job_data, flops_any_mean_per_core
                        )
                        return job_energy_value, job_energy_efficiency

                    logger.info(
                        "[Retry] Invalid job energy results in attempt %d. Retrying",
                        i + 1,
                    )

                    await self.__client_manager.check_client()

                except asyncio.TimeoutError:

                    logger.info("Timeout in attempt %d. Retrying...", i + 1)
                except Exception as e:

                    logger.info("Cannot process in attempt %d: %s", i + 1, e)
            raise RuntimeError(
                "Maximum retries reached for exec_energy_calculations."
            )
        except Exception as e:
            logger.warning(
                "[WARNING] Cannot process energy results for %d: %s",
                job_data.job_id,
                e,
            )
            return None, None
