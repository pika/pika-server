"""
Collectd runs on each compute node and writes performance metrics
(CPU utilization, IO rates ...) to the Influx database at regular intervals.

The SQL database only contains the metadata of the submitted jobs with the
requested resources (number of CPUs, number of GPUs, etc.).

In order to better analyze the jobs from the SQL database, an average value
(or maximum value) is calculated for each performance metric and stored
in the SQL database.

Based on the workload data, each job is labelled (JobLabeller)
and analyzed for problems (JobIssue).
"""

import math
import time
from multiprocessing import Process, cpu_count
from os import nice
from typing import Dict, List

import numpy as np
from apscheduler.schedulers.blocking import BlockingScheduler
from apscheduler.triggers.cron import CronTrigger
from scipy.fft import fft, fftfreq
from sqlalchemy import and_, delete, insert, or_, update

import sql.view.meta
import utils.common
import utils.specification
from influx.connection import InfluxConnection, InfluxError
from influx.model import TimelineMetric, TimelineParameter
from influx.query import FootprintQueryBuilder, IssueQueryBuilder
from sql.connection import SQLConnection
from sql.model import (
    FootprintAnalysisORM,
    FootprintBaseORM,
    FootprintFileioORM,
    FootprintGpuORM,
    JobDataFilter,
    JobDataORM,
    SQLError,
)
from sql.view.meta import TimelineORM
from utils.common import four_weeks_ago, three_weeks_ago
from utils.logger import logger
from utils.model import DBCredential

from .config import PostProcessingConfiguration

# mypy: ignore-errors


def main(
    config: PostProcessingConfiguration, spec: utils.specification.Specification
):
    """Register post-processing at execution schedule specified in configuration."""

    logger.info("registering post-processing for %s", config.execution_schedule)

    scheduler = (
        BlockingScheduler()
    )  # let the process sleep if we dont do any post-processing

    scheduler.add_job(
        func=post_processing_wrapper,
        trigger=CronTrigger.from_crontab(config.execution_schedule),
        args=[config, spec],
    )

    scheduler.start()


def post_processing_wrapper(
    _config: PostProcessingConfiguration,
    spec: utils.specification.Specification,
):
    """
    - search job uids we will do post processing on
    - start post processing
    """

    sql_connection = SQLConnection(_config.mariadb)

    query_filter = JobDataFilter(start_time=three_weeks_ago(), limit=False)
    # a job is allowed to run 3 weeks at maximum

    try:
        jobs = sql.view.meta.query_empty_footprint(sql_connection, query_filter)
    except SQLError as error:
        logger.critical(error)
        return

    job_uids = [job.uid for job in jobs]
    # e.g. job_uids = [338752, 338753, 338755]

    logger.info(f"starting post processing for {len(job_uids)} jobs")

    try:
        Postprocessing(
            _config.mariadb,
            _config.influxdb_st,
            _config.influxdb_lt,
            _config.extern_timeline,
            spec,
        ).run(job_uids)
    except SQLError as error:
        logger.critical(error)
        return

    logger.info("finished post processing")


class Postprocessing:
    """post processing consists of 5 steps:

    1. clean broken jobs
    2. calculate average,max resource utilization of jobs, also known as "footprints"
    3. calculate and insert tags for jobs based on footprints
    4. calculate and insert issues for jobs based on footprints
    5. insert footprints into footprint_base, _filio & _gpu, and mark the job as completed

    we are focusing on multiprocessing
    """

    def __init__(
        self,
        mariadb: DBCredential,
        influx_st: DBCredential,
        influx_lt: DBCredential,
        extern_timeline: dict[str, str],
        spec: utils.specification.Specification,
    ) -> None:
        self._mariadb = mariadb
        self._influx_st = influx_st
        self._influx_lt = influx_lt
        self._extern_timeline = extern_timeline

        self.spec = spec

    def run(self, job_uids: List[JobDataORM.uid] | List[int]):
        """start post processing

        Args:
            job_uids: list of job_data.uid

        Raises:
            sql.model.SQLError: sqldb api error
        """

        # we take job uids because they are indexed in the sql database,
        # so queries over several years are still fast, and the amount of data
        # doenst fill our heap

        # step 1
        # if you wonder why, perfoming post processing on less than hundred jobs
        # take so long, JobCleaner is the reason because of heavy sql queries

        sql_connection = SQLConnection(self._mariadb)
        cleaning_queries = create_cleaning_queries()
        for query in cleaning_queries:
            sql_connection.execute(query)

        # step 2,3,4,5,
        self._do_multiprocessing(job_uids)

    def _do_multiprocessing(self, job_uids: List[int]):
        """
        resposible for starting steps 2,3,4,5
        lower level implemetation detail
        """

        nice(10)
        # we are using many cores, so we want to be nice to the computer

        proc_count = (
            # use 24 cores at max
            cpu_count()
            if cpu_count() <= 24
            else 24
        )
        # e.g. 8

        job_count = len(job_uids)
        # e.g. 173

        jobs_per_process = int(job_count / proc_count)
        # e.g. 21

        if jobs_per_process == 0:
            # dont do multiprocessing when there are less jobs than cpus
            self._finalize(job_uids)
            return

        processes: List[Process] = []

        for i in range(0, proc_count):
            start_index = i * jobs_per_process
            # e.g. 0
            end_index = (i + 1) * jobs_per_process
            # e.g. 21, now you wonder: "but 20 should be last index !",
            # you are right, continue reading

            if i == proc_count - 1:
                # last process should create footrpints for remaining jobs
                piece_of_work = job_uids[start_index:]
            else:
                piece_of_work = job_uids[start_index:end_index]
                # python knowledge: array[start:stop] is actually "array from start to stop-1"

            processes.append(
                Process(
                    name=f"proc_{i}",
                    target=self._finalize,
                    args=[piece_of_work],
                )
            )

        for p in processes:
            p.start()

        for p in processes:
            p.join()

    def _finalize(self, job_uids: List[int]):
        """running steps 3,4,5,6"""

        sql_connection = SQLConnection(self._mariadb)
        influx_st_connection = InfluxConnection(self._influx_st)
        influx_lt_connection = InfluxConnection(self._influx_lt)

        if self._extern_timeline:
            from post_processing.job_energy import initialize_energy_processing

            job_energy, client_manager, eventloop = (
                initialize_energy_processing(
                    sql_connection,
                    self._extern_timeline["token"],
                    self._extern_timeline["server"],
                )
            )

        for uid in job_uids:
            try:
                job = sql.view.meta.query_timeline_meta(
                    sql_connection, JobDataFilter(uid=uid)
                )
                # e.g. [{'start_time': 1692359518, 'end_time': 1692359524, 'partition_name':
                # 'haswell256', 'uid': 239274, 'node_list': 'taurusi6549',
                # 'gpu_list': '', 'core_list': 'taurusi6549[0]', 'gpu_count': 0,
                # 'exclusive': 0}]
                # count = count + 1

                if not self._is_suitable_for_postprocessing(job):
                    continue

                influx_connection = (
                    influx_st_connection
                    if job.start_time > three_weeks_ago()
                    else influx_lt_connection
                )

                footprints = JobFootprintCreator(job, self.spec).run(
                    influx_connection
                )  # step 2

                # add job energy value if metricq available

                footprints["energy_in_kwh"] = None
                footprints["energy_efficiency_in_gflops_per_watt"] = None

                if self._extern_timeline:

                    (
                        footprints["energy_in_kwh"],
                        footprints["energy_efficiency_in_gflops_per_watt"],
                    ) = eventloop.run_until_complete(
                        job_energy.energy_results_for_job(
                            job,
                            footprints.get("flops_any_mean_per_core", 0),
                        )
                    )

                JobLabeller(job, self.spec).run(
                    sql_connection, footprints
                )  # step 3

                JobIssue(job, self.spec.metric).run(
                    sql_connection, influx_connection, self.spec
                )  # step 4

                JobFootprintInserter(job).run(
                    sql_connection, footprints
                )  # step 5

            except SQLError as error:
                logger.critical(error)
                return

        if self._extern_timeline:
            try:
                eventloop.run_until_complete(client_manager.stop_client())
                eventloop.close()

            except Exception as e:

                logger.warning(
                    "[WARNING] Unable to close eventloop and client in finalize:%s",
                    e,
                )

    def _is_suitable_for_postprocessing(self, job: TimelineORM):
        """calculating footprints for a job only makes sense after it has been
        running for at least 60 seconds, because collectd takes a snapshot of
        the workload every 30 or 60 seconds. Since the footprint generation
        can be controlled by the user, and thus queries can be submitted over
        several years, it is too much load for the database to perform an
        "end-start >= 60" calculation for every single job. A query that
        normally takes 5 seconds exceeds the 5 minute mark with the calculation

        For this reason, this function filters out all jobs that are not
        suitable for post processing"""

        try:
            return job.end_time - job.start_time >= 60
        except TypeError:
            # happens when end or start is None
            return False


class JobFootprintCreator:
    """calculate the jobs average or max resource utilizations"""

    def __init__(
        self, job: TimelineORM, spec: utils.specification.Specification
    ) -> None:
        self._job = job
        self.spec = spec

    def run(self, influx_connection: InfluxConnection) -> dict:
        """calculate footprints for a single job

        Args:
            partition (dict): result of sql.cache.partition
            influx_connection (InfluxConnection)

        Returns:
            dict: the calculatet footprints

        Example Return:
        ```
            {
                "cpu_used_mean_per_core": 0.5757557335823822,
                "host_mem_used_max_per_node": 3914268672.0,
                "ipc_mean_per_core": 1.9783703549050795,
                "read_bytes": 0.0,
                "write_bytes": 0.0,
                "fsname": "lustre_horse",
                "flops_any_mean_per_core": 2078545.623990613,
                "mem_bw_mean_per_socket": 1610570402.828602,
                "ib_bw_mean_per_node": 76336300.17374189,
                "cpu_power_mean_per_socket": 65.09498964787969,
            }
        ```
        """

        footprints = {}
        for q in self._create_influx_queries(self._job):
            try:
                footprint = influx_connection.expect_one_result(q)
            except InfluxError:
                # FootprintQueryBuilder will produce a None "where clause"
                # when dealing with julia partition and likwid socket cpus
                # fix this in FootprintQueryBuilder, by throwing an error
                continue
            try:
                footprints.update(footprint)
            except TypeError:
                # occurs when trying to push None into dict
                continue

        if len(footprints) > 0:
            # determine dominant file system that is used by the job and modify the io metrics

            # example for original metrics:
            # {'lustre_scratch2__read_bytes': 113571513.96247905,
            #  'lustre_scratch2__write_bytes': 7093948.97004989,
            #  'lustre_highiops__read_bytes': 0.0,
            #  'lustre_highiops__write_bytes': 981484119.7365148}

            # example for revised io metrics:
            # {'fsname': 'lustre_highiops', 'read_bytes': 0.0, 'write_bytes': 981484119.7365148}

            # (1) collect all io metrics and sum up read_bytes and write_bytes
            # (2) determine the maxium of summed up bytes
            # (3) get prefix of dominant file system (e.g. lustre_highiops) and create an item for it
            #     and remove prefix (e.g. lustre_highiops__) from the original keys
            # (4) remove all other io metrics

            # (1)
            io_metrics = {}
            for f, v in footprints.items():
                if "__read_bytes" in f or "__write_bytes" in f:
                    if v is None:
                        v = 0
                    fsname = f.split("__")[0]
                    if fsname:
                        # sum up read_bytes and write_bytes for each fsname
                        if fsname in io_metrics:
                            io_metrics[fsname] += v
                        else:
                            io_metrics[fsname] = v

            if len(io_metrics) > 0:
                # (2)
                dom_fsname = max(io_metrics, key=io_metrics.get)

                # (3) and (4)
                keys_to_delete = list(footprints.keys())
                for key in keys_to_delete:
                    if "__read_bytes" in key or "__write_bytes" in key:
                        fsname = key.split("__")[0]
                        if fsname == dom_fsname:
                            footprints["fsname"] = fsname
                            io_metric = key.split("__")[1]
                            footprints[io_metric] = footprints[key]

                        del footprints[key]

        return footprints

    def _create_influx_queries(self, job: TimelineORM) -> List[str]:
        """Create influx queries that determine the maximum or average resource
        utilization of the job

        Return example:
            `['SELECT mean("used") as cpu_used_mean_per_core FROM "cpu"
            WHERE ((hostname=\'taurusi8011\') and (cpu=\'24\' or cpu=\'25\'))
            and time >= 1677247344s and time <= 1677247465s', ...]`
        """

        footprint_builder = FootprintQueryBuilder(
            job, TimelineMetric.cpu_usage, TimelineParameter(), self.spec
        )

        footprint_queries = []

        for metric in self.spec.metric:
            try:
                configuration = self.spec.metric[metric]["footprint_conf"]
                # metrics which have a footprint_conf field will be inserted into the sql db
            except KeyError:
                continue

            aggregation = configuration["aggregation"]
            sql_table_name = configuration["table"]
            sql_column_name = configuration["name"]

            # one special case: footprint_filio is calculated as mean * job_duration
            if sql_table_name == "footprint_fileio":
                job_duration = job.end_time - job.start_time
                aggregation = f"{job_duration} * {aggregation}"

            footprint_builder._change_metric(metric)
            footprint_queries.append(
                footprint_builder.get_footprint_query(
                    label=sql_column_name,
                    aggregation=aggregation,
                )
            )

        return footprint_queries


def create_cleaning_queries():
    """create queries for cleaning and updating broken jobs"""

    queries = []

    # delete jobs wihtout name or corelist
    queries.append(
        delete(JobDataORM).filter(
            and_(
                JobDataORM.start_time >= four_weeks_ago(),
                or_(
                    JobDataORM.job_name.is_(None),
                    JobDataORM.core_list.is_(None),
                ),
            ),
        )
    )

    # update jobs without end date
    queries.append(
        update(JobDataORM)
        .filter(
            and_(
                JobDataORM.start_time >= four_weeks_ago(),
                JobDataORM.end_time.is_(None),
                JobDataORM.job_state != "running",
            )
        )
        .values(
            end_time=JobDataORM.start_time + JobDataORM.time_limit,
            property_id=8,
        )
    )

    # update jobs with negative duration
    queries.append(
        update(JobDataORM)
        .filter(
            and_(
                JobDataORM.start_time >= four_weeks_ago(),
                (JobDataORM.end_time - JobDataORM.start_time < 0),
            )
        )
        .values(start_time=JobDataORM.end_time - 40)
    )

    # update jobs with negativ pending time
    queries.append(
        update(JobDataORM)
        .filter(
            and_(
                JobDataORM.start_time >= four_weeks_ago(),
                (JobDataORM.start_time - JobDataORM.submit_time < 0),
            )
        )
        .values(submit_time=JobDataORM.start_time - 1)
    )

    # stop jobs which run longer than 3 weeks
    queries.append(
        update(JobDataORM)
        .filter(
            and_(
                JobDataORM.job_state == "running",
                JobDataORM.start_time >= four_weeks_ago(),  # newer than 4 weeks
                JobDataORM.start_time < three_weeks_ago(),  # older than 3 weeks
            )
        )
        .values(
            end_time=JobDataORM.start_time + JobDataORM.time_limit,
            job_state="corrupt",
            property_id=8,
        )
        # "property_id = 8" means that we have ended this job
    )

    # label jobs as corrupt, that are running and exceeded walltime
    queries.append(
        update(JobDataORM)
        .filter(
            and_(
                JobDataORM.job_state == "running",
                JobDataORM.start_time
                >= three_weeks_ago(),  # newer than 3 weeks
                JobDataORM.time_limit
                < (int(time.time()) - JobDataORM.start_time - 300),
                # walltime < runtime (5 min buffer to get timeout status)
            )
        )
        .values(
            end_time=JobDataORM.start_time + JobDataORM.time_limit,
            job_state="corrupt",
            property_id=8,
        )
    )

    return queries


class JobLabeller:
    """Every job gets a tag (Label)

    calling this class JobLabeller instead of JobTagger because google translate
    doesnt understand what a JobTagger is

    0_0000 normal job
    0_0001 memory bound
    0_0010 compute bound
    0_0100 io heavy
    0_1000 network heavy
    1_0000 GPU bound
    """

    NORMAL_JOB = 0b0_0000
    MEMORY_BOUND = 0b0_0001
    COMPUTE_BOUND = 0b0_0010
    IO_HEAVY = 0b0_0100
    NETWORK_HEAVY = 0b0_1000
    GPU_BOUND = 0b1_0000
    # specification according to job_tag (job_tag.json)

    def __init__(
        self, job: TimelineORM, spec: utils.specification.Specification
    ) -> None:
        self._job = job
        self.spec = spec

    def run(self, sql_connection: SQLConnection, footprints: dict) -> int:
        """create and inserts job tag

        Args:
            sql_connectoin (SQLConnection)
            partitions (dict): result of sql.cache.partition
            footprints (dict): sql.model.footprint_base,
                sql.model.footprint_fileio & sql.model.footprint_gpu as dict

        Returns:
            int: the calculated job tag

        Raises:
            sql.model.SQLError: sqldb api error
        """

        job_tag = self._create_tag(footprints)

        sql_connection.execute(
            update(JobDataORM)
            .filter(JobDataORM.uid == self._job.uid)
            .values(tags=job_tag)
        )

        return job_tag

    def _create_tag(self, footprints: dict):
        # tag_id for normal jobs is zero
        tag_id = self.NORMAL_JOB

        try:
            partition_info = self.spec.partition[self._job.partition_name]
        except KeyError:
            # KeyError: happens at invalid Partition e.g. datamover
            logger.critical(
                "jobid:%s runs on partition:%s, which does not exist in the pika specification",
                self._job.job_id,
                self._job.partition_name,
            )
            return tag_id

        try:
            memory_measured = footprints.get("mem_bw_mean_per_socket")
            memory_max = partition_info.max_mem_bw

            if memory_measured / memory_max > 0.8:
                tag_id += self.MEMORY_BOUND

        except TypeError:
            # happens at None values
            pass

        try:
            flops_measured = footprints.get("flops_any_mean_per_core")
            flops_max = partition_info.max_flops
            ipc_measured = footprints.get("ipc_mean_per_core")
            ipc_max = partition_info.max_ipc

            if (flops_measured / flops_max > 0.7) or (
                ipc_measured / ipc_max > 0.6
            ):
                tag_id += self.COMPUTE_BOUND

        except TypeError:
            pass

        # Maximal IO bandwidth in B/s
        lustre_io_max_bw = {"SCRATCH2": 19171839421, "HIGHIOPS": 24286765005}

        try:
            scratch2_measured = (
                footprints.get("lustre_scratch2_read_bytes")
                + footprints.get("lustre_scratch2_write_bytes")
            ) / (self._job.end_time - self._job.start_time)
            scratch2_max = lustre_io_max_bw["SCRATCH2"]

            highiops_measured = (
                footprints.get("lustre_highiops_read_bytes")
                + footprints.get("lustre_highiops_write_bytes")
            ) / (self._job.end_time - self._job.start_time)
            highiops_max = lustre_io_max_bw["HIGHIOPS"]

            if (scratch2_measured / scratch2_max) > 0.6 or (
                highiops_measured / highiops_max
            ) > 0.6:
                tag_id += self.IO_HEAVY

        except TypeError:
            pass

        # Maximal network traffic in B/s
        network_max_bw = {"INFINIBAND": 7000000000}

        try:
            ib_bw_measured = footprints.get("ib_bw_mean_per_node")
            ib_bw_max = network_max_bw["INFINIBAND"]

            if ib_bw_measured / ib_bw_max > 0.6:
                tag_id += self.NETWORK_HEAVY
        except TypeError:
            pass

        try:
            gpu_usage = footprints.get("used_mean_per_gpu")
            cpu_usage = footprints.get("cpu_used_mean_per_core")

            if gpu_usage > 0.7 or gpu_usage > cpu_usage:
                tag_id += self.GPU_BOUND

        except TypeError:
            pass

        return tag_id


class JobFootprintInserter:
    """removes old footprints of job and inserts new ones"""

    def __init__(self, job: TimelineORM) -> None:
        """
        Args:
            job (dict): sql.model.job_data as dict
        """

        self._job = job

    def run(self, sql_connection: SQLConnection, footprints: dict):
        """
        Raises:
            sql.model.SQLError
        """

        self._clean_mariadb(self._job.uid, sql_connection)
        self._insert_mariadb(self._job.uid, footprints, sql_connection)

        # mark the job as completed
        sql_connection.execute(
            update(JobDataORM)
            .filter(JobDataORM.uid == self._job.uid)
            .values(footprint_created=1)
            # footprint_created = 1 means, that we have succesfully calculated
            # footprints for this job
        )

    def _clean_mariadb(self, job_uid: int, sql_connection: SQLConnection):
        """remove entries in footprint_base, _fileio & _gpu

        Args:
            job_uid (int)
            sql_connection (SQLConnection)

        Raises:
            SQLError: sql driver problem
        """

        clean_fileio = delete(FootprintFileioORM).filter(
            FootprintFileioORM.uid == job_uid
        )
        clean_gpu = delete(FootprintGpuORM).filter(
            FootprintGpuORM.uid == job_uid
        )
        clean_base = delete(FootprintBaseORM).filter(
            FootprintBaseORM.uid == job_uid
        )

        for q in [clean_base, clean_fileio, clean_gpu]:
            sql_connection.execute(q)

    def _insert_mariadb(
        self,
        job_uid: int,
        footprints: Dict[str, float],
        sql_connection: SQLConnection,
    ):
        """inserts new footprints into the jobs sql table

        Raises:
            SQLError: sql driver problem

        Returns:
            List[SQLQuery]: _description_
        """

        fileio_query = insert(FootprintFileioORM).values(
            uid=job_uid,
            read_bytes=footprints.get("read_bytes"),
            write_bytes=footprints.get("write_bytes"),
            fsname=footprints.get("fsname"),
        )

        gpu_query = insert(FootprintGpuORM).values(
            uid=job_uid,
            used_mean_per_gpu=footprints.get("used_mean_per_gpu"),
            mem_used_max_per_gpu=footprints.get("mem_used_max_per_gpu"),
            power_mean_per_gpu=footprints.get("power_mean_per_gpu"),
        )

        base_query = insert(FootprintBaseORM).values(
            uid=job_uid,
            ipc_mean_per_core=footprints.get("ipc_mean_per_core"),
            cpu_used_mean_per_core=footprints.get("cpu_used_mean_per_core"),
            flops_any_mean_per_core=footprints.get("flops_any_mean_per_core"),
            mem_bw_mean_per_socket=footprints.get("mem_bw_mean_per_socket"),
            cpu_power_mean_per_socket=footprints.get(
                "cpu_power_mean_per_socket"
            ),
            ib_bw_mean_per_node=footprints.get("ib_bw_mean_per_node"),
            host_mem_used_max_per_node=footprints.get(
                "host_mem_used_max_per_node"
            ),
            energy_in_kwh=footprints.get("energy_in_kwh"),
            energy_efficiency_in_gflops_per_watt=footprints.get(
                "energy_efficiency_in_gflops_per_watt"
            ),
        )

        for q in [fileio_query, gpu_query, base_query]:
            sql_connection.execute(q)


class JobIssue:
    def __init__(self, job: TimelineORM, metric_data: dict) -> None:
        self.metric_data = metric_data

        self._job = job
        self._job_exclusive = 0
        self._total_aligned_measurement_points = 0

        # default issue values
        self._core_idle_ratio = 0  # used as "unused core ratio"
        self._core_load_imbalance = 0
        self._core_duration_idle = 0
        self._io_blocking = 0
        self._mem_leak = 0
        self._gpu_idle_ratio = 0  # used as "unused gpu ratio"
        self._gpu_load_imbalance = 0
        self._gpu_duration_idle = 0
        self._synchronous_offloading = 0
        self._io_blocking_detail = ""
        self._io_congestion = 0
        self._severity = 0
        self._unused_mem = 0

        # threshold to determine if device was unused
        self._threshold_idle_points = 2  # a device is unused, if the idle count per measurement point is greater than (n - 2) measurement points.

        # minimum number of periodic phases to determine inverse correlation
        self._min_period_num = 10

        # threshold of Pearson correlation coefficient for an inverse correlation
        self._corr_coef = -0.4

    def __check_job_eligibility(self) -> bool:
        core_duration = self._job.core_count * int(
            self._job.end_time - self._job.start_time
        )
        perform_issue_analysis = False
        if (
            3600 < core_duration <= 7200 and self._job.array_id > 0
        ) or core_duration > 7200:
            perform_issue_analysis = True

        if (
            self._job.job_state in ["completed", "timeout", "OOM"]
            and self._job.job_name != "bash"
            and perform_issue_analysis
        ):
            return True
        return False

    def __get_pu_usage_stat(
        self,
        issue_builder: IssueQueryBuilder,
        influx_connection: InfluxConnection,
        threshold_idle: float = 0.01,
        unit_type: str = "cpu",
    ) -> tuple:
        if unit_type == "gpu":
            issue_builder._change_metric("gpu_usage", threshold_idle)
        else:
            issue_builder._change_metric("cpu_usage", threshold_idle)

        query = issue_builder.get_pu_stat_query(unit_type)
        result = influx_connection.execute_query(query)

        idle_cnt = 0
        duration_idle = 0
        if len(result) > 0:
            for r in result.get_points():
                duration_idle += r["idle_num"] * 30
                if (
                    r["idle_num"]
                    > self._total_aligned_measurement_points
                    - self._threshold_idle_points
                ):
                    idle_cnt += 1

        if unit_type == "gpu":
            idle_ratio = round(idle_cnt / self._job.gpu_count, 2)
        else:
            idle_ratio = round(idle_cnt / self._job.core_count, 2)
        return (duration_idle, idle_ratio)

    def __get_pu_load_imbalance(
        self,
        issue_builder: IssueQueryBuilder,
        influx_connection: InfluxConnection,
        unit_type: str = "cpu",
    ) -> float:
        if unit_type == "gpu":
            metric = "gpu_usage"
            issue_builder._change_metric(metric)
        else:
            metric = "cpu_usage"
            issue_builder._change_metric(metric)

        query = issue_builder.get_detail_aggregated_timeline_query("stddev")
        result = influx_connection.execute_query(query)
        stddev_list = []
        if len(result) > 0:
            for r in result.get_points():
                stddev_list.append(r[metric])

            stddev_list = [0.0 if v is None else v for v in stddev_list]
            mean_stddev = np.mean(stddev_list)
            return round(mean_stddev, 2)

        return 0

    def __get_timelines(
        self,
        issue_builder: IssueQueryBuilder,
        influx_connection: InfluxConnection,
        ts_list: list[int],
        timeline_metrics: list[float],
        aggregation: str = "mean",
    ) -> dict[str, list]:
        timelines = {}
        # determine all io metrics
        metrics: List[str] = []
        for metric in timeline_metrics:
            if "_io" in metric:
                prev_io_metrics = [
                    "_read_bw",
                    "_write_bw",
                    "_read_requests",
                    "_write_reuests",
                    "_open",
                    "_close",
                ]
                for io_metric in self.metric_data[metric]["dbconf"]:
                    if any(
                        item in io_metric["name"] for item in prev_io_metrics
                    ):
                        metrics.append(io_metric["name"])
            else:
                metrics.append(metric)

        # get all mean values of each metric
        for metric in metrics:
            timelines[metric] = []
            issue_builder._change_metric(metric)
            query = issue_builder.get_detail_aggregated_timeline_query(
                aggregation
            )
            # print(query)
            result = influx_connection.execute_query(query)
            # assign values to ts list
            if len(result) > 0:
                points = list(result.get_points())
                result_iter = iter(points)
                values = next(result_iter, None)

                for ts in ts_list:
                    if values is not None:
                        while values["time"] < ts:
                            values = next(result_iter, None)
                        if values is not None:
                            if values["time"] > ts:
                                timelines[metric].append(None)
                            if values["time"] == ts:
                                timelines[metric].append(values[metric])
                                values = next(result_iter, None)
                # fill up until metric list has the same length as ts list
                if len(ts_list) != len(timelines[metric]):
                    while len(timelines[metric]) < len(ts_list):
                        timelines[metric].append(None)

            # fill up None values with 0.0 or the previous value
            timelines[metric] = [
                (
                    0.0
                    if i is None and idx == 0
                    else (
                        (0.0 if i is None else i)
                        if prev is None
                        else prev if i is None else i
                    )
                )
                for idx, (i, prev) in enumerate(
                    zip(timelines[metric], [0.0] + timelines[metric])
                )
            ]
            # check if metric is empty or all values are zero
            if int(sum(timelines[metric])) == 0:
                del timelines[metric]

        return timelines

    def __get_periodic_inverse_correlation(
        self,
        ts: list[int],
        ref_metric: dict[str, np.ndarray],
        corr_metrics: dict[str, np.ndarray],
    ) -> int:
        def perform_fft(y: np.ndarray) -> np.ndarray:
            # normalize y vector
            y = y - np.average(y)

            N = len(y)
            yf = fft(y)
            yf_mag = 2.0 / N * np.abs(yf[0 : N // 2])

            # normalize
            if max(yf_mag) > 0:
                yf_mag = yf_mag / max(yf_mag)
            return yf_mag

        def get_fft_period_num(
            xf: np.ndarray,
            y1: np.ndarray,
            y2: np.ndarray,
            job_duration: int,
            max_median=0.1,
        ) -> int:
            y_dom = perform_fft(y1) + perform_fft(y2)

            dom_freq = xf[np.argmax(y_dom)]
            period_num = math.ceil(job_duration * dom_freq)
            # period_len = round(1 / dom_freq / 60, 2)
            if max(y_dom) < 1.8 or np.median(y_dom) > 2 * max_median:
                return 0
            return period_num

        # check if metrics are eligible for further analysis
        if "cpu_usage" in ref_metric:
            if np.average(ref_metric["cpu_usage"]) < 0.1:
                return 0
            if (
                np.average(ref_metric["cpu_usage"])
                - np.std(ref_metric["cpu_usage"])
                > 0.3
            ):
                return 0
            ref_metric["cpu_usage"] = np.around(ref_metric["cpu_usage"], 1)

            if len(corr_metrics) == 1 and "gpu_usage" in corr_metrics:
                if np.average(corr_metrics["gpu_usage"]) < 0.1:
                    return 0
                if (
                    np.average(corr_metrics["gpu_usage"])
                    - np.std(corr_metrics["gpu_usage"])
                    > 0.3
                ):
                    return 0
                corr_metrics["gpu_usage"] = np.around(
                    corr_metrics["gpu_usage"], 1
                )
            else:
                # from here we have only io metrics
                for metric in corr_metrics:
                    if "_bw" in metric:
                        if np.average(corr_metrics[metric]) < 1e6:
                            continue
                        corr_metrics[metric] = np.around(
                            corr_metrics[metric] / 1e6, 1
                        )
                    else:
                        if np.average(corr_metrics[metric]) < 1:
                            continue
                    # only focus on peak values greater than average
                    corr_metrics[metric] = np.where(
                        corr_metrics[metric] < np.average(corr_metrics[metric]),
                        0,
                        corr_metrics[metric],
                    )

        # perform fft frecency spectrum
        N = len(ts)
        T = ts[1] - ts[0]
        xf = fftfreq(N, T)[: N // 2]

        job_duration = ts[len(ts) - 1]

        # check correlation between cpu_usage and all correlation metrics
        for metric in corr_metrics:
            # determine number of periodic phases
            period_num = get_fft_period_num(
                xf,
                ref_metric["cpu_usage"],
                corr_metrics[metric],
                job_duration,
            )

            if period_num >= self._min_period_num:
                # remove the first quarter and last quarter in terms of periods
                l = int(
                    (len(ref_metric["cpu_usage"]) / period_num)
                    * 0.25
                    * period_num
                )
                r = int(
                    (len(ref_metric["cpu_usage"]) / period_num)
                    * 0.75
                    * period_num
                )

                # check for inverse correlation via Pearson correlation coefficient
                corr_coef = np.corrcoef(
                    ref_metric["cpu_usage"][l:r], corr_metrics[metric][l:r]
                )[0][1]

                # once we have a valid correlation we return the number of periods
                if corr_coef < self._corr_coef:
                    if "gpu_usage" not in corr_metrics:
                        self._io_blocking_detail = metric
                    return period_num

        return 0

    def __get_io_congestion(self, io_metrics: np) -> int:
        # column-wise sum
        column_sums = np.sum(io_metrics, axis=0)
        # maximum of array multiply by number of nodes
        io_max = int(np.max(column_sums) * self._job.node_count)
        return io_max

    def __get_mem_leak_slope(
        self,
        x: list[int],
        y: list[float],
        sample_rate: float = 0.1,
        min_slope: float = 0.01,
        max_slope: float = 2.0,
    ) -> float:
        """Detects a potential memory leak using segment-based slope analysis."""

        try:
            # Convert to NumPy arrays
            x_arr = np.array(x, dtype=np.float64)
            y_arr = np.array(y, dtype=np.float64)

            n = len(x_arr)
            if n < 5:
                # print("Not enough data points to detect trends.")
                return 0

            # Define start and end indices to keep (remove first and last 25%)
            start_idx = int(n * 0.25)
            end_idx = int(n)

            x_arr = x_arr[start_idx:end_idx]
            y_arr = y_arr[start_idx:end_idx]

            # Ensure at least 4 elements remain
            if len(x_arr) < 4:
                # print("Not enough data points after filtering.")
                return 0

            # Normalize x and y
            if x_arr.max() > 0:
                x_arr /= x_arr.max()
            if y_arr.max() > 0:
                y_arr /= y_arr.max()

            # Compute overall trend using linear regression
            coeffs = np.polyfit(x_arr, y_arr, 1)
            overall_slope: float = coeffs[0]

            # print(f"Overall Slope: {overall_slope}")

            # Step 1: Ensure overall slope is within the acceptable range
            if not (min_slope <= overall_slope <= max_slope):
                # print("No memory leak detected (overall slope out of range).")
                return 0

            # Step 2: If within range, proceed with sampling-based trend detection
            step_size: int = max(1, int(len(x_arr) * sample_rate))

            # Generate sample indices safely
            sample_indices = np.arange(
                0, len(x_arr) - 1, step_size
            )  # Ensure we don't go out of bounds
            sample_indices = np.append(
                sample_indices, len(x_arr) - 1
            )  # Always include last index safely

            # Safe indexing
            sampled_x = x_arr[sample_indices]
            sampled_y = y_arr[sample_indices]

            # Compute slopes between sampled points
            slopes = np.diff(sampled_y) / np.diff(sampled_x)
            avg_slope: float = np.mean(slopes)

            # print(f"Sampled Slopes: {slopes.tolist()}")
            # print(f"Average Slope: {avg_slope}")

            # Detect memory leak if all sampled slopes are positive
            if np.all(slopes >= 0):
                # print("Memory leak detected: Consistently increasing trend.")
                return 1 if overall_slope > 1 else round(overall_slope, 2)
            else:
                # print("No memory leak detected (not consistently increasing).")
                return 0
        except Exception as e:

            logger.warning(
                f"[WARNING] Memory leak detection for jobid={self._job.job_id} failed: {e}"
            )
            return 0

    def __get_unused_mem_ratio(
        self, used_mem_usage: float, exlusive_mem_per_node: int
    ) -> float:
        try:
            aggregated_requested_mem = 0
            node_count = self._job.node_count
            core_count = self._job.core_count
            mem_per_cpu = self._job.mem_per_cpu
            mem_per_node = self._job.mem_per_node
            job_smt_mode = self._job.smt_mode

            if job_smt_mode > 1:
                core_count *= 2

            # whole memory if --exclusive flag or --mem=0 set by user
            if self._job.exclusive == 1 or (
                mem_per_cpu == None and mem_per_node == 0
            ):
                aggregated_requested_mem = exlusive_mem_per_node * node_count
            else:
                if not mem_per_node and mem_per_cpu:
                    aggregated_requested_mem = mem_per_cpu * core_count
                elif mem_per_node and not mem_per_cpu:
                    aggregated_requested_mem = mem_per_node * node_count

            unused_mem_ratio = round(
                1 - (used_mem_usage / aggregated_requested_mem), 2
            )
            return unused_mem_ratio
        except Exception as e:
            logger.warning(
                f"[WARNING] Unused memory ratio calculation for jobid={self._job.job_id} failed: {e}"
            )
            return 0

    def __check_issue_values_for_storage(self) -> bool:
        if self._core_idle_ratio > 0.1:
            return True
        elif self._core_load_imbalance > 0.2:
            return True
        elif self._gpu_idle_ratio > 0.1:
            return True
        elif self._gpu_load_imbalance > 0.2:
            return True
        elif self._io_blocking >= self._min_period_num:
            return True
        elif self._synchronous_offloading >= self._min_period_num:
            return True
        elif self._io_congestion >= 40:
            return True
        elif self._mem_leak > 0:
            return True
        elif self._unused_mem >= 0.8:
            return True

        return False

    def run(
        self,
        sql_connection: SQLConnection,
        influx_connection: InfluxConnection,
        spec: utils.specification.Specification,
    ):
        """
        Raises:
            sql.model.SQLError
        """

        # check if job is eligble
        if not self.__check_job_eligibility():
            return

        try:
            partition_info = spec.partition[self._job.partition_name]
        except KeyError:
            # KeyError: happens at invalid Partition e.g. datamover
            logger.critical(
                "jobid:%s runs on partition:%s, which does not exist in the pika specification",
                self._job.job_id,
                self._job.partition_name,
            )
            return

        issue_builder = IssueQueryBuilder(
            self._job, TimelineMetric.cpu_usage, TimelineParameter(), spec
        )

        # create timestamp list
        ts_list: List[int] = []
        ts = self._job.start_time - (self._job.start_time % 30) + 30
        while ts <= self._job.end_time:
            ts_list.append(ts)
            ts += 30

        # determine total number of measurement points
        self._total_aligned_measurement_points = len(ts_list)

        # normalize ts list by subtracting the first element from each element
        ts_list_norm = [element - ts_list[0] for element in ts_list]

        # check if job is exclusive without the Slurm exclusive flag specification
        if (
            self._job.node_count * partition_info.cpu_num
            == self._job.core_count
        ):
            self._job_exclusive = 1

        # check pu performance and load imbalance (cpu and gpu if available)
        (
            self._core_duration_idle,
            self._core_idle_ratio,
        ) = self.__get_pu_usage_stat(issue_builder, influx_connection)
        self._core_load_imbalance = self.__get_pu_load_imbalance(
            issue_builder, influx_connection
        )

        if self._job.gpu_count > 0:
            (
                self._gpu_duration_idle,
                self._gpu_idle_ratio,
            ) = self.__get_pu_usage_stat(
                issue_builder,
                influx_connection,
                threshold_idle=0.0,
                unit_type="gpu",
            )
            self._gpu_load_imbalance = self.__get_pu_load_imbalance(
                issue_builder, influx_connection, unit_type="gpu"
            )

            timeline_metrics = ["cpu_usage", "gpu_usage"]
            # get aligned timeline metrics based on timestamp list
            timelines = self.__get_timelines(
                issue_builder, influx_connection, ts_list, timeline_metrics
            )

            # check correlation between cpu usage and gpu usage to determine periodic synchronous offloading
            if "cpu_usage" in timelines and "gpu_usage" in timelines:
                ref_metric = {"cpu_usage": np.array(timelines["cpu_usage"])}
                corr_metrics = {"gpu_usage": np.array(timelines["gpu_usage"])}
                self._synchronous_offloading = (
                    self.__get_periodic_inverse_correlation(
                        ts_list_norm, ref_metric, corr_metrics
                    )
                )

        # check for mem leaks
        timelines = self.__get_timelines(
            issue_builder, influx_connection, ts_list, ["job_mem_used"]
        )
        if "job_mem_used" in timelines:
            self._mem_leak = self.__get_mem_leak_slope(
                ts_list_norm, timelines["job_mem_used"]
            )

        # calculate unused mem ratio
        timelines = self.__get_timelines(
            issue_builder, influx_connection, ts_list, ["job_mem_used"], "sum"
        )
        if "job_mem_used" in timelines:
            self._unused_mem = self.__get_unused_mem_ratio(
                np.max(timelines["job_mem_used"]),
                partition_info.mem_per_node,
            )

        if self._job_exclusive == 1:
            # get aligned timeline metrics based on timestamp list
            timeline_metrics = ["cpu_usage"]
            for metric in self.metric_data:
                if "_io" in metric:
                    timeline_metrics.append(metric)
            timelines = self.__get_timelines(
                issue_builder, influx_connection, ts_list, timeline_metrics
            )

            if "cpu_usage" in timelines:
                # check correlation between cpu usage and io metrics to determine periodic io blocking
                ref_metric = {"cpu_usage": np.array(timelines["cpu_usage"])}
                corr_metrics = {}
                prev_io_metrics = [
                    "_read_bw",
                    "_write_bw",
                    "_read_requests",
                    "_write_reuests",
                    "_open",
                    "_close",
                ]
                for metric in timelines:
                    if any(item in metric for item in prev_io_metrics):
                        corr_metrics[metric] = np.array(timelines[metric])
                self._io_blocking = self.__get_periodic_inverse_correlation(
                    ts_list_norm, ref_metric, corr_metrics
                )

            # determine whether the open and close operations are overloaded
            io_metrics = np.empty((0, len(ts_list)))
            for metric in timelines:
                if "open" in metric or "close" in metric:
                    # add values to numpy array
                    io_metrics = np.vstack(
                        [io_metrics, np.array(timelines[metric])]
                    )
            self._io_congestion = self.__get_io_congestion(io_metrics)

        # print("uid=", self._job.uid)
        # print("core_idle_ratio=", self._core_idle_ratio)
        # print("core_load_imbalance=", self._core_load_imbalance)
        # print("core_duration_idle=", self._core_duration_idle)
        # print("io_blocking=", self._io_blocking)
        # print("mem_leak=", self._mem_leak)
        # print("gpu_idle_ratio=", self._gpu_idle_ratio)
        # print("gpu_load_imbalance=", self._gpu_load_imbalance)
        # print("gpu_duration_idle=", self._gpu_duration_idle)
        # print("synchronous_offloading=", self._synchronous_offloading)
        # print("io_blocking_detail=", self._io_blocking_detail)
        # print("io_congestion=", self._io_congestion)
        # print("severity=", self._severity)
        # print("unused_mem=", self._unused_mem)

        if self.__check_issue_values_for_storage() is True:
            cleaning_query = delete(FootprintAnalysisORM).filter(
                FootprintAnalysisORM.uid == self._job.uid
            )

            sql_connection.execute(cleaning_query)

            insert_query = insert(FootprintAnalysisORM).values(
                uid=self._job.uid,
                core_idle_ratio=self._core_idle_ratio,
                core_load_imbalance=self._core_load_imbalance,
                core_duration_idle=self._core_duration_idle,
                io_blocking=self._io_blocking,
                mem_leak=self._mem_leak,
                gpu_idle_ratio=self._gpu_idle_ratio,
                gpu_load_imbalance=self._gpu_load_imbalance,
                gpu_duration_idle=self._gpu_duration_idle,
                synchronous_offloading=self._synchronous_offloading,
                io_blocking_detail=self._io_blocking_detail,
                io_congestion=self._io_congestion,
                severity=self._severity,
                unused_mem=self._unused_mem,
            )

            sql_connection.execute(insert_query)
