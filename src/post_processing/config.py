"""configuration of the post processing"""

from utils.model import ConfigurationError, DBCredential


class PostProcessingConfiguration:
    """Internal representation of the post processing configuration"""

    def __init__(
        self,
        config: dict,
        mariadb_config: dict,
        influx_st_config: dict,
        influxt_lt_config: dict,
        rest_api_config: dict,
    ) -> None:
        """
        Raises:
            utils.model.ConfigurationError
        """

        try:
            self._execution_schedule = str(config["execution_schedule"])
        except (KeyError, TypeError) as error:
            raise ConfigurationError(f"post processing -> {error}") from error

        try:
            self._mariadb_readwrite = DBCredential(
                {
                    "hostname": mariadb_config["hostname"],
                    "port": mariadb_config["port"],
                    "user": mariadb_config["admin_user"],
                    "password": mariadb_config["admin_password"],
                    "database": mariadb_config["database"],
                }
            )
        except (ConfigurationError, KeyError) as error:
            raise ConfigurationError(
                f"mariadb read write -> {error}"
            ) from error

        try:
            self._influxdb_st = DBCredential(influx_st_config)
        except ConfigurationError as error:
            raise ConfigurationError(f"influxdb-st -> {error}") from error

        try:
            self._influxdb_lt = DBCredential(influxt_lt_config)
        except ConfigurationError as error:
            raise ConfigurationError(f"influxdb-lt -> {error}") from error

        self._extern_timeline: dict[str, str] = rest_api_config.get(
            "features", {}
        ).get("extern_timeline", {})

    @property
    def execution_schedule(self) -> str:
        """get a execution schedula in a crontab format"""
        return self._execution_schedule

    @property
    def mariadb(self) -> DBCredential:
        """get read write mariadb credential"""
        return self._mariadb_readwrite

    @property
    def influxdb_st(self) -> DBCredential:
        """get read influx st credentials"""
        return self._influxdb_st

    @property
    def influxdb_lt(self) -> DBCredential:
        """get read influx lt credentials"""
        return self._influxdb_lt

    @property
    def extern_timeline(self) -> dict[str, str]:
        """get metricq credentials"""
        return self._extern_timeline
