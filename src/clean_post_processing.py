"""
NOT PART OF THE PIKA SERVER
this script is able to clean the performance metric tables in the pika db
"""

from argparse import ArgumentParser, RawTextHelpFormatter
from datetime import datetime
from multiprocessing import Process
from os import cpu_count, nice
from sys import argv
from typing import List

from sqlalchemy import delete, select, update

from pika_server import read_config_file
from post_processing.config import PostProcessingConfiguration
from sql.connection import SQLConnection
from sql.model import (
    FootprintAnalysisORM,
    FootprintBaseORM,
    FootprintFileioORM,
    FootprintGpuORM,
    JobDataORM,
    SQLError,
)
from utils.model import DBCredential


class CLIArguments:
    """parsed user supplied arguments"""

    start: int
    end: int
    config_file: str


class ArgError(Exception):
    """error happening in argument evaluation"""


def main(cli_arguments: CLIArguments) -> None:
    """clean metric tables"""

    print(f"READING {cli_arguments.config_file} AS CONFIG FILE")

    configuration = read_config_file(cli_arguments.config_file)
    parsed_config = PostProcessingConfiguration(
        {"execution_schedule": "doenst matter here"},
        configuration["mariadb"],
        configuration["influxdb-st"],
        configuration["influxdb-lt"],
        configuration["rest-api"],
    )

    sql_connection = SQLConnection(parsed_config.mariadb)

    try:
        result_set = sql_connection.execute(
            select(JobDataORM.uid)
            .select_from(JobDataORM)
            .filter(
                JobDataORM.start_time >= cli_arguments.start,
                JobDataORM.start_time < cli_arguments.end,
            )
        )
    except SQLError as error:
        raise SQLError from error

    uids = [result["uid"] for result in result_set]

    print(f"deleting footprints for {len(uids)} jobs")

    do_multiprocessing(parsed_config.mariadb, uids)

    print("done")


def do_multiprocessing(mariadb_cred: DBCredential, uids: List[int]):
    """implementation detail for multiprocessing

    todo: try to use postprocessing multiprocessing
    """

    nice(10)
    # we are using all available cores, so we want to be nice to the computer

    proc_count = (
        # use 24 cores at max
        cpu_count()
        if cpu_count() <= 24
        else 24
    )
    # e.g. 8

    job_count = len(uids)
    # e.g. 173

    jobs_per_process = int(job_count / proc_count)
    # e.g. 21

    if jobs_per_process == 0:
        # dont do multiprocessing when there are less jobs than cpus
        clean(mariadb_cred, uids)
        return

    processes: List[Process] = []

    for i in range(0, proc_count):
        start_index = i * jobs_per_process  # e.g. 0
        end_index = (i + 1) * jobs_per_process
        # e.g. 21, now you wonder: "but 20 should be last index !",
        # you are right, continue reading

        if i == proc_count - 1:
            # last process should create footrpints for remaining jobs
            piece_of_work = uids[start_index:]
        else:
            piece_of_work = uids[start_index:end_index]
            # python knowledge: array[start:stop] is actually "array from start to stop-1"

        processes.append(
            Process(
                name=f"proc_{i}",
                target=clean,
                args=[mariadb_cred, piece_of_work],
            )
        )

    for p in processes:
        p.start()

    for p in processes:
        p.join()


def clean(mariadb_cred: DBCredential, uids: List[int]):
    """cleaning queries"""

    sql_connection = SQLConnection(mariadb_cred)
    try:
        for uid in uids:
            sql_connection.execute(
                delete(FootprintGpuORM).filter(FootprintGpuORM.uid == uid)
            )

            sql_connection.execute(
                delete(FootprintFileioORM).filter(FootprintFileioORM.uid == uid)
            )

            sql_connection.execute(
                delete(FootprintBaseORM).filter(FootprintBaseORM.uid == uid)
            )

            sql_connection.execute(
                delete(FootprintAnalysisORM).filter(
                    FootprintAnalysisORM.uid == uid
                )
            )

            sql_connection.execute(
                update(JobDataORM)
                .filter(JobDataORM.uid == uid)
                .values(tags=None, footprint_created=None)
            )

    except SQLError as error:
        raise SQLError from error


def args() -> CLIArguments:
    """evaluate arguments"""

    parser = ArgumentParser(
        prog=argv[0],
        description="delete footprint-base, -fileio, -gpu, -analysis and set tags and footprint_created to NULL",
        epilog=(
            "Examples\n"
            "python "
            + argv[0]
            + """ --start "2023-08-25 5:00" --end "2023-08-25 10:00"\n"""
            "python "
            + argv[0]
            + """ --start "2023-08-25 5:00" --end "2023-08-25 10:00" --config pika.conf\n"""
        ),
        formatter_class=RawTextHelpFormatter,
    )
    parser.add_argument(
        "--start",
        help="overwrite start_time in JSON filter, specify as YYYY-MM-DD HH:mm",
        required=True,
    )
    parser.add_argument(
        "--end",
        help="overwrite end_time in JSON filter, specify as YYYY-MM-DD HH:mm",
        required=True,
    )
    parser.add_argument(
        "--config",
        help="use this config file, if not specified use /etc/pika.conf",
        default="/etc/pika.conf",
    )

    if len(argv) == 1:
        parser.print_help()
        exit(1)

    parsed_args = parser.parse_args()

    cli_arguments = CLIArguments()

    try:
        cli_arguments.start = int(
            datetime.strptime(parsed_args.start, "%Y-%m-%d %H:%M").timestamp()
        )
    except ValueError as error:
        raise ArgError(f"Option error --start: {error}") from error

    try:
        cli_arguments.end = int(
            datetime.strptime(parsed_args.end, "%Y-%m-%d %H:%M").timestamp()
        )
    except ValueError as error:
        raise ArgError(f"Option error --end: {error}") from error

    if parsed_args.config:
        try:
            cli_arguments.config_file = str(parsed_args.config)
        except ValueError as error:
            raise ArgError(f"Option error --config: {error}") from error

    return cli_arguments


if __name__ == "__main__":
    try:
        user_input = args()
    except ArgError as exp:
        print(exp)
        exit(1)

    try:
        main(user_input)
    except SQLError as exp:
        print(exp)
        exit(1)
