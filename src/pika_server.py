"""
"pika server" initialization code, starting desired processes

- called pika_server.py instead of main.py
  => easy pid finding
- import heavy packages when they are needed
  => speeds up execution if an error happens early on
"""

import signal
import tomllib
from argparse import ArgumentParser
from multiprocessing import Process
from sys import exit as _exit
from time import sleep
from typing import Any, Optional

import utils.common
import utils.specification
from sql.connection import test_read, test_write
from sql.model import SQLError
from utils.common import read_file_from_project_root
from utils.logger import init_logging, logger
from utils.model import ConfigurationError, OperationMode


def init(mode: OperationMode) -> None:
    """Entry point
    - initialize pika logger
    - reading and parsing the configuration file
    - read and parse specifications
    - testing connections to sql server
    - starting processes

    Args:
        mode (OperationMode): what mode is the server running in
    """

    init_logging(mode)

    match mode:
        case OperationMode.DEVELOPMENT:
            config_file_name = "pika.conf"
        case OperationMode.PRODUCTION:
            config_file_name = "/etc/pika.conf"

    try:
        configuration = read_config_file(config_file_name)
    except ConfigurationError as error:
        logger.critical(error)
        return

    try:
        parsed_config = PikaServerConfiguration(configuration)
    except ConfigurationError as error:
        logger.critical("%s: configuration error", error)
        return

    logger.info("starting server in %s mode", mode)

    try:
        version_file_content = read_file_from_project_root("pyproject.toml")
        tag_file_content = read_file_from_project_root("job_tag.json")
        partition_file_content = read_file_from_project_root("partition.json")
        metric_file_content = read_file_from_project_root("metric.json")
    except FileNotFoundError as error:
        logger.critical("cant find specification file -> %s", error)
        return

    try:
        spec = utils.specification.Specification(
            version_file_content,
            tag_file_content,
            partition_file_content,
            metric_file_content,
        )
    except utils.specification.SpecificationError as error:
        logger.critical("error in specification -> %s", error)
        return

    try:
        test_connection(parsed_config)
    except SQLError as error:
        logger.critical(error)
        return

    processes: list[Process] = []

    if parsed_config.rest_api:
        from rest_api import rest_api

        processes.append(
            Process(
                name="rest-api",
                target=rest_api.main,
                args=[mode, parsed_config.rest_api, spec],
            )
        )

    if parsed_config.post_processing:
        from post_processing import post_processing

        processes.append(
            Process(
                name="post-processing",
                target=post_processing.main,
                args=[parsed_config.post_processing, spec],
            )
        )

    if parsed_config.data_collections:
        from data_collection import data_collection

        for i, dc_config in enumerate(parsed_config.data_collections):
            processes.append(
                Process(
                    name="data-collection-" + str(i + 1),
                    target=data_collection.main,
                    args=[dc_config, spec],
                )
            )

    if len(processes) == 0:
        logger.info(
            "all pika services are disabled in %s, exiting", config_file_name
        )
        return

    run(processes)


class PikaServerConfiguration:
    """Internal representation of the configuration file"""

    def __init__(self, config: dict[str, Any]) -> None:
        """create minor configuration classes

        Args:
            config (dict): all items needed by the server to start

        Raises:
            ConfigurationError: error in the configuration file
        """

        # treat everything as optional because services can be
        # dissabled by commenting them out in the configuration file

        if "rest-api" in config.keys():
            from rest_api.model import ApiConfiguration

            try:
                self._rest_api: Optional[ApiConfiguration] = ApiConfiguration(
                    config["rest-api"],
                    config["mariadb"],
                    config["influxdb-st"],
                    config["influxdb-lt"],
                )
            except (ConfigurationError, KeyError) as error:
                raise ConfigurationError(error) from error

        else:
            self._rest_api = None

        if "post-processing" in config.keys():
            from post_processing.config import PostProcessingConfiguration

            try:
                self._post_processing: Optional[PostProcessingConfiguration] = (
                    PostProcessingConfiguration(
                        config["post-processing"],
                        config["mariadb"],
                        config["influxdb-st"],
                        config["influxdb-lt"],
                        config["rest-api"],
                    )
                )
            except (ConfigurationError, KeyError) as error:
                raise ConfigurationError(error) from error
        else:
            self._post_processing = None

        if "data-collections" in config.keys():
            from data_collection.config import DataCollectionConfig

            try:
                self._data_collections: list[DataCollectionConfig] = []

                for rabbitmq_config in config["data-collections"]:
                    self._data_collections.append(
                        DataCollectionConfig(rabbitmq_config, config["mariadb"])
                    )

            except (ConfigurationError, KeyError) as error:
                raise ConfigurationError(error) from error
        else:
            self._data_collections = None

    @property
    # type: ignore
    # cant specify return type, because type is importet conditionaly
    def rest_api(self):  # -> ApiConfiguration | None:
        """get the rest_api configuration

        Returns:
            ApiConfiguration: the configuration if rest_api was enabled in the
            configuration file
            None: data_colelction was disabled in the config file
        """
        return self._rest_api

    @property
    # type: ignore
    # cant specify return type, because type is importet conditionaly
    def post_processing(self):  # -> PostProcessingConfiguration | None:
        """get the post_processing configuration

        Returns:
            PostProcessingConfiguration: the configuration if post_processing
            was enabled in the configuration file
            None: data_colelction was disabled in the config file
        """
        return self._post_processing

    @property
    # type: ignore
    # cant specify return type, because type is importet conditionaly
    def data_collections(self) -> list:
        """get the data_collection configurations

        Returns:
            RabbitMQConfiguration: the configuration if data_collections was
            enabled in the configuration file
            None: data_collections was disabled in the config file
        """
        return self._data_collections


def test_connection(config: PikaServerConfiguration) -> None:
    """Test read and write capability for the database credentials specified in
    the configuration.

    Args:
        config (PikaServerConfiguration): the parsed configuration file

    Raises:
        SQLError: the credentials did not have the ability to read or write
    """

    # treat all Pika processes as optional, as they can be disabled

    if config.rest_api:
        try:
            test_read(config.rest_api.mariadb)
        except SQLError as error:
            raise SQLError(
                (
                    f"{error}, cant read from sql connection, "
                    "please check your pika.conf for user & password "
                    "in the mariadb section, also check your internet "
                    "connection"
                )
            ) from error

    if config.post_processing:
        try:
            test_read(config.post_processing.mariadb)
        except SQLError as error:
            raise SQLError(
                (
                    f"{error}, cant read from sql connection, "
                    "please check your pika.conf for admin_user & "
                    "admin_password in the mariadb section, also check your "
                    "internet connection"
                )
            ) from error

        try:
            test_write(config.post_processing.mariadb)
        except SQLError as error:
            raise SQLError(
                (
                    f"{error}, cant write to sql connection, "
                    "please check your pika.conf for admin_user &  "
                    "admin_password in the mariadb section, also check your "
                    "internet connection"
                )
            ) from error

    if config.data_collections:
        for credentials in config.data_collections:
            try:
                test_read(credentials.mariadb)
            except SQLError as error:
                raise SQLError(
                    (
                        f"{error}, cant read from sql connection, "
                        "please check your pika.conf for admin_user & "
                        "admin_password in the mariadb section, also check your"
                        " internet connection"
                    )
                ) from error

            try:
                test_write(credentials.mariadb)
            except SQLError as error:
                raise SQLError(
                    (
                        f"{error}, cant write to sql connection, "
                        "please check your pika.conf for admin_user & "
                        "admin_password in the mariadb section, also check your"
                        " internet connection"
                    )
                ) from error


def args() -> OperationMode:
    """Take care of all argument parser work

    Raises:
        ArgumentError: wrong user option

    Returns:
        OperationMode: the mode in which the server is running
    """

    parser = ArgumentParser()
    subparsers = parser.add_subparsers(dest="command")

    subparsers.add_parser(
        "dev",
        help="log level: DEBUG; read pika.conf from working directory",
    )
    subparsers.add_parser(
        "prod", help="log level: INFO; reading /etc/pika.conf"
    )

    arguments = parser.parse_args()

    if arguments.command == "dev":
        return OperationMode.DEVELOPMENT

    if arguments.command == "prod":
        return OperationMode.PRODUCTION

    parser.print_help()
    raise ArgumentError


class ArgumentError(Exception):
    """custom exception representing wrong arguments being passed to the
    server"""


def read_config_file(file_name: str) -> dict[str, Any]:
    """read the configuration file

    Args:
        file_name (str): path to the config file

    Raises:
        ConfigurationError: toml syntax violation in the config file

    Returns:
        dict[str, any]: the read configuration key-value pairs
    """

    try:
        with open(file_name, "rb") as file:
            configuration = tomllib.load(file)
    except tomllib.TOMLDecodeError as error:
        raise ConfigurationError(
            f"toml syntax violation in {file_name}: {error}, _exiting"
        ) from error
    except FileNotFoundError as error:
        raise ConfigurationError(
            f"cant find {file_name} configuration file"
        ) from error

    return configuration


def run(processes: list[Process]) -> None:
    """starting processes, handling signals from systemd and terminate the
    application

    Args:
        processes (List[Process]): all processes that are to be executed
    """

    for p in processes:
        p.start()

    def pika_exit_closure(*_args: Any) -> None:
        for p in processes:
            p.terminate()
            logger.info("terminating %s", p.name)
        _exit(0)

    signal.signal(signal.SIGTERM, pika_exit_closure)
    # register callback when recivieng SIGTERM from systemd

    try:
        while True:
            sleep(9999 * 9999)  # refactor, try to use the main process as well
    except KeyboardInterrupt:
        pika_exit_closure()


if __name__ == "__main__":
    try:
        init(args())
    except ArgumentError:
        pass
    except KeyboardInterrupt:
        # occurs on early exit if initialization isnt done
        pass
