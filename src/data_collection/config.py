"""configuration model for data_collection"""

import json

from utils.model import ConfigurationError, DBCredential


class RabbitMQConfig:
    def __init__(self, config: dict) -> None:
        """
        Raises:
            utils.model.ConfigurationError
        """

        try:
            self._rabbitmq_hostname = str(config["rabbitmq_hostname"])
            self._rabbitmq_vhost = str(config["rabbitmq_vhost"])
            self._rabbitmq_exchange = str(config["rabbitmq_exchange"])
            self._rabbitmq_exchange_type = str(config["rabbitmq_exchange_type"])
            self._rabbitmq_queue = str(config["rabbitmq_queue"])
            self._rabbitmq_mode_list: list[str] = config["rabbitmq_mode_list"]
            self._rabbitmq_username = str(config["rabbitmq_username"])
            self._rabbitmq_password = str(config["rabbitmq_password"])
            self._queue_arguments: dict[str, str] = config["queue_arguments"]
        except (KeyError, TypeError) as error:
            raise ConfigurationError(f"rabbitmq config -> {error}") from error

    @property
    def rabbitmq_hostname(self) -> str:
        """get hostname from rabbitmq"""
        return self._rabbitmq_hostname

    @property
    def rabbitmq_vhost(
        self,
    ) -> str:
        return self._rabbitmq_vhost

    @property
    def rabbitmq_exchange(
        self,
    ) -> str:
        return self._rabbitmq_exchange

    @property
    def rabbitmq_exchange_type(
        self,
    ) -> str:
        return self._rabbitmq_exchange_type

    @property
    def rabbitmq_queue(self) -> str:
        return self._rabbitmq_queue

    @property
    def rabbitmq_mode_list(self) -> list[str]:
        """get all rabbitmq queues"""
        return self._rabbitmq_mode_list

    @property
    def rabbitmq_username(self) -> str:
        """get the rabbitmq username"""
        return self._rabbitmq_username

    @property
    def rabbitmq_password(self) -> str:
        """return the rabbitmq password"""
        return self._rabbitmq_password

    @property
    def queue_arguments(self) -> dict[str, str]:
        """rabbitmq additional arguments"""
        return self._queue_arguments

    def __repr__(self) -> str:
        return json.dumps(self.__dict__, indent=2)


class DataCollectionConfig:
    def __init__(self, rabbitmq_config: dict, maridb_rw_config: dict) -> None:
        try:
            self._rabbitmq_config = RabbitMQConfig(rabbitmq_config)
        except ConfigurationError as error:
            raise ConfigurationError(f"data collection -> {error}") from error

        try:
            self._mariadb_rw = DBCredential(
                {
                    "hostname": maridb_rw_config["hostname"],
                    "port": maridb_rw_config["port"],
                    "user": maridb_rw_config["admin_user"],
                    "password": maridb_rw_config["admin_password"],
                    "database": maridb_rw_config["database"],
                }
            )
        except (ConfigurationError, KeyError) as error:
            raise ConfigurationError(
                f"mariadb read write -> {error}"
            ) from error

    @property
    def rabbitmq(self) -> RabbitMQConfig:
        "return rabbitmq config"
        return self._rabbitmq_config

    @property
    def mariadb(self) -> DBCredential:
        """return read, write mariadb credentials"""
        return self._mariadb_rw

    def __repr__(self) -> str:
        class_name = type(self).__name__
        return f"{class_name}({self.__dict__})"
