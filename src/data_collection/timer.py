import threading


class CallbackTimer:
    """
    - execute callback after a interval has expired as long as the timer has not been reset
    - this timer is nonblocking

    constructor parameter:
        - interval_sec: how much seconds to wait until the callback is executed
        - callback: a function pointer which will be executed after interval
        - auto_restart:
            - True: restart timer with same callback
            - False: execute callback only once

    public interface:
        - start_or_restart(): let the timer start from 0seconds
    """

    def __init__(
        self, interval_sec: float, callback: callable, auto_restart: bool = True
    ):
        self.interval = interval_sec
        self.original_callback = callback
        self.auto_restart = auto_restart

        self.timer: threading.Timer = None

    def _callback_wrapper(self):
        """restart the timer after the callback was executed if enabled"""
        self.original_callback()

        if self.auto_restart:
            self.start_or_restart()

    def start_or_restart(self):
        if self.timer is not None:
            self.timer.cancel()

        self.timer = threading.Timer(self.interval, self._callback_wrapper)
        self.timer.start()
