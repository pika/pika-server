import pika

from . import config as configuration


class RabbitMQSubscriber:
    """the connection to the rabbitmq server"""

    def __init__(
        self, config: configuration.RabbitMQConfig, callback: callable
    ) -> None:
        self._config = config
        self._callback = callback

    def connect_to_server(self) -> None:
        """
        - connect to rabbitmq server
        - setup rabbitmq communication

        Raising:
            pika.exceptions.AMQPConnectionError: cant connect to rabbitmq server
        """

        # connect to rabbitmq-server
        # raises AMQPConnectionError on connection error
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(
                host=self._config.rabbitmq_hostname,
                virtual_host=self._config.rabbitmq_vhost,
                credentials=pika.PlainCredentials(
                    self._config.rabbitmq_username,
                    self._config.rabbitmq_password,
                ),
            )
        )

        self._channel = connection.channel()

        self._channel.exchange_declare(
            exchange=self._config.rabbitmq_exchange,
            exchange_type=self._config.rabbitmq_exchange_type,
            durable=True,
        )

        self._channel.basic_qos(prefetch_count=400)

        result = self._channel.queue_declare(
            queue=self._config.rabbitmq_queue,
            durable=True,
            arguments=self._config.queue_arguments,
        )

        for rabbit_mode in self._config.rabbitmq_mode_list:
            self._channel.queue_bind(
                exchange=self._config.rabbitmq_exchange,
                queue=self._config.rabbitmq_queue,
                routing_key=rabbit_mode,
            )

        # - register callback
        # - disable `auto_ack`, so rabbitmq wont get any automatic
        #   acknowledgements when sending payload to us, instead
        #   we send acknowledgements manually so we can tell rabbitmq to hold
        #   the data in its buffer when we encouter a error
        self._channel.basic_consume(
            result.method.queue,
            self._callback,
            auto_ack=False,
        )

    def collect(self):
        """start the data collection, this function blocks the current thread"""

        self._channel.start_consuming()
