import time
import unittest

from .. import timer


class TestCallbackTimer(unittest.TestCase):
    def test(self):
        change_me = "original"

        def change_closure():
            nonlocal change_me
            change_me = "changed"

        timer.CallbackTimer(
            0, change_closure, auto_restart=False
        ).start_or_restart()
        time.sleep(0.001)  # make sure our timer has executed the callback

        self.assertEqual(change_me, "changed")
