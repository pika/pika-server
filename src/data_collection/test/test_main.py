import unittest

from utils.common import debug_sqlalchemy_query

from ..data_collection import on_finish, on_prolog
from ..handler import JobHandler

job_dict = {
    "38648144": {
        "JOB_RECORD__ARRAY_ID": 38642958,
        "JOB_RECORD__JOB_NAME": "scratchblocker",
        "JOB_RECORD__ACCOUNT": "hpcsupport",
        "JOB_RECORD__PARTITION": "gpu2",
        "JOB_RECORD__CPU_CNT": 1,
        "JOB_RECORD__CPU_IDS": [[0]],
        "JOB_RECORD__NODE_CNT": 1,
        "JOB_RECORD__NODE_NAMES": ["taurusi2102"],
        "JOB_RECORD__TIME_LIMIT": 10080,
        "JOB_RECORD__WHOLE_NODE": 0,
        "JOB_RECORD__START_TIME": "2023-07-28T03:23:40",
        "JOB_RECORD__USER": "mark",
        "JOB_RECORD__PARTITION_NAME": "gpu2",
        "JOB_RECORD__SMT_ENABLED": 0,
        "JOB_RECORD__GPU_ALLOC_CNT": 1,
        "JOB_RECORD__SUBMIT_TIME": "2023-07-26T06:54:31",
        "JOB_RECORD__END_TIME": "2023-07-28T14:30:24",
        "JOB_RECORD__JOB_STATE": "completed",
        "JOB_RECORD__GRES_DETAIL_STR": ["gpu:1(IDX:0)"],
        "JOB_RECORD__JOB_SCRIPT": "#!/bin/bash\n#SBATCH -L scratch:100000\n#SBATCH -t 7-0:00:00\n#SBATCH -c 1\n#SBATCH --gres=gpu:1\n#SBATCH -p gpu2\n#SBATCH --mail-type=all\n#SBATCH --mail-user=geheim.geheim@geheim.de\n#SBATCH --array=0-53\n#SBATCH --job-name=scratchblocker\nsleep 40000\n",
        "JOB_RECORD__CORE_CNT": 1,
        "JOB_RECORD__MEM_PER_CPU": 1900,
        "JOB_RECORD__MEM_PER_NODE": 12884901888,
        "JOB_RECORD__SUBMIT_UNIXTIME": 2,
        "JOB_RECORD__START_UNIXTIME": 3,
        "JOB_RECORD__END_UNIXTIME": 4,
    }
}

job_handler = JobHandler(job_dict, {})


class OnPrologTest(unittest.TestCase):
    def test_job_id(self):
        job_id, _ = on_prolog(job_handler)
        self.assertEqual("38648144", job_id)

    def test_query(self):
        _, query = on_prolog(job_handler)
        self.assertEqual(
            """INSERT INTO job_data (job_id, user_name, project_name, job_state, node_count, node_list, core_list, core_count, submit_time, start_time, job_name, time_limit, partition_name, exclusive, property_id, array_id, gpu_count, gpu_list, smt_mode, job_script, mem_per_cpu, mem_per_node) VALUES (38648144, 'mark', 'hpcsupport', 'completed', 1, 'taurusi2102', 'taurusi2102[0]', 1, 2, 3, 'scratchblocker', 604800, 'gpu2', 0, 0, 38642958, 1, 'taurusi2102[0]', 1, '#!/bin/bash
#SBATCH -L scratch:100000
#SBATCH -t 7-0:00:00
#SBATCH -c 1
#SBATCH --gres=gpu:1
#SBATCH -p gpu2
#SBATCH --mail-type=all
#SBATCH --mail-user=geheim.geheim@geheim.de
#SBATCH --array=0-53
#SBATCH --job-name=scratchblocker
sleep 40000
', 1992294400, 13510798882111488)""",
            debug_sqlalchemy_query(query),
        )


class OnFinishTest(unittest.TestCase):
    def test_jobid(self):
        job_id, _ = on_finish(job_handler)
        self.assertEqual("38648144", job_id)

    def test_query(self):
        _, query = on_finish(job_handler)
        self.assertEqual(
            """UPDATE job_data SET job_state='completed', end_time=4 WHERE job_data.job_id = '38648144' AND job_data.start_time = 3 AND job_data.partition_name = 'gpu2'""",
            debug_sqlalchemy_query(query),
        )
