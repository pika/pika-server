import unittest

from ..handler import JobHandler


class JobHandlerTest(unittest.TestCase):
    def setUp(self) -> None:
        self._body = {
            "38648144": {
                "JOB_RECORD__ARRAY_ID": 38642958,
                "JOB_RECORD__JOB_NAME": "scratchblocker",
                "JOB_RECORD__ACCOUNT": "hpcsupport",
                "JOB_RECORD__PARTITION": "gpu2",
                "JOB_RECORD__CPU_CNT": 1,
                "JOB_RECORD__CPU_IDS": [[0]],
                "JOB_RECORD__NODE_CNT": 1,
                "JOB_RECORD__NODE_NAMES": ["taurusi2102"],
                "JOB_RECORD__TIME_LIMIT": 10080,
                "JOB_RECORD__WHOLE_NODE": 0,
                "JOB_RECORD__START_TIME": "2023-07-28T03:23:40",
                "JOB_RECORD__USER": "mark",
                "JOB_RECORD__PARTITION_NAME": "gpu2",
                "JOB_RECORD__SMT_ENABLED": 0,
                "JOB_RECORD__GPU_ALLOC_CNT": 1,
                "JOB_RECORD__SUBMIT_TIME": "2023-07-26T06:54:31",
                "JOB_RECORD__END_TIME": "2023-07-28T14:30:24",
                "JOB_RECORD__JOB_STATE": "completed",
                "JOB_RECORD__GRES_DETAIL_STR": ["gpu:1(IDX:0)"],
                "JOB_RECORD__JOB_SCRIPT": "#!/bin/bash\n#SBATCH -L scratch:100000\n#SBATCH -t 7-0:00:00\n#SBATCH -c 1\n#SBATCH --gres=gpu:1\n#SBATCH -p gpu2\n#SBATCH --mail-type=all\n#SBATCH --mail-user=geheim.geheim@geheim.de\n#SBATCH --array=0-53\n#SBATCH --job-name=scratchblocker\nsleep 40000\n",
                "JOB_RECORD__CORE_CNT": 1,
                "JOB_RECORD__MEM_PER_CPU": 1900,
                "JOB_RECORD__MEM_PER_NODE": 12884901888,
            }
        }

        self._job_handler = JobHandler(self._body, {})

    def test_job_id(self):
        self.assertEqual("38648144", self._job_handler.get_job_id())

    def test_node_list(self):
        self.assertEqual("taurusi2102", self._job_handler.get_node_list())

    def test_cpu_core_list(self):
        self.assertEqual(
            "taurusi2102[0]", self._job_handler.get_cpu_core_list()
        )
