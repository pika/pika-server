import itertools
import json
import math
import re

import hostlist

from utils.logger import logger


class JobHandler:
    """understand the data we recieve from rabbitmq, and offer it"""

    def __init__(self, job_dict: dict, slurm_partition: dict) -> None:
        self._slurm_partitions = slurm_partition

        # dicts usually have multiple entries but we only want the first,
        # we know that rabbitmq sends us a dict with a single entry
        self._job_id: int
        self._job_data: dict
        self._job_id, self._job_data = next(iter(job_dict.items()))

        # data example:
        # {
        #     "38648144": {
        #         "JOB_RECORD__ARRAY_ID": 38642958,
        #         "JOB_RECORD__JOB_NAME": "scratchblocker",
        #         "JOB_RECORD__ACCOUNT": "hpcsupport",
        #         "JOB_RECORD__PARTITION": "gpu2",
        #         "JOB_RECORD__CPU_CNT": 1,
        #         "JOB_RECORD__CPU_IDS": [[0]],
        #         "JOB_RECORD__NODE_CNT": 1,
        #         "JOB_RECORD__NODE_NAMES": ["taurusi2102"],
        #         "JOB_RECORD__TIME_LIMIT": 10080,
        #         "JOB_RECORD__WHOLE_NODE": 0,
        #         "JOB_RECORD__START_TIME": "2023-07-28T03:23:40",
        #         "JOB_RECORD__USER": "mark",
        #         "JOB_RECORD__PARTITION_NAME": "gpu2",
        #         "JOB_RECORD__SMT_ENABLED": 0,
        #         "JOB_RECORD__GPU_ALLOC_CNT": 1,
        #         "JOB_RECORD__SUBMIT_TIME": "2023-07-26T06:54:31",
        #         "JOB_RECORD__END_TIME": "2023-07-28T14:30:24",
        # JOB_RECORD__END_TIME is only available when the job is finished
        #         "JOB_RECORD__JOB_STATE": "completed",
        #         "JOB_RECORD__GRES_DETAIL_STR": ["gpu:1(IDX:0)"],
        #         "JOB_RECORD__JOB_SCRIPT": "#!/bin/bash\n#SBATCH -L scratch:100000\n#SBATCH -t 7-0:00:00\n#SBATCH -c 1\n#SBATCH --gres=gpu:1\n#SBATCH -p gpu2\n#SBATCH --mail-type=all\n#SBATCH --mail-user=geheim.geheim@geheim.de\n#SBATCH --array=0-53\n#SBATCH --job-name=scratchblocker\nsleep 40000\n",
        #         "JOB_RECORD__CORE_CNT": 1,
        #         "JOB_RECORD__MEM_PER_CPU": 1900,
        #         "JOB_RECORD__MEM_PER_NODE": 12884901888,
        #     }
        # }

    def get_job_id(self):
        return self._job_id

    def get_user_name(self):
        user_name = self._job_data.get("JOB_RECORD__USER")
        return user_name

    def get_project_name(self):
        project_name = self._job_data.get("JOB_RECORD__ACCOUNT")
        return project_name

    def get_job_state(self):
        job_state = self._job_data.get("JOB_RECORD__JOB_STATE")
        return job_state

    def get_node_count(self):
        node_count = self._job_data.get("JOB_RECORD__NODE_CNT")
        return node_count

    def get_node_list(self):
        original_node_list = self._job_data.get("JOB_RECORD__NODE_NAMES")
        short_node_list = hostlist.collect_hostlist(
            original_node_list, silently_discard_bad=True
        )
        return short_node_list

    def get_cpu_core_list(self):
        exclusive = self._job_data.get("JOB_RECORD__WHOLE_NODE")
        cpu_ids = self._job_data.get("JOB_RECORD__CPU_IDS")
        node_names = self._job_data.get("JOB_RECORD__NODE_NAMES")
        num_nodes = self._job_data.get("JOB_RECORD__NODE_CNT")
        job_id = self._job_id

        def list_to_ranges(L):
            """heiliger bim bam"""
            G = (
                list(x)
                for _, x in itertools.groupby(
                    L, lambda x, c=itertools.count(): next(c) - x
                )
            )
            return ",".join(
                "-".join(map(str, (g[0], g[-1])[: len(g)])) for g in G
            )

        try:
            job_cpulist = ""
            if exclusive != 1:
                for idx, val in enumerate(cpu_ids):
                    job_cpulist += (
                        node_names[idx]
                        + str("[")
                        + str(list_to_ranges(val))
                        + str("],")
                    )
                # remove last comma from string
                job_cpulist = job_cpulist[:-1]

            # save all nodes for exclusive jobs (node number > 1) in cpu list in
            # order to search jobs by a specific node
            elif exclusive == 1 and num_nodes > 1:
                job_cpulist = " ".join(node_names)

            return job_cpulist
        except Exception as error:
            logger.warning(f"{repr(error)}, cpulist error, job_id={job_id}")
            return None

    def get_core_count(self):
        core_count = self._job_data.get("JOB_RECORD__CORE_CNT")
        return core_count

    def get_submit_time(self):
        submit_time = self._job_data.get("JOB_RECORD__SUBMIT_UNIXTIME")
        return submit_time

    def get_start_time(self):
        start_time = self._job_data.get("JOB_RECORD__START_UNIXTIME")
        return start_time

    def get_job_name(self):
        jobname = self._job_data.get("JOB_RECORD__JOB_NAME")
        job_id = self._job_id
        try:
            # Check if job name is too long (maximum length is 256)
            truncated_job_name = (
                jobname[:252] + "..." if len(jobname) > 252 else jobname
            )

            # Remove any characters that are not letters, numbers, ".", "_", "-", "+", or spaces
            sanitized_job_name = re.sub(
                r"[^A-Za-z0-9_.\s\-\+]+", "", truncated_job_name
            )

            return sanitized_job_name
        except Exception as error:
            logger.warning(f"{repr(error)}, jobname error, job_id={job_id}")
            return None

    def get_time_limit(self):
        walltime = self._job_data.get("JOB_RECORD__TIME_LIMIT")
        job_id = self._job_id

        try:
            return walltime * 60
        except Exception as error:
            logger.warning(f"{repr(error)}, walltime error, job_id={job_id}")
            return None

    def get_partition_name(self):
        partition_name = self._job_data.get("JOB_RECORD__PARTITION_NAME")
        return partition_name

    def get_exclusive(self):
        exclusive = self._job_data.get("JOB_RECORD__WHOLE_NODE")
        return exclusive

    def get_array_id(self):
        array_id = self._job_data.get("JOB_RECORD__ARRAY_ID")
        return array_id

    def get_smt_mode(self):
        job_exclusive = self._job_data.get("JOB_RECORD__WHOLE_NODE")
        cpu_count = self._job_data.get("JOB_RECORD__CPU_CNT")
        core_count = self._job_data.get("JOB_RECORD__CORE_CNT")
        smt_enables = self._job_data.get("JOB_RECORD__SMT_ENABLED")
        partition = self._job_data.get("JOB_RECORD__PARTITION_NAME")
        job_id = self._job_id
        slurm_partition = self._slurm_partitions

        pika_smt_mode = 1
        try:
            if job_exclusive != 1:
                pika_smt_mode = math.ceil(cpu_count / core_count)
            else:
                if smt_enables == 1:
                    pika_smt_mode = slurm_partition[partition].smt_mode

            return pika_smt_mode
        except Exception as error:
            logger.warning(f"{repr(error)}, smtmode error, job_id={job_id}")
            return None

    def get_gpu_count(self):
        gpu_count = self._job_data.get("JOB_RECORD__GPU_ALLOC_CNT")
        return gpu_count

    def get_gpu_list(self):
        gpu_count = self._job_data.get("JOB_RECORD__GPU_ALLOC_CNT")
        exclusive = self._job_data.get("JOB_RECORD__WHOLE_NODE")
        node_names = self._job_data.get("JOB_RECORD__NODE_NAMES")
        gpu_detail_list = self._job_data.get("JOB_RECORD__GRES_DETAIL_STR")
        job_id = self._job_id

        pika_gpulist = ""
        try:
            if gpu_count > 0 and exclusive != 1:
                for hostname, gpulist in zip(node_names, gpu_detail_list):
                    # split gpulist, second element, remove last bracket
                    gpus_split = gpulist.split("IDX:")[1][:-1]
                    # string may only contain numbers "-" or ','
                    gpus = re.sub(r"[^0-9,\-]+", "", gpus_split)
                    pika_gpulist += hostname + str("[") + str(gpus) + str("],")

                # remove last comma from string
                pika_gpulist = pika_gpulist[:-1]

            return pika_gpulist
        except Exception as error:
            logger.warning(f"{repr(error)}, gpulist error, job_id={job_id}")
            return None

    def get_job_script(self):
        script = self._job_data.get("JOB_RECORD__JOB_SCRIPT")
        job_id = self._job_id

        try:
            return script.replace("\\", "\\\\").replace('"', '\\"')
        except Exception as error:
            logger.warning(f"{repr(error)}, jobscript error, job_id={job_id}")
            return None

    def _get_requested_memory(self, mem, job_id) -> int | None:

        try:
            # The Slurm PrEp plugin returns (2^64)-2 if the user has not specified any memory.
            # Workaround: Maximum valid value is a Petabyte
            if mem <= 2**50:
                # convert from megabytes to bytes
                return mem * 2**20
            return None
        except Exception as error:
            logger.warning(f"{repr(error)}, mem error, job_id={job_id}")
            return None

    def get_memory_per_cpu(self):
        mem_cpu = self._job_data.get("JOB_RECORD__MEM_PER_CPU")
        result = self._get_requested_memory(mem_cpu, self._job_id)

        return result

    def get_memory_per_node(self):
        mem_cpu = self._job_data.get("JOB_RECORD__MEM_PER_NODE")
        result = self._get_requested_memory(mem_cpu, self._job_id)

        return result

    def get_end_time(self):
        """will only be in data if the job has finished"""
        end_time = self._job_data.get("JOB_RECORD__END_UNIXTIME")
        return end_time
