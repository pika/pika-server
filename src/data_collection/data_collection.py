"""
Responsible for receiving data from RabbitMQ message broker

we are recieving the jobs with its settings specified in the
"slurm batch script" and writing this information into the sql database

recieve information about running jobs on zih clusters, and store them
"""

import json
from time import sleep

import pika
import pika.exceptions
from sqlalchemy import insert, update
from sqlalchemy.sql import Insert as InsertQuery
from sqlalchemy.sql import Update as UpdateQuery

import utils.specification
from sql.connection import SQLConnection
from sql.model import JobDataORM, SQLError
from utils.common import ONE_DAY_IN_SEC
from utils.logger import logger

from . import config
from .handler import JobHandler
from .rabbitmq import RabbitMQSubscriber
from .timer import CallbackTimer

# mypy: ignore-errors


def rabbitmq_callback(
    sql_connection: SQLConnection,
    dirty_jobs: dict[int, int],
    callback_timer: CallbackTimer,
    vhost_name: str,
    spec: utils.specification.Specification,
    channel: pika.adapters.blocking_connection.BlockingChannel,
    method: pika.spec.Basic.Deliver,
    properties: pika.spec.BasicProperties,
    body: bytes,
):
    """this callback will be called twice per job from the RabbitMQSubscriber

    1. (prolog) when the job starts, this callback inserts the job into the
       pika database -> this makes it possible to view live jobs

    2. (epilog) when the job ends, we will update the inserted job from
       step 1 that the job has ended

    Args:
        sql_connection: connection to sql db
        dirty_jobs: collection of jobs which have problems going into the db
        callback_timer: a timer which will log criticals if rabbitmq doesnt send any data
        vhost_name: name of rabbitmq host, used for debugging
        spec
        channel: connection to rabbitmq
        method: ?
        properties: ?
        body: rabbitmq's payload (the job data)
    """

    callback_timer.start_or_restart()

    mode = method.routing_key
    partitions = spec.partition
    job_dict = json.loads(body)
    job_handler = JobHandler(job_dict, partitions)

    match mode:
        case "prolog":
            job_id, query = on_prolog(job_handler)
        case "epilog":
            job_id, query = on_finish(job_handler)
        case _:
            logger.critical(
                "recieved unknown mode from %s: %s", vhost_name, mode
            )
            return

    try:
        sql_connection.execute(query)
        # respond with succesfull acknowledgement to rabbitmq
        channel.basic_ack(delivery_tag=method.delivery_tag)
    except SQLError as error:
        check_jobdirtiness(
            vhost_name, dirty_jobs, channel, method, job_id, error
        )


def on_prolog(job_handler: JobHandler) -> tuple[int, InsertQuery]:
    """create the insert query for the started job"""

    query = insert(JobDataORM).values(
        job_id=job_handler.get_job_id(),
        user_name=job_handler.get_user_name(),
        project_name=job_handler.get_project_name(),
        job_state=job_handler.get_job_state(),
        node_count=job_handler.get_node_count(),
        node_list=job_handler.get_node_list(),
        core_list=job_handler.get_cpu_core_list(),
        core_count=job_handler.get_core_count(),
        submit_time=job_handler.get_submit_time(),
        start_time=job_handler.get_start_time(),
        job_name=job_handler.get_job_name(),
        time_limit=job_handler.get_time_limit(),
        partition_name=job_handler.get_partition_name(),
        exclusive=job_handler.get_exclusive(),
        property_id=0,
        array_id=job_handler.get_array_id(),
        smt_mode=job_handler.get_smt_mode(),
        gpu_count=job_handler.get_gpu_count(),
        gpu_list=job_handler.get_gpu_list(),
        job_script=job_handler.get_job_script(),
        mem_per_cpu=job_handler.get_memory_per_cpu(),
        mem_per_node=job_handler.get_memory_per_node(),
    )

    return (job_handler.get_job_id(), query)


def on_finish(job_handler: JobHandler) -> tuple[int, UpdateQuery]:
    """create the update query for finishing the running job"""

    query = (
        update(JobDataORM)
        .filter(
            JobDataORM.job_id == job_handler.get_job_id(),
            JobDataORM.start_time == job_handler.get_start_time(),
            JobDataORM.partition_name == job_handler.get_partition_name(),
        )
        .values(
            job_state=job_handler.get_job_state(),
            end_time=job_handler.get_end_time(),
        )
    )

    # remove jobscript out of short or cancelled jobs
    # if (
    #     job_handler.get_end_time() - job_handler.get_start_time() < 60
    #     or job_handler.get_job_state() == "cancelled"
    # ):
    #     query = query.values(job_script=None)

    return (job_handler.get_job_id(), query)


def check_jobdirtiness(
    vhost_name: str,
    dirty_jobs: dict[int, int],
    channel: pika.adapters.blocking_connection.BlockingChannel,
    method: pika.spec.Basic.Deliver,
    job_id: int,
    error: SQLError,
) -> None:
    """everytime we fail to insert a job into the pika database, we tell
    rabbitmq to hold the job in its buffer and send it again

    apart from internal server errors in this python script it is also
    possible that the job has invalid values and cant be inserted into the
    database

    because of this we count how often a job failes and abandon it on to
    many fails

    Args:
        vhost_name: name of rabbitmq channel, used for debugging
        dirty_jobs: collections of dirty jobs
        channel (BlockingChannel): connection to rabbitmq
        method (Basic.Deliver): ?
        job_id (int): job_id which cant be correctly inserted into the db
        error (SQLError): reason why the job cant be inserted into the db
    """

    dirtniess = dirty_jobs.get(job_id)

    if dirtniess is None:
        dirty_jobs.update({job_id: 1})
        # respond to hold the data and send it later again
        channel.basic_nack(delivery_tag=method.delivery_tag)
        return

    if dirtniess < 10:
        dirty_jobs.update({job_id: dirtniess + 1})
        channel.basic_nack(delivery_tag=method.delivery_tag)
    else:
        dirty_jobs.pop(job_id)
        logger.critical(
            "job is dirty: %s, error: %s, origin: %s", job_id, error, vhost_name
        )
        # respond with succesfull acknowledgement to rabbitmq, abandon the
        # job
        channel.basic_ack(delivery_tag=method.delivery_tag)


def main(
    settings: config.DataCollectionConfig,
    spec: utils.specification.Specification,
) -> None:
    """start the data collection service"""

    sql_connection = SQLConnection(settings.mariadb)
    dirty_jobs: dict[int, int] = {}
    callback_timer = CallbackTimer(
        ONE_DAY_IN_SEC,
        lambda: (
            logger.critical(
                "no data has arrived from %s for one day",
                settings.rabbitmq.rabbitmq_vhost,
            )
            if settings.rabbitmq.rabbitmq_vhost not in {"julia", "power9"}
            else None
        ),
    )
    callback_timer.start_or_restart()

    # we have to do callback programing becuase rabbitmq python package requires it
    def rabbitmq_callback_wrapper(
        channel: pika.adapters.blocking_connection.BlockingChannel,
        method: pika.spec.Basic.Deliver,
        properties: pika.spec.BasicProperties,
        body: bytes,
    ):
        """add sqlconection, dirty_jobs, callback_timer and spec to rabbitmq callback"""
        rabbitmq_callback(
            sql_connection,
            dirty_jobs,
            callback_timer,
            settings.rabbitmq.rabbitmq_vhost,
            spec,
            channel,
            method,
            properties,
            body,
        )

    data_source = RabbitMQSubscriber(
        settings.rabbitmq, rabbitmq_callback_wrapper
    )

    while True:
        try:
            data_source.connect_to_server()
        except pika.exceptions.AMQPConnectionError:
            logger.critical(
                "cant connect to %s-rabbitmq server, reconnecting...",
                settings.rabbitmq.rabbitmq_vhost,
            )
            sleep(300)
            continue

        logger.info(
            "starting data-collection %s", settings.rabbitmq.rabbitmq_vhost
        )

        try:
            data_source.collect()
        except pika.exceptions.ChannelClosed as error:
            logger.critical(
                "%s-rabbitmq channel was closed by the broker: %s, reconnecting...",
                settings.rabbitmq.rabbitmq_vhost,
                error,
            )
            sleep(300)
