"""Containers and classes used in the entire Pika server"""

import json
from enum import StrEnum

from pydantic import BaseModel

from .common import UTC_TIMEZONES


class FormatRequest(BaseModel):
    """specifys how does the user want his response to be formated

    Args:
        BaseModel: fastapi interop
    """

    utc_timezone: str = "00:00"
    # the user requestet timezone

    user_wants_formating: bool = True
    # whether the user wants a formated response

    time_format: str = "%d/%m/%Y %H:%M:%S"
    # user wanted time format, spec: datetime.strftime()

    def evaluate(self) -> None:
        """check if the user send us a valid utc timezone

        Raises:
            FormatRequestError
        """

        timezone_is_valid = any(
            tz.utc_timezone == self.utc_timezone for tz in UTC_TIMEZONES
        )

        if not timezone_is_valid:
            raise FormatRequestError(
                f"{self.utc_timezone} is not a valid utc timezone"
            )

    def translate_timezone(self, unix_timestamp: int) -> int:
        """
        - translate unixtimestamp to the timezone of self.utc_timezone
        - the input unix_timestamp is treated as utc00
        - before using this function make sure that self.utc_timezone is valid
          by running self.evaluate()
        """

        target_timezone = next(
            tz for tz in UTC_TIMEZONES if tz.utc_timezone == self.utc_timezone
        )

        translated_timestamp = unix_timestamp + (
            target_timezone.offset_minutes * 60
        )

        return translated_timestamp


class FormatRequestError(Exception): ...


class OperationMode(StrEnum):
    """Information about what mode the server is running in"""

    DEVELOPMENT = "dev"
    PRODUCTION = "prod"


class ConfigurationError(Exception):
    """custom error representing errors in the configuration file"""


class DBCredential:
    """Internal representation of Database Credentials"""

    def __init__(self, config: dict[str, str]) -> None:
        """build the DBCredentials from the given config

        Args:
            config (dict[str, any]): information requried to build DBCredentials

        Raises:
            ConfigurationError: error in the config
        """

        try:
            self._hostname = str(config["hostname"])
            self._port = int(config["port"])
            self._user = str(config["user"])
            self._password = str(config["password"])
            self._database = str(config["database"])
        except (KeyError, ValueError) as error:
            raise ConfigurationError(error) from error

        # only available on sql credentials, not on influx
        try:
            self._async_select_timeout_minutes = float(
                config["async_select_timeout_minutes"]
            )
        except:
            self._async_select_timeout_minutes = None

    @property
    def async_select_timeout_minutes(self) -> float:
        """time amount after how long the async sql select query should be
        canceled

        Returns:
            int: timeout in minutes
        """
        return self._async_select_timeout_minutes

    @property
    def hostname(self) -> str:
        """get the configured database hostname

        Returns:
            str: database hostname
        """
        return self._hostname

    @property
    def port(self) -> int:
        """get the port on which the database listens

        Returns:
            int: database port
        """
        return self._port

    @property
    def user(self) -> str:
        """get the username with which to interact with the database

        Returns:
            str: database username
        """
        return self._user

    @property
    def password(self) -> str:
        """get the usernames password

        Returns:
            str: usernames password
        """
        return self._password

    @property
    def database(self) -> str:
        """get the name of the database

        Returns:
            str: database name
        """
        return self._database

    def __repr__(self) -> str:
        """make object printable"""
        return json.dumps(self.__dict__, indent=2)
