"""Logging initialization and configuration"""

import logging

from .model import OperationMode

logger = logging.getLogger("pika-restapi")


def init_logging(mode: OperationMode) -> None:
    """Configure logger verbosity by mode and disable loggers from other
    packages

    Args:
        mode (OperationMode): the mode in which the server is running
    """

    logging.getLogger("asyncio").setLevel(logging.CRITICAL)
    logging.getLogger("aioinflux").setLevel(logging.CRITICAL)
    logging.getLogger("apscheduler").setLevel(logging.CRITICAL)
    logging.getLogger("tzlocal").setLevel(logging.CRITICAL)
    logging.getLogger("urllib3").setLevel(logging.CRITICAL)
    logging.getLogger("pika").setLevel(
        logging.CRITICAL
    )  # this is not our logger (https://pypi.org/project/pika/)
    # disable loggers from other packages

    match mode:
        case OperationMode.DEVELOPMENT:
            logging_level = logging.DEBUG
        case OperationMode.PRODUCTION:
            logging_level = logging.INFO

    logging.basicConfig(
        level=logging_level,
        style="{",
        format="{levelname:8} [{asctime}] {filename}:{lineno} {message}",
        datefmt="%d.%m.%y %H:%M:%S",
    )
