"""unit tests for the utils package

- utils.model:
    - [x] FormatRequest
- [ ] utils.common:
    - [x] ...
"""

from time import time
from unittest import TestCase

from sqlalchemy import select

from sql.model import JobDataORM

from .common import (
    ONE_DAY_IN_SEC,
    ONE_HOUR_IN_SEC,
    ONE_MINUTE_IN_SEC,
    ONE_WEEK_IN_SEC,
    ONE_YEAR_IN_SEC,
    calculate_bin_width,
    debug_sqlalchemy_query,
    format_duration,
    format_duration_year,
    four_weeks_ago,
    one_week_ago,
    three_weeks_ago,
)
from .model import (
    ConfigurationError,
    DBCredential,
    FormatRequest,
    FormatRequestError,
)


class FormatRequestTest(TestCase):
    def test_evaluate_wrong_timezone(self):
        request = FormatRequest(utc_timezone="hallo")

        with self.assertRaises(FormatRequestError):
            request.evaluate()

    def test_evaluate_correct_timezone(self):
        request = FormatRequest(utc_timezone="+10:00")
        request.evaluate()

    def test_translate_timezone(self):
        request = FormatRequest(utc_timezone="+01:00")
        result = request.translate_timezone(0)
        self.assertEqual(result, 3600)


class DebugSQLAlchemyQueryTest(TestCase):
    """unit tests for the utils.common.debug_sqlalchemy_query"""

    def test_correct_input(self):
        """white box test"""

        query = debug_sqlalchemy_query(select(JobDataORM.job_id))

        expected_query = "SELECT job_data.job_id \nFROM job_data"

        self.assertEqual(query, expected_query)


class CalculateBinWidthTest(TestCase):
    """unit tests for utils.common.calculate_bin_width"""

    def test_normal(self):
        result = calculate_bin_width(0, 20, 5)
        self.assertEqual(result, 4)

    def test_from_negativ(self):
        result = calculate_bin_width(-5, 5, 2)
        self.assertEqual(result, 5)

    def test_zero_bins(self):
        result = calculate_bin_width(0, 10, 0)
        self.assertEqual(result, 1)

    def test_from_neg_to_pos(self):
        result = calculate_bin_width(10, -10, 5)
        self.assertEqual(result, 4)

    def test_many_bins(self):
        result = calculate_bin_width(0, 5, 10)
        self.assertEqual(result, 0.5)

    def test_negativ_bins(self):
        result = calculate_bin_width(0, 10, -2)
        self.assertEqual(result, 5)


class WeekAgoTest(TestCase):
    """unit tests for utils.common.one_week_ago, three_weeks_ago & four_weeks_ago"""

    def test_one_week_ago(self):
        """test one_week_ago"""

        with self.subTest("return value"):
            self.assertIsInstance(one_week_ago(), int)

        with self.subTest("value"):
            self.assertAlmostEqual(
                one_week_ago(), int(time() - ONE_WEEK_IN_SEC), delta=1
            )

    def test_three_weeks_ago(self):
        """test three_weeks_ago"""

        with self.subTest("return value"):
            self.assertIsInstance(three_weeks_ago(), int)

        with self.subTest("value"):
            self.assertAlmostEqual(
                three_weeks_ago(), int(time() - (3 * ONE_WEEK_IN_SEC)), delta=1
            )

    def test_four_weeks_ago(self):
        """test four_weeks_ago"""

        with self.subTest("return value"):
            self.assertIsInstance(four_weeks_ago(), int)

        with self.subTest("value"):
            self.assertAlmostEqual(
                four_weeks_ago(), int(time() - (4 * ONE_WEEK_IN_SEC)), delta=1
            )


class FormatDurationYearTest(TestCase):
    """unit test for utils.common.format_duration_year"""

    def test_correct_input(self):
        """white box test"""

        with self.subTest("small int"):
            self.assertEqual(format_duration_year(300), "0000y 000d 00h 05min")

        with self.subTest("big int"):
            self.assertEqual(
                format_duration_year(25920000), "0000y 300d 00h 00min"
            )

        with self.subTest("str"):
            self.assertEqual(
                format_duration_year("259200"), "0000y 003d 00h 00min"
            )

    def test_wrong_input(self):
        """black box test"""

        with self.subTest("none"):
            self.assertEqual(format_duration_year(None), None)

        with self.subTest("tuple"):
            self.assertEqual(format_duration_year(()), ())


class FormatDurationTest(TestCase):
    """unit tests for utils.common.format_duration"""

    def test_correct_input(self):
        """white box test"""

        with self.subTest("format big int"):
            self.assertEqual(format_duration(999999), "11d 13h 46min 39s")

        with self.subTest("format str"):
            self.assertEqual(format_duration("300"), "00d 00h 05min 00s")

        with self.subTest("format int"):
            self.assertEqual(format_duration(3600), "00d 01h 00min 00s")

    def test_wrong_input(self):
        """tests with wrong input"""

        with self.subTest("not convertable str"):
            self.assertEqual(format_duration("blue"), "blue")

        with self.subTest("dictionary"):
            self.assertEqual(format_duration({"test": 123}), {"test": 123})

        with self.subTest("none"):
            self.assertEqual(format_duration(None), None)


class CommonConstantsTest(TestCase):
    """unit test for constants in utils.common"""

    def test_constants(self):
        """unit test for time constants"""
        self.assertEqual(ONE_YEAR_IN_SEC, 31540000)
        self.assertEqual(ONE_WEEK_IN_SEC, 604800)
        self.assertEqual(ONE_DAY_IN_SEC, 86400)
        self.assertEqual(ONE_HOUR_IN_SEC, 3600)
        self.assertEqual(ONE_MINUTE_IN_SEC, 60)


class DBCredentialTest(TestCase):
    """unit tests for utils.model.DBCredential"""

    def test_configuration_error(self):
        """expect `ConfigurationError` on wrong input"""

        with self.subTest("wrong key value"):
            with self.assertRaises(ConfigurationError):
                DBCredential(
                    {
                        "hoame": "hans.de",
                        "port": 3000,
                        "user": "hans",
                        "password": "1234",
                        "database": "filme",
                    }
                )

        with self.subTest("port as string"):
            with self.assertRaises(ConfigurationError):
                DBCredential(
                    {
                        "hostname": "123hans.de",
                        "port": "23s",
                        "user": "hans",
                        "password": "1234",
                        "database": "filme",
                    }
                )

    def test_property(self):
        """test the property functions"""

        cred = DBCredential(
            {
                "hostname": "123hans.de",
                "port": 3000,
                "user": "hans",
                "password": "1234",
                "database": "filme",
            }
        )

        with self.subTest("hostname property"):
            self.assertEqual(cred.hostname, "123hans.de")

        with self.subTest("port property"):
            self.assertEqual(cred.port, 3000)

        with self.subTest("user property"):
            self.assertEqual(cred.user, "hans")

        with self.subTest("password property"):
            self.assertEqual(cred.password, "1234")

        with self.subTest("database property"):
            self.assertEqual(cred.database, "filme")
