"""
code only called once on server initialization
"""

import json
import tomllib
from typing import Any

import pydantic


class Partition(pydantic.BaseModel):
    """structure of a partition in the partition specificiation"""

    cpu_num: int | None
    socket_num: int | None
    likwid_socket_cpu: str | None
    smt_mode: int | None
    gpu_num: int | None
    likwid_arch_name: str | None
    node_list: str | None
    max_mem_bw: int | None
    max_flops: int | None
    max_ipc: int | None
    active: int | None
    cluster: str | None
    mem_per_node: int | None


class SpecificationError(Exception): ...


class Specification:
    """
    offer static data about the hpc pika environment

    public interface:
        - pika_server_version: version of this server
        - job_tag: bit pattern about the job_tag column in the JobDataOrm
        - partition: zih hpc slurm partitions
        - metric: ?
        - cc_metric: ?
        - cc_subcluster_metric: ?
    """

    def __init__(
        self,
        version_file_content: str,
        tag_file_content: str,
        partition_file_content: str,
        metric_file_content: str,
    ) -> None:
        """
        raises:
            SpecificationError
        """

        try:
            self.pika_server_version = extract_pika_server_version(
                version_file_content
            )
        except tomllib.TOMLDecodeError as error:
            raise SpecificationError(f"extracting server version -> {error}")

        try:
            self.job_tag = parse_job_tags(tag_file_content)
        except json.decoder.JSONDecodeError as error:
            raise SpecificationError(f"parsing job tags -> {error}")

        try:
            self.partition = parse_partitions(partition_file_content)
        except json.decoder.JSONDecodeError as error:
            raise SpecificationError(f"parsing partitions -> {error}")

        try:
            metric_parser = Metric(metric_file_content)
        except json.decoder.JSONDecodeError as error:
            raise SpecificationError(f"parsing metric -> {error}")

        self.metric = metric_parser.get_metrics()
        self.cc_metric = metric_parser.get_cc_metrics()
        self.cc_subcluster_metrics = metric_parser.get_cc_subcluster_metrics()


def extract_pika_server_version(file_content: str) -> str:
    """
    read project-version in pyproject.toml

    used for:
        - check pika-web against pika-server version

    raises:
        - tomllib.TOMLDecodeError
    """
    # not using `importlib.metadata.version("pika-server")` because:
    #     - magic numbers will be applied if version is a string: e.g. "dev" becomes "dev0"
    #     - project needs to be reinstalled for the version change to take effect

    config = tomllib.loads(file_content)
    version = config["tool"]["poetry"]["version"]

    return version


def parse_job_tags(file_content: str) -> dict[int, str]:
    """
    raises:
        json.decoder.JSONDecodeError
    """

    job_tags = json.loads(file_content)
    without_description = {item["id"]: item["tag_name"] for item in job_tags}

    return without_description


def parse_partitions(file_content: str) -> dict[str, Partition]:
    """
    raises:
        json.decoder.JSONDecodeError
    """

    raw = json.loads(file_content)
    partitions = {item.pop("name"): Partition(**item) for item in raw}

    return partitions


class Metric:
    """
    the structure of pika performance metrics derived from metric.json
    """

    def __init__(self, file_content: str) -> None:

        self._file_content = file_content
        self.metrics: dict[str, dict[str, Any]] = {}
        self.cc_metrics: dict[str, dict[str, Any]] = {}
        self.cc_subcluster_metrics: dict[str, dict[str, Any]] = {}
        self._load_metric_data()

    def _load_metric_data(self) -> None:

        data = json.loads(self._file_content)

        # 1) fill cc_metrics and cc_subcluster_metrics
        cc_filesystems: list[Any] = []
        for file_system in data["file_io"]["file_systems"]:
            for instance in file_system["instances"]:
                cc_filesystems.append(
                    {
                        "name": instance["name"],
                        "type": file_system["type"],
                        "ops": [
                            {
                                "metric": "read_bw",
                                "pika_metric": instance["name"] + "_read_bw",
                            },
                            {
                                "metric": "write_bw",
                                "pika_metric": instance["name"] + "_write_bw",
                            },
                        ],
                    }
                )

        self.cc_metrics.update(
            {
                "cpu_used": {"base_unit": "load", "pika_metric": "cpu_usage"},
                "ipc": {"base_unit": "IPC", "pika_metric": "ipc"},
                "flops_any": {"base_unit": "F/s", "pika_metric": "flops"},
                "mem_bw": {"base_unit": "B/s", "pika_metric": "mem_bw"},
                "net_bw": {"base_unit": "B/s", "pika_metric": "infiniband_bw"},
                "mem_used": {"base_unit": "B", "pika_metric": "mem_used"},
                "cpu_power": {"base_unit": "W", "pika_metric": "cpu_power"},
                "acc_used": {"base_unit": "load", "pika_metric": "gpu_usage"},
                "acc_mem_used": {"base_unit": "B", "pika_metric": "gpu_mem"},
                "acc_power": {"base_unit": "W", "pika_metric": "gpu_power"},
                "acc_temp": {
                    "base_unit": "°C",
                    "pika_metric": "gpu_temperature",
                },
                "filesystems": {
                    "base_unit": "B/s",
                    "scope": "node",
                    "timestep": 30,
                    "systems": cc_filesystems,
                },
            }
        )

        # TODO: make this more dynamic! Use partition definition to decide if gpu metrics are needed
        default_gpu_cc_metrics = []
        for metric in self.cc_metrics:
            default_gpu_cc_metrics.append(metric)

        for gpu_partition in ["gpu", "alpha", "ml", "capella"]:
            self.cc_subcluster_metrics[gpu_partition] = default_gpu_cc_metrics

        # remove gpu metrics
        default_cc_metrics = default_gpu_cc_metrics.copy()
        for gpu_metric in ["acc_used", "acc_mem_used", "acc_power", "acc_temp"]:
            default_cc_metrics.remove(gpu_metric)
        self.cc_subcluster_metrics["default"] = default_cc_metrics

        # 2) fill metric_data
        self._create_io_metric_structure(data["file_io"])
        self._create_local_io_metric_structure()
        data.pop("file_io")
        self.metrics.update(data)

        # only required for ZIH, as we started with cpi and later switched to ipc
        self.metrics["cpi"] = {
            "unit": "",
            "timestep": "60",
            "scope": "core",
            "dbconf": [
                {
                    "name": "cpi",
                    "field": "cpi",
                    "measurement": "likwid_cpu",
                    "tags": "hostname,cpu",
                }
            ],
        }

    def _create_io_metric_structure(
        self, file_io: dict[str, dict[str, Any]]
    ) -> None:
        io_metrics: dict[str, Any] = {}
        for file_system in file_io["file_systems"]:
            io_type = file_system["type"]

            # create definition for {io_type}_io
            dbconf = []
            for instance in file_system["instances"]:
                for op in file_io["ops"]:
                    dbconf.append(
                        {
                            "name": instance["name"] + "_" + op,
                            "field": op,
                            "measurement": instance["measurement"],
                            "tags": "hostname",
                        }
                    )
            io_metrics[io_type + "_io"] = {
                "unit": file_io["ops_unit"],
                "timestep": file_io["timestep"],
                "scope": file_io["scope"],
                "dbconf": dbconf,
            }

            # create definition for {io_type}_io_meta
            dbconf = []
            for instance in file_system["instances"]:
                for op in file_io["meta_ops"]:
                    dbconf.append(
                        {
                            "name": instance["name"] + "_" + op,
                            "field": op,
                            "measurement": instance["measurement"],
                            "tags": "hostname",
                        }
                    )
            io_metrics[io_type + "_io_meta"] = {
                "unit": file_io["meta_ops_unit"],
                "timestep": file_io["timestep"],
                "scope": file_io["scope"],
                "dbconf": dbconf,
            }

            # create definition for single io metrics
            for instance in file_system["instances"]:
                for op in file_io["ops"]:
                    io_metrics[instance["name"] + "_" + op] = {
                        "unit": file_io["ops_unit"],
                        "timestep": file_io["timestep"],
                        "scope": file_io["scope"],
                        "dbconf": [
                            {
                                "name": instance["name"] + "_" + op,
                                "field": op,
                                "measurement": instance["measurement"],
                                "tags": "hostname",
                            }
                        ],
                        "footprint_conf": {
                            "table": file_io["footprint_conf"]["table"],
                            "name": io_type
                            + "_"
                            + instance["name"]
                            + "__"
                            + op.split("_")[0]
                            + "_bytes",
                            "aggregation": file_io["footprint_conf"][
                                "aggregation"
                            ],
                        },
                    }
            for instance in file_system["instances"]:
                for op in file_io["meta_ops"]:
                    io_metrics[instance["name"] + "_" + op] = {
                        "unit": file_io["ops_unit"],
                        "timestep": file_io["timestep"],
                        "scope": file_io["scope"],
                        "dbconf": [
                            {
                                "name": instance["name"] + "_" + op,
                                "field": op,
                                "measurement": instance["measurement"],
                                "tags": "hostname",
                            }
                        ],
                    }

        self.metrics.update(io_metrics)

    def _create_local_io_metric_structure(self) -> None:
        self.metrics.update(
            {
                "local_io": {
                    "unit": "Bps",
                    "timestep": "30",
                    "scope": "node",
                    "dbconf": [
                        {
                            "name": "read_bw",
                            "field": "read_bytes",
                            "measurement": "disk",
                            "tags": "hostname",
                        },
                        {
                            "name": "write_bw",
                            "field": "write_bytes",
                            "measurement": "disk",
                            "tags": "hostname",
                        },
                    ],
                },
                "read_bw": {
                    "unit": "Bps",
                    "timestep": "30",
                    "scope": "node",
                    "dbconf": [
                        {
                            "name": "read_bw",
                            "field": "read_bytes",
                            "measurement": "disk",
                            "tags": "hostname",
                        }
                    ],
                },
                "write_bw": {
                    "unit": "Bps",
                    "timestep": "30",
                    "scope": "node",
                    "dbconf": [
                        {
                            "name": "write_bw",
                            "field": "write_bytes",
                            "measurement": "disk",
                            "tags": "hostname",
                        }
                    ],
                },
                "local_io_meta": {
                    "unit": "iops",
                    "timestep": "30",
                    "scope": "node",
                    "dbconf": [
                        {
                            "name": "read_ops",
                            "field": "read_ops",
                            "measurement": "disk",
                            "tags": "hostname",
                        },
                        {
                            "name": "write_ops",
                            "field": "write_ops",
                            "measurement": "disk",
                            "tags": "hostname",
                        },
                    ],
                },
                "read_ops": {
                    "unit": "iops",
                    "timestep": "30",
                    "scope": "node",
                    "dbconf": [
                        {
                            "name": "read_ops",
                            "field": "read_ops",
                            "measurement": "disk",
                            "tags": "hostname",
                        }
                    ],
                },
                "write_ops": {
                    "unit": "iops",
                    "timestep": "30",
                    "scope": "node",
                    "dbconf": [
                        {
                            "name": "write_ops",
                            "field": "write_ops",
                            "measurement": "disk",
                            "tags": "hostname",
                        }
                    ],
                },
            }
        )

    def get_metrics(self) -> dict[str, Any]:
        return self.metrics

    def get_cc_metrics(self) -> dict[str, Any]:
        return self.cc_metrics

    def get_cc_subcluster_metrics(self) -> dict[str, Any]:
        return self.cc_subcluster_metrics
