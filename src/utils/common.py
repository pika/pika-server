"""a collection of helper functions used in the entire Pika server"""

import pathlib
from datetime import UTC, datetime
from time import time

from pydantic import BaseModel
from sqlalchemy.dialects import mysql
from sqlalchemy.orm import Query

# mypy: ignore-errors

ONE_YEAR_IN_SEC = 31540000
ONE_WEEK_IN_SEC = 604800
ONE_DAY_IN_SEC = 86400
ONE_HOUR_IN_SEC = 3600
ONE_MINUTE_IN_SEC = 60


class UTCTimezone(BaseModel):
    utc_timezone: str
    offset_minutes: int


# https://en.wikipedia.org/wiki/List_of_UTC_offsets
UTC_TIMEZONES = [
    UTCTimezone(utc_timezone="-12:00", offset_minutes=-720),
    UTCTimezone(utc_timezone="-11:00", offset_minutes=-660),
    UTCTimezone(utc_timezone="-10:00", offset_minutes=-600),
    UTCTimezone(utc_timezone="-09:30", offset_minutes=-570),
    UTCTimezone(utc_timezone="-09:00", offset_minutes=-540),
    UTCTimezone(utc_timezone="-08:00", offset_minutes=-480),
    UTCTimezone(utc_timezone="-07:00", offset_minutes=-420),
    UTCTimezone(utc_timezone="-06:00", offset_minutes=-360),
    UTCTimezone(utc_timezone="-05:00", offset_minutes=-300),
    UTCTimezone(utc_timezone="-04:00", offset_minutes=-240),
    UTCTimezone(utc_timezone="-03:30", offset_minutes=-210),
    UTCTimezone(utc_timezone="-03:00", offset_minutes=-180),
    UTCTimezone(utc_timezone="-02:00", offset_minutes=-120),
    UTCTimezone(utc_timezone="-01:00", offset_minutes=-60),
    UTCTimezone(utc_timezone="00:00", offset_minutes=0),
    UTCTimezone(utc_timezone="+01:00", offset_minutes=60),
    UTCTimezone(utc_timezone="+02:00", offset_minutes=120),
    UTCTimezone(utc_timezone="+03:00", offset_minutes=180),
    UTCTimezone(utc_timezone="+03:30", offset_minutes=210),
    UTCTimezone(utc_timezone="+04:00", offset_minutes=240),
    UTCTimezone(utc_timezone="+04:30", offset_minutes=270),
    UTCTimezone(utc_timezone="+05:00", offset_minutes=300),
    UTCTimezone(utc_timezone="+05:30", offset_minutes=330),
    UTCTimezone(utc_timezone="+05:45", offset_minutes=345),
    UTCTimezone(utc_timezone="+06:00", offset_minutes=360),
    UTCTimezone(utc_timezone="+06:30", offset_minutes=390),
    UTCTimezone(utc_timezone="+07:00", offset_minutes=420),
    UTCTimezone(utc_timezone="+08:00", offset_minutes=480),
    UTCTimezone(utc_timezone="+08:45", offset_minutes=525),
    UTCTimezone(utc_timezone="+09:00", offset_minutes=540),
    UTCTimezone(utc_timezone="+09:30", offset_minutes=570),
    UTCTimezone(utc_timezone="+10:00", offset_minutes=600),
    UTCTimezone(utc_timezone="+10:30", offset_minutes=630),
    UTCTimezone(utc_timezone="+11:00", offset_minutes=660),
    UTCTimezone(utc_timezone="+12:00", offset_minutes=720),
    UTCTimezone(utc_timezone="+12:45", offset_minutes=765),
    UTCTimezone(utc_timezone="+13:00", offset_minutes=780),
    UTCTimezone(utc_timezone="+14:00", offset_minutes=840),
]


def read_file_from_project_root(file_name: str):
    """
    read file located in project root (pika-server/)

    WARNING:
        - this function is sensitive to the location where it is defined
        - currently it is defined in .../pika-server/src/utils/specification.py
        -> we know that the 3rd parent is our project-root (pika-server/) in which file_name is located

    raises:
        FileNotFoundError
    """

    path_to_this_file = pathlib.Path(__file__)
    project_root_path = path_to_this_file.parents[2]

    # raises FileNotFoundError
    with open(f"{project_root_path}/{file_name}", "r") as file:
        content = file.read()

    return content


def format_date(unix_timestamp: int, format_string: str) -> str:
    """format a unix time stamp to `format_string`

    Args:
        unix_timestamp (int): seconds since 1.1.1970

    Returns:
        str: formatted date
    """

    return datetime.fromtimestamp(unix_timestamp, UTC).strftime(format_string)


def debug_sqlalchemy_query(query: Query) -> str:
    """return the compiled sqlalchemy query

    Args:
        query (Query): the sqlalchemy query

    Returns:
        str: the compiled sqlalchemy query
    """
    return str(
        query.compile(
            dialect=mysql.dialect(), compile_kwargs={"literal_binds": True}
        )
    )


def format_duration(
    seconds: int | str, remove_leading_zeros: bool = False
) -> str:
    """Format unix timestamp to human-readable format (d h m s)
    >>> assert format_duration(60) == "00d 00h 01m 00s"

    Args:
        seconds (int or str): the seconds to be formatted

    Error:
        returns your input

    Returns:
        str: the human readable time
    """

    try:
        seconds = int(seconds)
    except (ValueError, TypeError):
        return seconds

    days = seconds // ONE_DAY_IN_SEC
    seconds = seconds % ONE_DAY_IN_SEC

    hours = seconds // ONE_HOUR_IN_SEC
    seconds = seconds % ONE_HOUR_IN_SEC

    minutes = seconds // ONE_MINUTE_IN_SEC
    seconds = seconds % ONE_MINUTE_IN_SEC

    if remove_leading_zeros:
        parts = []
        if days > 0:
            parts.append(f"{days}d")
        if hours > 0:
            parts.append(f"{hours}h")
        if minutes > 0:
            parts.append(f"{minutes}min")
        parts.append(f"{seconds}s")  # Always include seconds
        return " ".join(parts)
    else:
        return f"{days:02d}d {hours:02d}h {minutes:02d}min {seconds:02d}s"


def format_duration_year(
    seconds: int | str, remove_leading_zeros: bool = False
) -> str:
    """Format unix timestamp to human-readable format (y d h m)
    >>> assert format_duration_year(300) == "0000y 000d 00h 05m"

    Args:
        seconds (int or str): the seconds to be formatted

    Error:
        returns your input

    Returns:
        str: the human readable time
    """

    try:
        seconds = int(seconds)
    except (ValueError, TypeError):
        return seconds

    years = seconds // ONE_YEAR_IN_SEC
    seconds = seconds % ONE_YEAR_IN_SEC

    days = seconds // ONE_DAY_IN_SEC
    seconds = seconds % ONE_DAY_IN_SEC

    hours = seconds // ONE_HOUR_IN_SEC
    seconds = seconds % ONE_HOUR_IN_SEC

    minutes = seconds // ONE_MINUTE_IN_SEC
    seconds = seconds % ONE_MINUTE_IN_SEC

    if remove_leading_zeros:
        parts = []
        if years > 0:
            parts.append(f"{years}y")
        if days > 0:
            parts.append(f"{days}d")
        if hours > 0:
            parts.append(f"{hours}h")
        parts.append(f"{minutes}min")  # Always include minutes
        return " ".join(parts)
    else:
        return f"{years:04d}y {days:03d}d {hours:02d}h {minutes:02d}min"


def format_hour(seconds: int | str) -> str:
    """Format unix seconds to hours

    Args:
        seconds (int or str): the seconds to be formatted

    Error:
        returns your input

    Returns:
        str: duration in hours with SI unit
    """

    try:
        seconds = int(seconds)
    except (ValueError, TypeError):
        return "n/a"

    n: float = seconds / 3600
    symbols = ("", "k", "M", "G", "T", "P", "E")
    prefix = 0
    while n >= 1000 and prefix < len(symbols) - 1:
        n /= 1000
        prefix += 1
    return f"{n:.2f}{symbols[prefix]}"


def one_week_ago() -> int:
    """Return unix timestamp from one week ago

    Returns:
        int: unix timestamp one week ago
    """

    return int(time() - ONE_WEEK_IN_SEC)


def three_weeks_ago() -> int:
    """Return unix timestamp from 3 weeks ago

    Returns:
        int: unix timestamp 3 weeks ago
    """

    return int(time() - (3 * ONE_WEEK_IN_SEC))


def four_weeks_ago() -> int:
    """Return unix timestamp from 4 weeks ago

    Returns:
        int: unix timestamp 4 weeks ago
    """

    return int(time() - (4 * ONE_WEEK_IN_SEC))


def calculate_bin_width(
    min_value: float, max_value: float, num_bins: int
) -> float:
    """
    calc:
        - calculate the size of a fraction:
            - from the range of min_value to max_value, splitted by num_bins

    return:
        - will always return a positiv number
        - default fraction size of 1 for bad values
    """

    try:
        bin_width = (max_value - min_value) / num_bins
    except (ZeroDivisionError, TypeError):
        bin_width = 1.0

    bin_width = abs(bin_width)
    if bin_width == 0.0:
        bin_width = 1.0

    return bin_width
