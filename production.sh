#!/usr/bin/env bash

source development.sh

production::service() {
    systemctl stop pika-server >/dev/null 2>&1
    systemctl disable pika-server >/dev/null 2>&1

    local SERVICE_FILE=/etc/systemd/system/pika-server.service

    if [ -f $SERVICE_FILE ]; then
        rm -f $SERVICE_FILE
    fi

    local PYTHON_PATH=$($POETRY_PATH run which python)
    # uses python binary from python env

    local APP_PATH=$(realpath src/pika_server.py)

    # must use ? as sed delimiter bacuse the path has / in it
    sed -e "s?<python_path>?$PYTHON_PATH?" \
        -e "s?<app_path>?$APP_PATH prod?" template/pika-server.service >$SERVICE_FILE

    systemctl daemon-reload

    systemctl enable pika-server >/dev/null 2>&1
    systemctl start pika-server

    log_success "service"
}

production::config() {
    if [ -f /etc/pika.conf ]; then
        log_success "found production pika config"
        return
    fi

    cp -f pika.conf /etc/pika.conf

    if [ $? -eq 0 ]; then
        log_success "copied development config to production config"
    else
        log_error "couldnt copy development config to production config"
    fi
}

production::main() {
    if [ "$#" -lt 1 ]; then
        help
        exit 1
    fi
    development::check_flags "$@"
    development::check_poetry_path
    development::check_poetry_version
    development::build_poetry_install_command "$1"
    $POETRY_INSTALL_COMMAND
    development::create_pika_config

    production::service
    production::config
}

if [ "${BASH_SOURCE[0]}" == "${0}" ]; then
    production::main $@
fi
